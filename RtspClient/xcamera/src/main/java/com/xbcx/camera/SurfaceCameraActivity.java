package com.xbcx.camera;

import java.util.Collections;
import java.util.List;

import com.xbcx.camera.VideoCamera.OnVideoListener;
import com.xbcx.camera.XCamera.UpdateCameraParameters;
import com.xbcx.camera.ui.SurfaceParent;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Handler;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;

import gan.camera.R;

@SuppressWarnings("deprecation")
public class SurfaceCameraActivity extends ActivityPlugin<BaseActivity> implements XCameraListener, UpdateCameraParameters, View.OnClickListener,OnVideoListener, PreviewCallback{
	
	private final static String tag =  "SurfaceCameraActivity";
	
	int         					mFrameRate = 30;
	
	XCamera							mXCamera;
	CameraErrorListener				mCameraErrorListener;
	
	boolean							mKeepCameraAlive;
	boolean							mUseCamera = true;
	boolean							mIsVideoPicture;
	
	boolean							mIsSurfaceAlive;
	CameraInterceptListener			mCameraInterceptListener;
	CameraView						mCameraView;
	SurfaceParent					mSurfaceParent;
	private long  					mCameraCloseDelay = 50;
	Handler							mMainHandler = XApplication.getMainThreadHandler();
	
	private Runnable	mPauseCamera = new Runnable() {
		@Override
		public void run() {
			if(mXCamera!=null){
				close();
				mKeepCameraAlive = false;
			}
		}
	};
	
	public static SurfaceCameraActivity get(BaseActivity activity){
		for(SurfaceCameraActivity surfaceCameraActivity: activity.getPlugins(SurfaceCameraActivity.class)){
			return surfaceCameraActivity;
		}
		SurfaceCameraActivity surfaceCameraActivity = null;
		activity.registerPlugin(surfaceCameraActivity = new SurfaceCameraActivity());
		return surfaceCameraActivity;
	}
	
	private SurfaceCameraActivity(){
	}
	
	public void setCameraCloseDelay(long cameraCloseDelay) {
		this.mCameraCloseDelay = cameraCloseDelay;
	}
 	
	public SurfaceCameraActivity setCameraErrorListener(CameraErrorListener cameraErrorListener) {
		this.mCameraErrorListener = cameraErrorListener;
		return this;
	}
	
	public SurfaceCameraActivity setKeepCameraAlive(boolean keepCameraAlive) {
		this.mKeepCameraAlive = keepCameraAlive;
		return this;
	}
	
	public SurfaceCameraActivity setCanVideoPicture(boolean isVideoPicture) {
		this.mIsVideoPicture = isVideoPicture;
		return this;
	}
	
	public boolean canVideoPicture() {
		return mIsVideoPicture;
	}
	
	@Override
	protected void onAttachActivity(BaseActivity activity) {
		super.onAttachActivity(activity);
		onCreate();
		mSurfaceParent = (SurfaceParent) mActivity.findViewById(R.id.cameraLayout);
		View view = mActivity.findViewById(R.id.surface);
		if(view instanceof SurfaceView){
			mCameraView = new SurfaceViewCamera((SurfaceView)view, this);
		}else if(view instanceof TextureView){
			mCameraView = new TextureViewCamera((TextureView)view, this);
		}
		try {
			activity.findViewById(R.id.flashmode).setOnClickListener(this);
			activity.findViewById(R.id.cameraid).setOnClickListener(this);
		} catch (Exception e) {
		}
		
		CameraOrientationActivityPlugin.get(activity);
		
		if(mActivity.isResume()){
			onResume();
		}
	}
	
	public int getOrientation() {
		return CameraOrientationManager.get().getOrientation();
	}
	
	public boolean isPortait(){
		final Configuration configuration = mActivity.getResources().getConfiguration();
		return configuration.orientation == Configuration.ORIENTATION_PORTRAIT;
	}

	public SurfaceCameraActivity video(){
		mActivity.findViewById(R.id.picture).setVisibility(View.GONE);
		mActivity.findViewById(R.id.layoutTop).setVisibility(View.GONE);
		mActivity.findViewById(R.id.video).setVisibility(View.VISIBLE);
		return this;
	}
	
	public SurfaceCameraActivity picture(){
		mActivity.findViewById(R.id.video).setVisibility(View.GONE);
		mActivity.findViewById(R.id.layoutTop).setVisibility(View.VISIBLE);
		mActivity.findViewById(R.id.picture).setVisibility(View.VISIBLE);
		return this;
	}
	
	public void release(){
		XbLog.i(tag, "release");
		close();
		mActivity.removePlugin(this);
	}
	
	public SurfaceCameraActivity addRotateView(RotateView imageView){
		CameraOrientationActivityPlugin.get(mActivity).addRotateView(imageView);
		return this;
	}
	
	protected void onCreate(){
		mXCamera = XCamera.get();
		mMainHandler.removeCallbacks(mPauseCamera);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		XbLog.i(tag, "onResume");
		mMainHandler.removeCallbacks(mPauseCamera);
		if(needOpen()){
			open();
		}
	}
	
	@Override
	protected void onPause() {
		XbLog.i(tag, "onPause");
		if(colseAble()){
			if(mCameraCloseDelay <= 0) {
				close();
			}else {
				closeDelay(mCameraCloseDelay);
			}
		}
		super.onPause();
	}
	
	public void closeDelay(long delay) {
		mMainHandler.removeCallbacks(mPauseCamera);
		mMainHandler.postDelayed(mPauseCamera, delay);
	}
	
	public boolean needOpen(){
		return mUseCamera;
	}
	
	public boolean colseAble(){
		return mUseCamera&&!mKeepCameraAlive;
	}
	
	public void open(){
		mSurfaceParent.resume();
		mMainHandler.removeCallbacks(mPauseCamera);
		mXCamera.addCameraListener(this);
		mXCamera.addUpdateCameraParameters(this);
		mXCamera.addPreviewCallBack(this);
		if(mCameraInterceptListener!=null){
			mCameraInterceptListener.onOpen();
		}
		mXCamera.onResume();
	}
	
	public void close(){
		mXCamera.onPause();
		mSurfaceParent.pause();
		if(mCameraInterceptListener!=null){
			mCameraInterceptListener.onClose();
		}
		mXCamera.removeCameraListener(this);
		mXCamera.removeUpdateCameraParameters(this);
		mXCamera.removePreviewCallBack(this);
	}
	
	public boolean isAlive(){
		return XCamera.get().isAlive();
	}
	
	public void setCameraEnable(boolean enable){
		if(enable){
			mUseCamera = true;
			open();
		}else{
			close();
			mUseCamera = false;
		}
	}
	
	protected void onStop() {
		XbLog.i(tag, "onDestroy");
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		XbLog.i(tag, "onDestroy");
		mXCamera.removeCameraListener(this);
		mXCamera.removeUpdateCameraParameters(this);
		mXCamera.removePreviewCallBack(this);
	}
	
	public void startPreview(){
		if(mXCamera.isAlive()){
			mCameraView.setCameraView(this);
			mXCamera.startPreview();
		}
	}
	
	public void restartPreview() {
		stopPreview();
		startPreview();
	}
	
	public void setPreviewDisplayAsync(SurfaceHolder holder){
		if(mXCamera!=null&&mIsSurfaceAlive){
			mXCamera.setPreviewSurface(holder);
		}
	}
	
	public void setPreviewTextureAsync(SurfaceTexture surfaceTexture){
		if(mXCamera!=null&&mIsSurfaceAlive){
			mXCamera.setPreviewTexture(surfaceTexture);
		}
	}
	
	public void setSurfaceAlive(boolean surfaceAlive) {
		this.mIsSurfaceAlive = surfaceAlive;
	}
	
	public void stopPreview(){
		mXCamera.stopPreview();
	}

	public int resetRotation(){
		int rotation = getRotation();
		mXCamera.setRotation(rotation);
		return rotation;
	}
	
	public void setRotation(int rotation){
		mXCamera.setRotation(rotation);
	}
	
	@Override
	public void onCameraOpend(Camera camera) {
		XbLog.i(tag, "onCameraOpend");
		setCameraDeviceDisplayOrientation();
		if(mIsSurfaceAlive) {
			restartPreview();//正常情况下startPreview 就可以的，但是X1 手机会出现callback数据错误， 必须要调用下stopPreview,
		}
		onPreviewChanged(mXCamera.getParameters().getPreviewSize());
		for(CameraActivityPlugin activityPlugin: mActivity.getPlugins(CameraActivityPlugin.class)){
			activityPlugin.onCameraOpend(mXCamera);
		}
	}
	
	public void setCameraDeviceDisplayOrientation() {
		try {
			if (Build.VERSION.SDK_INT > 8) {
				mXCamera.setDisplayOrientation(CameraUtil.determineDisplayOrientation(mActivity, XCamera.getCameraId()));
			} else {
				mXCamera.setDisplayOrientation(90);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onCameraClose() {
		for(CameraActivityPlugin activityPlugin: mActivity.getPlugins(CameraActivityPlugin.class)){
			activityPlugin.onCameraClosed();
		}
	}

	@Override
	public void onCameraError(int error) {
		XbLog.i(tag, "onCameraError"+error);
		if(mCameraErrorListener!=null){
			if(mCameraErrorListener.onCameraError(error)){
				return ;
			}
		}
		mActivity.showYesNoDialog(R.string.ok, 0, R.string.camera_error,new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				mActivity.finish();
			}
		}).setCancelable(false);
	}

	@Override
	public void onUpdateParameters(Parameters parameters) {
		parameters.setRotation(getRotation());
		parameters.setRecordingHint(true);
		
		List<Integer> frameRates = parameters.getSupportedPreviewFrameRates();
		int fps = 0;
		if (frameRates != null) {
			for (Integer fr : frameRates) {
				if (fr <= mFrameRate && fr >= fps)
					fps = fr;
			}
		}
		if (fps == 0) {
			Collections.sort(frameRates);
			if (frameRates != null && frameRates.size() > 0)
				fps = frameRates.get(frameRates.size() - 1);
		}
		
		parameters.setPreviewFrameRate(fps);
		
		XbLog.i(tag,"PreviewFrameRate:"+fps);
		
		for(UpdateCameraParametersPlugin plugin:mActivity.getPlugins(UpdateCameraParametersPlugin.class)){
			plugin.onUpdateParameters(parameters);
		}
	}

	public static interface UpdateCameraParametersPlugin extends ActivityBasePlugin{
		public void onUpdateParameters(Parameters parameters);
	}
	
	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if(id == R.id.cameraid){
			switchCamera();
		}else if(id == R.id.flashmode){
			if(isFlashOn()){
				setFlashMode("off");
			}else{
				setFlashMode("on");
			}
			onFlashModeChanged(isFlashOn());
		}
	}
	
	public void switchCamera(){
		mXCamera.switchCamera();
		onCameraChanged();
	}
	
	protected void onCameraChanged(){
		View flashmode = mActivity.findViewById(R.id.flashmode);
		if(flashmode!=null){
			if (!XCamera.isFacingbackCamera()) {
				CameraUtil.setViewEnable(flashmode, false);
			} else {
				CameraUtil.setViewEnable(flashmode, true);
			}
		}
	}
	
	public void setFlashMode(String value){
		mXCamera.setFlashMode(value);
	}
	
	public boolean isFlashOn(){
		return "on".equals(mXCamera.getFlashMode());
	}
	
	protected void onFlashModeChanged(boolean on){
		ImageView flashmode = (ImageView) mActivity.findViewById(R.id.flashmode);
		flashmode.setImageResource(isFlashOn()? R.drawable.camera_light:R.drawable.camera_light_d);
	}
	
	public Camera getCamera(){
		return mXCamera.getCamera();
	}
	
	@SuppressLint("NewApi")
	public int getRotation() {
		return CameraOrientationActivityPlugin.get(mActivity).getRotation();
	}

	@Override
	public void onRecordStart(final XVideoRecorder recorder) {
	}
	
	@Override
	public void onRecordEnd(XVideoRecorder recorder) {
		XbLog.i(tag, "onRecordEnd");
	}
	
	public int setPreviewSize(String value){
		int[] size = CameraUtil.parseSize(value);
		int width = size[0];
		int height = size[1];
		return setPreviewSize(width, height);
	}
	
	public int setPreviewSize(Size size){
		return setPreviewSize(size.width, size.height);
	}
	
	public int setPreviewSize(int width,int height){
		if(mXCamera.isAlive()){
			XbLog.i(tag, "setPreviewSize"+width+"x"+height);
			Parameters parameters = mXCamera.getParameters();
			if(XCamera.isSupportedSize(width, height, parameters.getSupportedPreviewSizes())){
				stopPreview();
				parameters.setPreviewSize(width, height);
				mXCamera.setParameters(parameters);
				startPreview();
				onPreviewChanged(parameters.getPreviewSize());
				return 0;
			}else{
				XbLog.i(tag, width+"x"+height+"is not supported");
				return -1;
			}
		}
		return -1;
	}
	
	protected void onPreviewChanged(Size size){
		XbLog.i(tag, "preview size:"+size.width+"x"+size.height);
		for(OnPreviewSizeChangedPlugin plugin: mActivity.getPlugins(OnPreviewSizeChangedPlugin.class)){
			plugin.onPreviewSizeChanged(size);
		}
	}
	
	public void startPreviewCallback(int format){
		mXCamera.setPreviewFormat(format);
		startPreviewCallback();
	}

	public void startPreviewCallback(){
		XbLog.i(tag, "startPreviewCallback");
		mXCamera.startPreviewCallback();
	}
	
	public void restartPreviewCallback(){
		stopPreviewCallBack();
		startPreviewCallback();
	}
	
	public void stopPreviewCallBack(){
		XbLog.i(tag, "stopPreviewCallBack");
		mXCamera.stopPreviewCallBack();
	}
	
	public boolean isPreviewCallBack() {
		return mXCamera.isPreviewCallBack();
	}
	
	public boolean isCameraClosed() {
		return mXCamera.isClosed();
	}
	
	@Override
	public void onPreviewFrame(byte[] data,Camera camera) {
		if(data == null||isCameraClosed()){
			return;
		}
		
		for(PreviewCallBackPlugin plugin:mActivity.getPlugins(PreviewCallBackPlugin.class)){
			plugin.onPreviewFrame(this, data, camera);
		}
	}
	
	public static interface PreviewCallBackPlugin extends ActivityBasePlugin{
		public void onPreviewFrame(SurfaceCameraActivity cameraActivity, byte[] data, Camera camera);
	}
	
	public static interface OnPreviewSizeChangedPlugin extends ActivityBasePlugin{
		public void onPreviewSizeChanged(Size size);
	}
	
	public static interface CameraErrorListener{
		public boolean onCameraError(int error);
	}
	
	public void enableShutterSound(boolean enabled) throws Exception{
		mXCamera.enableShutterSound(enabled);
	}
	
	public boolean canCamera(){
		return isAlive();
	}
	
	public void setPictureSize(String value){
		int[] size = CameraUtil.parseSize(value);
		int width = size[0];
		int height = size[1];
		setPictureSize(width, height);
	}
	
	public void setPictureSize(Size size){
		setPictureSize(size.width, size.height);
	}
	
	public void setPictureSize(int width,int height){
		if(mXCamera.isAlive()){
			Parameters parameters = mXCamera.getParameters();
			if(XCamera.isSupportedSize(width, height, parameters.getSupportedPictureSizes())){
				parameters.setPictureSize(width, height);
				mXCamera.setParameters(parameters);
				mXCamera.updateCameraPreviewSize();
			}else{
				XbLog.i(tag, width+"x"+height+"is not supported");
			}
		}
	}
	
	public static interface OnOrientationchangedPlugin extends ActivityBasePlugin{
		public void onOrientationchanged(SurfaceCameraActivity activity, int orientation);
	}
	
	public SurfaceCameraActivity setCameraInterceptListener(CameraInterceptListener cameraInterceptListener) {
		this.mCameraInterceptListener = cameraInterceptListener;
		return this;
	}
	
	public static interface CameraInterceptListener{
		public void onOpen();
		public void onClose();
	}

	@Override
	public void onRecordError(XVideoRecorder recorder, int error) {
		if(error == 0){
			mActivity.showYesNoDialog(R.string.camera_video_error, new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					mActivity.finish();
				}
			}).setCancelable(false);
		}
	}
	
	public static interface CameraView{
		public void setCameraView(SurfaceCameraActivity activity);
	}
}
