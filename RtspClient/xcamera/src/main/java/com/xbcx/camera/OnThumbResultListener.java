package com.xbcx.camera;

import android.graphics.Bitmap;

public interface OnThumbResultListener {
	public boolean onThumbResult(Bitmap result, String filePath);
}
