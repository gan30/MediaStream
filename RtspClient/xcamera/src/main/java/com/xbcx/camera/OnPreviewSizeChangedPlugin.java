package com.xbcx.camera;

import com.xbcx.core.ActivityBasePlugin;

import android.hardware.Camera.Size;

public interface OnPreviewSizeChangedPlugin extends ActivityBasePlugin{
	public void onPreviewSizeChanged(Size size);
}
