package com.xbcx.camera;

import com.xbcx.core.ActivityBasePlugin;

public interface CameraServiceActivityPlugin extends ActivityBasePlugin{
	public void onAttachService(CameraService service);
	public void onDeathService(CameraService service);
}
