package com.xbcx.camera;

import com.xbcx.camera.PictureCamera.OnInterceptTakePictureListener;
import com.xbcx.camera.SurfaceCameraActivity.OnPreviewSizeChangedPlugin;
import com.xbcx.camera.VideoCamera.OnInterceptTakeVideoListener;
import com.xbcx.camera.ui.PreviewSize;

import android.graphics.Bitmap;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;

import gan.camera.R;

public class XCameraActivity extends CameraBaseActivity implements OnThumbResultListener, OnChooseViewListener, LockListener, OnInterceptTakePictureListener, OnInterceptTakeVideoListener,OnPreviewSizeChangedPlugin{

	private final static String Tag = "XCameraActivity";
	
	boolean			mIsLock;
	PictureCamera	mPictureCamera;
	VideoCamera		mVideoCamera;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(isVideo()){
			registerPlugin(mVideoCamera = new VideoCamera());
			SurfaceCameraActivity.get(this).video()
			.setCameraCloseDelay(0);
		}else if(isPicture()){
			registerPlugin(mPictureCamera = new PictureCamera()
					.setOnChooseViewListener(this));
			SurfaceCameraActivity.get(this).picture()
			.setCameraCloseDelay(0);
		}else{
			registerPlugin(mPictureCamera = new PictureCamera()
					.setOnThumbResultListener(this)
					.setOnInterceptTakePictureListener(this));
			registerPlugin(mVideoCamera = new VideoCamera()
					.setOnThumbResultListener(this)
					.setOnInterceptTakeVideoListener(this));
			SurfaceCameraActivity.get(this);
		}
		
		registerPlugin(this);
		registerPlugin(new AdjustZoomSizePlugin());
	}
	
	@Override
	protected void onInitAttribute(BaseAttribute ba) {
		super.onInitAttribute(ba);
		ba.mHasTitle = false;
		ba.mActivityLayoutId = R.layout.activity_camera;
	}
	
	public boolean isPicture() {
		return isPickImage();
	}
	
	public boolean isVideo() {
		return isPickVideo();
	}
	
	public boolean isPickVideo() {
		return CameraUtil.isPickVideo(this);
	}
	
	public boolean isPickImage() {
		return CameraUtil.isPickImage(this);
	}
	
	public SurfaceView getSurfaceView(){
		return (SurfaceView) findViewById(R.id.surface);
	}
	
	public void addRotateView(RotateView imageView){
		SurfaceCameraActivity.get(this).addRotateView(imageView);
	}

	@Override
	public void takePicture() {
		if(mPictureCamera!=null) {
			mPictureCamera.takePicture();
		}
	}
	
	@Override
	public boolean onThumbResult(Bitmap result,String filePath) {
		setThumb(result, filePath);
		return true;
	}
	
	public PictureCamera getPictureCamera(){
		return mPictureCamera;
	}
	
	public VideoCamera getVideoCamera(){
		return mVideoCamera;
	}

	public int getRotation(){
		return SurfaceCameraActivity.get(this).getRotation();
	}
	
	@Override
	public void onChooseViewVisiableChanged(boolean visiable) {
		if(visiable){
			findViewById(R.id.layoutBottom).setVisibility(View.GONE);
			findViewById(R.id.layoutTop).setVisibility(View.GONE);
		}else{
			findViewById(R.id.layoutBottom).setVisibility(View.VISIBLE);
			findViewById(R.id.layoutTop).setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void setLock(long time){
		mIsLock = true;
		removeCallbacks(mLockRunnable);
		postDelayed(mLockRunnable, time);
	}
	
	@Override
	public boolean isLocked() {
		return mIsLock;
	}
	
	Runnable mLockRunnable = new Runnable() {
		@Override
		public void run() {
			mIsLock = false;
		}
	};

	@Override
	public boolean checkAndLocked(long time) {
		if(isLocked()){
			return true;
		}
		setLock(time);
		return false;
	}

	@Override
	public boolean onIntercepTakePicture(PictureCamera camera) {
		return checkAndLocked(200);
	}

	@Override
	public boolean onIntercepTakeVideo(VideoCamera camera) {
		return checkAndLocked(500);
	}
	
	@Override
	public void onPreviewSizeChanged(Size size) {
		PreviewSize.resizePreview(getSurfaceView(), size);
	}

	@Override
	public void setThumb(Bitmap bitmap, String filePath) {
	}
}
