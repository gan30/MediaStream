package com.xbcx.camera.ui;

import com.xbcx.camera.XCamera;

import android.hardware.Camera.Size;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;

public class PreviewSize {

	public static void resizePreview(View surfaceView,Size size) {
		if(XCamera.isXCamera()) {
			if(surfaceView!=null
					&&surfaceView instanceof CameraView) {
				float ratio = (float)size.height/(float)size.width;
				((CameraView)surfaceView).setRatio(ratio);
			}else {
				fixPreviewSize(surfaceView, size);
			}
		}else {
			fixPreviewSize(surfaceView, size);
		}
	}
	
	public static void fixPreviewSize(View surfaceView,Size size) {
		if(surfaceView==null) {
			return;
		}
		if(surfaceView instanceof CameraView) {
			((CameraView)surfaceView).setRatio(-1);
		}
		View rootView = (View) surfaceView.getParent();
		if(rootView!=null) {
			@SuppressWarnings("deprecation")
			float ratio = (float)size.width/(float)size.height;
			int width = rootView.getWidth();
			int height = rootView.getHeight();
			int fix_height = (int) (width*ratio);
			if(fix_height<height){
				int fix_width = (int) (height/ratio);
				MarginLayoutParams lp = (MarginLayoutParams) surfaceView.getLayoutParams();
				lp.width = fix_width;
				lp.height = height;
				lp.topMargin = 0;
				lp.bottomMargin = 0;
				lp.leftMargin = (width - fix_width)/2;
				lp.rightMargin = lp.leftMargin;
				surfaceView.setLayoutParams(lp);
			}else{
				MarginLayoutParams lp = (MarginLayoutParams) surfaceView.getLayoutParams();
				lp.width = width;
				lp.height = fix_height;
				lp.leftMargin = 0;
				lp.rightMargin = 0;
				lp.topMargin = (height - fix_height)/2;
				lp.bottomMargin = lp.topMargin;
				surfaceView.setLayoutParams(lp);
			}
		}
	}
}
