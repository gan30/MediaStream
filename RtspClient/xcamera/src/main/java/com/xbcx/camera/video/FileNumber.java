package com.xbcx.camera.video;

import java.io.File;

public class FileNumber {

	String  mFilePath;
	String 	mFileParent;
	String 	mFileName;
	String  mSuffix;
	int 	mIndex;
	
	public FileNumber(String filePath) {
		mFilePath = filePath;
		File file = new File(filePath);
		mFileParent = file.getParent();
		
		final String fileName = file.getName();
		int index = fileName.lastIndexOf(".");
		mFileName = fileName.substring(0, index);
		mSuffix = fileName.substring(index, fileName.length());
	}
	
	public String nextFilePath(){
		if(mIndex == 0) {
			mIndex++;
			return mFilePath;
		}
		return mFileParent+"/"+ mFileName + "("+ mIndex++ + ")" + mSuffix;
	}
}
