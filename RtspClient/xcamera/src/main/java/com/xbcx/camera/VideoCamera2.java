package com.xbcx.camera;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore.Video.Thumbnails;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.xbcx.camera.video.VideoEngine;
import com.xbcx.camera.video.VideoRecoderListener;
import com.xbcx.camera.video.VideoRecorder;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.util.XbLog;

import gan.camera.R;

public class VideoCamera2 extends ActivityPlugin<BaseActivity> implements OnClickListener, VideoRecoderListener,CameraServiceActivityPlugin{

	final static String tag = "VideoCamera2";
	
	XCamera							mCamera = XCamera.get();
	ImageView						mIvVideo;
	View							mChooseView;
	ChooseViewHelper				mChooseViewHelper;
	OnThumbResultListener			mOnThumbResultListener;
	OnInterceptTakeVideoListener	mOnInterceptTakeVideoListener;
	OnPrepareListener				mOnPrepareListener;
	
	VideoEngine						mVideoEngine;
	
	public VideoCamera2 setOnInterceptTakeVideoListener(OnInterceptTakeVideoListener onInterceptTakeVideoListener) {
		this.mOnInterceptTakeVideoListener = onInterceptTakeVideoListener;
		return this;
	}
	
	public VideoCamera2 setVideoEngine(VideoEngine videoEngine) {
		if(mVideoEngine!=null) {
			mVideoEngine.removeVideoRecoderListener(this);
		}
		this.mVideoEngine = videoEngine;
		if(mVideoEngine!=null) {
			mVideoEngine.addVideoRecoderListener(this);
		}
		return this;
	}
	
	public VideoCamera2 setSimpleVideoEngine() {
		setVideoEngine(new VideoRecorder());
		return this;
	}

	@Override
	protected void onAttachActivity(BaseActivity activity) {
		super.onAttachActivity(activity);
		
		mIvVideo = (ImageView) activity.findViewById(R.id.video);
		if(mIvVideo != null){
			mIvVideo.setOnClickListener(this);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(mVideoEngine!=null) {
			mVideoEngine.release();
			mVideoEngine = null;
		}
	}
	
	public VideoCamera2 setOnThumbResultListener(OnThumbResultListener onThumbResultListener) {
		this.mOnThumbResultListener = onThumbResultListener;
		return this;
	}
	
	public void setIvVideo(ImageView ivVideo) {
		this.mIvVideo = ivVideo;
		mIvVideo.setOnClickListener(this);
	}

	public XCamera getCamera(){
		return mCamera;
	}

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if(mIvVideo == v){
			if(mOnInterceptTakeVideoListener!=null){
				if(mOnInterceptTakeVideoListener.onIntercepTakeVideo(this)){
					return;
				}
			}
			toggleVideo();
		}else if(id == R.id.thumbView){
			playVideo();
		}else if(id == R.id.cancel){
			mActivity.finish();
		}
	}
	
	public void toggleVideo(){
		if (isRecording()) {
			stopVideo();
		} else {
			startVideo();
		}
	}
	
	public boolean startVideo(){
		if(XCamera.isCameraBusy()){
			XbLog.i(tag, "startVideo isCameraBusy");
			return false;
		}
		prepare();
		return start(CameraFile.generateVideoFilePath());
	}
	
	public boolean startVideo(final String filePath){
		if(XCamera.isCameraBusy()){
			XbLog.i(tag, "startVideo filePath isCameraBusy");
			return false;
		}
		prepare();
		return start(filePath);
	}
	
	protected boolean start(String filePath) {
		if(mVideoEngine!=null) {
			try {
				return mVideoEngine.startVideo(filePath);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean stopVideo(){
		if(mVideoEngine!=null){
			return mVideoEngine.stopVideo();
		}
		return false;
	}
	
	protected void prepare(){
		int rotation = CameraOrientationManager.get().getRotation();
		mCamera.setRotation(rotation);
		mVideoEngine.setCamera(mCamera.getCamera());
		mVideoEngine.setRotation(rotation);

		if(mOnPrepareListener!=null){
			mOnPrepareListener.onPrepare(this);
		}
	}
	
	public boolean isRecording(){
		if(mVideoEngine!=null) {
			return mVideoEngine.isVideoRecording();
		}
		return false;
	}

	public void playVideo(){
		final String filePath = mVideoEngine.getVideoFile();
		if(!TextUtils.isEmpty(filePath)){
			Intent intent = new Intent(Intent.ACTION_VIEW);
			String type = "video/*";
			Uri uri = Uri.parse("file://"+filePath);
			intent.setDataAndType(uri, type);
			mActivity.startActivity(intent);
		}
	}
	
	@Override
	public void onRecordStart(VideoEngine recoder) {
		mIvVideo.setImageResource(R.drawable.animlist_recording_1);
		for(OnVideoListener plugin:mActivity.getPlugins(OnVideoListener.class)){
			plugin.onRecordStart(recoder);
		}
	}

	@Override
	public void onRecordEnd(VideoEngine recoder) {
		mCamera.restartPreview();
		mIvVideo.setImageResource(R.drawable.btn_recorder);
		if(CameraUtil.isPickVideo(mActivity)){
			try {
				Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(getVideoRecordFile(), Thumbnails.MINI_KIND);
				showChooseView(bitmap);
			} catch (OutOfMemoryError e) {
				showChooseView(null);
			}
		}
		
		if(mOnThumbResultListener!=null){
			mOnThumbResultListener.onThumbResult(null, recoder.getVideoFile());
		}
		
		for(OnVideoListener plugin:mActivity.getPlugins(OnVideoListener.class)){
			plugin.onRecordEnd(recoder);
		}
	}
	
	public String getVideoRecordFile(){
		return mVideoEngine.getVideoFile();
	}
	
	public void showChooseView(Bitmap bitmap){
		if(mChooseViewHelper == null){
			mChooseViewHelper = new ChooseViewHelper(mActivity);
		}
		mChooseView = mChooseViewHelper.showChooseView(bitmap,true);
		mChooseView.findViewById(R.id.cancel).setOnClickListener(this);
		mChooseView.findViewById(R.id.done).setOnClickListener(this);
		mChooseView.findViewById(R.id.thumbView).setOnClickListener(this);
		onChooseViewVisiableChanged(true);
	}
	
	public void hideChooseView(){
		if(mChooseViewHelper!=null){
			mChooseViewHelper.hideChooseView();
		}
		onChooseViewVisiableChanged(false);
	}
	
	protected void onChooseViewVisiableChanged(boolean visiable) {
		
	}
	
	public VideoCamera2 setOnPrepareListener(OnPrepareListener onPrepareListener) {
		this.mOnPrepareListener = onPrepareListener;
		return this;
	}
	
	public static interface OnVideoListener extends ActivityBasePlugin{
		public void onRecordStart(VideoEngine recorder);
		public void onRecordEnd(VideoEngine recorder);
		public void onRecordError(VideoEngine recorder, int error);
	}
	
	public static interface OnInterceptTakeVideoListener{
		public boolean onIntercepTakeVideo(VideoCamera2 camera);
	}

	public static interface OnPrepareListener{
		public void onPrepare(VideoCamera2 camera);
	}
	
	@Override
	public void onRecordError(VideoEngine recoder, int error) {
		for(OnVideoListener plugin:mActivity.getPlugins(OnVideoListener.class)){
			plugin.onRecordError(recoder,error);
		}
	}

	@Override
	public void onAttachService(CameraService service) {
		setVideoEngine(service.mVideoServicePlugin);
	}

	@Override
	public void onDeathService(CameraService service) {
		setVideoEngine(null);
	}	
}
