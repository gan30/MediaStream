package com.xbcx.camera;

import com.xbcx.camera.SurfaceCameraActivity.CameraInterceptListener;
import com.xbcx.camera.SurfaceCameraActivity.CameraView;
import com.xbcx.util.XbLog;

import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;

public class SurfaceViewCamera implements Callback, CameraInterceptListener,CameraView{
	
	final static String tag = "SurfaceViewCamera";
	
	SurfaceCameraActivity	mCameraActivity;	
	SurfaceView				mSurfaceView;
	SurfaceHolder			mSurfaceHolder;
	
	@SuppressWarnings("deprecation")
	public SurfaceViewCamera(SurfaceView surfaceView,SurfaceCameraActivity activity) {
		mCameraActivity = activity;
		mSurfaceView = surfaceView;
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.addCallback(this);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		
		activity.setCameraInterceptListener(this);
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		XbLog.i(tag, "surfaceCreated");
		mSurfaceHolder = holder;
		mCameraActivity.setSurfaceAlive(true);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		XbLog.i(tag, "surfaceChanged");
		mSurfaceHolder = holder;
		mCameraActivity.setSurfaceAlive(true);
		mCameraActivity.startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		XbLog.i(tag, "surfaceDestroyed");
		mCameraActivity.setSurfaceAlive(false);
		mCameraActivity.stopPreview();
	}

	@Override
	public void onOpen() {
		mSurfaceView.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClose() {
		mSurfaceView.setVisibility(View.GONE);
	}

	@Override
	public void setCameraView(SurfaceCameraActivity activity) {
		activity.setPreviewDisplayAsync(mSurfaceHolder);
	}
}
