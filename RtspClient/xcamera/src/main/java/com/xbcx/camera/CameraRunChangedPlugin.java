package com.xbcx.camera;

public interface CameraRunChangedPlugin extends CameraBasePlugin{
	
	public void onRunChanged(boolean runing);
}
