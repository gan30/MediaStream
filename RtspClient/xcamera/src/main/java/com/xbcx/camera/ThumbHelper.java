package com.xbcx.camera;

import java.io.File;

import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener;
import com.nineoldandroids.view.ViewHelper;
import com.xbcx.core.IDObject;
import com.xbcx.core.XApplication;
import com.xbcx.core.db.XDB;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;
import android.provider.MediaStore.Video.Thumbnails;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import gan.camera.R;

public class ThumbHelper {
	
	public static boolean isVideoPath(String path){
		if(path == null) {
			return false;
		}
		return path.endsWith(".mp4")||path.endsWith(".3gp");
	}
	
	public static void saveThumbPath(String filePath) {
		writePath("picpath", filePath);
		if(isImagePath(filePath)) {
			writePath("picpath_image", filePath);
		}else if(isVideoPath(filePath)) {
			writePath("picpath_video", filePath);
		}
	}
	
	public static boolean isImagePath(String path) {
		if(path == null) {
			return false;
		}
		return path.endsWith(".jpg")||path.endsWith(".jpeg")||path.endsWith(".png");
	}
	
	public static void rehandleThumbPhoto(final View thumbView) {
		rehandleThumbPhoto(thumbView, 0);
	}
	
	public static void rehandleThumbPhoto(final View thumbView,final int media){
		XApplication.runOnBackground(new Runnable() {
			@Override
			public void run() {
				String filePath = null;	
				if(media == 1) {
					filePath  = readPath("picpath_image");
				}else if(media == 2) {
					filePath  = readPath("picpath_video");
				}else {
					filePath = readPath("picpath");
				}
				
				final String temp = filePath;
				XApplication.getMainThreadHandler().post(new Runnable() {
					@Override
					public void run() {
						if(!TextUtils.isEmpty(temp)){
							handleThumbPhoto(thumbView, temp);
						}else{
							final View thumbVideo = thumbView.findViewById(R.id.thumbVideo);
							thumbVideo.setVisibility(View.GONE);
						}
					}
				});
			}
		});
	}
	
	public static SharedPreferences getSharedPreferences() {
		return XApplication.getApplication().getSharedPreferences("camera", 
				Context.MODE_WORLD_READABLE);
	}
	
	public static void handleThumbPhoto(final View thumbView,Bitmap bitmap, String path) {
		final ImageView thumbPicture = (ImageView) thumbView.findViewById(R.id.thumbPicture);
		final View thumbVideo = thumbView.findViewById(R.id.thumbVideo);
		if (bitmap != null) {
			thumbVideo.setVisibility(View.GONE);
			thumbPicture.setImageBitmap(bitmap);
			startScaleAnimate(thumbPicture);
			thumbView.setTag(path);
		}else{
			handleThumbPhoto(thumbView, path);
		}
		saveThumbPath(path);
	}
	
	private static void writePath(String key,String path) {
		XDB.getInstance().updateOrInsert(new Path(key, path), false);
	}
	
	public static String readPath(String key) {
		Path path = XDB.getInstance().readById(key, Path.class, false);
		if(path!=null) {
			return path.mPath;
		}
		return null;
	}
	
	public static class Path extends IDObject{
		private static final long serialVersionUID = 1L;

		public String mPath;
		
		public Path(String key,String path) {
			super(key);
			mPath = path;
		}
	}
	
	public static void handleThumbPhoto(final View thumbView, String path) {
		final ImageView thumbPicture = (ImageView) thumbView.findViewById(R.id.thumbPicture);
		final View thumbVideo = thumbView.findViewById(R.id.thumbVideo);
		if (!TextUtils.isEmpty(path)) {
			File file = new File(path);
			if(!file.exists()){
				thumbVideo.setVisibility(View.GONE);
				thumbPicture.setImageResource(0);
				thumbView.setTag(null);
				return ;
			}
			thumbView.setTag(path);
			if (isVideoPath(path)) {
				thumbVideo.setVisibility(View.VISIBLE);
				AsyncTask<String, Void, Bitmap> create = new AsyncTask<String, Void, Bitmap>() {
					@Override
					protected Bitmap doInBackground(String... params) {
						try {
							Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(params[0], Thumbnails.MINI_KIND);
							bitmap = ThumbnailUtils.extractThumbnail(bitmap, 100, 100);
							return bitmap;
						} catch (Exception e) {
						}
						return null;
					}

					@Override
					protected void onPostExecute(Bitmap result) {
						super.onPostExecute(result);
						if (result == null)
							return;
						thumbPicture.setImageBitmap(result);
					}
				};
				create.execute(path);
			} else {
				CameraUtil.safeSetImageBitmap(thumbPicture, path, 200, 200);
				thumbVideo.setVisibility(View.GONE);
			}
		}
	}
	
	public static void startScaleAnimate(final View view) {
		ValueAnimator scale = ValueAnimator.ofFloat(0.2f, 1f);
		ViewHelper.setPivotX(view, view.getWidth() / 2);
		ViewHelper.setPivotY(view, view.getHeight() / 2);
		scale.setInterpolator(new DecelerateInterpolator());
		scale.addUpdateListener(new AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator arg0) {
				float s = (Float) arg0.getAnimatedValue();
				ViewHelper.setScaleX(view, s);
				ViewHelper.setScaleY(view, s);
			}
		});
		scale.setDuration(200);
		scale.start();
	}
}
