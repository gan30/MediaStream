package com.xbcx.camera;

import java.util.ArrayList;
import java.util.List;

import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Build;
import android.view.OrientationEventListener;

public class CameraOrientationManager{
	
	final static String tag = "CameraOrientationManager";
	
	private static CameraOrientationManager sInstance = null;
	
	public static CameraOrientationManager get() {
		if(sInstance == null) {
			sInstance = new CameraOrientationManager();
		}
		return sInstance;
	}
	
	Context									mContext;
	int 									mLastOrientation;
	OrientationEventListener				mOrientationListener;
	List<OrientationchangedListener>		mOrientationchangedListeners;
	
	private CameraOrientationManager() {
		mContext = XApplication.getApplication();
		mOrientationListener = new OrientationEventListener(mContext) {
			@Override
			public void onOrientationChanged(int orientation) {
				if (orientation != ORIENTATION_UNKNOWN) {
					orientation += 90;
				}
				orientation = CameraUtil.roundOrientation(orientation);
				if (orientation != mLastOrientation) {
					mLastOrientation = orientation;
					onOrientationchanged(orientation);
				}
			}
		};
	}
	
	public void enable() {
		mOrientationListener.enable();
	}
	
	public void disable() {
		mOrientationListener.disable();
	}
	
	public void enable(OrientationchangedListener listener) {
		addOrientationchangedListener(listener);
		enable();
	}
	
	public void disable(OrientationchangedListener listener) {
		mOrientationListener.disable();
		removeOrientationchangedListener(listener);
	}
	
	public void onOrientationchanged(int orientation){
		XbLog.i(tag, "onOrientationchanged" + orientation);
		if(mOrientationchangedListeners!=null) {
			for(OrientationchangedListener listener:mOrientationchangedListeners) {
				listener.onOrientationchanged(orientation);
			}
		}
	}
	
	public int getOrientation() {
		return mLastOrientation;
	}
	
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public int getRotation() {
		final Configuration configuration = mContext.getResources().getConfiguration();
		if(configuration.orientation == Configuration.ORIENTATION_LANDSCAPE){
			return 0;
		}
		
		if (Build.VERSION.SDK_INT <= 9) {
			return 90;
		} 
		
		if (mLastOrientation != OrientationEventListener.ORIENTATION_UNKNOWN) {
			CameraInfo info = new CameraInfo();
			Camera.getCameraInfo(XCamera.getCameraId(), info);
			if (info.facing == CameraInfo.CAMERA_FACING_FRONT) {
				int rotation = (info.orientation + mLastOrientation + 90) % 360;
				rotation = (360 - rotation) % 360;
			} else if(XCamera.isXCamera()){ // back-facing camera
				if(mLastOrientation==0) {
					return 270;
				}
				return mLastOrientation-90;
			}else {
				return mLastOrientation;
			}
		}
		
		return 90;
	}
	
	public void addOrientationchangedListener(OrientationchangedListener listener) {
		if(mOrientationchangedListeners == null) {
			mOrientationchangedListeners = new ArrayList<>();
		}
		mOrientationchangedListeners.add(listener);
	}
	
	public void removeOrientationchangedListener(OrientationchangedListener listener) {
		if(mOrientationchangedListeners!=null) {
			mOrientationchangedListeners.remove(listener);
		}
	}
	
	public void clearOrientationchangedListener() {
		if(mOrientationchangedListeners!=null) {
			mOrientationchangedListeners.clear();
		}
	}
	
	public static interface OrientationchangedListener{
		public void onOrientationchanged(int orientation);
	}
}
