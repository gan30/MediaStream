package com.xbcx.camera;

import com.xbcx.camera.XVideoRecorder.OnVideoRecoderListener;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.util.XbLog;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore.Video.Thumbnails;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import gan.camera.R;

public class VideoCamera extends ActivityPlugin<BaseActivity> implements CameraActivityPlugin, OnClickListener, OnVideoRecoderListener{

	final static String tag = "VideoCamera";
	
	private static final String ACTION_SHUTDOWN = "android.intent.action.ACTION_SHUTDOWN"; 
	BroadcastReceiver	 mBroadcastReceiver;
	
	ImageView			mIvVideo;
	XVideoRecorder		mXVideoRecorder;
	
	View				mChooseView;
	ChooseViewHelper	mChooseViewHelper;
	XCamera				mCamera;
	
	OnThumbResultListener			mOnThumbResultListener;
	OnInterceptTakeVideoListener	mOnInterceptTakeVideoListener;
	
	OnPrepareListener	mOnPrepareListener;
	
	public VideoCamera setOnInterceptTakeVideoListener(OnInterceptTakeVideoListener onInterceptTakeVideoListener) {
		this.mOnInterceptTakeVideoListener = onInterceptTakeVideoListener;
		return this;
	}
	
	@Override
	protected void onAttachActivity(BaseActivity activity) {
		super.onAttachActivity(activity);
		mXVideoRecorder = new XVideoRecorder(mActivity);
		mXVideoRecorder.addOnVideoRecoderListener(this);
		
		mIvVideo = (ImageView) activity.findViewById(R.id.video);
		if(mIvVideo != null){
			mIvVideo.setOnClickListener(this);
		}
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_SHUTDOWN);
		mActivity.registerReceiver(mBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onExceptionExit();
			}
		}, filter);
	}
	
	protected void onExceptionExit(){
		if(isRecording()){
			stopVideo();
		}
	}
	
	@Override
	protected void onDestroy() {
		onExceptionExit();
		super.onDestroy();
		if(mXVideoRecorder!=null) {
			mXVideoRecorder.removeVideoRecoderListener(this);
		}
		if(mBroadcastReceiver!=null){
			mActivity.unregisterReceiver(mBroadcastReceiver);
		}
	}
	
	public VideoCamera setOnThumbResultListener(OnThumbResultListener onThumbResultListener) {
		this.mOnThumbResultListener = onThumbResultListener;
		return this;
	}
	
	public void setIvVideo(ImageView ivVideo) {
		this.mIvVideo = ivVideo;
		mIvVideo.setOnClickListener(this);
	}
	
	@Override
	public void onCameraOpend(XCamera camera) {
		mCamera = camera;
		mXVideoRecorder.setCamera(camera.getCamera());
	}

	@Override
	public void onCameraClosed() {
		mXVideoRecorder.setCamera(null);
	}
	
	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if(mIvVideo == v){
			if(mOnInterceptTakeVideoListener!=null){
				if(mOnInterceptTakeVideoListener.onIntercepTakeVideo(this)){
					return;
				}
			}
			if(mCamera==null){
				return;
			}
			toggleVideo();
		}else if(id == R.id.thumbView){
			playVideo();
		}else if(id == R.id.cancel){
			mActivity.finish();
		}else if(id == R.id.done){
			setResultVideo();
		}
	}
	
	public void toggleVideo(){
		if (isRecording()) {
			stopVideo();
		} else {
			startVideo();
		}
	}
	
	public void startVideo(){
		if(XCamera.isCameraBusy()){
			XbLog.i(tag, "startVideo isCameraBusy");
			return;
		}
		prepare();
		mActivity.post(new Runnable() {
			@Override
			public void run() {
				mXVideoRecorder.startRecord();
			}
		});
	}
	
	public void startVideo(final String filePath){
		if(XCamera.isCameraBusy()){
			XbLog.i(tag, "startVideo filePath isCameraBusy");
			return;
		}
		prepare();
		mActivity.post(new Runnable() {
			@Override
			public void run() {
				mXVideoRecorder.startRecord(filePath);
			}
		});
	}
	
	public boolean stopVideo(){
		return mXVideoRecorder.stopRecord();
	}
	
	protected void prepare(){
		int rotation = CameraOrientationManager.get().getRotation();
		mCamera.setRotation(rotation);
		mXVideoRecorder.setOrientationHint(rotation);
		
		try {
			mCamera.setLongshot(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(mOnPrepareListener!=null){
			mOnPrepareListener.onPrepare(this, mXVideoRecorder);
		}
	}
	
	public boolean isRecording(){
		if(mXVideoRecorder!=null){
			return mXVideoRecorder.isRecoding();
		}
		return false;
	}

	public void playVideo(){
		final String filePath = mXVideoRecorder.getVideoFile();
		if(!TextUtils.isEmpty(filePath)){
			Intent intent = new Intent(Intent.ACTION_VIEW);
			String type = "video/*";
			Uri uri = Uri.parse("file://"+filePath);
			intent.setDataAndType(uri, type);
			mActivity.startActivity(intent);
		}
	}
	
	public void setResultVideo(){
		Intent intent = mXVideoRecorder.getReslutIntent();
		mActivity.setResult(Activity.RESULT_OK, intent);
		mActivity.finish();
	}
	
	@Override
	public void onRecordStart(XVideoRecorder recoder) {
		mIvVideo.setImageResource(R.drawable.animlist_recording_1);
		for(OnVideoListener plugin:mActivity.getPlugins(OnVideoListener.class)){
			plugin.onRecordStart(recoder);
		}
	}

	@Override
	public void onRecordEnd(XVideoRecorder recoder) {
		mCamera.restartPreview();
		mIvVideo.setImageResource(R.drawable.btn_recorder);
		if(CameraUtil.isPickVideo(mActivity)){
			try {
				Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(getVideoRecordFile(), Thumbnails.MINI_KIND);
				showChooseView(bitmap);
			} catch (OutOfMemoryError e) {
				showChooseView(null);
			}
		}
		
		if(mOnThumbResultListener!=null){
			mOnThumbResultListener.onThumbResult(null, getVideoRecordFile());
		}
		
		for(OnVideoListener plugin:mActivity.getPlugins(OnVideoListener.class)){
			plugin.onRecordEnd(recoder);
		}
	}
	
	public String getVideoRecordFile(){
		return mXVideoRecorder.getVideoFile();
	}
	
	public void showChooseView(Bitmap bitmap){
		if(mChooseViewHelper == null){
			mChooseViewHelper = new ChooseViewHelper(mActivity);
		}
		mChooseView = mChooseViewHelper.showChooseView(bitmap,true);
		mChooseView.findViewById(R.id.cancel).setOnClickListener(this);
		mChooseView.findViewById(R.id.done).setOnClickListener(this);
		mChooseView.findViewById(R.id.thumbView).setOnClickListener(this);
		onChooseViewVisiableChanged(true);
	}
	
	public void hideChooseView(){
		if(mChooseViewHelper!=null){
			mChooseViewHelper.hideChooseView();
		}
		onChooseViewVisiableChanged(false);
	}
	
	protected void onChooseViewVisiableChanged(boolean visiable) {
		
	}
	
	public XVideoRecorder getVideoRecorder() {
		return mXVideoRecorder;
	}
	
	public VideoCamera setOnPrepareListener(OnPrepareListener onPrepareListener) {
		this.mOnPrepareListener = onPrepareListener;
		return this;
	}
	
	public static interface OnVideoListener extends ActivityBasePlugin{
		public void onRecordStart(XVideoRecorder recorder);
		public void onRecordEnd(XVideoRecorder recorder);
		public void onRecordError(XVideoRecorder recorder, int error);
	}
	
	public static interface OnInterceptTakeVideoListener{
		public boolean onIntercepTakeVideo(VideoCamera camera);
	}

	public static interface OnPrepareListener{
		public void onPrepare(VideoCamera camera, XVideoRecorder recorder);
	}
	
	@Override
	public void onRecordError(XVideoRecorder recoder, int error) {
		for(OnVideoListener plugin:mActivity.getPlugins(OnVideoListener.class)){
			plugin.onRecordError(recoder,error);
		}
	}
}
