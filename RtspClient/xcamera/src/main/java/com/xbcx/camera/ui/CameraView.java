package com.xbcx.camera.ui;

public interface CameraView {

	public void setRatio(float ratio);
	
	public void setKeepSurfaceAlive(boolean alive);
}
