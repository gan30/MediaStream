package com.xbcx.camera;

import com.xbcx.core.BaseActivity;

import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;

public abstract class CameraBaseActivity extends BaseActivity{

	protected XCamera mCamera = XCamera.get();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public XCamera getXCamera() {
		return mCamera;
	}
	
	@SuppressWarnings("deprecation")
	public Camera getCamera() {
		return mCamera.getCamera();
	}
	
	public abstract void takePicture();
	public abstract void setThumb(Bitmap bitmap,String filePath);
	
	public void switchCamera() {
		mCamera.switchCamera();
	}
	
	public void startPreviewCallBack() {
		mCamera.startPreviewCallback();
	}
	
	public void stopPreviewCallBack() {
		mCamera.stopPreviewCallBack();
	}
	
	public boolean isVideoRuning() {
		return false;
	}
}
