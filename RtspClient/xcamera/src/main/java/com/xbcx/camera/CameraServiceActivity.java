package com.xbcx.camera;

import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.xbcx.camera.CameraService.CameraServiceBinder;
import com.xbcx.camera.PictureCamera.OnInterceptTakePictureListener;
import com.xbcx.camera.SurfaceCameraActivity.UpdateCameraParametersPlugin;
import com.xbcx.camera.VideoCamera2.OnInterceptTakeVideoListener;
import com.xbcx.camera.XCamera.UpdateCameraParameters;
import com.xbcx.camera.video.RestartManager;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.util.XbLog;

import java.util.ArrayList;
import java.util.List;

import gan.camera.R;

public class CameraServiceActivity extends CameraBaseActivity implements XCameraListener, ServiceConnection, OnThumbResultListener, OnInterceptTakePictureListener,LockListener, OnInterceptTakeVideoListener, PreviewCallback, UpdateCameraParameters{
	
	private final static String Tag = "CameraServiceActivity";
	
	public XCamera				mCamera = XCamera.get();
	
	protected CameraService		mCameraService;
	protected RelativeLayout	mRootView;
	
	boolean 				mIsLock;
	PictureCamera			mPictureCamera;
	VideoCamera2			mVideoCamera;
	View					mCameraView;
	
	List<ActivityCaller>    mActivityCallers;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		XbLog.i(Tag, "onCreate");
		RestartManager.reset();
		mRootView = (RelativeLayout) findViewById(R.id.rootView);
		CameraOrientationActivityPlugin.get(this);
		registerPlugin(mPictureCamera = new PictureCamera()
				.setOnThumbResultListener(this)
				.setOnInterceptTakePictureListener(this));
		
		registerPlugin(mVideoCamera = new VideoCamera2()
				.setOnThumbResultListener(this)
				.setOnInterceptTakeVideoListener(this));
		
		if(findViewById(R.id.layoutZoom)!=null) {
			registerPlugin(new AdjustZoomSizePlugin());
		}
	}
	
	@Override
	protected void onInitAttribute(BaseAttribute ba) {
		super.onInitAttribute(ba);
		ba.mActivityLayoutId = R.layout.activity_camera2;
	}
	
	public Class<?> getServiceClass() {
		return CameraService.class;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		onResumeCamera();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		onPauseCamera();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if(mActivityCallers != null){
			for(ActivityCaller caller : mActivityCallers){
				caller.mActivity = null;
			}
			mActivityCallers.clear();
			mActivityCallers = null;
		}
	}
	
	protected void onResumeCamera() {
		mCamera.addCameraListener(this);
		mCamera.addUpdateCameraParameters(this);
		startCameraService();
		bindCameraService();
	}
	
	protected void onPauseCamera() {
		unbindCameraService();
		stopCameraService();
		mCamera.removeCameraListener(this);
		mCamera.removeUpdateCameraParameters(this);
	}
	
	public void switchCamera() {
		mCameraView.setVisibility(View.GONE);
		mCameraView.setVisibility(View.VISIBLE);
		mCamera.switchCamera();
	}
	
	public void setCameraEnable(boolean enable){
		mCameraView.setVisibility(enable? View.VISIBLE:View.GONE);
		if(mCamera!=null) {
			mCamera.setCameraEnable(enable);
		}
	}

	@Override
	public void onCameraOpend(Camera camera) {
		XbLog.i(Tag, "onCameraOpend");
		mCamera.setDisplayOrientation(getCameraDeviceDisplayOrientation());
		for(CameraActivityPlugin plugin: getPlugins(CameraActivityPlugin.class)) {
			plugin.onCameraOpend(mCamera);
		}
	}
	
	public synchronized void takePicture() {
		mPictureCamera.takePicture();
	}
	
	@Override
	public void onCameraClose() {
		XbLog.i(Tag, "onCameraClose");
		for(CameraActivityPlugin plugin: getPlugins(CameraActivityPlugin.class)) {
			plugin.onCameraClosed();
		}
	}

	@Override
	public void onCameraError(int error) {
		XbLog.i(Tag, "onCameraError");
		showYesNoDialog(R.string.ok, 0, R.string.camera_error,new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		}).setCancelable(false);
	}
	
	@Override
	public void onServiceConnected(ComponentName name, IBinder service) {
		XbLog.i(Tag, "onServiceConnected");
		mCameraService = ((CameraServiceBinder)service).getService();
		onAttachService(mCameraService);
		mCameraService.onAttachActivity(this);
	}
	
	@Override
	public void onServiceDisconnected(ComponentName name) {
		XbLog.i(Tag, "onServiceDisconnected");
	}
	
	public void startCameraService() {
		XbLog.i(Tag, "startCameraService");
		Intent service = new Intent(getDialogContext(), getServiceClass());
		startService(service);
	}
	
	public void stopCameraService() {
		XbLog.i(Tag, "stopCameraService");
		Intent service = new Intent(getDialogContext(), getServiceClass());
		stopService(service);
	}
	
	public void bindCameraService() {
		XbLog.i(Tag, "bindCameraService");
		Intent intent = new Intent(getDialogContext(), getServiceClass());
		getDialogContext().bindService(intent, this, Context.BIND_AUTO_CREATE);
	}
	
	public void unbindCameraService() {
		if(mCameraService!=null) {
			XbLog.i(Tag, "unbindCameraService");
			try {
				getDialogContext().unbindService(this);
			} catch (Exception e) {
			}
			mCameraService.onDeathActivity(this);
			onDeathService(mCameraService);
		}
	}
	
	protected void onAttachService(CameraService service) {
		mCameraView = service.getCameraView();
		onSurfaceAttachActivity();
		for(CameraServiceActivityPlugin plugin:getPlugins(CameraServiceActivityPlugin.class)) {
			plugin.onAttachService(service);
		}
	}

	protected void onSurfaceAttachActivity() {
		ViewGroup parent = (ViewGroup) mCameraView.getParent();
		if (parent != null) {
			parent.removeView(mCameraView);
		}
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.addRule(RelativeLayout.CENTER_IN_PARENT);
		mRootView.addView(mCameraView, params);
	}
	
	protected void onSurfaceDetachedActivity() {
		mRootView.removeView(mCameraView);
	}
	
	protected void onDeathService(CameraService service) {
		onSurfaceDetachedActivity();
		for(CameraServiceActivityPlugin plugin:getPlugins(CameraServiceActivityPlugin.class)) {
			plugin.onDeathService(service);
		}	
		mCameraService = null;
	}
	
	public int getCameraDeviceDisplayOrientation() {
		if (Build.VERSION.SDK_INT > 8) {
			return CameraUtil.determineDisplayOrientation(this, XCamera.getCameraId());
		} else {
			return 90;
		}
	}
	
	@Override
	public void setLock(long time){
		mIsLock = true;
		removeCallbacks(mLockRunnable);
		postDelayed(mLockRunnable, time);
	}
	
	@Override
	public boolean isLocked() {
		return mIsLock;
	}
	
	Runnable mLockRunnable = new Runnable() {
		@Override
		public void run() {
			mIsLock = false;
		}
	};

	@Override
	public boolean checkAndLocked(long time) {
		if(isLocked()){
			return true;
		}
		setLock(time);
		return false;
	}

	@Override
	public boolean onIntercepTakePicture(PictureCamera camera) {
		return checkAndLocked(200);
	}

	@Override
	public boolean onIntercepTakeVideo(VideoCamera2 camera) {
		return checkAndLocked(500);
	}
	
	public boolean isPickVideo() {
		return CameraUtil.isPickVideo(this);
	}
	
	public boolean isPickImage() {
		return CameraUtil.isPickImage(this);
	}
	
	public View getCameraView(){
		return mCameraView;
	}
	
	public void addRotateView(RotateView imageView){
		CameraOrientationActivityPlugin.get(this).addRotateView(imageView);
	}

	public Camera getCamera(){
		return XCamera.get().getCamera();
	}
	
	@Override
	public boolean onThumbResult(Bitmap result,String filePath) {
		setThumb(result, filePath);
		return true;
	}
	
	public PictureCamera getPictureCamera(){
		return mPictureCamera;
	}
	
	public VideoCamera2 getVideoCamera(){
		return mVideoCamera;
	}
	
	public boolean startVideo() {
		return mCameraService.startVideo(CameraFile.generateVideoFilePath());
	}
	
	public boolean stopVideo() {
		return mCameraService.stopVideo();
	}
	
	public boolean isVideoRecording() {
		return mCameraService.isVideoRecording();
	}
	
	public int resetRotation(){
		int rotation = CameraOrientationManager.get().getRotation();
		mCamera.setRotation(rotation);
		return rotation;
	}
	
	public void startPreviewCallBack() {
		if(mCamera.isAlive()) {
			mCamera.addPreviewCallBack(this);
			mCamera.startPreviewCallback();
		}
	}
	
	public void stopPreviewCallBack() {
		if(mCamera.isAlive()) {
			mCamera.removePreviewCallBack(this);
			mCamera.stopPreviewCallBack();
		}
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		for(PreviewCallBackPlugin2 plugin:getPlugins(PreviewCallBackPlugin2.class)) {
			plugin.onPreviewFrame(data, camera);
		}
	}
	
	public static interface PreviewCallBackPlugin2 extends ActivityBasePlugin{
		public void onPreviewFrame(byte[] data, Camera camera);
	}

	@Override
	public void onUpdateParameters(Parameters parameters) {
		for(UpdateCameraParametersPlugin plugin:getPlugins(UpdateCameraParametersPlugin.class)) {
			plugin.onUpdateParameters(parameters);
		}
	}
	
	public int setPreviewSize(String value){
		return mCamera.setPreviewSize(value);
	}
	
	public int setPreviewSize(Size size){
		return mCamera.setPreviewSize(size);
	}
	
	public int setPreviewSize(int width,int height){
		return mCamera.setPreviewSize(width, height);
	}

	@Override
	public void setThumb(Bitmap bitmap, String filePath) {
	}
	
	public void onCameraPerpared() {
		
	}
	
	public ActivityCaller createActivityCaller(){
		if(mActivityCallers==null){
			mActivityCallers = new ArrayList<ActivityCaller>();
		}
		ActivityCaller caller = new ActivityCaller(this);
		mActivityCallers.add(caller);
		return caller;
	}
	
	public static class ActivityCaller{
		CameraServiceActivity mActivity;
		public ActivityCaller(CameraServiceActivity activity){
			mActivity = activity;
		}
		public void onCameraPerpared() {
			if(mActivity!=null) {
				mActivity.onCameraPerpared();
			}
		}
	}
}
