package com.xbcx.camera.settings;

import com.xbcx.camera.XCamera;
import com.xbcx.core.XApplication;

import android.content.SharedPreferences;
import android.media.CameraProfile;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class XCameraSettings {

	public static String key(String prefix){
		return prefix + XCamera.getCameraId();
	}
	
	public final static String Pixel_200w 		= "1920x1152";
	public final static String Pixel_800w		= "3600x2160";
	public final static String Pixel_1600w 		= "5120x3072";
	public final static String Pixel_1600x960  	= "1600x960";
	
	public static String PIXEL_DEFALUT   	    = Pixel_800w;
	
	public final static String Picture_PreviewSize = "1280x720";
	
	public final static String Video_720p 		= "1280x720";
	public final static String Video_1080p 		= "1920x1080";
	
	public final static String Video_DEFALUT 	= Video_1080p;
	
	public static SharedPreferences M_SHARED_PREFERENCES;
	
	public static final String KEY_PICTURE_SIZE 			= "key_picture_size";
	public static final String KEY_PICTURE_SIZE_DEFAULT 	= "key_picture_size_default";
	
	public static final String KEY_Capture_Sound 		= "key_capture_sound";
	public static final String KEY_VIDEO_SIZE 			= "key_video_size";
	public static final String KEY_VIDEO_SIZE_DEFAULT 	= "key_video_size_default";
	
	public static final String KEY_VIDEO_WAY  		= "key_video_way";
	public static final String KEY_JPEG_QUALITY		= "key_jpeg_quality";
	public static final String KEY_COLOR_EFFECT     = "key_color_effect";
	public static final String KEY_EXPOSURE			= "key_exposure";
	public static final String KEY_SCENE_MODE		= "key_scene_mode";
	public static final String KEY_WHITE_BALANCE    = "key_white_balance";
	public static final String KEY_FLASH_MODE		= "KEY_FLASH_MODE";
	public static final String KEY_FOCUS_MODE		= "KEY_FOCUS_MODE";
	
	public static final String KEY_RECORD_WATER 	= "xbcx-watermark-on";
	public static final String KEY_WATER_SUPPORT 	= "xbcx_watermark_support";
	
	public final static String Rate_Def  = "6";
	public final static String VideoFrame_Def  = "30";
	
	public static void switchSound(boolean sound){
		getSharedPreferences().edit()
		.putBoolean(KEY_Capture_Sound, sound)
		.commit();
	}
	
	public static boolean soundEnable(){
		return getSharedPreferences().getBoolean(KEY_Capture_Sound, true);
	}
	
	public static void setPictureSize(String value){
		getSharedPreferences().edit()
		.putString(key(KEY_PICTURE_SIZE), value)
		.commit();
	}
	
	public static void setDefaultVideoWay(String way){
		String value = readStringSetting(KEY_VIDEO_WAY, null);
		if(TextUtils.isEmpty(value)){
			setVideoWay(way);
		}
	}
	
	public static SharedPreferences getSharedPreferences(){
		if(M_SHARED_PREFERENCES == null){
			M_SHARED_PREFERENCES = PreferenceManager.getDefaultSharedPreferences(XApplication.getApplication());
		}
		return M_SHARED_PREFERENCES;
	}
	
	public static boolean readBoolSetting(String key,boolean defValue){
		return getSharedPreferences().getBoolean(key(key), defValue);
	}
	
	public static String readStringSetting(String key,String defValue){
		return getSharedPreferences().getString(key(key), defValue);
	}
	
	public static void setStringSetting(String key,String value){
		getSharedPreferences().edit()
		.putString(key(key), value)
		.commit();
	}
	
	public static void setVideoSize(String value) {
		setVideoSize(XCamera.getCameraId(), value);
	}
	
	public static void setVideoSize(int cameraId,String value){
		getSharedPreferences().edit()
		.putString(KEY_VIDEO_SIZE+cameraId, value)
		.commit();
	}
	
	public static String getVideoSize(){
		return readStringSetting(KEY_VIDEO_SIZE, getVideoSizeDefault(XCamera.getCameraId()));
	}
	
	public static void setVideoSizeDefault(int cameraId,String size) {
		getSharedPreferences().edit()
		.putString(KEY_VIDEO_SIZE_DEFAULT+cameraId, size)
		.commit();
	}
	
	public static String getVideoSizeDefault(int cameraId) {
		String value = getSharedPreferences().getString(KEY_VIDEO_SIZE_DEFAULT+cameraId, Video_DEFALUT);
		return value;
	}
	
	public static String getPictureSize(){
		return readStringSetting(KEY_PICTURE_SIZE, getPictureSizeDefault(XCamera.getCameraId()));
	}
	
	public static String getPictureSizeDefault(int cameraId) {
		return getSharedPreferences().getString(KEY_PICTURE_SIZE_DEFAULT+cameraId, PIXEL_DEFALUT);
	}
	
	public static void setPictureSizeDefault(int cameraId,String size) {
		getSharedPreferences().edit()
		.putString(KEY_PICTURE_SIZE_DEFAULT+cameraId, size)
		.commit();
	}
	
	public static void setPictureSize(int cameraId,String size) {
		getSharedPreferences().edit()
		.putString(KEY_PICTURE_SIZE+cameraId, size)
		.commit();
	}
	
	public static String getPictureSize(int cameraId) {
		return getPictureSize(cameraId, getPictureSizeDefault(cameraId));
	}
	
	public static String getPictureSize(int cameraId,String defValue) {
		return getSharedPreferences().getString(KEY_PICTURE_SIZE+cameraId, defValue);
	}
	
	public static void setVideoWay(String way){
		getSharedPreferences().edit()
		.putString(key(KEY_VIDEO_WAY), way)
		.commit();
	}
	
	public static void setJpegQuality(int quality){
		getSharedPreferences().edit()
		.putInt(key(KEY_JPEG_QUALITY), quality)
		.commit();
	}
	
	public static int getJpegQuality(){
		return getSharedPreferences().getInt(key(KEY_JPEG_QUALITY), CameraProfile.QUALITY_MEDIUM);
	}
	
	public static void switchWaterSupport(boolean support) {
		getSharedPreferences().edit()
		.putBoolean(KEY_WATER_SUPPORT, support)
		.commit();
	}
	
	public static boolean isSupportWater() {
		return getSharedPreferences().getBoolean(KEY_WATER_SUPPORT, false);
	}
}
