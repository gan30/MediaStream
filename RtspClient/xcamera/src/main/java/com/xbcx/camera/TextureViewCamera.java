package com.xbcx.camera;

import com.xbcx.camera.SurfaceCameraActivity.CameraView;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;

public class TextureViewCamera implements SurfaceTextureListener,CameraView{
	
	final static String tag = "TextureViewCamera";
	
	SurfaceCameraActivity	mCameraActivity;	
	TextureView				mTextureView;
	SurfaceTexture			mSurfaceTexture;
	
	public TextureViewCamera(TextureView surfaceView,SurfaceCameraActivity activity) {
		mCameraActivity = activity;
		mTextureView = surfaceView;
		mTextureView.setSurfaceTextureListener(this);
	}
	
	@SuppressLint("NewApi")
	public void onResume(){
		if(mSurfaceTexture!=null){
			if(Build.VERSION.SDK_INT >= 16){
				mTextureView.setSurfaceTexture(mSurfaceTexture);
			}
		}
	}

	@Override
	public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
		XbLog.i(tag, "onSurfaceTextureAvailable");
		mSurfaceTexture = surface;
		mCameraActivity.setSurfaceAlive(true);
		mCameraActivity.startPreview();
	}

	@Override
	public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
		XbLog.i(tag, "onSurfaceTextureSizeChanged");
		mSurfaceTexture = surface;
		mCameraActivity.setSurfaceAlive(true);
	}

	@Override
	public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
		XbLog.i(tag, "onSurfaceTextureDestroyed");
		mCameraActivity.setSurfaceAlive(false);
		mCameraActivity.stopPreview();
		return true;
	}

	@Override
	public void onSurfaceTextureUpdated(SurfaceTexture surface) {
	}

	@Override
	public void setCameraView(SurfaceCameraActivity activity) {
		activity.setPreviewTextureAsync(mSurfaceTexture);
	}
}
