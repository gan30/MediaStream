package com.xbcx.camera.ui;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.xbcx.camera.RotateView;

@SuppressLint("AppCompatCustomView")
public class CircleRotateImageView extends ImageView implements RotateView {

	protected static final int ANIMATION_SPEED = 180; // 180 deg/sec

	protected int mCurrentDegree = 0; // [0, 359]
	protected int mStartDegree = 0;
	protected int mTargetDegree = 0;

	protected boolean mClockwise = false;

	protected long mAnimationStartTime = 0;
	protected long mAnimationEndTime = 0;

	private Paint paint;
	private int roundWidth = 2;
	private int roundHeight = 2;
	private Paint paint2;

	private Canvas mCanvas;
	private Bitmap mBmp;

	private Matrix matrix;

	public CircleRotateImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	private void init(Context context, AttributeSet attrs) {
		mCanvas = new Canvas();
		matrix = new Matrix();
		paint = new Paint();
		paint.setColor(Color.WHITE);
		paint.setAntiAlias(true);
		paint.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
		paint2 = new Paint();
		paint2.setXfermode(null);
	}

	@Override
	public void setDegree(int degree) {
		// make sure in the range of [0, 359]
		degree = degree >= 0 ? degree % 360 : degree % 360 + 360;
		if (degree == mTargetDegree)
			return;

		mTargetDegree = degree;
		mStartDegree = mCurrentDegree;
		mAnimationStartTime = AnimationUtils.currentAnimationTimeMillis();

		int diff = mTargetDegree - mCurrentDegree;
		diff = diff >= 0 ? diff : 360 + diff; // make it in range [0, 359]

		// Make it in range [-179, 180]. That's the shorted distance between the
		// two angles
		diff = diff > 180 ? diff - 360 : diff;

		mClockwise = diff >= 0;
		mAnimationEndTime = mAnimationStartTime + Math.abs(diff) * 1000 / ANIMATION_SPEED;

		invalidate();
	}

	@Override
	public void draw(Canvas canvas) {
		// TODO Auto-generated method stub
		if (mBmp != null) {
			if (mCurrentDegree != mTargetDegree) {
				long time = AnimationUtils.currentAnimationTimeMillis();
				if (time < mAnimationEndTime) {
					int deltaTime = (int) (time - mAnimationStartTime);
					int degree = mStartDegree + ANIMATION_SPEED * (mClockwise ? deltaTime : -deltaTime) / 1000;
					degree = degree >= 0 ? degree % 360 : degree % 360 + 360;
					mCurrentDegree = degree;
					invalidate();
				} else {
					mCurrentDegree = mTargetDegree;
				}
			}

			mCanvas.setBitmap(mBmp);
			mCanvas.drawColor(0x00000000, Mode.CLEAR);
			super.draw(mCanvas);
			drawLiftUp(mCanvas);
			drawRightUp(mCanvas);
			drawLiftDown(mCanvas);
			drawRightDown(mCanvas);
			matrix.reset();
			matrix.postTranslate(roundHeight - mBmp.getWidth() / 2, roundWidth - mBmp.getHeight() / 2);
			matrix.postRotate(-mCurrentDegree, mBmp.getWidth() / 2, mBmp.getHeight() / 2);
			canvas.drawBitmap(mBmp, matrix, paint2);
		} else {
			super.draw(canvas);
		}
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		roundHeight = h / 2;
		roundWidth = w / 2;
		if (w > 0 && h > 0) {
			try {
				mBmp = Bitmap.createBitmap(w, h, Config.ARGB_8888);
				mCanvas.setBitmap(mBmp);
			} catch (OutOfMemoryError e) {
				e.printStackTrace();
			}
		}
	}

	private void drawLiftUp(Canvas canvas) {
		Path path = new Path();
		path.moveTo(0, roundHeight);
		path.lineTo(0, 0);
		path.lineTo(roundWidth, 0);
		path.arcTo(new RectF(0, 0, roundWidth * 2, roundHeight * 2), -90, -90);
		path.close();
		canvas.drawPath(path, paint);
	}

	private void drawLiftDown(Canvas canvas) {
		Path path = new Path();
		path.moveTo(0, getHeight() - roundHeight);
		path.lineTo(0, getHeight());
		path.lineTo(roundWidth, getHeight());
		path.arcTo(new RectF(0, getHeight() - roundHeight * 2, 0 + roundWidth * 2, getHeight()), 90, 90);
		path.close();
		canvas.drawPath(path, paint);
	}

	private void drawRightDown(Canvas canvas) {
		Path path = new Path();
		path.moveTo(getWidth() - roundWidth, getHeight());
		path.lineTo(getWidth(), getHeight());
		path.lineTo(getWidth(), getHeight() - roundHeight);
		path.arcTo(new RectF(getWidth() - roundWidth * 2, getHeight() - roundHeight * 2, getWidth(), getHeight()), 0,
				90);
		path.close();
		canvas.drawPath(path, paint);
	}

	private void drawRightUp(Canvas canvas) {
		Path path = new Path();
		path.moveTo(getWidth(), roundHeight);
		path.lineTo(getWidth(), 0);
		path.lineTo(getWidth() - roundWidth, 0);
		path.arcTo(new RectF(getWidth() - roundWidth * 2, 0, getWidth(), 0 + roundHeight * 2), -90, 90);
		path.close();
		canvas.drawPath(path, paint);
	}
}
