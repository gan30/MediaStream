package com.xbcx.camera;

import java.util.ArrayList;
import java.util.List;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewConfiguration;

public class PointFocusManager implements OnTouchListener, OnClickListener {
	private View  surfaceView;
	private float pointX, pointY;
	private int mTouchSlot;
	private Camera cameraInst = null;
	private Camera.Parameters parameters = null;
	private PointFocusManagerInterface linstener;
	private MuiltPointerZoomSizeChangeListener zoomlistener;
	
	private boolean mIsTouchEnable = true;

	public PointFocusManager(View surfaceView) {
		this.surfaceView = surfaceView;
		this.surfaceView.setFocusable(true);
		this.surfaceView.setOnTouchListener(this);
		this.surfaceView.setOnClickListener(this);
		final ViewConfiguration config = ViewConfiguration.get(surfaceView.getContext());
		mTouchSlot = (int) (config.getScaledTouchSlop()*1.5);
	}

	public void setCamera(Camera cameraInst) {
		this.cameraInst = cameraInst;
	}

	public void setLinstener(PointFocusManagerInterface linstener) {
		this.linstener = linstener;
	}

	public void setZoomlistener(MuiltPointerZoomSizeChangeListener zoomlistener) {
		this.zoomlistener = zoomlistener;
	}
	
	public void setIsTouchEnable(boolean isTouchEnable) {
		this.mIsTouchEnable = isTouchEnable;
	}

	@Override
	public void onClick(View v) {
		try {
			pointFocus((int) pointX, (int) pointY);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void pointFocus(int x, int y) {
		if (linstener != null && linstener.interfacePointFocus())
			return;
		if(XCamera.get().isAlive()){
			cameraInst.cancelAutoFocus();
			parameters = cameraInst.getParameters();
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
				showPoint(x, y);
			}
			cameraInst.setParameters(parameters);
			autoFocus();
		}
	}

	private void autoFocus() {
		new Thread() {
			@Override
			public void run() {
				try {
					sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (cameraInst == null) {
					return;
				}
				surfaceView.post(new Runnable() {
					@Override
					public void run() {
						if (linstener != null)
							linstener.autoFocus((int) pointX, (int) pointY);
					}
				});
			}
		}.start();
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private void showPoint(int x, int y) {
		if (parameters.getMaxNumMeteringAreas() > 0) {
			List<Camera.Area> areas = new ArrayList<Camera.Area>();
			int rectY = -x * 2000 / surfaceView.getWidth() + 1000;
			int rectX = y * 2000 / surfaceView.getHeight() - 1000;

			int left = rectX < -900 ? -1000 : rectX - 100;
			int top = rectY < -900 ? -1000 : rectY - 100;
			int right = rectX > 900 ? 1000 : rectX + 100;
			int bottom = rectY > 900 ? 1000 : rectY + 100;
			Rect area1 = new Rect(left, top, right, bottom);
			areas.add(new Camera.Area(area1, 800));
			parameters.setMeteringAreas(areas);
		}

		List<String> focuses = parameters.getSupportedFocusModes();
		if (focuses != null) {
			if (focuses.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
				parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
			}
		}
	}

	private int perxy;
	private int changelength;
	private int startchangelength;
	private boolean start;
	private boolean multiPointer;

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(mIsTouchEnable == false){
			return false;
		}
		boolean retresult = false;
		int pointcount = event.getPointerCount();
		switch (event.getAction() & MotionEvent.ACTION_MASK) {
		case MotionEvent.ACTION_POINTER_UP:
			perxy = 0;
			changelength = 0;
			startchangelength = 0;
			start = false;
			if (zoomlistener != null)
				zoomlistener.zoomSizeChange(ZoomChangeStatus.End,changelength);
			return retresult;
		case MotionEvent.ACTION_POINTER_DOWN:
			multiPointer = true;
			perxy = 0;
			start = false;
			changelength = 0;
			startchangelength = 0;
			if (zoomlistener != null)
				zoomlistener.zoomSizeChange(ZoomChangeStatus.Start,changelength);
			break;
		case MotionEvent.ACTION_DOWN:
			multiPointer = false;
			pointX = event.getX();
			pointY = event.getY();
			break;
		case MotionEvent.ACTION_UP:
			retresult = multiPointer;
			break;
		}
		if (pointcount >= 2) {
			int left = Integer.MAX_VALUE;
			int top = Integer.MAX_VALUE;
			int right = Integer.MIN_VALUE;
			int bottom = Integer.MIN_VALUE;
			for (int n = 0; n < pointcount; n++) {
				int x = (int) event.getX(n);
				int y = (int) event.getY(n);
				if (top > y)
					top = y;
				if (bottom < y)
					bottom = y;
				if (left > x)
					left = x;
				if (right < x)
					right = x;
			}
			int heigh = bottom - top;
			int width = right - left;
			int temperxy = (int) Math.sqrt(heigh * heigh + width * width);
			if (perxy <= 0) {

			} else {
				if(start){
					changelength += (temperxy - perxy);
					if (zoomlistener != null)
						zoomlistener.zoomSizeChange(ZoomChangeStatus.Zooming,changelength);
				}else{
					startchangelength+=(temperxy - perxy);
					if(Math.abs(startchangelength)>mTouchSlot)
						start=true;
				}
				
			}
			perxy = temperxy;
		}
		return retresult;
	}

	public static interface PointFocusManagerInterface {
		public boolean interfacePointFocus();

		public void autoFocus(int x, int y);
	}

	public static interface MuiltPointerZoomSizeChangeListener {
		public boolean zoomSizeChange(ZoomChangeStatus state, int size);
	}
	
	public static enum ZoomChangeStatus{
		Start,
		Zooming,
		End
	}
}
