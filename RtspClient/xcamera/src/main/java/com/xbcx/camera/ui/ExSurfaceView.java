/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.xbcx.camera.ui;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class ExSurfaceView extends SurfaceView {
	
	boolean mIsKeepAlive;
	
	public ExSurfaceView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public ExSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public ExSurfaceView(Context context) {
		super(context);
		init();
	}
	
	private void init(){
		setZOrderMediaOverlay(true);
        getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	public void setVisibility(int visibility) {
		super.setVisibility(visibility);
	}
	
	@Override
	protected void onWindowVisibilityChanged(int visibility) {
		super.onWindowVisibilityChanged(mIsKeepAlive?View.VISIBLE:visibility);
	}
	
	@Override
	protected void onVisibilityChanged(View changedView, int visibility) {
		super.onVisibilityChanged(changedView, mIsKeepAlive?View.VISIBLE:visibility);
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
	} 
	
	public void setKeepSurfaceAlive(boolean keepAlive) {
		this.mIsKeepAlive = keepAlive;
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onWindowSystemUiVisibilityChanged(int visible) {
		super.onWindowSystemUiVisibilityChanged(mIsKeepAlive?View.VISIBLE:visible);
	}
}
