package com.xbcx.camera;

import android.hardware.Camera;

public interface XCameraListener {
	public void onCameraOpend(Camera camera);
	public void onCameraClose();
	public void onCameraError(int error);
}
