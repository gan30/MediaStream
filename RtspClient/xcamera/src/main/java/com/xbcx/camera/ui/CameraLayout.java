package com.xbcx.camera.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class CameraLayout extends FrameLayout {

	private float mBeliel = -1;
	
	public CameraLayout(Context context) {
		super(context);
	}
	
	public CameraLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public CameraLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(mBeliel!=-1){
			int height = getMeasuredHeight();
			int width = (int) (height/mBeliel);
			setMeasuredDimension(width, height);
		}
	}
	
	public void setBeliel(float beliel) {
		this.mBeliel = beliel;
		invalidate();
	}
}
