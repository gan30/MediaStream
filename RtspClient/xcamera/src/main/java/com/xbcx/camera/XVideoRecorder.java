package com.xbcx.camera;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.xbcx.camera.settings.XCameraSettings;
import com.xbcx.core.ToastManager;
import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import gan.camera.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class XVideoRecorder implements OnErrorListener, OnInfoListener {
	final static String tag = "XVideoRecoder";
	
	private Activity						mActivity;
	private MediaRecorder 					mMediaRecorder;
	private List<OnVideoRecoderListener> 	mOnVideoRecoderListeners;
	private CamcorderProfileCreator			mCamcorderProfileCreator;
	private CamcorderProfile 				mProfile;
	
	private OnPrepareListener				mOnPerpareListener;
	private OnFrameListener					mOnFrameListener;
	
	private Camera					mCamera;
	
	private String					mVideoFilename;
	private boolean					mMediaRecorderRecording;
	
	private List<Size>				mSupportedSizes;
	
	private ParcelFileDescriptor	mVideoFileDescriptor;
	private Uri						mCurrentUri;
	
	private TextView				mTextViewRecordTime;
	private long           			mRecordingStartTime;
	private boolean					mIsShowTime = true;
	private int 					mOrientation = 90;
	
	@SuppressLint("NewApi")
	public XVideoRecorder(Activity activity) {
		mActivity = activity;
	}

	@SuppressLint("NewApi")
	public void setCamera(Camera camera) {
		this.mCamera = camera;
		if(camera!=null){
			List<Size> videosize = camera.getParameters().getSupportedVideoSizes();
			if(videosize!=null){
				mSupportedSizes = new ArrayList<Size>();
				mSupportedSizes.addAll(videosize);
				for(Size size : videosize){
					XbLog.i(tag, "videoSize :" +size.width+"x"+size.height);
				}
			}
		}
	}
	
	public void setIsShowTime(boolean isShowTime) {
		this.mIsShowTime = isShowTime;
		if(isRecoding()){
			if(mIsShowTime){
				updateRecordingTime();
			}else{
				goneRecoreTime();
			}
		}
	}
	
	private void closeVideoFileDescriptor() {
        if (mVideoFileDescriptor != null) {
            try {
                mVideoFileDescriptor.close();
            } catch (IOException e) {
            }
            mVideoFileDescriptor = null;
        }
    }
	
	public Context getContext(){
		return mActivity;
	}
	
	public List<Size> getSupportedSizes(){
		return mSupportedSizes;
	}
	
	public void release(){
		releaseMediaRecorder();
		if(mOnVideoRecoderListeners!=null) {
			mOnVideoRecoderListeners.clear();
		}
	}
	
	public void toggleRecord(){
		if(mMediaRecorderRecording){
			stopRecord();
		}else{
			startRecord(mVideoFilename);
		}
	}
	
	public static String newFile(){
		File file = new File(Environment.getExternalStorageDirectory().toString()+"/xbcx/Camera");
		if(!file.exists()){
			file.mkdirs();
		}
		return file.getAbsolutePath()+"/"+ newFileName(); 
	}
	
	public static String newFileName() {
		return "VID_"+UUID.randomUUID().toString()+".mp4";
	}
	
	public void startRecord(){
		startRecord(null);
	}
	
	public void startRecord(String filePath){
		if(mCamera == null){
			throw new NullPointerException("camera disallow null");
		}
		if(mMediaRecorderRecording){
			return ;
		}
		
		try {
			initRecoder();
			
			long requestedSizeLimit = 0;
			int duration = 0;
			if(isPick()){
				if(CameraUtil.isPickVideo(mActivity)){
					Bundle myExtras = mActivity.getIntent().getExtras();
					Uri saveUri = (Uri) myExtras.getParcelable(MediaStore.EXTRA_OUTPUT);
					if (saveUri != null) {
						try {
							mVideoFileDescriptor = mActivity.getContentResolver().openFileDescriptor(saveUri, "rw");
							mCurrentUri = saveUri;
						} catch (java.io.FileNotFoundException ex) {
						}
					}
					requestedSizeLimit = myExtras.getLong(MediaStore.EXTRA_SIZE_LIMIT);
					duration = myExtras.getInt(MediaStore.EXTRA_DURATION_LIMIT);
					int quality = CamcorderProfile.QUALITY_720P;
					quality = myExtras.getInt(MediaStore.EXTRA_VIDEO_QUALITY);
					mMediaRecorder.setProfile(mProfile=getCamcorderProfile(quality));
				}else{
					mMediaRecorder.setProfile(mProfile=getCamcorderProfile(CamcorderProfile.QUALITY_720P));
				}
			}else{
				if(mCamcorderProfileCreator == null){
					mCamcorderProfileCreator = new SimpleCamcorderProfileCreator();
				}
				mMediaRecorder.setProfile(mProfile=mCamcorderProfileCreator.onCreateCamcorderProfile(mActivity, this));
			}
			
			if(mProfile.videoFrameHeight == 1088) {
				mProfile.videoFrameHeight = 1080;
			}
			
			mMediaRecorder.setVideoSize(mProfile.videoFrameWidth, mProfile.videoFrameHeight);
			if(mVideoFileDescriptor!=null){
				mMediaRecorder.setOutputFile(mVideoFileDescriptor.getFileDescriptor());
			}else{
				if(TextUtils.isEmpty(filePath)){
					filePath = newFile();
				}
				File file = new File(filePath);
				if(CameraUtil.isAvailableFile(file)){
					mMediaRecorder.setOutputFile(filePath);
					mVideoFilename = filePath;
					CameraUtil.insertVideoToMediaStore(mActivity, file);
				}else{
					onRecordError(1);
					ToastManager.getInstance(mActivity).show(R.string.camera_file_fail);
					return;
				}
			}
			
			if(requestedSizeLimit>0){
				mMediaRecorder.setMaxFileSize(requestedSizeLimit);
			}
			if(duration>0){
				mMediaRecorder.setMaxDuration(duration*1000);
			}
			
			try {
				prepare();
				mMediaRecorder.prepare();
			} catch (Exception e) {
				e.printStackTrace();
				onRecordError(1);
				throw new RuntimeException("prepare fail");
			}
			
			pauseAudioPlayback();
			mMediaRecorder.start();
			mMediaRecorder.setOnErrorListener(this);
			
			try {
				Class<?> OnFrameListener = Class.forName("android.media.MediaRecorder$OnFrameListener");
				OnInfoListener listener = (OnInfoListener) Proxy.newProxyInstance(OnFrameListener.getClassLoader(),   
						new Class[] { OnFrameListener }, new FrameListenerDynamicProxy(this));  
				mMediaRecorder.setOnInfoListener(listener);
			} catch (Exception e) {
				mMediaRecorder.setOnInfoListener(this);
			}
		} catch (Exception e) {
			e.printStackTrace();
			onRecordError(0);
			Toast.makeText(mActivity, "record failed", Toast.LENGTH_SHORT).show();
		}
		
		mRecordingStartTime = SystemClock.uptimeMillis();
		if(mIsShowTime){
			updateRecordingTime();
		}
		mMediaRecorderRecording = true;
		if(mOnVideoRecoderListeners!=null){
			for(OnVideoRecoderListener listener:mOnVideoRecoderListeners){
				listener.onRecordStart(this);
			}
		}
	}
	
	public CamcorderProfile getProfile() {
		return mProfile;
	}
	
	protected void prepare(){
		if(mOnPerpareListener!=null){
			mOnPerpareListener.onPrepare(this);
		}
	}
	
	private void pauseAudioPlayback() {
        AudioManager am = (AudioManager) mActivity.getSystemService(Context.AUDIO_SERVICE);
        am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }
	
	protected void onRecordError(int error){
		cleanupEmptyFile();
		if(mOnVideoRecoderListeners!=null){
			for(OnVideoRecoderListener listener:mOnVideoRecoderListeners){
				listener.onRecordError(this, error);
			}
		}
	}
	
	private void updateRecordingTime() {
		if(mTextViewRecordTime==null){
			FrameLayout	layout = new FrameLayout(mActivity);
			mActivity.addContentView(layout, 
					new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			mTextViewRecordTime = new TextView(mActivity);
			mTextViewRecordTime.setTextSize(15);
			mTextViewRecordTime.setTextColor(Color.WHITE);
			
			FrameLayout.LayoutParams lParams = new FrameLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, 
					LayoutParams.WRAP_CONTENT);
			lParams.topMargin = 100;
			lParams.leftMargin = 50;
			layout.addView(mTextViewRecordTime, lParams);
		}
		
		long now = SystemClock.uptimeMillis();
        long delta = now - mRecordingStartTime;
		mTextViewRecordTime.setText(CameraUtil.millisecondToTimeString(delta, false));
		
		mTextViewRecordTime.removeCallbacks(mTimeRunnable);
		mTextViewRecordTime.postDelayed(mTimeRunnable, 999);
		mTextViewRecordTime.setVisibility(View.VISIBLE);
	}
	
	public long getRecordTime(){
		long now = SystemClock.uptimeMillis();
		return now - mRecordingStartTime;
	}
	
	public void goneRecoreTime(){
		if(mTextViewRecordTime!=null){
			mTextViewRecordTime.removeCallbacks(mTimeRunnable);
			mTextViewRecordTime.setVisibility(View.GONE);
		}
	}
	
	public boolean stopRecord(){
		return stopRecord(false);
	}
	
	public boolean stopRecord(boolean cancel){
		boolean fail = true;
		try {
			mMediaRecorderRecording = false;
			mMediaRecorder.setOnInfoListener(null);
			mMediaRecorder.setOnErrorListener(null);
			mMediaRecorder.stop();
			onStopVideoRecording(cancel);
		} catch (Exception e) {
			e.printStackTrace();
			fail = false;
		}
        return fail;
	}
	
	public boolean isPick(){
		return CameraUtil.isPickVideo(mActivity);
	}
	
	@SuppressLint({ "InlinedApi", "NewApi" })
	private void initRecoder() throws Exception{
		if(mMediaRecorder == null){
			mMediaRecorder = new MediaRecorder();
		}else{
			mMediaRecorder.reset();
		}
		
		try {
			mCamera.unlock();
			mMediaRecorder.setCamera(mCamera);
			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			mMediaRecorder.setOrientationHint(mOrientation);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public void setOrientationHint(int degrees){
		mOrientation = degrees;
	}
	
	public Camera getCamera(){
		return mCamera;
	}
	
	private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
        mVideoFilename = null;
    }
	
	private void cleanupEmptyFile() {
        if (mVideoFilename != null) {
            File f = new File(mVideoFilename);
            if (f.length() == 0 && f.delete()) {
                mVideoFilename = null;
            }
        }
    }
	
	public String getVideoFile(){
		return mVideoFilename!=null? mVideoFilename:
			mCurrentUri!=null? mCurrentUri.getPath():null;
	}
	
	public boolean isRecoding(){
		return mMediaRecorderRecording;
	}
	
	public XVideoRecorder addOnVideoRecoderListener(OnVideoRecoderListener onVideoRecoderListener) {
		if(mOnVideoRecoderListeners == null){
			mOnVideoRecoderListeners = new ArrayList<OnVideoRecoderListener>();
		}
		mOnVideoRecoderListeners.add(onVideoRecoderListener);
		return this;
	}
	
	public void removeVideoRecoderListener(OnVideoRecoderListener listener) {
		if(mOnVideoRecoderListeners!=null) {
			mOnVideoRecoderListeners.remove(listener);
		}
	}
	
	public XVideoRecorder setCamcorderProfileCreator(CamcorderProfileCreator camcorderProfileCreator) {
		this.mCamcorderProfileCreator = camcorderProfileCreator;
		return this;
	}
	
	public static interface OnVideoRecoderListener{
		
		public void onRecordStart(XVideoRecorder recoder);
		public void onRecordEnd(XVideoRecorder recoder);
		public void onRecordError(XVideoRecorder recoder, int error);
	}
	
	public static class MediaRecoderConfig{
		public TextureView		mSurfaceView;
	}
	
	public static class MediaRecoderConfigBuilder{
		
		MediaRecoderConfig	mMediaRecoderConfig;
		
		public MediaRecoderConfigBuilder() {
			mMediaRecoderConfig = new MediaRecoderConfig();
		}
		
		public MediaRecoderConfigBuilder setSurfaceView(TextureView surfaceView){
			mMediaRecoderConfig.mSurfaceView = surfaceView;
			return this;
		}
		
		public MediaRecoderConfig build(){
			return mMediaRecoderConfig;
		}
	}
	
	public static interface MediaRecoderConfigCreator{
		public MediaRecoderConfig onCreateMediaRecoderConfig();
	}

	public static interface CamcorderProfileCreator{
		public CamcorderProfile onCreateCamcorderProfile(Activity activity, XVideoRecorder recorder);
	}
	
	@Override
	public void onInfo(MediaRecorder mr, int what, int extra) {
		XbLog.i(tag, "info"+"what:"+what+",extra:"+extra);
		 if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
	            if (mMediaRecorderRecording) stopRecord(false);
	        } else if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED) {
	            if (mMediaRecorderRecording) stopRecord(false);
	        }
	}
	
	public void onFrame(MediaRecorder mr, byte[] data, int type) {
		XbLog.i(tag, "onFrame data length:"+data.length+",type:" + type);
		if(mOnFrameListener!=null) {
			mOnFrameListener.onFrame(this, mr, data, type);
		}
	}

	@Override
	public void onError(MediaRecorder mr, int what, int extra) {
		XbLog.i(tag, "onError"+"what:"+what+",extra:"+extra);
		if (what == MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN) {
	         stopRecord(true);
	    }
	}
	
	private void onStopVideoRecording(boolean cancel) {
		goneRecoreTime();
		closeVideoFileDescriptor();
		cleanupEmptyFile();
		if(!TextUtils.isEmpty(mVideoFilename)){
			if(cancel){
				deleteVideoFile(mVideoFilename);
			}else{
				CameraUtil.sendMeidaScan(mActivity, new File(mVideoFilename));
			}
		}
		mMediaRecorderRecording = false;
		if(mOnVideoRecoderListeners!=null){
			for(OnVideoRecoderListener listener:mOnVideoRecoderListeners){
				listener.onRecordEnd(this);
			}
		}
	}
	
	private void deleteVideoFile(String fileName) {
        File f = new File(fileName);
        if (!f.delete()) {
        	CameraUtil.deleteVideoToMiastore(getContext(), fileName);
        }
    }
	
	public void takePiture(OnTakePictureCallBack callBack){
//		if(mMetadataRetriever == null){
//			mMetadataRetriever = new MediaMetadataRetriever();
//		}
//		mMetadataRetriever.setDataSource(getVideoFile());
//		callBack.onTakePictureEnd(mMetadataRetriever.getFrameAtTime(getRecordTime()));
	}
	
	public Intent getReslutIntent(){
		Intent resultIntent = new Intent();
		if (mCurrentUri == null) {
			try {
				mCurrentUri = Uri.fromFile(new File(mVideoFilename));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		resultIntent.setData(mCurrentUri);
		return resultIntent;
	}
	
	protected void doReturnToCaller(boolean valid) {
        Intent resultIntent = new Intent();
        int resultCode;
        if (valid) {
            resultCode = Activity.RESULT_OK;
            resultIntent = getReslutIntent();
        } else {
            resultCode = Activity.RESULT_CANCELED;
        }
        mActivity.setResult(resultCode, resultIntent);
        mActivity.finish();
    }
	
	public static class SimpleCamcorderProfileCreator implements CamcorderProfileCreator{

		@Override
		public CamcorderProfile onCreateCamcorderProfile(Activity activity,XVideoRecorder recorder) {
			CamcorderProfile profile = getCamcorderProfile(getQualitformPrefrences(recorder.getContext()));
			
			try {
				final String frame = XCameraSettings.readStringSetting("video_frame", XCameraSettings.VideoFrame_Def);
				profile.videoFrameRate = Integer.valueOf(frame);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				final String bitRate = XCameraSettings.readStringSetting("video_bitrate", XCameraSettings.Rate_Def);
				profile.videoBitRate = profile.videoFrameWidth*profile.videoFrameHeight*Integer.valueOf(bitRate);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return profile;
		}
	}
	
	@SuppressLint({ "InlinedApi", "NewApi" })
	public static int getQualitformPrefrences(Context context){
		final String value = XCameraSettings.getVideoSize();
		int[] size = CameraUtil.parseSize(value);
		int quality = CamcorderProfile.QUALITY_HIGH;
		if(1080 == size[1]){
			quality = CamcorderProfile.QUALITY_1080P;
		}else if(720 == size[1]){
			quality = CamcorderProfile.QUALITY_720P;
		}
		if(XCamera.getCameraId()<2) {
			if (CamcorderProfile.hasProfile(XCamera.getCameraId(), quality) == false) {
				quality = CamcorderProfile.QUALITY_480P;
			}
		}
		return quality;
	}
	
	public static CamcorderProfile getCamcorderProfile(int quality){
		return CamcorderProfile.get(quality);
	}
	
	public void setOnPerpareListener(OnPrepareListener onPerpareListener) {
		this.mOnPerpareListener = onPerpareListener;
	}
	
	public void setOnFrameListener(OnFrameListener onFrameListener) {
		this.mOnFrameListener = onFrameListener;
	}
	
	private Runnable	mTimeRunnable = new Runnable() {
		@Override
		public void run() {
			updateRecordingTime();
		}
	};
	
	public static interface OnTakePictureCallBack{
		public void onTakePictureEnd(Bitmap bitmap);
	}
	
	public static interface OnPrepareListener{
		public void onPrepare(XVideoRecorder recorder);
	}
	
	public interface OnFrameListener{
		public void onFrame(XVideoRecorder recorder, MediaRecorder mr, byte[] data, int type);
	}
	
	public void closeWarn(){
		AudioManager audioManager = (AudioManager) XApplication.getApplication().getSystemService(Context.AUDIO_SERVICE);
	    audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
	    audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
	    audioManager.setStreamVolume(AudioManager.STREAM_ALARM, 0, 0);
	    audioManager.setStreamVolume(AudioManager.STREAM_DTMF, 0, 0);
	    audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0);
	    audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
	}
	
	public void resumeWarn(){
		
	}
	
	private static class FrameListenerDynamicProxy implements InvocationHandler{

		final static String TAG = "FrameListenerDynamicProxy";
		
		XVideoRecorder	mRecorder;
		
		public FrameListenerDynamicProxy(XVideoRecorder recorder) {
			mRecorder = recorder;
		}
		
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
	        try {  
	            XbLog.i(TAG, "invoke, method: " + method.getName());  
	            if("onFrame".equals(method.getName())) {  
	            	MediaRecorder mr = (MediaRecorder)args[0];
	            	byte[] data = (byte[])args[1];
	            	int type = (Integer) args[2];
	                mRecorder.onFrame(mr, data, type);
	            }else if("onInfo".equals(method.getName())){
	            	MediaRecorder mr = (MediaRecorder)args[0];
	            	int what = (Integer)args[1];
	            	int extra = (Integer)args[2];
	                mRecorder.onInfo(mr, what, extra);
	            }
	        } catch (Exception e) {  
	        	e.printStackTrace();
	        }  
	        return proxy;  
		}
	}
}
