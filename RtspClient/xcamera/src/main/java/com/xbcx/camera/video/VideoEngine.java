package com.xbcx.camera.video;

import android.hardware.Camera;

import com.xbcx.camera.CameraBasePlugin;

public interface VideoEngine extends CameraBasePlugin{
	public boolean 	startVideo(final String filePath) throws Exception;
	public boolean 	stopVideo();
	public boolean 	clipVideo();
	public boolean 	isVideoRecording();
	public long    	getVideoStartTime();
	public void   		setRotation(int orientation);
	public String  		getVideoFile();
	public void   		setCamera(Camera camera);
	public void   		addVideoRecoderListener(VideoRecoderListener listener);
	public void   		removeVideoRecoderListener(VideoRecoderListener listener);
	public void  		release();
}
