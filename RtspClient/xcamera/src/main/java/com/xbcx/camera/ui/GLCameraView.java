package com.xbcx.camera.ui;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.ViewGroup.LayoutParams;  
  
@SuppressLint("NewApi")
public class GLCameraView extends GLSurfaceView implements CameraView,Renderer, OnFrameAvailableListener{  
   
	private static final String TAG = "GLCameraView";
    
    Context 			mContext;
    SurfaceTexture 		mSurface;
	int 				mTextureID = -1;
	GLCameraDrawer 		mDirectDrawer;
	
	OnSurfaceCallBack	mOnSurfaceCallBack;
	boolean 			mIsKeepAlive;
	
	float 	mRatio = -1;
	int 	mMeasuredWidth = -1;
    
    public GLCameraView(Context context) {
        super(context);
        mContext = context;  
        initGL();
    }
    
    public GLCameraView(Context context, AttributeSet attrs) {  
        super(context, attrs);  
        mContext = context;
        initGL();
    }  
    
    protected void initGL() {
        setEGLContextClientVersion(2);  
        setRenderer(this); 
        setRenderMode(RENDERMODE_WHEN_DIRTY);
    }
    
    @Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    	Log.i(TAG, "onSurfaceCreated...");
		mTextureID = createTextureID();
		mSurface = new SurfaceTexture(mTextureID);
		mSurface.setOnFrameAvailableListener(this);
		if(mOnSurfaceCallBack!=null) {
			mOnSurfaceCallBack.onSurfaceCreated(mSurface);
		}
		mDirectDrawer = new GLCameraDrawer(mTextureID);
	}
	
	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		Log.i(TAG, "onSurfaceChanged...");
		GLES20.glViewport(0, 0, width, height);
		if(!mIsKeepAlive) {
			if(mOnSurfaceCallBack!=null) {
				mOnSurfaceCallBack.onSurfaceChanged(mSurface, width, height);
			}
		}
	}
	
	@Override
	public void onDrawFrame(GL10 gl) {
		Log.i(TAG, "onDrawFrame...");
		GLES20.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
		mSurface.updateTexImage();
		float[] mtx = new float[16];
		mSurface.getTransformMatrix(mtx);
		mDirectDrawer.draw(mtx);
	}
	
	private int createTextureID(){
		int[] texture = new int[1];

		GLES20.glGenTextures(1, texture, 0);
		GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, texture[0]);
		GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_LINEAR);        
		GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
		GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,
				GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

		GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);
		
		return texture[0];
	}
	
	public SurfaceTexture getSurfaceTexture(){
		return mSurface;
	}
	
	@Override
	public void onFrameAvailable(SurfaceTexture surfaceTexture) {
		Log.i(TAG, "onFrameAvailable...");
		this.requestRender();
	}
	
	@Override
	protected void onAttachedToWindow() {
		super.onAttachedToWindow();
		Log.i(TAG, "onAttachedToWindow...");
	}
	
	@Override
	protected void onDetachedFromWindow() {
		if(!mIsKeepAlive) {
			super.onDetachedFromWindow();
			mMeasuredWidth = -1;
			Log.i(TAG, "onDetachedFromWindow...");
		}
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		if(!mIsKeepAlive) {
			Log.i(TAG, "surfaceCreated...");
			super.surfaceCreated(holder);
		}
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h){
		if(!mIsKeepAlive) {
			Log.i(TAG, "surfaceChanged...");
			super.surfaceChanged(holder, format, w, h);
		}
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if(!mIsKeepAlive) {
			Log.i(TAG, "surfaceDestroyed...");
			super.surfaceDestroyed(holder);
		}
	}
	
	protected void onSurfaceDestroyed() {
		if(mOnSurfaceCallBack!=null) {
			mOnSurfaceCallBack.onSurfaceDestroyed(mSurface);
		}
	}
	
	public void setOnSurfaceCallBack(OnSurfaceCallBack onSurfaceCallBack) {
		this.mOnSurfaceCallBack = onSurfaceCallBack;
	}
	
	public static interface OnSurfaceCallBack{
		public void onSurfaceCreated(SurfaceTexture texture);
		public void onSurfaceChanged(SurfaceTexture texture, int width, int height);
		public void onSurfaceDestroyed(SurfaceTexture texture);
	}

	public boolean isLandscape() {
		return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}
	
	@Override
	public void setRatio(float ratio) {
		this.mRatio = ratio;
		resizeView();
	}

	@Override
	public void setKeepSurfaceAlive(boolean alive) {
		mIsKeepAlive = alive;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(mRatio!=-1) {
			if(isLandscape()) {
				heightMeasureSpec = (int)(getMeasuredWidth()/mRatio);
			}else {
				heightMeasureSpec = (int)(getMeasuredWidth()*mRatio);
			}
			setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
		}
		mMeasuredWidth = getMeasuredWidth();
	}
	
	public void resizeView() {
		mMeasuredWidth = getMeasuredWidth();
		if(mMeasuredWidth!=-1 && mMeasuredWidth>10) {
			LayoutParams lp = getLayoutParams();
			if (isLandscape()) {
				lp.height = (int) (mMeasuredWidth / mRatio);
			} else {
				lp.height = (int) (mMeasuredWidth * mRatio);
			}
			setLayoutParams(lp);
		}
	}
}  