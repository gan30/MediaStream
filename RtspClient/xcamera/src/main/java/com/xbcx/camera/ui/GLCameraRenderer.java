package com.xbcx.camera.ui;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.xbcx.util.XbLog;

import android.opengl.GLSurfaceView.Renderer;

public class GLCameraRenderer implements Renderer{
	
	final static String TAG = GLCameraRenderer.class.getSimpleName();

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		 XbLog.i(TAG, "onSurfaceCreated...");
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		
	}
}
