package com.xbcx.camera;

import java.util.Collection;
import java.util.Collections;

import com.xbcx.camera.CameraServiceActivity.ActivityCaller;
import com.xbcx.camera.XCamera.PreviewChangedListener;
import com.xbcx.camera.ui.CameraView;
import com.xbcx.camera.ui.GLCameraView.OnSurfaceCallBack;
import com.xbcx.camera.ui.PreviewSurfaceView;
import com.xbcx.camera.video.RestartManager;
import com.xbcx.camera.video.VideoEngine;
import com.xbcx.camera.video.VideoRecoderListenerPlugin;
import com.xbcx.camera.video.VideoServicePlugin;
import com.xbcx.core.AndroidEventManager;
import com.xbcx.core.CrashHandler.OnCrashHandler;
import com.xbcx.core.PluginHelper;
import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.app.Activity;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;

public class CameraService extends Service implements Callback,XCameraListener,VideoRecoderListenerPlugin, PreviewCallback, OnSurfaceCallBack, PreviewChangedListener{

	final static String Tag = "CameraService";
	
	public static final String ACTION_SHUTDOWN = "android.intent.action.ACTION_SHUTDOWN";
	
	protected	AndroidEventManager	mEventManager = AndroidEventManager.getInstance();
	
	private		PluginHelper<CameraBasePlugin>	mPluginHelper;
	
	CameraServiceBinder		mCameraServiceBinder;
	
	protected XCamera		mCamera = XCamera.get();
	
	VideoServicePlugin		mVideoServicePlugin;
	BroadcastReceiver		mBroadcastReceiver;
	IntentFilter			mIntentFilter;
	View					mCameraView;
	SurfaceHolder			mSurfaceHolder;
	SurfaceTexture			mSurface;
	
	protected CameraWindowManager		mCameraWindowManager;
	int									mWindowOrientation = 90;
	int									mCameraDisplayOrientation = 90;
	
	ActivityCaller 						mCameraActivityCaller;
	
	@Override
	public IBinder onBind(Intent intent) {
		if(mCameraServiceBinder == null) {
			mCameraServiceBinder = new CameraServiceBinder();
		}
		return mCameraServiceBinder;
	}
	
	public class CameraServiceBinder extends Binder{
		public CameraService getService() {
			return CameraService.this;
		}
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		XbLog.i(Tag, "onCreate");
		registerPlugin(this);
		registerPlugin(mVideoServicePlugin = new VideoServicePlugin());
		
		mIntentFilter = new IntentFilter();
		mIntentFilter.addAction(ACTION_SHUTDOWN);
		registerReceiver(mBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				onBroadcastReceive(intent);
			}
		}, mIntentFilter);
		
		for(CameraServicePluginImp p : XApplication.getManagers(CameraServicePluginImp.class)){
			registerPlugin(p.createServicePlugin());
		}
		
		onCreateCamera();
		
		XApplication.addManager(new OnCrashHandler() {
			@Override
			public void onUncaughtException(Thread thread, Throwable ex) {
				if(isRuning()) {
					stopRun();
				}
			}
		});
	}
	
	protected void onCreateCamera() {
		mCameraView = onCreateCameraView();
		mCameraWindowManager = new CameraWindowManager(this);
		mCameraWindowManager.setCameraView(mCameraView);
		if(needRestart()) {
			window(100, 50);
			mCameraWindowManager.dismissCamera();
		}
		
		CameraOrientationManager.get().enable();
		mCamera.addCameraListener(this);
		mCamera.addPreviewCallBack(this);
		mCamera.addPreviewChangedListener(this);
		onResumeCamera();
	}
	
	protected View onCreateCameraView() {
//		GLCameraView cameraView = new GLCameraView(this);
//		cameraView.setOnSurfaceCallBack(this);
		
		SurfaceView cameraView = new PreviewSurfaceView(this);
		cameraView.getHolder().addCallback(this);
		
		return cameraView;
	}
	
	protected void window(int width,int height) {
		mCameraWindowManager.showWindow(width, height);
	}

	protected void dismiss() {
		mCameraWindowManager.dismiss();
	}
	
	protected void onAttachActivity(CameraServiceActivity activity) {
		XbLog.i(Tag, "onAttachActivity");
		if(mCameraActivityCaller==null) {
			mCameraActivityCaller = activity.createActivityCaller();
		}
		
		mWindowOrientation = CameraUtil.getRotationAngle(activity);
		onResumeCamera();
		dismiss();
		for(BindActivityPlugin plugin:getPlugins(BindActivityPlugin.class)) {
			plugin.onAttachActivity(activity);
		}
	}
	
	protected void onDeathActivity(Activity activity) {
		XbLog.i(Tag, "onDeathActivity");
		mCameraActivityCaller = null;
		if(isRuning()) {
			RestartManager.setRestart(true);
		}
		for(BindActivityPlugin plugin:getPlugins(BindActivityPlugin.class)) {
			plugin.onDeathActivity(activity);
		}
		onPauseCamera();
	}
	
	public View getCameraView() {
		return mCameraView;
	}
	
	public SurfaceHolder getSurfaceHolder() {
		return mSurfaceHolder;
	}
	
	public void addBroadcastAction(String action) {
		mIntentFilter.addAction(action);
	}
	
	protected void onBroadcastReceive(Intent intent) {
		final String action = intent.getAction();
		if(CameraService.ACTION_SHUTDOWN.equals(action)) {
			onExceptionExit();
		}
		for(OnBroadcastReceiverPlugin p : getPlugins(OnBroadcastReceiverPlugin.class)){
			p.onBroadcastReceive(CameraService.this, intent);
		}
	}
	
	protected void onResumeCamera() {
		getCameraView().setVisibility(View.VISIBLE);
		mCamera.onResume();
	}
	
	protected void onPauseCamera() {
		if(isRuning()) {
			return;
		}
		XbLog.i(Tag, "onPauseCamera");
		mCamera.onPause();
		getCameraView().setVisibility(View.GONE);
	}
	
	protected void onDestoryCamera() {
		XbLog.i(Tag, "onDestoryCamera");
		if(isRuning()) {
			stopRun();
		}
		CameraOrientationManager.get().disable();
		mCamera.removePreviewCallBack(this);
		mCamera.removeCameraListener(this);
		mCamera.removePreviewChangedListener(this);
		mCamera.onDestory();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		XbLog.i(Tag, "onDestroy");
		onDestoryCamera();
		unregisterReceiver(mBroadcastReceiver);
		if(mPluginHelper != null){
			for(CameraServicePlugin<?> p : getPlugins(CameraServicePlugin.class)){
				p.onServiceDestory();
			}
			mPluginHelper.clear();
		}
	}
	
	protected void onExceptionExit() {
		if(isRuning()) {
			stopRun();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void registerPlugin(CameraBasePlugin plugin){
		if(mPluginHelper == null){
			mPluginHelper = new PluginHelper<CameraBasePlugin>();
		}
		mPluginHelper.addManager(plugin);
		if(plugin instanceof CameraServicePlugin){
			((CameraServicePlugin)plugin).onAttachService(this);
		}
	}
	
	public <T extends CameraBasePlugin> Collection<T> getPlugins(Class<T> cls){
		if(mPluginHelper == null){
			return Collections.emptySet();
		}
		return mPluginHelper.getManagers(cls);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends CameraBasePlugin> T get(Class<T> c) {
		for(CameraBasePlugin plugin:getPlugins(c)) {
			return (T) plugin;
		}
		return null;
	}
	
	@Override
	public void onCameraOpend(Camera camera) {
		XbLog.i(Tag, "onCameraOpend");
		for(CameraListenerPlugin plugin : getPlugins(CameraListenerPlugin.class)) {
			plugin.onCameraOpend(camera);
		}
		
		if(mSurfaceHolder!=null||mSurface!=null) {
			previewCamera();
		}
	}

	@Override
	public void onCameraClose() {
		XbLog.i(Tag, "onCameraClose");
		for(CameraListenerPlugin plugin : getPlugins(CameraListenerPlugin.class)) {
			plugin.onCameraClosed();
		}
	}

	@Override
	public void onCameraError(int error) {
		XbLog.i(Tag, "onCameraError");
	}

	private void previewCamera() {
		startPreview();
		onCameraPerpared();
	}
	
	/**
	 * camera perpared can takepicture and video
	 */
	protected void onCameraPerpared() {
		checkAndRestartVideo();
		if(mCameraActivityCaller!=null) {
			mCameraActivityCaller.onCameraPerpared();
		}
	}
	
	public boolean startVideo(final String filePath) {
		return mVideoServicePlugin.startVideo(filePath);
	}
	
	public boolean stopVideo() {
		return mVideoServicePlugin.stopVideo();
	}
	
	public boolean isVideoRecording() {
		return mVideoServicePlugin.isVideoRecording();
	}
	
	public long getVideoTime() {
		return mVideoServicePlugin.getVideoStartTime();
	}
	
	public static interface OnBroadcastReceiverPlugin extends CameraBasePlugin{
		public void onBroadcastReceive(CameraService service, Intent intent);
	}
	
	public static interface CameraListenerPlugin extends CameraBasePlugin{
		public void onCameraOpend(Camera camera);
		public void onCameraClosed();
	}

	public static interface BindActivityPlugin extends CameraBasePlugin{
		public void onAttachActivity(Activity activity);
		public void onDeathActivity(Activity activity);
	}
	
	@Override
	public void onRecordStart(VideoEngine recoder) {
		onVideoRuningChanged(true);
	}

	@Override
	public void onRecordEnd(VideoEngine recoder) {
		onVideoRuningChanged(false);
		ThumbHelper.saveThumbPath(recoder.getVideoFile());
	}

	@Override
	public void onRecordError(VideoEngine recoder, int error) {
		onVideoRuningChanged(false);
	}
	
	protected void onVideoRuningChanged(boolean runing) {
		RestartManager.setRestart(runing);
		for(CameraRunChangedPlugin plugin:getPlugins(CameraRunChangedPlugin.class)) {
			plugin.onRunChanged(runing);
		}
	}
	
	public boolean isRuning() {
		return isVideoRecording();
	}
	
	public void stopRun() {
		stopVideo();
	}
	
	public void startRun() {
		startVideo(CameraFile.generateVideoFilePath());
	}
	
	public void restartRun() {
		if(isRuning()) {
			stopRun();
		}
		startRun();
	}

	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		for(ServicePreviewCallBackPlugin plugin:getPlugins(ServicePreviewCallBackPlugin.class)) {
			plugin.onPreviewFrame(data, camera);
		}
	}
	
	public static interface ServicePreviewCallBackPlugin extends CameraBasePlugin{
		public void onPreviewFrame(byte[] data, Camera camera);
	}

	public void startPreview() {
		if(mSurfaceHolder!=null) {
			mCamera.setPreviewSurface(mSurfaceHolder);
		}else if(mSurface != null){
			mCamera.setPreviewTexture(mSurface);
		}
		mCamera.setDisplayOrientation(mCameraDisplayOrientation = getCameraDeviceDisplayOrientation());
		mCamera.startPreview();
	}
	
	public void stopPreview() {
		mCamera.stopPreview();
	}
	
	public void setSurfaceHolder(SurfaceHolder surfaceHolder) {
		this.mSurfaceHolder = surfaceHolder;
	}
	
	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		XbLog.i(Tag, "surfaceCreated");
		mSurfaceHolder = holder;
		if(mCamera.isAlive()) {
			previewCamera();
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		XbLog.i(Tag, "surfaceChanged");
		mSurfaceHolder = holder;
	}

	public int getCameraDeviceDisplayOrientation() {
		if (Build.VERSION.SDK_INT > 8) {
			return CameraUtil.determineDisplayOrientation(mWindowOrientation, XCamera.getCameraId());
		} else {
			return 90;
		}
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		XbLog.i(Tag, "surfaceDestroyed");
		mSurfaceHolder = null;
		stopPreview();
	}

	@Override
	public void onSurfaceCreated(SurfaceTexture texture) {
		mSurface = texture;
		if(mCamera.isAlive()) {
			previewCamera();
		}
	}

	@Override
	public void onSurfaceChanged(SurfaceTexture texture, int width, int height) {
		mSurface = texture;
	}

	@Override
	public void onSurfaceDestroyed(SurfaceTexture texture) {
		XbLog.i(Tag, "onSurfaceDestroyed");
		mSurfaceHolder = null;
		stopPreview();
	}
	
	public void restartPreview() {
		stopPreview();
		startPreview();
	}
	
	public void checkAndRestartVideo() {
		if(needRestart()) {
			restartRun();
		}
	}
	
	public boolean needRestart() {
		return !isRuning()&&RestartManager.needRestart();
	}
	
	@Override
	public void onPreviewChanged(Size size) {
		XbLog.i(Tag, "preview size:"+size.width+"x"+size.height);
		if(mCameraView!=null
				&&mCameraView instanceof CameraView) {
			float ratio = (float)size.width/(float)size.height;
			((CameraView)mCameraView).setRatio(ratio);
		}
	}
	
}
