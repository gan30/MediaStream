package com.xbcx.camera;

import com.xbcx.core.ActivityBasePlugin;

public interface CameraActivityPlugin extends ActivityBasePlugin{
	void onCameraOpend(XCamera camera);
	void onCameraClosed();
}
