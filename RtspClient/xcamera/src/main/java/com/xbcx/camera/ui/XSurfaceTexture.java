package com.xbcx.camera.ui;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;

public class XSurfaceTexture {
	
	int 			mTextureID;
	SurfaceTexture	mSurface;

	public XSurfaceTexture() {
		mTextureID = createTextureID();
		mSurface = new SurfaceTexture(mTextureID);
	}
	
	private int createTextureID(){  
        int[] texture = new int[1];  
  
        GLES20.glGenTextures(1, texture, 0);  
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, texture[0]);  
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,  
                GL10.GL_TEXTURE_MIN_FILTER,GL10.GL_LINEAR);          
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,  
                GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);  
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,  
                GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);  
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES,  
                GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);  
  
      //解除纹理绑定
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);
        
        return texture[0];  
    }
	
	public SurfaceTexture getSurface() {
		return mSurface;
	}
	
	public int getTextureID() {
		return mTextureID;
	}
	
	public void updateTexImage() {
		mSurface.updateTexImage();
	}
}
