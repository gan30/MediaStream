package com.xbcx.camera;

public interface LockListener{
	public boolean isLocked();
	public void setLock(long time);
	public boolean checkAndLocked(long time);
}
