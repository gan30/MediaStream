package com.xbcx.camera.video;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;

import com.xbcx.camera.CameraFile;
import com.xbcx.camera.CameraUtil;
import gan.camera.R;
import com.xbcx.camera.XCamera;
import com.xbcx.camera.settings.XCameraSettings;
import com.xbcx.core.ToastManager;
import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.media.MediaRecorder.OnErrorListener;
import android.media.MediaRecorder.OnInfoListener;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.TextureView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class VideoRecorder implements OnErrorListener, OnInfoListener,VideoEngine{
	
	final static String tag = "VideoRecorder";
	
	private Context							mContext;
	private MediaRecorder 					mMediaRecorder;
	private List<VideoRecoderListener> 		mOnVideoRecoderListeners;
	private CamcorderProfileCreator			mCamcorderProfileCreator;
	private CamcorderProfile 				mProfile;
	
	private OnPrepareListener				mOnPerpareListener;
	private OnFrameListener					mOnFrameListener;
	private OnInfoListener					mOnInfoListener;
	
	private Camera					mCamera;
	
	private String					mVideoFilename;
	private boolean				mMediaRecorderRecording;
	
	private List<Size>				mSupportedSizes;
	
	private ParcelFileDescriptor	mVideoFileDescriptor;
	private Uri						mCurrentUri;
	
	private long           		mRecordingStartTime;
	private int 					mOrientation = 90;
	private 	boolean				mCliping;
	private 	FileNumber			mFileNumber;
	
	public VideoRecorder() {
		this(XApplication.getApplication());
	}
	
	public VideoRecorder(Context context) {
		mContext = context;
	}

	@SuppressLint("NewApi")
	public void setCamera(Camera camera) {
		this.mCamera = camera;
		if(camera!=null){
			List<Size> videosize = camera.getParameters().getSupportedVideoSizes();
			if(videosize!=null){
				if(mSupportedSizes == null) {
					mSupportedSizes = new ArrayList<Size>();
				}
				mSupportedSizes.clear();
				mSupportedSizes.addAll(videosize);
				for(Size size : videosize){
					XbLog.i(tag, "videoSize :" +size.width+"x"+size.height);
				}
			}
		}
	}
	
	private void closeVideoFileDescriptor() {
        if (mVideoFileDescriptor != null) {
            try {
                mVideoFileDescriptor.close();
            } catch (IOException e) {
            }
            mVideoFileDescriptor = null;
        }
    }
	
	public Context getContext(){
		return mContext;
	}
	
	public List<Size> getSupportedSizes(){
		return mSupportedSizes;
	}
	
	public void release(){
		if(mOnVideoRecoderListeners!=null) {
			mOnVideoRecoderListeners.clear();
		}
	}
	
	public void toggleRecord(){
		if(mMediaRecorderRecording){
			stopVideo();
		}else{
			startVideo(mVideoFilename);
		}
	}
	
	public static String newFile(){
		File file = new File(Environment.getExternalStorageDirectory().toString()+"/xbcx/Camera");
		if(!file.exists()){
			file.mkdirs();
		}
		return file.getAbsolutePath()+"/"+ newFileName(); 
	}
	
	public static String newFileName() {
		return "VID_"+CameraFile.formatFile(".mp4");
	}
	
	public boolean startVideo(){
		return startVideo(null);
	}
	
	public boolean startVideo(String filePath){
		if(mCamera == null) {
			setCamera(XCamera.get().getCamera());
		}
		if(mCamera == null){
			throw new NullPointerException("camera disallow null");
		}
		if(mMediaRecorderRecording){
			return false;
		}
		
		try {
			initRecoder();
			
			long requestedSizeLimit = 0;
			int duration = 0;
			if (mCamcorderProfileCreator == null) {
				mCamcorderProfileCreator = new SimpleCamcorderProfileCreator();
			}
			int videoWidth = 640;
			int videoHeight = 480;
			int outputFormat = MediaRecorder.OutputFormat.MPEG_4;
			mProfile = mCamcorderProfileCreator.onCreateCamcorderProfile(mContext, this);
			if(mProfile!=null) {
				mMediaRecorder.setProfile(mProfile);
				videoWidth = mProfile.videoFrameWidth;
				videoHeight = mProfile.videoFrameHeight;
			}else {
				mMediaRecorder.setOutputFormat(outputFormat);
				mMediaRecorder.setVideoFrameRate(30);
				String videoSize = XCameraSettings.getVideoSize();
				if(XCamera.isSupportedSize(videoSize, mSupportedSizes)) {
					int[] size = CameraUtil.parseSize(videoSize);
					videoWidth = size[0];
					videoHeight = size[1];
				}else {
					if(!XCamera.isSupportedSize(640, 480, mSupportedSizes)) {
						Size size = mSupportedSizes.get(0);
						videoWidth = size.width;
						videoHeight = size.height;
					}
				}
			}
			try {
				//x1没有1088
				if(videoWidth == 1088) {
					videoHeight = 1080;
				}
				mMediaRecorder.setVideoSize(videoWidth, videoHeight);
			} catch (Exception e) {
				e.printStackTrace();//手机mSupportedSizes 列表支持的不一定能设置成功
				mMediaRecorder.setVideoSize(1920, 1080);
			}
			
			if(mVideoFileDescriptor!=null){
				mMediaRecorder.setOutputFile(mVideoFileDescriptor.getFileDescriptor());
			}else{
				if(TextUtils.isEmpty(filePath)){
					filePath = newFile();
				}
				if(CameraUtil.isAvailableFile(filePath)){
					mMediaRecorder.setOutputFile(filePath);
					mVideoFilename = filePath;
				}else{
					onRecordError(1);
					ToastManager.getInstance(mContext).show(R.string.camera_file_fail);
					return false;
				}
			}
			if(requestedSizeLimit>0){
				mMediaRecorder.setMaxFileSize(requestedSizeLimit);
			}
			if(duration>0){
				mMediaRecorder.setMaxDuration(duration*1000);
			}
			
			try {
				prepare();
				mMediaRecorder.prepare();
			} catch (Exception e) {
				e.printStackTrace();
				XbLog.i(tag, "start e:"+e.getMessage());
				onRecordError(1);
				throw new RuntimeException("prepare fail");
			}
			
			pauseAudioPlayback();
			mMediaRecorder.start();
			mMediaRecorder.setOnErrorListener(this);
			
			try {
				Class<?> OnFrameListener = Class.forName("android.media.MediaRecorder$OnFrameListener");
				MediaRecorder.OnInfoListener listener = (MediaRecorder.OnInfoListener) Proxy.newProxyInstance(OnFrameListener.getClassLoader(),   
						new Class[] { OnFrameListener }, new FrameListenerDynamicProxy(this));  
				mMediaRecorder.setOnInfoListener(listener);
			} catch (Exception e) {
				mMediaRecorder.setOnInfoListener(this);
			}
			
			mRecordingStartTime = SystemClock.uptimeMillis();
			mMediaRecorderRecording = true;
			onRecordStart();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			XbLog.i(tag, "start e:"+e.getMessage());
			onRecordError(0);
			Toast.makeText(mContext, "record failed", Toast.LENGTH_SHORT).show();
			return false;
		}
	}
	
	public synchronized boolean clipVideo() {
		mCliping = true;
		if(isVideoRecording()) {
			stopVideo();
		}
		boolean clipResult = startVideo(mFileNumber.nextFilePath());
		mCliping = false;
		return clipResult;
	}
	
	protected void onRecordStart() {
		if(mCliping) {
			return;
		}
		mFileNumber = new FileNumber(mVideoFilename);
		if(mOnVideoRecoderListeners!=null){
			for(VideoRecoderListener listener: mOnVideoRecoderListeners){
				listener.onRecordStart(this);
			}
		}
	}
	
	protected void onRecordEnd(String file) {
		CameraUtil.insertVideoToMediaStore(mContext, file);
		if(mCliping) {
			return;
		}
		if(mOnVideoRecoderListeners!=null){
			for(VideoRecoderListener listener: mOnVideoRecoderListeners){
				listener.onRecordEnd(this);
			}
		}
	}
	
	public CamcorderProfile getProfile() {
		return mProfile;
	}
	
	protected void prepare(){
		XbLog.d(tag, "prepare");
		if(mOnPerpareListener!=null){
			mOnPerpareListener.onPrepare(this);
		}
	}
	
	private void pauseAudioPlayback() {
        AudioManager am = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
        am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }
	
	protected void onRecordError(int error){
		XbLog.d(tag, "error:"+error);
		cleanupEmptyFile();
		if(mOnVideoRecoderListeners!=null){
			for(VideoRecoderListener listener: mOnVideoRecoderListeners){
				listener.onRecordError(this, error);
			}
		}
	}
	
	public long getVideoTime(){
		long now = SystemClock.uptimeMillis();
		return now - mRecordingStartTime;
	}
	
	public long getVideoStartTime() {
		return mRecordingStartTime;
	}
	
	public boolean stopVideo(){
		return stopVideo(false);
	}
	
	public boolean stopVideo(boolean cancel){
		boolean fail = true;
		try {
			mMediaRecorderRecording = false;
			mMediaRecorder.setOnInfoListener(null);
			mMediaRecorder.setOnErrorListener(null);
			mMediaRecorder.stop();
			onStopVideoRecording(cancel);
		} catch (Exception e) {
			e.printStackTrace();
			fail = false;
		}
        return fail;
	}
	
	@SuppressLint({ "InlinedApi", "NewApi" })
	private void initRecoder() throws Exception{
		if(mMediaRecorder == null){
			mMediaRecorder = new MediaRecorder();
		}else{
			mMediaRecorder.reset();
		}
		
		try {
			mCamera.unlock();
			mMediaRecorder.setCamera(mCamera);
			mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
			mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
			mMediaRecorder.setOrientationHint(mOrientation);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public void setRotation(int degrees){
		mOrientation = degrees;
	}
	
	public Camera getCamera(){
		return mCamera;
	}
	
	private void releaseMediaRecorder() {
        if (mMediaRecorder != null) {
            mMediaRecorder.release();
            mMediaRecorder = null;
        }
        mVideoFilename = null;
    }
	
	private void cleanupEmptyFile() {
        if (mVideoFilename != null) {
            File f = new File(mVideoFilename);
            if (f.length() == 0 && f.delete()) {
                mVideoFilename = null;
            }
        }
    }
	
	public String getVideoFile(){
		return mVideoFilename!=null? mVideoFilename:
			mCurrentUri!=null? mCurrentUri.getPath():null;
	}
	
	@Override
	public boolean isVideoRecording() {
		return mMediaRecorderRecording;
	}
	
	public VideoRecorder setCamcorderProfileCreator(CamcorderProfileCreator camcorderProfileCreator) {
		this.mCamcorderProfileCreator = camcorderProfileCreator;
		return this;
	}
	
	public static class MediaRecoderConfig{
		public TextureView		mSurfaceView;
	}
	
	public static class MediaRecoderConfigBuilder{
		
		MediaRecoderConfig	mMediaRecoderConfig;
		
		public MediaRecoderConfigBuilder() {
			mMediaRecoderConfig = new MediaRecoderConfig();
		}
		
		public MediaRecoderConfigBuilder setSurfaceView(TextureView surfaceView){
			mMediaRecoderConfig.mSurfaceView = surfaceView;
			return this;
		}
		
		public MediaRecoderConfig build(){
			return mMediaRecoderConfig;
		}
	}
	
	public static interface MediaRecoderConfigCreator{
		public MediaRecoderConfig onCreateMediaRecoderConfig();
	}

	public static interface CamcorderProfileCreator{
		public CamcorderProfile onCreateCamcorderProfile(Context context, VideoRecorder recorder);
	}
	
	@Override
	public void onInfo(MediaRecorder mr, int what, int extra) {
		XbLog.i(tag, "info"+"what:"+what+",extra:"+extra);
		if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) {
			stopVideo(false);
		} else if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED) {
			stopVideo(false);
		}
		
		if(mOnInfoListener!=null) {
			mOnInfoListener.onInfo(this, mr, what, extra);
		}
	}
	
	public void onFrame(MediaRecorder mr, byte[] data, int type) {
		XbLog.i(tag, "onFrame data length:"+data.length+",type:" + type);
		if(mOnFrameListener!=null) {
			mOnFrameListener.onFrame(this, mr, data, type);
		}
	}

	@Override
	public void onError(MediaRecorder mr, int what, int extra) {
		XbLog.i(tag, "onError"+"what:"+what+",extra:"+extra);
		if (what == MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN) {
			stopVideo(true);
	    }
	}
	
	private void onStopVideoRecording(boolean cancel) {
		closeVideoFileDescriptor();
		cleanupEmptyFile();
		if(!TextUtils.isEmpty(mVideoFilename)){
			if(cancel){
				deleteVideoFile(mVideoFilename);
			}
		}
		mMediaRecorderRecording = false;
		onRecordEnd(mVideoFilename);
	}
	
	private void deleteVideoFile(String fileName) {
        File f = new File(fileName);
        if (!f.delete()) {
        	CameraUtil.deleteVideoToMiastore(getContext(), fileName);
        }
    }
	
	public void takePiture(OnTakePictureCallBack callBack){
//		if(mMetadataRetriever == null){
//			mMetadataRetriever = new MediaMetadataRetriever();
//		}
//		mMetadataRetriever.setDataSource(getVideoFile());
//		callBack.onTakePictureEnd(mMetadataRetriever.getFrameAtTime(getRecordTime()));
	}
	
	public Intent getReslutIntent(){
		Intent resultIntent = new Intent();
		if (mCurrentUri == null) {
			try {
				mCurrentUri = Uri.fromFile(new File(mVideoFilename));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		resultIntent.setData(mCurrentUri);
		return resultIntent;
	}
	
	public static class SimpleCamcorderProfileCreator implements CamcorderProfileCreator{

		@Override
		public CamcorderProfile onCreateCamcorderProfile(Context context,VideoRecorder recorder) {
			CamcorderProfile profile = null;
			try {
				profile = CamcorderProfile.get(getQualitformPrefrences(recorder.getContext()));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
			
			try {
				final String frame = XCameraSettings.readStringSetting("video_frame", XCameraSettings.VideoFrame_Def);
				profile.videoFrameRate = Integer.valueOf(frame);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			try {
				final String bitRate = XCameraSettings.readStringSetting("video_bitrate", XCameraSettings.Rate_Def);
				profile.videoBitRate = profile.videoFrameWidth*profile.videoFrameHeight*Integer.valueOf(bitRate);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return profile;
		}
	}
	
	@SuppressLint({ "InlinedApi", "NewApi" })
	public static int getQualitformPrefrences(Context context){
		final String value = XCameraSettings.getVideoSize();
		int[] size = CameraUtil.parseSize(value);
		int quality = CamcorderProfile.QUALITY_HIGH;
		if(1080 == size[1]){
			quality = CamcorderProfile.QUALITY_1080P;
		}else if(720 == size[1]){
			quality = CamcorderProfile.QUALITY_720P;
		}
		if(XCamera.getCameraId()<2) {
			if (CamcorderProfile.hasProfile(XCamera.getCameraId(), quality) == false) {
				quality = CamcorderProfile.QUALITY_480P;
			}
		}
		return quality;
	}
	
	public void setOnPerpareListener(OnPrepareListener onPerpareListener) {
		this.mOnPerpareListener = onPerpareListener;
	}
	
	public void setOnFrameListener(OnFrameListener onFrameListener) {
		this.mOnFrameListener = onFrameListener;
	}
	
	public static interface OnTakePictureCallBack{
		public void onTakePictureEnd(Bitmap bitmap);
	}
	
	public static interface OnPrepareListener{
		public void onPrepare(VideoRecorder recorder);
	}
	
	public static interface OnInfoListener{
		public void onInfo(VideoRecorder recorder, MediaRecorder mr, int what, int extra);
	}
	
	public static interface OnFrameListener{
		public void onFrame(VideoRecorder recorder, MediaRecorder mr, byte[] data, int type);
	}
	
	public void closeWarn(){
		AudioManager audioManager = (AudioManager) XApplication.getApplication().getSystemService(Context.AUDIO_SERVICE);
	    audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
	    audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
	    audioManager.setStreamVolume(AudioManager.STREAM_ALARM, 0, 0);
	    audioManager.setStreamVolume(AudioManager.STREAM_DTMF, 0, 0);
	    audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, 0, 0);
	    audioManager.setStreamVolume(AudioManager.STREAM_RING, 0, 0);
	}
	
	public void resumeWarn(){
		
	}
	
	private static class FrameListenerDynamicProxy implements InvocationHandler{

		final static String TAG = "FrameListenerDynamicProxy";
		
		VideoRecorder	mRecorder;
		
		public FrameListenerDynamicProxy(VideoRecorder recorder) {
			mRecorder = recorder;
		}
		
		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
	        try {  
	            XbLog.i(TAG, "invoke, method: " + method.getName());  
	            if("onFrame".equals(method.getName())) {  
	            	MediaRecorder mr = (MediaRecorder)args[0];
	            	byte[] data = (byte[])args[1];
	            	int type = (int)args[2];
	                mRecorder.onFrame(mr, data, type);
	            }else if("onInfo".equals(method.getName())){
	            	MediaRecorder mr = (MediaRecorder)args[0];
	            	int what = (int)args[1];
	            	int extra = (int)args[2];
	                mRecorder.onInfo(mr, what, extra);
	            }
	        } catch (Exception e) {  
	        	e.printStackTrace();
	        }  
	        return proxy;  
		}
	}

	@Override
	public void addVideoRecoderListener(VideoRecoderListener listener) {
		if(mOnVideoRecoderListeners == null){
			mOnVideoRecoderListeners = new ArrayList<>();
		}
		mOnVideoRecoderListeners.add(listener);
	}

	@Override
	public void removeVideoRecoderListener(VideoRecoderListener listener) {
		if(mOnVideoRecoderListeners!=null) {
			mOnVideoRecoderListeners.remove(listener);
		}
	}
}
