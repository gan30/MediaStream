package com.xbcx.camera.video;

public interface VideoRecoderListener{
	public void onRecordStart(VideoEngine recoder);
	public void onRecordEnd(VideoEngine recoder);
	public void onRecordError(VideoEngine recoder, int error);
}
