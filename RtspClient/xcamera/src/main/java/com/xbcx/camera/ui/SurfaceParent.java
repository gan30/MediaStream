package com.xbcx.camera.ui;

import gan.camera.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class SurfaceParent extends RelativeLayout{
	
	View mSurfaceView;
	View mCoverView;

	public SurfaceParent(Context context) {
		super(context);
	}
	
	public SurfaceParent(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public SurfaceParent(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public View getSurface(){
		if(mSurfaceView == null){
			return mSurfaceView = findViewById(R.id.surface);
		}
		return mSurfaceView;
	}
	
	public void pause(){
		removeView(getSurface());
	}
	
	public void resume(){
		View child = getSurface();
		if(child.getParent() == null){
			addView(child, 0);
		}
	}
}
