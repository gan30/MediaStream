package com.xbcx.camera.video;

import com.xbcx.core.SharedPreferenceDefine;

public class RestartManager {

	public static void setRestart(boolean restart) {
		SharedPreferenceDefine.setBooleanValue("video_run", restart);
	}
	
	public static void reset() {
		setRestart(false);
	}
	
	public static boolean needRestart() {
		return SharedPreferenceDefine.getBooleanValue("video_run", false);
	}
	
	public static void setRestart(String key,boolean restart) {
		SharedPreferenceDefine.setBooleanValue(key, restart);
	}
	
	public static boolean isRestart(String key) {
		return SharedPreferenceDefine.getBooleanValue(key, false);
	}
	
}
