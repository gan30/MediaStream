package com.xbcx.camera;

import com.xbcx.utils.SystemUtils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.RelativeLayout;

import gan.camera.R;

public class CameraWindowManager implements OnClickListener {
	
	Context		  mContext;
	WindowManager mWindowManager;
	View	      mCameraView;
	View		  mRootView;
	boolean 	  mIsShown;
	
	int 		  mWidth,mHeight;
	
	public CameraWindowManager(Context context) {
		mContext = context;
		mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
	}
	
	public void setCameraView(View cameraView) {
		this.mCameraView = cameraView;
	}

	public void windowTop(int width,int height) {
		LayoutParams params = new LayoutParams();
		params.type = LayoutParams.TYPE_TOAST;
		int flags = LayoutParams.FLAG_ALT_FOCUSABLE_IM | LayoutParams.FLAG_NOT_FOCUSABLE
				| LayoutParams.FLAG_NOT_TOUCH_MODAL;
		
		params.flags = flags;
		params.format = PixelFormat.TRANSLUCENT;
		params.width = width;
		params.height = height;
		params.gravity = Gravity.TOP|Gravity.START;
		window(width, height, params);
	}
	
	public void windowBottom(int width,int height) {
		LayoutParams params = new LayoutParams();
		params.type = LayoutParams.TYPE_TOAST;
		int flags = LayoutParams.FLAG_ALT_FOCUSABLE_IM | LayoutParams.FLAG_NOT_FOCUSABLE
				| LayoutParams.FLAG_NOT_TOUCH_MODAL;
		
		params.flags = flags;
		params.format = PixelFormat.TRANSLUCENT;
		params.width = width;
		params.height = height;
		params.gravity = Gravity.BOTTOM|Gravity.START;
		window(width, height, params);
	}
	
	public void window(int width,int height,LayoutParams params) {
		if(mCameraView == null) {
			throw new NullPointerException("surfaceView is null");
		}
		if(mRootView == null) {	
			mRootView = SystemUtils.inflate(mContext, R.layout.layout_window);
		}
		
		ViewGroup parent = (ViewGroup) mCameraView.getParent();
		if (parent != null) {
			parent.removeView(mCameraView);
		}
		final RelativeLayout layoutCamera = (RelativeLayout) mRootView.findViewById(R.id.layoutCamera);
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		layoutCamera.addView(mCameraView, lp);
		mWindowManager.addView(mRootView, params);
	}

	public void dismiss() {
		if (mWindowManager != null) {
			try {
				mWindowManager.removeView(mRootView);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void dismissCamera() {
		View layoutCamera = mRootView.findViewById(R.id.layoutCamera);
		ViewGroup.LayoutParams lp = layoutCamera.getLayoutParams();
		lp.width = 1;
		lp.height = 1;
		layoutCamera.setLayoutParams(lp);
	}
	
	public void showCamera() {
		View layoutCamera = mRootView.findViewById(R.id.layoutCamera);
		ViewGroup.LayoutParams lp = layoutCamera.getLayoutParams();
		lp.width = mWidth;
		lp.height = mHeight;
		layoutCamera.setLayoutParams(lp);
	}
	
	public void reshowWindow() {
		windowTop(mWidth, mHeight);
	}
	
	public void showWindow(int width,int height) {
		mWidth = width;
		mHeight = height;
		windowTop(width, height);
	}
	
	@Override
	public void onClick(View v) {
	}
}
