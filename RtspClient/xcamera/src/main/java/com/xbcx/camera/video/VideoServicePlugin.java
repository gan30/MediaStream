package com.xbcx.camera.video;

import com.xbcx.camera.CameraService;
import com.xbcx.camera.CameraService.CameraListenerPlugin;
import com.xbcx.camera.CameraService.OnBroadcastReceiverPlugin;
import com.xbcx.camera.CameraServicePlugin;

import android.content.Intent;
import android.hardware.Camera;

public class VideoServicePlugin implements CameraServicePlugin<CameraService>,OnBroadcastReceiverPlugin,CameraListenerPlugin,VideoEngine, VideoRecoderListener{
	
	VideoRecorder 		mVideoRecorder;
	CameraService		mCameraService;
	OnPrepareListener	mOnPrepareListener;
	
	@Override
	public void onAttachService(CameraService service) {
		mCameraService = service;
		mVideoRecorder = new VideoRecorder(service);
		mVideoRecorder.addVideoRecoderListener(this);
	}

	public VideoRecorder getVideoRecorder() {
		return mVideoRecorder;
	}
	
	protected void onExceptionExit(){
		if(isVideoRecording()){
			stopVideo();
		}
	}
	
	@Override
	public void onServiceDestory() {
		onExceptionExit();
		release();
	}
	
	@Override
	public boolean clipVideo() {
		if(mVideoRecorder!=null) {
			return mVideoRecorder.clipVideo();
		}
		return false;
	}
	
	@Override
	public boolean startVideo(String filePath) {
		if(mOnPrepareListener!=null) {
			mOnPrepareListener.onPrepare(this);
		}
		return mVideoRecorder.startVideo(filePath);
	}
	
	@Override
	public boolean stopVideo() {
		return mVideoRecorder.stopVideo();
	}
	
	@Override
	public boolean isVideoRecording() {
		if(mVideoRecorder!=null) {
			return mVideoRecorder.isVideoRecording();
		}
		return false;
	}
	
	@Override
	public long getVideoStartTime() {
		if(mVideoRecorder!=null) {
			return mVideoRecorder.getVideoStartTime();
		}
		return 0;
	}

	@Override
	public void onBroadcastReceive(CameraService service, Intent intent) {
		final String action = intent.getAction();
		if(CameraService.ACTION_SHUTDOWN.equals(action)) {
			onExceptionExit();
		}
	}

	@Override
	public void onCameraOpend(Camera camera) {
		if(mVideoRecorder!=null) {
			mVideoRecorder.setCamera(camera);
		}
	}

	@Override
	public void onCameraClosed() {
		onExceptionExit();
		if(mVideoRecorder!=null) {
			mVideoRecorder.setCamera(null);
		}
	}

	@Override
	public void setRotation(int orientation) {
		if(mVideoRecorder!=null) {
			mVideoRecorder.setRotation(orientation);
		}
	}

	@Override
	public String getVideoFile() {
		if(mVideoRecorder!=null) {
			return mVideoRecorder.getVideoFile();
		}
		return null;
	}

	@Override
	public void addVideoRecoderListener(VideoRecoderListener listener) {
		if(mVideoRecorder!=null) {
			mVideoRecorder.addVideoRecoderListener(listener);
		}
	}

	@Override
	public void removeVideoRecoderListener(VideoRecoderListener listener) {
		if(mVideoRecorder!=null) {
			mVideoRecorder.removeVideoRecoderListener(listener);
		}
	}

	@Override
	public void setCamera(Camera camera) {
		if(mVideoRecorder!=null) {
			mVideoRecorder.setCamera(camera);
		}
	}

	@Override
	public void release() {
		if(mVideoRecorder!=null) {
			mVideoRecorder.removeVideoRecoderListener(this);
			mVideoRecorder.release();
			mVideoRecorder = null;
		}
	}

	@Override
	public void onRecordStart(VideoEngine recoder) {
		for(VideoRecoderListenerPlugin plugin: mCameraService.getPlugins(VideoRecoderListenerPlugin.class)) {
			plugin.onRecordStart(recoder);
		}
	}

	@Override
	public void onRecordEnd(VideoEngine recoder) {
		for(VideoRecoderListenerPlugin plugin: mCameraService.getPlugins(VideoRecoderListenerPlugin.class)) {
			plugin.onRecordEnd(recoder);
		}
	}

	@Override
	public void onRecordError(VideoEngine recoder, int error) {
		for(VideoRecoderListenerPlugin plugin: mCameraService.getPlugins(VideoRecoderListenerPlugin.class)) {
			plugin.onRecordError(recoder, error);
		}
	}
	
	public VideoServicePlugin setOnPrepareListener(OnPrepareListener onPrepareListener) {
		this.mOnPrepareListener = onPrepareListener;
		return this;
	}
	
	public static interface OnPrepareListener{
		public void onPrepare(VideoServicePlugin plugin);
	}
}
