package com.xbcx.camera;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.xbcx.camera.settings.XCameraSettings;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import gan.camera.R;

public class PictureCamera extends ActivityPlugin<BaseActivity> implements OnClickListener, ShutterCallback,CameraActivityPlugin{

	public final static String tag = "PictureCamera";
	
	View			mPicture;
	XCamera			mCamera = XCamera.get();
	Uri 			mOutFile;
	
	private byte[] 	mJpegImageData;//
	private View	mChooseView;
	
	OnThumbResultListener		mOnThumbResultListener;
	PictureFileProvider			mPictureFileProvider;
	OnTakeDataCallBackListener	mOnTakeDataCallBackListener;
	OnChooseViewListener		mOnChooseViewListener;
	ChooseViewHelper			mChooseViewHelper;
	
	CameraPictureTakenIntercepter	mCameraPictureTakenIntercepter;
	CameraPictureTakenListener		mCameraPictureTakenListener;
	OnInterceptTakePictureListener	mOnInterceptTakePictureListener;
	OnPerpareTakePictureListener	mOnPerpareTakePictureListener;
	
	private boolean	mCanTake 	= true;
	
	public PictureCamera setOnPerpareTakePictureListener(OnPerpareTakePictureListener onPerpareTakePictureListener) {
		this.mOnPerpareTakePictureListener = onPerpareTakePictureListener;
		return this;
	}
	
	public PictureCamera setOnChooseViewListener(OnChooseViewListener onChooseViewListener) {
		this.mOnChooseViewListener = onChooseViewListener;
		return this;
	}
	
	public PictureCamera setIvPicture(View picture) {
		this.mPicture = picture;
		mPicture.setOnClickListener(this);
		return this;
	}
	
	@Override
	protected void onAttachActivity(BaseActivity activity) {
		super.onAttachActivity(activity);
		View view = activity.findViewById(R.id.picture);
		if(view!=null){
			setIvPicture((ImageView)view);
		}
		
		if(CameraUtil.isPickImage(mActivity)){
			Bundle myExtras = mActivity.getIntent().getExtras();
			if (myExtras != null) {
				mOutFile = (Uri) myExtras.getParcelable(MediaStore.EXTRA_OUTPUT);
			}
		}
	}
	
	public PictureCamera setOnThumbResultListener(OnThumbResultListener onThumbResultListener) {
		this.mOnThumbResultListener = onThumbResultListener;
		return this;
	}
	
	public PictureCamera setOnTakeDataCallBackListener(OnTakeDataCallBackListener onTakeDataCallBackListener) {
		this.mOnTakeDataCallBackListener = onTakeDataCallBackListener;
		return this;
	}
	
	public PictureCamera setPictureFileProvider(PictureFileProvider pictureFileProvider) {
		this.mPictureFileProvider = pictureFileProvider;
		return this;
	}
	
	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if(mPicture == v){
			if(CameraUtil.isPickImage(mActivity)){
				takePicturePick();
			}else{
				takePicture();
			}
		}else if(id == R.id.cancel){
			mActivity.finish();
		}else if(id == R.id.done){
			hanldePick(mJpegImageData, mOutFile);
		}
	}
	
	public synchronized void takePicturePick(){
		takePicturePick(true);
	}
	
	@SuppressWarnings("deprecation")
	public synchronized void takePicturePick(final boolean releaseCamera){
		try {
			if(mOnInterceptTakePictureListener!=null){
				if(mOnInterceptTakePictureListener.onIntercepTakePicture(this)){
					return;
				}
			}
			if(mCamera.isClosed()){
				return;
			}
			prepare();
			XbLog.i(tag, "takePicture start");
			mCamera.takePicture(this, null, null, new PictureCallback() {
				@Override
				public void onPictureTaken(byte[] data, Camera camera) {
					mJpegImageData = data;
					XApplication.getMainThreadHandler().post(new Runnable() {
						@Override
						public void run() {
							try {
								showChooseView(BitmapFactory.decodeByteArray(mJpegImageData, 0, mJpegImageData.length));
							} catch (OutOfMemoryError e) {
								e.printStackTrace();
								showChooseView(null);
							}
							if(releaseCamera){
								mActivity.postDelayed(new Runnable() {
									@Override
									public void run() {
										XCamera.get().close();
									}
								}, 200);
							}
						}
					});
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public byte[] getJpegImageData() {
		return mJpegImageData;
	}
	
	protected void prepare(){
		int rotation = CameraOrientationManager.get().getRotation();
		XbLog.i(tag, "rotation:"+rotation);
		@SuppressWarnings("deprecation")
		Parameters parameters = mCamera.getParameters();
		parameters.setRotation(rotation);
		parameters.setPictureFormat(ImageFormat.JPEG);
		mCamera.setParameters(parameters);
		
		if(XCamera.isXCamera()) {
			mCamera.setLongshot(true);
		}
		
		@SuppressWarnings("deprecation")
		final Size size = parameters.getPictureSize();
		XbLog.i(tag, "pictrueSize:"+size.width+"x"+size.height);
		if(mOnPerpareTakePictureListener!=null){
			mOnPerpareTakePictureListener.onPerpareTakePicture(this);
		}
	}
	
	public XCamera getCamera() {
		return mCamera;
	}
	
	public synchronized void takeData(final OnTakeDataCallBackListener listener){
		try {
			if(mCamera.isClosed()){
				return;
			}
			prepare();
			XbLog.i(tag, "takePicture start");
			mCamera.takePicture(this, null, null, new PictureCallback() {
				@Override
				public void onPictureTaken(final byte[] data, Camera camera) {
					XApplication.getMainThreadHandler().post(new Runnable() {
						@Override
						public void run() {
							listener.onTakeData(data);
						}
					});
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			XbLog.i(tag, "takeData e:"+e.getMessage());
		}
	}
	
	protected void onTakePictureStart(){
		if(mCameraPictureTakenListener!=null){
			mCameraPictureTakenListener.onTakePictureStart();
		}
	}
	
	protected void onTakePictureEnd(){
		XbLog.i(tag, "onTakePictureEnd");
		if(mCameraPictureTakenListener!=null){
			mCameraPictureTakenListener.onTakePictureEnd();
		}
	}
	
	public synchronized void takePicture(){
		if(canTake()){
			if(mPictureFileProvider!=null){
				takePicture(mPictureFileProvider.getPictureFilePath(mActivity));
			}else{
				takePicture(CameraFile.generatePictureFilePath());
			}
		}
	}
	
	protected boolean canTake() {
		return mCamera!=null&&mCamera.isAlive()&&mCanTake;
	}
	
	private final synchronized void takePicture(final String filePath){
		if(mOnInterceptTakePictureListener!=null){
			if(mOnInterceptTakePictureListener.onIntercepTakePicture(this)){
				return;
			}
		}
		if(filePath == null){
			throw new NullPointerException("filePath must not null");
		}
		
		prepare();
		XbLog.i(tag, "takePicture start");
		mCamera.takePicture(PictureCamera.this, null, null, 
				new JpegPictureCallback(filePath));
		mCanTake = false;
		onTakePictureStart();
	}
	
	protected void onThumbResult(Bitmap result,String filePath){
		XbLog.i(tag, "result:"+(result==null));
		if(mOnThumbResultListener!=null){
			mOnThumbResultListener.onThumbResult(result,filePath);
		}
	}
	
	public void resumePreview(){
		mCamera.startPreview();
		mCanTake = true;
	}
	
	public boolean canTakePicture(){
		return mCamera!=null;
	}
	
	protected void onPictureComplete(String filePath){
		CameraUtil.checkEmptyFile(filePath);
		if(mActivity == null) {
			return;
		}
		for(OnPictureCompletePlugin plugin:mActivity.getPlugins(OnPictureCompletePlugin.class)){
			plugin.onPictureComplete(this, filePath);
		}
	}
	
	public static interface PictureFileProvider{
		public String getPictureFilePath(Activity activity);
	}
	
	public static interface OnPictureCompletePlugin extends ActivityBasePlugin{
		public void onPictureComplete(PictureCamera camera, String filePath);
	}
	
	HashMap<String, ThreadPoolExecutor> mPictureCache = new HashMap<String, ThreadPoolExecutor>();
	Vector<String> task = new Vector<String>();
	
	public void asyDirectSaveBitmap(final byte[] jpeg, final String filePath) {
		final String pictureSize = XCameraSettings.getPictureSize();
		ThreadPoolExecutor threadPoolExecutor= mPictureCache.get(pictureSize);
		if(threadPoolExecutor == null) {
			int corePoolSize = 3;
			int maximumPoolSize = 3;
			if(XCameraSettings.Pixel_800w.equals(pictureSize)) {
				corePoolSize = 2;
				maximumPoolSize = 2;
			}else if(XCameraSettings.Pixel_1600w.equals(pictureSize)) {
				corePoolSize = 0;
				maximumPoolSize = 1;
			}
			threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 60, TimeUnit.SECONDS, 
					new LinkedBlockingQueue<Runnable>());
			mPictureCache.put(pictureSize, threadPoolExecutor);
		}
		threadPoolExecutor.execute(new JpegRunnable(jpeg, filePath,mCameraPictureTakenIntercepter));
	}
	
	private class JpegRunnable implements Runnable{
		byte[] jpeg;
		String mFilePath;
		CameraPictureTakenIntercepter	mCameraPictureTakenIntercepter;
		
		public JpegRunnable(byte[] jpeg,String filePath,CameraPictureTakenIntercepter cameraPictureTakenIntercepter) {
			this.jpeg = jpeg;
			mFilePath = filePath;
			mCameraPictureTakenIntercepter = cameraPictureTakenIntercepter;
		}
		
		@Override
		public void run() {
			Object src = jpeg;
			if (mCameraPictureTakenIntercepter != null) {
				src = mCameraPictureTakenIntercepter.onIntercepterTakeData(jpeg, mFilePath);
			}
			if(src != null) {
				if(src instanceof Bitmap){
					CameraUtil.saveBitmapToFile(mFilePath, ((Bitmap)src), 100);
					((Bitmap)src).recycle();
					src = null;
					System.gc();
				}else {
					CameraUtil.addImage((byte[]) src, mFilePath);
				}
			}
			if(CameraUtil.checkEmptyFile(mFilePath)) {
				CameraUtil.insertImageToMediaStore(mFilePath, 0);
			}
			task.remove(mFilePath);
			XApplication.getMainThreadHandler().post(new Runnable() {
				@Override
				public void run() {
					onPictureComplete(mFilePath);
				}
			});
		}
	}
	
	public boolean hasPictureSaving(){
		return task.size()>0;
	}

	@SuppressLint("NewApi")
	public void asyShowThumbBitmap(byte[] jpeg, final String path) {
		AsyncTask<Object, Void, Bitmap> save = new AsyncTask<Object, Void, Bitmap>() {
			@Override
			protected Bitmap doInBackground(Object... params) {
				return CameraUtil.getPictureThumb((byte[]) params[0]);
			}

			@Override
			protected void onPostExecute(Bitmap result) {
				if(result!=null) {
					onThumbResult(result,path);
				}
			}
		};
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			save.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jpeg);
		}else {
			save.execute(jpeg);
		}
	}
	
	private class JpegPictureCallback implements PictureCallback{

		String mFilePath;
		
		public JpegPictureCallback(String filePath) {
			mFilePath = filePath;
		}
		
		@Override
		public void onPictureTaken(byte[] jpegData, @SuppressWarnings("deprecation") Camera camera) {
			if(jpegData!=null
					&&CameraUtil.isAvailableFile(mFilePath)){
				task.add(mFilePath);
				XbLog.i(tag,"onPictureTaken file:"+ mFilePath);
				asyShowThumbBitmap(jpegData, mFilePath);
				asyDirectSaveBitmap(jpegData, mFilePath);
			}
			mActivity.post(new Runnable() {
				@Override
				public void run() {
					resumePreview();
					onTakePictureEnd();
				}
			});
		}
	}

	public static interface CameraPictureTakenIntercepter {
		public Object onIntercepterTakeData(final byte[] jpegData, String filePath);
	}

	public PictureCamera setCameraPictureTakenIntercepter(CameraPictureTakenIntercepter cameraPictureTakenIntercepter) {
		this.mCameraPictureTakenIntercepter = cameraPictureTakenIntercepter;
		return this;
	}
	
	@Override
	public void onShutter() {
		
	}
	
	public void showChooseView(Bitmap bitmap){
		if(mChooseViewHelper == null){
			mChooseViewHelper = new ChooseViewHelper(mActivity);
		}
		mChooseView = mChooseViewHelper.showChooseView(bitmap,false);
		mChooseView.findViewById(R.id.cancel).setOnClickListener(this);
		mChooseView.findViewById(R.id.done).setOnClickListener(this);
		if(mOnChooseViewListener!=null){
			mOnChooseViewListener.onChooseViewVisiableChanged(true);
		}
	}
	
	public void hideChooseView(){
		if(mChooseViewHelper!=null){
			mChooseViewHelper.hideChooseView();
		}
		if(mOnChooseViewListener!=null){
			mOnChooseViewListener.onChooseViewVisiableChanged(false);
		}
	}
	
	public void hanldePick(byte[] jpegData, Uri output) {
		OutputStream outputStream = null;
		try {
			outputStream = mActivity.getContentResolver().openOutputStream(output);
			outputStream.write(jpegData);
			outputStream.close();

			mActivity.setResult(Activity.RESULT_OK);
			mActivity.finish();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			CameraUtil.closeSilently(outputStream);
		}
	}
	
	public PictureCamera setCameraPictureTakenListener(CameraPictureTakenListener cameraPictureTakenListener) {
		this.mCameraPictureTakenListener = cameraPictureTakenListener;
		return this;
	}
	
	public PictureCamera setOnInterceptTakePictureListener(OnInterceptTakePictureListener onInterceptTakePictureListener) {
		this.mOnInterceptTakePictureListener = onInterceptTakePictureListener;
		return this;
	}
	
	public static interface OnTakeDataCallBackListener{
		public void onTakeData(byte[] jpegData);
	}
	
	public static interface CameraPictureTakenListener {
		public void onTakePictureStart();
		public void onTakePictureEnd();
	}
	
	public static interface OnInterceptTakePictureListener{
		public boolean onIntercepTakePicture(PictureCamera camera);
	}
	
	public static interface OnPerpareTakePictureListener{
		public void onPerpareTakePicture(PictureCamera camera);
	}

	@Override
	public void onCameraOpend(XCamera camera) {
		mCanTake = true;
	}

	@Override
	public void onCameraClosed() {
		
	}
}
