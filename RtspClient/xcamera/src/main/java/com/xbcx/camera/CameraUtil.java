package com.xbcx.camera;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.xbcx.core.XApplication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.provider.MediaStore.Images.ImageColumns;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

@SuppressWarnings("deprecation")
public class CameraUtil {

	private static final String TAG = "CameraUtil";

	public static final int NO_STORAGE_ERROR = -1;
	public static final int CANNOT_STAT_ERROR = -2;

	public static int calculatePicturesRemaining() {
		try {
			if (!hasStorage()) {
				return NO_STORAGE_ERROR;
			} else {
				String storageDirectory = Environment.getExternalStorageDirectory().toString();
				StatFs stat = new StatFs(storageDirectory);
				final int PICTURE_BYTES = 1500000;
				float remaining = ((float) stat.getAvailableBlocks() * (float) stat.getBlockSize()) / PICTURE_BYTES;
				return (int) remaining;
			}
		} catch (Exception ex) {
			// if we can't stat the filesystem then we don't know how many
			// pictures are remaining. it might be zero but just leave it
			// blank since we really don't know.
			return CANNOT_STAT_ERROR;
		}
	}

	/**
	 * OSX requires plugged-in USB storage to have path /DCIM/NNNAAAAA to be
	 * imported. This is a temporary fix for bug#1655552.
	 */
	public static void ensureOSXCompatibleFolder() {
		File nnnAAAAA = new File(Environment.getExternalStorageDirectory().toString() + "/DCIM/100ANDRO");
		if ((!nnnAAAAA.exists()) && (!nnnAAAAA.mkdir())) {
			Log.e(TAG, "create NNNAAAAA file: " + nnnAAAAA.getPath() + " failed");
		}
	}

	private static boolean checkFsWritable() {
		// Create a temporary file to see whether a volume is really writeable.
		// It's important not to put it in the root directory which may have a
		// limit on the number of files.
		String directoryName = Environment.getExternalStorageDirectory().toString() + "/DCIM";
		File directory = new File(directoryName);
		if (!directory.isDirectory()) {
			if (!directory.mkdirs()) {
				return false;
			}
		}
		return directory.canWrite();
	}

	public static boolean checkEmptyFile(String path) {
        if (path != null) {
            File f = new File(path);
            if (f.length() == 0 && f.delete()) {
            	return false;
            }
        }
        return true;
    }
	
	public static boolean hasStorage() {
		return hasStorage(true);
	}

	public static boolean hasStorage(boolean requireWriteAccess) {
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
			if (requireWriteAccess) {
				boolean writable = checkFsWritable();
				return writable;
			} else {
				return true;
			}
		} else if (!requireWriteAccess && Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			return true;
		}
		return false;
	}

	public static int roundOrientation(int orientationInput) {
		int orientation = orientationInput;

		if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN) {
			orientation = 90;
		}

		orientation = orientation % 360;
		int retVal;
		if (orientation < (0 * 90) + 45) {
			retVal = 0;
		} else if (orientation < (1 * 90) + 45) {
			retVal = 90;
		} else if (orientation < (2 * 90) + 45) {
			retVal = 180;
		} else if (orientation < (3 * 90) + 45) {
			retVal = 270;
		} else {
			retVal = 0;
		}

		return retVal;
	}

	public static Bitmap rotate(Bitmap b, int degrees) {
		if (degrees != 0 && b != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) b.getWidth() / 2, (float) b.getHeight() / 2);
			try {
				Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
				if (b != b2) {
					b.recycle();
					b = b2;
				}
			} catch (OutOfMemoryError ex) {
				// We have no memory to rotate. Return the original bitmap.
			}
		}
		return b;
	}

	public static Bitmap toRoundBitmap(Bitmap bitmap) {
		if (bitmap == null) {
			return null;
		}

		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		float roundPx;
		float left, top, right, bottom, dst_left, dst_top, dst_right, dst_bottom;
		if (width <= height) {
			roundPx = width / 2;
			top = 0;
			bottom = width;
			left = 0;
			right = width;
			height = width;
			dst_left = 0;
			dst_top = 0;
			dst_right = width;
			dst_bottom = width;
		} else {
			roundPx = height / 2;
			float clip = (width - height) / 2;
			left = clip;
			right = width - clip;
			top = 0;
			bottom = height;
			width = height;
			dst_left = 0;
			dst_top = 0;
			dst_right = height;
			dst_bottom = height;
		}

		Bitmap output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect src = new Rect((int) left, (int) top, (int) right, (int) bottom);
		final Rect dst = new Rect((int) dst_left, (int) dst_top, (int) dst_right, (int) dst_bottom);
		final RectF rectF = new RectF(dst);

		paint.setAntiAlias(true);

		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, src, dst, paint);
		if (bitmap != null && !bitmap.isRecycled()) {
			bitmap.recycle();
			bitmap = null;
		}
		return output;
	}

	@SuppressLint("NewApi")
	public static void setViewEnable(View v, boolean bEnable) {
		if (v != null) {
			if (bEnable) {
				v.setEnabled(true);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					v.setAlpha(1.f);
			} else {
				v.setEnabled(false);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
					v.setAlpha(.5f);
			}
		}
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			if (width > height) {
				inSampleSize = Math.round((float) height / (float) reqHeight);
			} else {
				inSampleSize = Math.round((float) width / (float) reqWidth);
			}

			final float totalPixels = width * height;

			final float totalReqPixelsCap = reqWidth * reqHeight * 2;

			while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
				inSampleSize++;
			}
		}
		return inSampleSize;
	}

	public static boolean addImage(byte[] jpegData, String filepath) {
		// We should store image data earlier than insert it to ContentProvider,
		// otherwise we may not be able to generate thumbnail in time.
		OutputStream outputStream = null;
		try {
			File dir = new File(filepath);
			if (!dir.getParentFile().exists())
				dir.getParentFile().mkdirs();
			outputStream = new FileOutputStream(filepath);
			outputStream.write(jpegData);
			return true;
		} catch (FileNotFoundException ex) {
			Log.w(TAG, ex);
			return false;
		} catch (IOException ex) {
			Log.w(TAG, ex);
			return false;
		} finally {
			closeSilently(outputStream);
		}
	}

	public static int getExifOrientation(String filepath) {
		int degree = 0;
		ExifInterface exif = null;
		try {
			exif = new ExifInterface(filepath);
		} catch (IOException ex) {
			Log.e(TAG, "cannot read exif", ex);
		}
		if (exif != null) {
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1);
			if (orientation != -1) {
				// We only recognize a subset of orientation tag values.
				switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					degree = 90;
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					degree = 180;
					break;
				case ExifInterface.ORIENTATION_ROTATE_270:
					degree = 270;
					break;
				}

			}
		}
		return degree;
	}

	public static void closeSilently(Closeable c) {
		if (c == null)
			return;
		try {
			c.close();
		} catch (Throwable t) {
			// do nothing
		}
	}
	
	public static boolean safeSetImageBitmap(ImageView iv,String path,int reqWidth,int reqHeight){
		final Bitmap bmp = decodeSampledBitmapFromFilePath(path, reqWidth, reqHeight);
		if(bmp != null){
			iv.setImageBitmap(bmp);
			return true;
		}
		return false;
	}
	
	public static Bitmap decodeSampledBitmapFromFilePath(String path,int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        options.inPurgeable = true;
        BitmapFactory.decodeFile(path, options);
        if(options.outWidth > 0){
        	options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;
            try{
            	return BitmapFactory.decodeFile(path,options);
            }catch(OutOfMemoryError e){
            	e.printStackTrace();
            	options.inSampleSize = options.inSampleSize * 2;
            	try{
            		return BitmapFactory.decodeFile(path, options);
            	}catch(OutOfMemoryError e1){
            		e1.printStackTrace();
            	}
            }
        }
        return null;
    }
	
	public static void insertImageToMediaStore(String filePath,int orientation) {
		insertImageToMediaStore(XApplication.getApplication(), new File(filePath), orientation);
	}
	
	public static void insertImageToMediaStore(Context context, File path,int orientation) {
		if(path.exists()&&path.length() == 0) {
			path.delete();
			return;
		}
		insertImage(context, path, orientation);
		sendMediaAdded(context);
	}
	
	public static void insertImage(Context context, File path,int orientation){
		addImage(context.getContentResolver(), path.getName(), System.currentTimeMillis(), orientation, (int) path.length(), path.getAbsolutePath(),null);
	}
	
	static Runnable senMediaBroadcast = new Runnable() {
		@Override
		public void run() {
			XApplication.getApplication().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE));
			count = 0;
		}
	};
	
	private static int count;
	public static void sendMediaAdded(Context context){
		if(count>=5) {
			XApplication.getMainThreadHandler().post(senMediaBroadcast);
			count = 0;
			return;
		}
		XApplication.getMainThreadHandler().removeCallbacks(senMediaBroadcast);
		XApplication.getMainThreadHandler().postDelayed(senMediaBroadcast, 1000);
		++count;
	}
	
	public static void sendMeidaScan(Context context, File path) {
		// 最后通知图库更新
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path.getAbsolutePath())));
	}
	
	
	public static void insertVideoToMediaStore(Context context, String path) {
		insertVideoToMediaStore(context, new File(path));
	}
	
	public static void insertVideoToMediaStore(Context context, File file) {
		if(file.exists()&&file.length() == 0) {
			file.delete();
			return;
		}
		
		ContentResolver localContentResolver = context.getContentResolver();
		ContentValues localContentValues = getVideoContentValues(context, file, System.currentTimeMillis());
		localContentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, localContentValues);
		sendMediaAdded(context);
	}
	
	public static void deleteVideoToMiastore(Context context,String path){
		ContentResolver localContentResolver = context.getContentResolver();
		localContentResolver.delete(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, MediaStore.Video.Media.DATA+" like ?", new String[]{path});
	}

	private static ContentValues getVideoContentValues(Context paramContext, File paramFile, long paramLong) {
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("title", paramFile.getName());
		localContentValues.put("_display_name", paramFile.getName());
		localContentValues.put("mime_type", "video/avc");
		localContentValues.put("datetaken", Long.valueOf(paramLong));
		localContentValues.put("date_modified", Long.valueOf(paramLong));
		localContentValues.put("date_added", Long.valueOf(paramLong));
		localContentValues.put("_data", paramFile.getAbsolutePath());
		localContentValues.put("_size", Long.valueOf(paramFile.length()));
		return localContentValues;
	}
	
	// Add the image to media store.
    public static Uri addImage(ContentResolver resolver, String title,
            long date, int orientation, int jpegLength,
            String path,Location location) {
    	
        ContentValues values = new ContentValues(9);
        values.put(ImageColumns.TITLE, title);
        values.put(ImageColumns.DISPLAY_NAME, title + ".jpg");
        values.put(ImageColumns.DATE_TAKEN, date);
        values.put(ImageColumns.ORIENTATION, orientation);
        values.put(ImageColumns.MIME_TYPE, "image/jpeg");
        values.put(ImageColumns.DATA, path);
        values.put(ImageColumns.SIZE, jpegLength);

        if (location != null) {
            values.put(ImageColumns.LATITUDE, location.getLatitude());
            values.put(ImageColumns.LONGITUDE, location.getLongitude());
        }

        try {
            Uri uri =  resolver.insert(Images.Media.EXTERNAL_CONTENT_URI, values);
            return uri;
        } catch (Throwable th)  {
        	return null;
        }
    }
    
    public static Bitmap getPictureThumb(byte[] data, int degree) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		Bitmap lastPictureThumb = null;
		try {
			options.inSampleSize = 32;
			lastPictureThumb = BitmapFactory.decodeByteArray(data, 0, data.length, options);
			lastPictureThumb = CameraUtil.rotate(lastPictureThumb, degree);
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			System.gc();
			options.inSampleSize = 64;
			try {
				lastPictureThumb = BitmapFactory.decodeByteArray(data, 0, data.length, options);
				lastPictureThumb = CameraUtil.rotate(lastPictureThumb, degree);
			} catch (OutOfMemoryError e1) {
				System.gc();
			}
		}
		return lastPictureThumb;
	}
    
    public static Bitmap getPictureThumb(byte[] data) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		Bitmap lastPictureThumb = null;
		try {
			options.inSampleSize = 32;
			lastPictureThumb = BitmapFactory.decodeByteArray(data, 0, data.length, options);
		} catch (OutOfMemoryError e) {
			e.printStackTrace();
			System.gc();
			options.inSampleSize = 64;
			try {
				lastPictureThumb = BitmapFactory.decodeByteArray(data, 0, data.length, options);
			} catch (OutOfMemoryError e1) {
				System.gc();
			}
		}
		return lastPictureThumb;
	}
    
    public static boolean saveBitmapToFile(String pathDst,Bitmap bmp,int quality){
    	FileOutputStream fos = null;
		try {
			fos = createFileOutputStream(pathDst);
			bmp.compress(Bitmap.CompressFormat.JPEG, quality, fos);
			fos.flush();
			fos.close();
			fos = null;
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			if(fos!=null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
    
    public static FileOutputStream createFileOutputStream(String strPath) throws Exception{
		final File file = new File(strPath);
		try{
			return new FileOutputStream(file);
		}catch(Exception e){
			final File fileParent = file.getParentFile();
			if(!fileParent.exists()){
				if(fileParent.mkdirs()){
					return new FileOutputStream(file);
				}
			}
		}
		
		return null;
	}
    
    public static boolean isPick(Activity activity){
    	return isPickImage(activity)||isPickVideo(activity);
    }
    
    public static boolean isPickImage(Activity activity){
		final String action = activity.getIntent().getAction();
		return "android.media.action.IMAGE_CAPTURE".equals(action);
	}
    
    public static boolean isPickVideo(Activity activity){
    	final String action = activity.getIntent().getAction();
		return	"android.media.action.VIDEO_CAMERA".equals(action)
				|| "android.media.action.VIDEO_CAPTURE".equals(action);
    }
    
    public static boolean isSettingsDefalutIntent(Activity activity){
    	return "default".equals(activity.getIntent().getStringExtra("settings"));
    }
    
    public static String getPictureSize(Activity activity){
    	return activity.getIntent().getStringExtra("picture_size");
    }
    
    public static String millisecondToTimeString(long milliSeconds, boolean displayCentiSeconds) {
        long seconds = milliSeconds / 1000; // round down to compute seconds
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long remainderMinutes = minutes - (hours * 60);
        long remainderSeconds = seconds - (minutes * 60);

        StringBuilder timeStringBuilder = new StringBuilder();

        // Hours
		if (hours < 10) {
			timeStringBuilder.append('0');
		}
		timeStringBuilder.append(hours);

		timeStringBuilder.append(':');

        // Minutes
        if (remainderMinutes < 10) {
            timeStringBuilder.append('0');
        }
        timeStringBuilder.append(remainderMinutes);
        timeStringBuilder.append(':');

        // Seconds
        if (remainderSeconds < 10) {
            timeStringBuilder.append('0');
        }
        timeStringBuilder.append(remainderSeconds);

        // Centi seconds
        if (displayCentiSeconds) {
            timeStringBuilder.append('.');
            long remainderCentiSeconds = (milliSeconds - seconds * 1000) / 10;
            if (remainderCentiSeconds < 10) {
                timeStringBuilder.append('0');
            }
            timeStringBuilder.append(remainderCentiSeconds);
        }

        return timeStringBuilder.toString();
    }
    
    @SuppressLint("NewApi")
	public static int determineDisplayOrientation(Activity activity, int defaultCameraId) {
		int displayOrientation = 0;
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
			CameraInfo cameraInfo = new CameraInfo();
			Camera.getCameraInfo(defaultCameraId, cameraInfo);

			int degrees = getRotationAngle(activity);

			if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
				displayOrientation = (cameraInfo.orientation + degrees) % 360;
				displayOrientation = (360 - displayOrientation) % 360;
			} else {
				displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
			}
		}
		return displayOrientation;
	}
    
    public static int determineDisplayOrientation(int degrees, int defaultCameraId) {
		int displayOrientation = 0;
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.FROYO) {
			CameraInfo cameraInfo = new CameraInfo();
			Camera.getCameraInfo(defaultCameraId, cameraInfo);
			if (cameraInfo.facing == CameraInfo.CAMERA_FACING_FRONT) {
				displayOrientation = (cameraInfo.orientation + degrees) % 360;
				displayOrientation = (360 - displayOrientation) % 360;
			} else {
				displayOrientation = (cameraInfo.orientation - degrees + 360) % 360;
			}
		}
		return displayOrientation;
	}
    
    public static int getRotationAngle(Activity activity) {
		int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;

		switch (rotation) {
		case Surface.ROTATION_0:
			degrees = 0;
			break;

		case Surface.ROTATION_90:
			degrees = 90;
			break;

		case Surface.ROTATION_180:
			degrees = 180;
			break;

		case Surface.ROTATION_270:
			degrees = 270;
			break;
		}
		return degrees;
	}
    
    public static Size fixSize(String setSize,List<Size> sizes){
		int index = setSize.indexOf("x");
		int width = Integer.parseInt(setSize.substring(0, index));
		int height = Integer.parseInt(setSize.substring(index+1, setSize.length()));
		if(sizes.size()>0){
			Size result = sizes.get(0); 
			int temp = Math.abs(Math.abs(result.width - width) - Math.abs(result.height - height));
			for(Size size:sizes){
				int wx = Math.abs(size.width - width);
				int hx = Math.abs(size.height - height);
				int tempx = Math.abs(wx - hx);
				if(tempx<temp){
					temp = tempx;
					result = size;
				}
			}
			return result;
		}
		return null;
	}
	
    public static void sortList(List<Size> sizes){
    	Collections.sort(sizes, new Comparator<Size>() {
			@Override
			public int compare(Size lhs, Size rhs) {
				if(rhs.width>lhs.width&&rhs.height>lhs.height){
					return 1;
				}
				return -1;
			}
		});
    }
    
    public static Size fixSize(float ratio,List<Size> sizes){
    	return fixSize(ratio, sizes, 720);
    }
    
	public static Size fixSize(float ratio,List<Size> sizes,int minWidth){
		if(sizes.size()>0){
			sortList(sizes);
			Size result = sizes.get(0); 
			float ratioX = (float)result.width/(float)result.height;
			float temp = Math.abs(ratioX - ratio);
			for(Size size:sizes){
				if(size.width<minWidth){
					break;
				}
				float width = size.width;
				float height = size.height;
				float tempRatio = width/height;
				float tempx = Math.abs(tempRatio - ratio);
				if(tempx<temp){
					temp = tempx;
					result = size;
				}
			}
			return result;
		}
		return null;
	}
	
	public static Size findSizeByRatio(float ratio,List<Size> sizes,int minWidth){
		if(sizes.size()>0){
			sortList(sizes);
			for(Size size:sizes){
				if(size.width<minWidth){
					break;
				}
				float width = size.width;
				float height = size.height;
				float tempRatio = width/height;
				float tempx = Math.abs(tempRatio - ratio);
				if(tempx<=0.00000001){
					return size;
				}
			}
		}
		return null;
	}
	
	public static int[] parseSize(String value){
		int index = value.indexOf("x");
		int width = Integer.parseInt(value.substring(0, index));
		int height = Integer.parseInt(value.substring(index+1, value.length()));
		return new int[]{width,height};
	}
	
	public static void Assert(boolean cond) {
        if (!cond) {
            throw new AssertionError();
        }
    }
	
	public static boolean isAvailableFile(String filePath){
		return isAvailableFile(new File(filePath));
	}
	
	public static boolean isAvailableFile(File file){
		if(file.exists()){
			return true;
		}else{
			String path = null;
			try {
				String parent = file.getParent();
				File folder = new File(parent);
				if(!folder.exists()){
					folder.mkdirs();
				}
				file.createNewFile();
				path = file.getAbsolutePath();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				checkEmptyFile(path);
			}
		}
		return false;
	}
	
	public static Size fixSize(int width,List<Size> sizes){
		if(sizes.size()>0){
			sortList(sizes);
			Size result = sizes.get(0); 
			for(Size size:sizes){
				if(size.width == width){
					return size;
				}
			}
			return result;
		}
		return null;
	}
	
	public static int getRotation(int cameraId){
		CameraInfo camInfo = new CameraInfo();
        Camera.getCameraInfo(cameraId, camInfo);
        int cameraRotationOffset = camInfo.orientation;
        return cameraRotationOffset;
	}
	
	 public static int getDisplayRotation(Context context) {
	        WindowManager windowManager = (WindowManager) context
	                .getSystemService(Context.WINDOW_SERVICE);
	        int rotation = windowManager.getDefaultDisplay()
	                .getRotation();
	        switch (rotation) {
	            case Surface.ROTATION_0:
	                return 0;
	            case Surface.ROTATION_90:
	                return 90;
	            case Surface.ROTATION_180:
	                return 180;
	            case Surface.ROTATION_270:
	                return 270;
	        }
	        return 0;
	}
}
