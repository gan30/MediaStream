package com.xbcx.camera;

import com.xbcx.camera.PointFocusManager.MuiltPointerZoomSizeChangeListener;
import com.xbcx.camera.PointFocusManager.PointFocusManagerInterface;
import com.xbcx.camera.PointFocusManager.ZoomChangeStatus;
import com.xbcx.camera.VideoCamera.OnVideoListener;
import com.xbcx.camera.ui.FastFocusRectangle;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.util.XbLog;
import com.xbcx.utils.SystemUtils;

import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import gan.camera.R;

public class AdjustZoomSizePlugin extends ActivityPlugin<BaseActivity> implements CameraActivityPlugin,MuiltPointerZoomSizeChangeListener, OnSeekBarChangeListener, OnClickListener,OnVideoListener, PointFocusManagerInterface{
	
	final static String tag = "AdjustZoomSizePlugin";
	
	FastFocusRectangle	mFastFocusRectangle;
	PointFocusManager 	mPointFocusManager;
	
	int 				mZoomHeight;
	int 				mMaxHeight;
	
	View      			mLayoutZoom;
	SeekBar				mSeekBar;
	View				mRootView;
	boolean				mIsEnable;
	
	@Override
	protected void onAttachActivity(BaseActivity activity) {
		super.onAttachActivity(activity);
		setZoom(activity.findViewById(R.id.layoutZoom));
		mSeekBar = (SeekBar) activity.findViewById(R.id.zoomSeek);
		mSeekBar.setOnSeekBarChangeListener(this);
		try {
			mFastFocusRectangle = (FastFocusRectangle) activity.findViewById(R.id.fastfocus_rectangle);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		mRootView = activity.findViewById(R.id.rootView);
		mPointFocusManager = new PointFocusManager(mRootView);
		mPointFocusManager.setZoomlistener(this);
		mPointFocusManager.setLinstener(this);
		mMaxHeight = SystemUtils.dipToPixel(mActivity, 200);
		mSeekBar.setMax(getZoomHeigt());
		mIsEnable = XCamera.isCameraAlive();
	}
	
	public void setZoom(View layoutZoom) {
		mLayoutZoom = layoutZoom;
		mLayoutZoom.setVisibility(View.GONE);
		mLayoutZoom.findViewById(R.id.zoomin).setOnClickListener(this);
		mLayoutZoom.findViewById(R.id.zoomout).setOnClickListener(this);
	}
	
	public void setEnable(boolean enable){
		mIsEnable = enable;
		mPointFocusManager.setIsTouchEnable(enable);
	}
	
	public static XCamera camera(){
		return XCamera.get();
	}
	
	public int getZoomMax(){
		if(camera().isAlive()) {
			return camera().getParameters().getMaxZoom();
		}
		return 0;
	}
	
	public void zoomIn(){
		if(camera().isAlive()){
			int value = camera().getParameters().getZoom();
			int max = camera().getParameters().getMaxZoom();
			value += max / 10;
			mSeekBar.setProgress(value);
		}
	}

	public void zoomOut(){
		if(camera().isAlive()){
			int value = camera().getParameters().getZoom();
			int max = camera().getParameters().getMaxZoom();
			value -= max / 10;
			mSeekBar.setProgress(value);
		}
	}
	
	protected void onZoomChanged(int value){
		resetGoneRunnable();
	}

	int cur = 0;
	int max = 0;
	
	@Override
	public boolean zoomSizeChange(ZoomChangeStatus status, int size) {
		XbLog.i(tag, "zoomSizeChange size:"+size);
		switch (status) {
		case Start:
			cur = mSeekBar.getProgress();
			mLayoutZoom.setVisibility(View.VISIBLE);
			break;
		case Zooming:
			float ratio = (float)size/(float)mMaxHeight;
			int addProgress = (int) (ratio*getZoomHeigt());
			max = mSeekBar.getMax();
			mSeekBar.setProgress(Math.max(0, Math.min(max, cur+addProgress)));
			break;
		case End:
			cur = mSeekBar.getProgress();
			resetGoneRunnable();
			break;
		default:
			break;
		}
		return false;
	}
	
	public boolean isEnable() {
		return camera().isAlive()&&mIsEnable;
	}
	
	private Runnable mSetZoom = new Runnable() {
		@Override
		public void run() {
			if(isEnable()){
				int progress = mSeekBar.getProgress();
				int zoom = progress;
				camera().setZoom(zoom);
				onZoomChanged(zoom);
			}
		}
	};

	public int getZoomHeigt() {
		if(mZoomHeight == 0) {
			mZoomHeight = getZoomMax();
		}
		return mZoomHeight;
	}
	
	@Override
	public void onCameraOpend(XCamera camera) {
		mSeekBar.setMax(getZoomHeigt());
		mSeekBar.setProgress(0);
		
		if(mPointFocusManager!=null){
			mPointFocusManager.setCamera(camera.getCamera());
			if(mFastFocusRectangle!=null){
				int width = mRootView.getMeasuredWidth();
				int height = mRootView.getMeasuredHeight();
				int fWidth = mFastFocusRectangle.getMeasuredWidth();
				int fHeight = mFastFocusRectangle.getMeasuredHeight();
				int firstX = width/2 - fWidth/2;
				int firstY = height/2 - fHeight/2;
				autoFocus(firstX, firstY);
			}
		}
		setEnable(true);
	}

	@Override
	public void onCameraClosed() {
		setEnable(false);
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		if(mIsEnable){
			mSeekBar.removeCallbacks(mSetZoom);
			mSeekBar.postDelayed(mSetZoom, 10);
		}
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		mLayoutZoom.removeCallbacks(mGoneRunnable);
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		resetGoneRunnable();
	}
	
	public void resetGoneRunnable(){
		mLayoutZoom.removeCallbacks(mGoneRunnable);
		mLayoutZoom.postDelayed(mGoneRunnable, 5000);
	}
	
	public Runnable mGoneRunnable = new Runnable() {
		@Override
		public void run() {
			mLayoutZoom.setVisibility(View.GONE);
		}
	};

	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if(id == R.id.zoomin){
			zoomIn();
		}else if(id == R.id.zoomout){
			zoomOut();
		}
	}

	@Override
	public void onRecordStart(XVideoRecorder recorder) {
		setEnable(false);
	}

	@Override
	public void onRecordEnd(XVideoRecorder recorder) {
		setEnable(true);
	}

	@Override
	public void onRecordError(XVideoRecorder recorder, int error) {
		setEnable(true);
	}

	@Override
	public boolean interfacePointFocus() {
		return false;
	}

	@Override
	public void autoFocus(int x, int y) {
		if(mFastFocusRectangle!=null){
			final int fixY = y - mFastFocusRectangle.getHeight();
			mFastFocusRectangle.setX(x);
			mFastFocusRectangle.setY(fixY);
		}
		focus();
	}
	
	int offsetx,offsety;
	
	public void setOffsetx(int offsetx) {
		this.offsetx = offsetx;
	}

	public void setOffsety(int offsety) {
		this.offsety = offsety;
	}
	
	@SuppressWarnings("deprecation")
	public void focus(){
		mFastFocusRectangle.showStart();
		camera().autoFocus(new AutoFocusCallback() {
			@Override
			public void onAutoFocus(boolean success, Camera camera) {
				mFastFocusRectangle.post(new Runnable() {
					@Override
					public void run() {
						mFastFocusRectangle.showEnd(true);
					}
				});
			}
		});
		mFastFocusRectangle.postDelayed(new Runnable() {
			@Override
			public void run() {
				mFastFocusRectangle.showEnd(true);
			}
		}, 500);
	}
}
