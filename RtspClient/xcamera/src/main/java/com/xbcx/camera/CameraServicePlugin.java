package com.xbcx.camera;

public interface CameraServicePlugin<T extends CameraService> extends CameraBasePlugin{

	public void onAttachService(T service);
		
	public void onServiceDestory();
}
