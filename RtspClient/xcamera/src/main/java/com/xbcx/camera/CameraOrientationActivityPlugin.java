package com.xbcx.camera;

import java.util.Vector;

import com.xbcx.camera.CameraOrientationManager.OrientationchangedListener;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;

public class CameraOrientationActivityPlugin extends ActivityPlugin<BaseActivity> implements OrientationchangedListener{
	
	final static String tag = "CameraOrientationActivityPlugin";

	CameraOrientationManager 		mCameraOrientationManager = CameraOrientationManager.get();
	Vector<RotateView>     			mRotateViews;
	
	private CameraOrientationActivityPlugin() {
	}
	
	public static CameraOrientationActivityPlugin get(BaseActivity activity) {
		for(CameraOrientationActivityPlugin manager:activity.getPlugins(CameraOrientationActivityPlugin.class)) {
			return manager;
		}
		CameraOrientationActivityPlugin manager = new CameraOrientationActivityPlugin();
		activity.registerPlugin(manager);
		return manager;
	}
	
	@Override
	protected void onAttachActivity(BaseActivity activity) {
		super.onAttachActivity(activity);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mCameraOrientationManager.enable(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		mCameraOrientationManager.disable(this);
	}
	
	public void addRotateView(RotateView imageView){
		if(mRotateViews == null){
			mRotateViews = new Vector<RotateView>();
		}
		mRotateViews.add(imageView);
	}
	
	public void removeRotateView(RotateView rotateView) {
		if(mRotateViews!=null) {
			mRotateViews.remove(rotateView);
		}
	}
	
	public void onOrientationchanged(int orientation){
		XbLog.i(tag, "onOrientationchanged" + orientation);
		if(mRotateViews!=null){
			for(RotateView rotateView:mRotateViews){
				rotateView.setDegree(orientation - 90);
			}
		}
		
		for(OnOrientationchangedPlugin plugin:mActivity.getPlugins(OnOrientationchangedPlugin.class)){
			plugin.onOrientationchanged(orientation);
		}
	}
	
	public int getOrientation() {
		return mCameraOrientationManager.getOrientation();
	}
	
	@SuppressLint("NewApi")
	public int getRotation() {
		return mCameraOrientationManager.getRotation();
	}
	
	public static interface OnOrientationchangedPlugin extends ActivityBasePlugin{
		public void onOrientationchanged(int orientation);
	}
}
