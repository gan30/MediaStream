package com.xbcx.camera.ui;

import java.util.LinkedList;
import java.util.List;

import com.xbcx.camera.CameraUtil;
import gan.camera.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.ScaleAnimation;

public class FastFocusRectangle extends View {

	private boolean mFocusEnd;
	private boolean mshowEnd;
	private boolean mFocusSuccess;
	private List<View> bindUnclickView = new LinkedList<View>();
	public FastFocusRectangle(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public void addBindUnclickView(View bindUnclickView) {
		this.bindUnclickView.add(bindUnclickView);
	}
	
	public void removeBindUnclickView(View bindUnclickView) {
		this.bindUnclickView.remove(bindUnclickView);
	}

	@SuppressWarnings("deprecation")
	private void setDrawable(int resid) {
		setBackgroundDrawable(getResources().getDrawable(resid));
	}
	
	@SuppressWarnings("deprecation")
	public void clear() {
		ScaleAnimation scale = (ScaleAnimation) getAnimation();
		if (scale != null)
			scale.cancel();
		removeCallbacks(clean);
        setBackgroundDrawable(null);
    }

	public void showStart() {
		for(View view:bindUnclickView)
			CameraUtil.setViewEnable(view, false);
		mFocusEnd = false;
		mshowEnd = false;
		mFocusSuccess = false;
		removeCallbacks(clean);
		setDrawable(R.drawable.focus_focusing);
		ScaleAnimation scale = (ScaleAnimation) getAnimation();
		if (scale == null) {
			scale = new ScaleAnimation(1.3f, 1f, 1.3f, 1f, Animation.RELATIVE_TO_SELF, 0.5f,
					Animation.RELATIVE_TO_SELF, 0.5f);
			scale.setDuration(700);
			scale.setAnimationListener(new AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
				}

				@Override
				public void onAnimationEnd(Animation animation) {
					mFocusEnd = true;
					showFocus();
				}
			});
			setAnimation(scale);
		}
		startAnimation(scale);
	}

	public void showEnd(final boolean success) {
		mshowEnd = true;
		mFocusSuccess = success;
		showFocus();
	}

	private void showFocus() {
		if (mFocusEnd && mshowEnd) {
			if (mFocusSuccess)
				showSuccess();
			else
				showFail();
			for(View view:bindUnclickView)
				CameraUtil.setViewEnable(view, true);
		}
	}
	
	private Runnable clean = new Runnable() {
		@Override
		public void run() {
			clear();
		}
	};

	private void showSuccess() {
		setDrawable(R.drawable.focus_focused);
		postDelayed(clean, 300);
	}

	private void showFail() {
		setDrawable(R.drawable.focus_focus_failed);
		postDelayed(clean, 300);
	}
	
}
