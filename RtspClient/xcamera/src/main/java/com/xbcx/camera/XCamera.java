package com.xbcx.camera;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import com.xbcx.camera.CameraManager.CameraProxy;
import com.xbcx.camera.settings.XCameraSettings;
import com.xbcx.core.CrashHandler.OnCrashHandler;
import com.xbcx.core.XApplication;
import com.xbcx.util.XbLog;

import android.annotation.SuppressLint;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.media.CameraProfile;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.view.SurfaceHolder;

public class XCamera implements PreviewCallback {
	
	private final static String tag = "XCamera";

	/**
	 * open() return null
	 */
	public final static int Error_Camera 	= 0;
	
	/**
	 * reconnect() fail
	 */
	public final static int Error_Reconnect = 1;
	
	CameraProxy		mCamera;
	Camera			camera;
	
	List<XCameraListener>			mXCameraListeners;
	List<UpdateCameraParameters>	mUpdateCameraParameters;
	Vector<PreviewCallback>   		mPreviewCallbacks;
	List<PreviewChangedListener>    mPreviewChangedListeners;
	
	private Parameters		mParameters;
	private int				mRotation;
	
	boolean 	mIsPause;
	boolean 	mPreviewing;
	boolean		mIsOpening;
	Boolean		mIsSupportWater;
	boolean		mEnable = true;
	
	int         mFrameRate = 30;
	byte[]		buffer,buffer2,buffer3,buffer4;
	boolean		mIsPreviewCallBack;
	
	Handler		mCameraHandler;
	Runnable	mOpenSyn = new Runnable() {
		@Override
		public void run() {
			open();
		}
	};
	
	public final static int Opened = 1;
	public final static int Error = 3;
	Handler mMainHandler = new Handler(Looper.getMainLooper()){
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Opened:
				onCameraOpend();
				break;
			case Error:
				onCameraError(msg.arg1);
				break;
			default:
				break;
			}
		};
	};
	
	static XCamera	mXCamera = null;	
	
	public static XCamera get(){
		if(mXCamera == null){
			mXCamera = new XCamera();
		}
		return mXCamera;
	}
	
	private XCamera(){
		HandlerThread thread = new HandlerThread("camera thread",
				Process.THREAD_PRIORITY_DISPLAY);
		XbLog.i("camera thread Priority", thread.getPriority()+"");
		thread.start();
		mCameraHandler = new Handler(thread.getLooper());
		
		XApplication.addManager(new OnCrashHandler() {
			@Override
			public void onUncaughtException(Thread thread, Throwable ex) {
				try {
					close();
					onDestory();
					CameraManager.instance().release();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void setCameraEnable(boolean enable){
		mEnable = enable;
		if(enable){
			openAsync();
		}else{
			close();
		}
	}
	
	public void onResume(){
		XbLog.i(tag, "onResume");
		mIsPause = false;
		if(needOpen()){
			openAsync();
		}
	}
	
	public void openAsync() {
		mCameraHandler.removeCallbacksAndMessages(null);
		mCameraHandler.post(mOpenSyn);
		mIsOpening = true;
	}
	
	public void openAsync(int position){
		CameraConfig.position = position;
		openAsync();
	}
	
	protected boolean needOpen(){
		return mEnable&&!isAlive()&&!mIsOpening;
	}
	
	public void onPause(){
		XbLog.i(tag, "onPause");
		mIsPause = true;
		close();
	}
	
	public boolean isClosed() {
		return mCamera == null||camera==null;
	}
	
	public void onDestory(){
		XbLog.i(tag, "onDestory");
		if(mXCameraListeners!=null) {
			mXCameraListeners.clear();
		}
		if(mPreviewCallbacks!=null) {
			mPreviewCallbacks.clear();
		}
		if(mUpdateCameraParameters!=null) {
			mUpdateCameraParameters.clear();
		}
		if(mPreviewChangedListeners!=null) {
			mPreviewChangedListeners.clear();
		}
	}
	
	public synchronized void open(int position){
		CameraConfig.position = position;
		open();
	}
	
	public synchronized void open(){
		mIsOpening = true;
		mCamera = CameraManager.instance().getCameraProxy();
		if(mCamera == null){
			try {
				XbLog.i(tag, "open start");
				@SuppressWarnings("deprecation")
				int cameraNum = Camera.getNumberOfCameras();
				if(CameraConfig.position>=cameraNum) {
					CameraConfig.position = 0;
				}
				mCamera = CameraManager.instance().cameraOpen(CameraConfig.position);
				XbLog.i(tag, "open end");
			} catch (RuntimeException e) {
				CameraManager.instance().release();
				XbLog.i(tag,"create mCameraDevice null");
			}
			if(mCamera != null){
				mMainHandler.sendEmptyMessage(Opened);
			}else{
				cameraError(Error_Camera);
			}
		}
		mIsOpening = false; 
	}
	
	private void cameraError(int error){
		CameraManager.instance().release();
		Message message = new Message();
		message.what = Error;
		message.obj = error;
		mMainHandler.sendMessage(message);
	}
	
	public synchronized void close(){
		if(mCamera!=null){
			XbLog.i(tag, "close");
			stopPreview();
			mCamera.release();
			onCameraClose();
		}
	}
	
	public static boolean isCameraAlive() {
		return get().isAlive();
	}
	
	public boolean isAlive(){
		return camera!=null&&mCamera!=null;
	}
	
	public static boolean isCameraIdll(){
		return get().isAlive()&&CameraManager.instance().isCameraIdll();
	}
	
	public static boolean isCameraBusy(){
		return !isCameraIdll();
	}
	
	protected void onCameraOpend(){
		XbLog.i(tag, "onCameraOpend");
		if(mIsPause){
			close();
			return;
		}
		if(mCamera == null){
			return;
		}
		
		camera = mCamera.getCamera();
		mParameters = camera.getParameters();
		// 检查系统ROM 是否已经支持水印录像
		if(mParameters!=null) {
			mIsSupportWater = !TextUtils.isEmpty(mParameters.get(XCameraSettings.KEY_RECORD_WATER));
			XCameraSettings.switchWaterSupport(mIsSupportWater);
			updateCameraParameters();
		}
		if(mXCameraListeners!=null){
			for(XCameraListener listener:mXCameraListeners) {
				listener.onCameraOpend(camera);
			}
		}
	}
	
	public boolean isSupportWater() {
		if(mIsSupportWater == null) {
			mIsSupportWater = XCameraSettings.isSupportWater();
		}
		return mIsSupportWater;
	}
	
	protected void onCameraClose(){
		XbLog.i(tag, "onCameraClosed");
		if(mXCameraListeners!=null){
			for(XCameraListener listener:mXCameraListeners) {
				listener.onCameraClose();
			}
		}
		synchronized (mCamera) {
			camera = null;
			mCamera = null;
		}
	}
	
	protected void onCameraError(int error){
		XbLog.i(tag, "onCameraError");
		if(mXCameraListeners!=null){
			for(XCameraListener listener:mXCameraListeners) {
				listener.onCameraError(error);
			}
		}
	}
	
	public void setDisplayOrientation(int degrees) {
		mCamera.setDisplayOrientation(degrees);
	}
	
	public void setPreviewSurface(SurfaceHolder holder) {
		try {
			camera.setPreviewDisplay(holder);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setPreviewTexture(SurfaceTexture surfaceTexture){
		try {
			camera.setPreviewTexture(surfaceTexture);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public final void stopPreview(){
		if(camera!=null){
			XbLog.i(tag, "stopPreview");
			camera.stopPreview();
			mPreviewing = false;
			if(mIsPreviewCallBack) {
				stopPreviewCallBack();
			}
		}
	}
	
	public final synchronized void startPreview(){
		firstFrame = true;
		if(camera!=null){
			XbLog.i(tag, "startPreview");
			try {
				camera.startPreview();
				mPreviewing = true;
				if(mIsPreviewCallBack){
					startPreviewCallback();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public final void restartPreview() {
		stopPreview();
		startPreview();
	}
	
	public final void takePicture(final ShutterCallback shutter, final PictureCallback raw,
            final PictureCallback postview, final PictureCallback jpeg) {
		mCameraHandler.post(new Runnable() {
			@Override
			public void run() {
				try {
					camera.takePicture(shutter, raw, postview, jpeg);
				} catch (Exception e) {
					e.printStackTrace();
					if(raw!=null) {
						raw.onPictureTaken(null, camera);
					}
					if(postview!=null) {
						postview.onPictureTaken(null, camera);
					}
					if(jpeg!=null) {
						jpeg.onPictureTaken(null, camera);
					}
				}
			}
		});
	}
	
	public void autoFocus(AutoFocusCallback callback) {
//		mCamera.autoFocus(callback);
	}
	
	public CameraProxy getCameraProxy(){
		return mCamera;
	}
	
	public Camera getCamera() {
		return camera;
	}
	
	public synchronized void setRotation(int rotation){
		mParameters.setRotation(rotation);
		camera.setParameters(mParameters);
		mRotation = rotation;
	}
	
	public int getRotation() {
		return mRotation;
	}
	
	public static int getCameraId(){
		int cameraNum = Camera.getNumberOfCameras();
		if(CameraConfig.position>=cameraNum) {
			CameraConfig.position = 0;
		}
		return CameraConfig.position;
	}
	
	@SuppressLint("InlinedApi")
	public static boolean isFacingbackCamera() {
		return CameraConfig.position != CameraInfo.CAMERA_FACING_FRONT;
	}
	
	public synchronized boolean setFlashMode(String mode){
		if(isSupported(mode, mParameters.getSupportedFlashModes())) {
			XCameraSettings.setStringSetting(XCameraSettings.KEY_FLASH_MODE, mode);
			mParameters.setFlashMode(mode);
			camera.setParameters(mParameters);
			return true;
		}
		return false;
	}
	
	public synchronized void setZoom(int value){
		if (mParameters.isZoomSupported()) {
			mParameters.setZoom(value);
			camera.setParameters(mParameters);
		}
	}
	
	public Parameters getParameters() {
		return mParameters;
	}
	
	public synchronized void setParameters(Parameters parameters) {
		this.mParameters = parameters;
		camera.setParameters(parameters);
	}
	
	public String getFlashMode(){
		if(mParameters==null){
			return XCameraSettings.readStringSetting(XCameraSettings.KEY_FLASH_MODE, "auto");
		}
		return mParameters.getFlashMode();
	}
	
	@SuppressLint("NewApi")
	public void enableShutterSound(boolean enabled) throws Exception{
		if(android.os.Build.VERSION.SDK_INT >= 17){
			if(camera!=null){
				camera.enableShutterSound(enabled);
			}
		}else{
			throw new Exception("SDK_INT must > 17");
		}
	}
	
	@SuppressLint("NewApi")
	public synchronized void switchCamera(){
		CameraConfig.position++;
		switchCamera(CameraConfig.position);
	}
	
	public synchronized void switchCamera(int cameraId) {
		int cameraNum = Camera.getNumberOfCameras();
		if(cameraId>=cameraNum) {
			cameraId = 0;
		}
		close();
		openAsync(cameraId);
	}
	
	public void addCameraListener(XCameraListener cameraListener) {
		if(mXCameraListeners==null) {
			mXCameraListeners = new ArrayList<XCameraListener>();
		}
		if(!mXCameraListeners.contains(cameraListener)) {
			mXCameraListeners.add(cameraListener);
		}
	}
	
	public void removeCameraListener(XCameraListener CameraListener) {
		if(mXCameraListeners!=null) {
			mXCameraListeners.remove(CameraListener);
		}
	}
	
	public void addPreviewCallBack(PreviewCallback callback) {
		if(mPreviewCallbacks==null) {
			mPreviewCallbacks = new Vector<>();
		}
		synchronized (mPreviewCallbacks) {
			if(!mPreviewCallbacks.contains(callback)) {
				mPreviewCallbacks.add(callback);
			}
		}
	}
	
	public void removePreviewCallBack(PreviewCallback callback) {
		if(mPreviewCallbacks!=null) {
			synchronized (mPreviewCallbacks) {
				mPreviewCallbacks.remove(callback);
			}
		}
	}
	
	public static interface SwitchCameraListener {
		public void switchCameraResult(int Cameraid, boolean success);
	}
	
	public static boolean isSupported(String value, List<String> supported) {
		return supported == null ? false : supported.indexOf(value) >= 0;
	}
	
	public static boolean isSupportedSize(String value, List<Size> supported) {
		int size[] = CameraUtil.parseSize(value);
		return isSupportedSize(size[0], size[1], supported);
	}
	
	public static boolean isSupportedSize(Size size, List<Size> supported) {
		return isSupportedSize(size.width, size.height, supported);
	}
	
	public static boolean isSupportedSize(int width,int height, List<Size> supported) {
		if(supported == null){
			return false;
		}
		for(Size s:supported){
			if(s.width == width && s.height == height){
				return true;
			}
		}
		return false;
	}
	
	public void unlock(){
		mCamera.unlock();
	}
	
	public void setParameters(String key,String value){
		mParameters.set(key, value);
		camera.setParameters(mParameters);
	}
	
	public synchronized void setLongshot(final boolean enable){
		try {
			Class<?> clazz = Class.forName("android.hardware.Camera");
			final Method m = clazz.getDeclaredMethod("setLongshot", boolean.class);
			m.setAccessible(true);
			m.invoke(camera, enable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressLint("NewApi")
	public synchronized void updateCameraParameters(){
		updateCameraPictureSize();
		updateCameraPreviewSize();
		
		mParameters.setPictureFormat(ImageFormat.JPEG);
		
		int jpegQuality;
        if(getCameraId()>1) {
            jpegQuality=95; //Temproray Solution for camera ids greater than 1. Proper fix TBD.
        } else {
            jpegQuality = CameraProfile.getJpegEncodingQualityParameter(getCameraId(),CameraProfile.QUALITY_HIGH);
        }
        mParameters.setJpegQuality(jpegQuality);
		
		String colorEffect = XCameraSettings.readStringSetting(XCameraSettings.KEY_COLOR_EFFECT, "none");
		if (isSupported(colorEffect, mParameters.getSupportedColorEffects())) {
			if (!mParameters.getColorEffect().equals(colorEffect)) {
				mParameters.setColorEffect(colorEffect);
			}
		}
		
		try {
			String exposure = XCameraSettings.readStringSetting(XCameraSettings.KEY_EXPOSURE, "0");
			int value = Integer.parseInt(exposure);
			int max = mParameters.getMaxExposureCompensation();
			int min = mParameters.getMinExposureCompensation();
			if (value >= min && value <= max) {
				mParameters.setExposureCompensation(value);
			}
		} catch (NumberFormatException e) {
		}
		
		final String sceneMode = XCameraSettings.readStringSetting(XCameraSettings.KEY_SCENE_MODE, "auto");
		if (isSupported(sceneMode, mParameters.getSupportedSceneModes())) {
			if (!mParameters.getSceneMode().equals(sceneMode)) {
				mParameters.setSceneMode(sceneMode);
			}
		}
		
		// Set white balance parameter.
		String whiteBalance = XCameraSettings.readStringSetting(XCameraSettings.KEY_WHITE_BALANCE, "auto");
		if (isSupported(whiteBalance, mParameters.getSupportedWhiteBalance())) {
			if(!mParameters.getWhiteBalance().equals(whiteBalance)) {
				mParameters.setWhiteBalance(whiteBalance);
			}
		} 
		
		String flashMode = XCameraSettings.readStringSetting(XCameraSettings.KEY_FLASH_MODE, "auto");
		List<String> supportedFlash = mParameters.getSupportedFlashModes();
		if (isSupported(flashMode, supportedFlash)) {
			if(!mParameters.getFlashMode().equals(flashMode)) {
				mParameters.setFlashMode(flashMode);
			}
		}
		
		String focusMode = XCameraSettings.readStringSetting(XCameraSettings.KEY_FOCUS_MODE, "auto");
		List<String> focusModes = mParameters.getSupportedFocusModes();
		if(isSupported(focusMode, focusModes)) {
			if(!mParameters.getFocusMode().equals(focusMode)) {
				mParameters.setFocusMode(focusMode);
			}
		}
		
		if(mParameters.isVideoSnapshotSupported()) {
			mParameters.setRecordingHint(true);
		}
		
		if(android.os.Build.VERSION.SDK_INT >= 15) {
			if(mParameters.isVideoStabilizationSupported()) {
				mParameters.setVideoStabilization(true);
			}
		}
		
		if(mUpdateCameraParameters!=null){
			for(UpdateCameraParameters updateCameraParameters:mUpdateCameraParameters) {
				updateCameraParameters.onUpdateParameters(mParameters);
			}
		}
		
		camera.setParameters(mParameters);
		notifyPreviewChanged();
		
		try {
			enableShutterSound(XCameraSettings.soundEnable());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateCameraPictureSize(){
		String pictureSize = XCameraSettings.getPictureSize();
		List<Size> supported = mParameters.getSupportedPictureSizes();
		if(!setCameraPictureSize(pictureSize, supported, mParameters)){
			initialCameraPictureSize(mParameters);
		}else{
			for(Size size: supported){
				XbLog.i(tag, "PictureSize:"+size.width+"x"+ size.height );
			}
		}
		
		Size size = mParameters.getPictureSize();
		XbLog.i(tag, "current pictureSize:" + size.width+"x"+size.height);
		camera.setParameters(mParameters);
	}
	
	public Size updateCameraPreviewSize(){
		Size pictureSize = mParameters.getPictureSize();
		float ratio = (float)pictureSize.width/(float)pictureSize.height;
		List<Size> supported = mParameters.getSupportedPreviewSizes();
		Size previewSize = CameraUtil.fixSize(ratio, supported);
		if(previewSize!=null){
			mParameters.setPreviewSize(previewSize.width, previewSize.height);
		}
		for(Size size: supported){
			XbLog.i(tag, "previewSize:"+size.width+"x"+ size.height);
		}
		camera.setParameters(mParameters);
		XbLog.i(tag, "current previewSize:" + previewSize.width+"x"+previewSize.height);
		return previewSize;
	}
	
	protected boolean setCameraPictureSize(String candidate, List<Size> supported, Parameters parameters) {
		int index = candidate.indexOf('x');
		if (index == -1)
			return false;
		int width = Integer.parseInt(candidate.substring(0, index));
		int height = Integer.parseInt(candidate.substring(index + 1));
		for (Size size : supported) {
			if (size.width == width && size.height == height) {
				parameters.setPictureSize(width, height);
				return true;
			}
		}
		return false;
	}
	
	private void initialCameraPictureSize(Parameters parameters) {
		List<Size> supported = parameters.getSupportedPictureSizes();
		int width = 0;
		int height = 0;
		for (Size s : supported){
			XbLog.i(tag,"pictureSize:" + s.width +"x"+ s.height);
			if(s.width * s.height > width * height){
				width = s.width;
				height = s.height;
			}
		}
		if(width>0){
			parameters.setPictureSize(width, height);
		}
	}
	
	public synchronized int setPreviewFormat(int format){
		if(isClosed()) {
			return -2;
		}
		if(format == mParameters.getPreviewFormat()) {
			return 0;
		}
		List<Integer> formats = mParameters.getSupportedPreviewFormats();
		if(formats.contains(format)){
			stopPreview();
			mParameters.setPreviewFormat(format);
			camera.setParameters(mParameters);
			startPreview();
			return 1;
		}else{
			XbLog.i(tag," not support format:" + format);
			return -1;
		}
	}
	
	public void addUpdateCameraParameters(UpdateCameraParameters updateCameraParameters) {
		if(mUpdateCameraParameters==null) {
			mUpdateCameraParameters = new ArrayList<UpdateCameraParameters>();
		}
		if(!mUpdateCameraParameters.contains(updateCameraParameters)) {
			mUpdateCameraParameters.add(updateCameraParameters);
		}
	}
	
	public void removeUpdateCameraParameters(UpdateCameraParameters updateCameraParameters) {
		if(mUpdateCameraParameters!=null) {
			mUpdateCameraParameters.remove(updateCameraParameters);
		}
	}

	public synchronized void startPreviewCallback(){
		if(isClosed()){
			return;
		}
		firstFrame = true;
		Size size = mParameters.getPreviewSize();
		XbLog.i(tag, "startPreviewCallback: preview" + size.width+"x"+size.height+"format:"+mParameters.getPreviewFormat());
		if (buffer == null||buffer.length!=(size.height * size.width * 3 / 2)) {
			camera.setPreviewCallbackWithBuffer(null);
			buffer = null;
			buffer2 = null;
			buffer3 = null;
			buffer4 = null;
			buffer = new byte[size.height * size.width * 3 / 2];
			buffer2 = new byte[size.height * size.width * 3 / 2];
			buffer3 = new byte[size.height * size.width * 3 / 2];
			buffer4 = new byte[size.height * size.width * 3 / 2];
			camera.addCallbackBuffer(buffer);
			camera.addCallbackBuffer(buffer2);
			camera.addCallbackBuffer(buffer3);
			camera.addCallbackBuffer(buffer4);
			camera.setPreviewCallbackWithBuffer(this);
		}else{
			camera.setPreviewCallbackWithBuffer(this);
		}
		firstFrame = true;
		mIsPreviewCallBack = true;
	}
	
	public synchronized void restartPreviewCallback(){
		stopPreviewCallBack();
		startPreviewCallback();
	}
	
	public synchronized void stopPreviewCallBack(){
		if (camera!=null) {
			camera.setPreviewCallbackWithBuffer(null);
		}
		firstFrame = true;
		buffer = null;
		buffer2 = null;
		buffer3 = null;
		buffer4 = null;
		mIsPreviewCallBack = false;
		XbLog.i(tag, "stopPreviewCallBack");
	}
	
	public boolean isPreviewCallBack() {
		return mIsPreviewCallBack;
	}
	
	public static interface UpdateCameraParameters{
		public void onUpdateParameters(Parameters parameters);
	}

	private long 	times;
	private long 	time;
	private boolean firstFrame = true;//x1手机上第一帧 数据 format 与数据不对应
	
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		if(data == null){
			return;
		}
		if(firstFrame){
			firstFrame = false;
			camera.addCallbackBuffer(data);
			return;
		}
		
		if (time == 0) {
			time = System.currentTimeMillis();
		}
		times++;
		long currenttime = System.currentTimeMillis();
		long timex = currenttime - time;
		if (timex >= 1000) {
			XbLog.d(tag, "onPreviewFrame:" + times * 1000 / timex);
			time = currenttime;
			times = 0;
		}
		
		if(mPreviewCallbacks!=null&&!mPreviewCallbacks.isEmpty()) {
			synchronized (mPreviewCallbacks) {
				for (Iterator<PreviewCallback> it = mPreviewCallbacks.iterator(); it.hasNext();) {
					PreviewCallback callback = it.next();
					callback.onPreviewFrame(data, camera);
				}
			}
		}
		
		camera.addCallbackBuffer(data);
	}
	
	public void setPictureSize(String value){
		int[] size = CameraUtil.parseSize(value);
		int width = size[0];
		int height = size[1];
		setPictureSize(width, height);
	}
	
	public void setPictureSize(Size size){
		setPictureSize(size.width, size.height);
	}
	
	public void setPictureSize(int width,int height){
		if(isCameraAlive()){
			Parameters parameters = getParameters();
			if(XCamera.isSupportedSize(width, height, parameters.getSupportedPictureSizes())){
				parameters.setPictureSize(width, height);
				setParameters(parameters);
				updateCameraParameters();
			}else{
				XbLog.i(tag, width+"x"+height+"is not supported");
			}
		}
	}
	
	public int setPreviewSize(String value){
		int[] size = CameraUtil.parseSize(value);
		int width = size[0];
		int height = size[1];
		return setPreviewSize(width, height);
	}
	
	public int setPreviewSize(Size size){
		return setPreviewSize(size.width, size.height);
	}
	
	public int setPreviewSize(int width,int height){
		if(isClosed()) {
			return -2;
		}
		Size size = mParameters.getPreviewSize();
		if(size.width==width&&size.height==height) {
			XbLog.i(tag, "setPreviewSize is equals old");
			return 0;
		}
		if (XCamera.isSupportedSize(width, height, mParameters.getSupportedPreviewSizes())) {
			XbLog.i(tag, "setPreviewSize" + width + "x" + height);
			stopPreview();
			mParameters.setPreviewSize(width, height);
			camera.setParameters(mParameters);
			startPreview();
			notifyPreviewChanged();
			return 1;
		} else {
			XbLog.i(tag, width + "x" + height + "is not supported");
			return -1;
		}
	}
	
	public void notifyPreviewChanged() {
		onPreviewChanged(mParameters.getPreviewSize());
	}
	
	protected void onPreviewChanged(Size size){
		XbLog.i(tag, "preview size:"+size.width+"x"+size.height);
		if(mPreviewChangedListeners!=null) {
			for(PreviewChangedListener listener:mPreviewChangedListeners) {
				listener.onPreviewChanged(size);
			}
		}
	}
	
	public void addPreviewChangedListener(PreviewChangedListener listener) {
		if(mPreviewChangedListeners==null) {
			mPreviewChangedListeners = new ArrayList<>();
		}
		if(!mPreviewChangedListeners.contains(listener)) {
			mPreviewChangedListeners.add(listener);
		}
	}
	
	public void removePreviewChangedListener(PreviewChangedListener listener) {
		if(mPreviewChangedListeners!=null) {
			mPreviewChangedListeners.remove(listener);
		}
	}
	
	/**
	 * 是否是广角相机
	 */
	public static boolean isXCamera() {
		return CameraConfig.position==2;
	}
	
	public static interface PreviewChangedListener{
		public void onPreviewChanged(Size size);
	}
}
