package com.xbcx.camera;

import com.xbcx.core.module.AppBaseListener;

public interface CameraServicePluginImp extends AppBaseListener{

	public CameraBasePlugin createServicePlugin();
}
