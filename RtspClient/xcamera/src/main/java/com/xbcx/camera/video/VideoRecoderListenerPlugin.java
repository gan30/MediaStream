package com.xbcx.camera.video;

import com.xbcx.camera.CameraBasePlugin;

public interface VideoRecoderListenerPlugin extends CameraBasePlugin{
	public void onRecordStart(VideoEngine recoder);
	public void onRecordEnd(VideoEngine recoder);
	public void onRecordError(VideoEngine recoder, int error);
}
