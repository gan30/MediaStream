package com.xbcx.camera;

public class PreviewTakePicture {
	
	/**
	 * @param data
	 * @param width
	 * @param height
	 * @param callBack
	 * {@link ARGBCallBack} 
	 */
	@Deprecated
	public static void takePicture(byte[] data,int width,int height,AgbCallBack callBack){
		callBack.onAgbFormat(I420toARGB(data, width, height));
	}
	
	public static void takePicture(byte[] data,int width,int height,ARGBCallBack callBack){
		callBack.onARGBFormat(I420toARGB(data, width, height),width,height);
	}
	
	public static int[] I420toARGB(byte[] yuv, int width, int height)  
    {  
        boolean invertHeight=false;  
        if (height<0)  
        {  
            height=-height;  
            invertHeight=true;  
        }  
  
        boolean invertWidth=false;  
        if (width<0)  
        {  
            width=-width;  
            invertWidth=true;  
        }  
  
        int iterations=width*height;  
        int[] rgb = new int[iterations];  
  
        for (int i = 0; i<iterations;i++)  
        {  
            int nearest = (i/width)/2 * (width/2) + (i%width)/2;  
  
            int y = yuv[i] & 0x000000ff;  
            int u = yuv[iterations+nearest] & 0x000000ff;  
            int v = yuv[iterations + iterations/4 + nearest] & 0x000000ff;  
  
            int b = (int)(y+1.8556*(u-128));  
  
            int g = (int)(y - (0.4681*(v-128) + 0.1872*(u-128)));  
  
            int r = (int)(y+1.5748*(v-128));  
  
            if (b>255){b=255;}  
            else if (b<0 ){b = 0;}  
            if (g>255){g=255;}  
            else if (g<0 ){g = 0;}  
            if (r>255){r=255;}  
            else if (r<0 ){r = 0;}  
  
            int targetPosition=i;  
  
            if (invertHeight)  
            {  
                targetPosition=((height-1)-targetPosition/width)*width   +   (targetPosition%width);  
            }  
            if (invertWidth)  
            {  
                targetPosition=(targetPosition/width)*width    +    (width-1)-(targetPosition%width);  
            }  
  
  
            rgb[targetPosition] =  (0xff000000) | (0x00ff0000 & r << 16) | (0x0000ff00 & g << 8) | (0x000000ff & b);  
        }  
        return rgb;  
    }  
	
	@Deprecated
	public static interface AgbCallBack{
		public void onAgbFormat(int[] argb);
	}
	
	public static interface ARGBCallBack{
		public void onARGBFormat(int[] argb, int width, int height);
	}
}
