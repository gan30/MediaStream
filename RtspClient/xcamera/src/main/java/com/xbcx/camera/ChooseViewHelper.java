package com.xbcx.camera;

import com.xbcx.core.BaseActivity;
import com.xbcx.utils.SystemUtils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;

import gan.camera.R;

public class ChooseViewHelper {
	
	BaseActivity	mActivity;
	View			mChooseView;
	
	public ChooseViewHelper(BaseActivity activity) {
		mActivity = activity;
	}

	public View showChooseView(final Bitmap bitmap,boolean isVideo){
		if(mChooseView == null){
			mChooseView = SystemUtils.inflate(mActivity, R.layout.layout_choose);
			
			LayoutParams lParams = new LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			mActivity.addContentView(mChooseView, lParams);
			
			if(isVideo){
				mChooseView.findViewById(R.id.thumbVideo).setVisibility(View.VISIBLE);
			}else{
				mChooseView.findViewById(R.id.thumbVideo).setVisibility(View.GONE);
			}
			if(bitmap!=null){
				final ImageView ivPic = (ImageView) mChooseView.findViewById(R.id.thumbPicture);
				ivPic.setImageBitmap(bitmap);
				ivPic.setTag(bitmap);
			}
		}else{
			mChooseView.setVisibility(View.VISIBLE);
		}
		return mChooseView;
	}
	
	public void hideChooseView(){
		if(mChooseView!=null){
			mChooseView.setVisibility(View.GONE);
		}
	}
}
