package com.xbcx.camera;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.xbcx.core.FilePaths;

import android.annotation.SuppressLint;
import android.os.Environment;

public class CameraFile {
	
	public static SimpleDateFormat mFormat;
	
	public static final String DCIM_FilePath =
            Environment.getExternalStorageDirectory().toString()
            +"/"+"DCIM";
	
	private static String newPictureName() {
		return "IMG_"+formatFile(".jpg");
	}
	
	public static String generateCameraFilePath(){
		return FilePaths.getCameraTempFolderPath() + newPictureName();
	}
	
	@Deprecated
	public static String generateCameraTempPath(){
		return FilePaths.getCameraTempFolderPath() + newPictureName();
	}
	
	public static String generateCameraTempFilePath(){
		return FilePaths.getCameraTempFolderPath() + "camera_temp.jpg";
	}
	
	public static String generateVideoFilePath() {
		return generateVideoFilePath(".mp4");
	}
	
	public static String generateVideoFilePath(String suffix) {
		return DCIM_FilePath+"/video"+ "/VID_"+formatFile(suffix);
	}

	public static String generatePictureFilePath() {
		return generatePictureFilePath(".jpg");
	}
	
	public static String generatePictureFilePath(String suffix) {
		return DCIM_FilePath+"/picture"+ "/IMG_"+formatFile(suffix);
	}
	
	@SuppressLint("SimpleDateFormat")
	public static String formatFile(String suffix) {
		if(mFormat == null) {
			mFormat = new SimpleDateFormat("yyMMdd_HHmmss");
		}
		return mFormat.format(new Date())+suffix;
	}
}
