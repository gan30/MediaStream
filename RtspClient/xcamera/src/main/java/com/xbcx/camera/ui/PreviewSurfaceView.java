package com.xbcx.camera.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.ViewGroup.LayoutParams;

public class PreviewSurfaceView extends ExSurfaceView implements CameraView{
	
	float 	mRatio = -1;
	int 	mMeasuredWidth = -1;
	
	public PreviewSurfaceView(Context context) {
        super(context);
    }
	
	public PreviewSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

	public void setRatio(float ratio) {
		this.mRatio = ratio;
		resizeView();
	}
	
	public boolean isLandscape() {
		return getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		if(mRatio!=-1) {
			if(isLandscape()) {
				heightMeasureSpec = (int)(getMeasuredWidth()/mRatio);
			}else {
				heightMeasureSpec = (int)(getMeasuredWidth()*mRatio);
			}
			setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
		}
		mMeasuredWidth = getMeasuredWidth();
	}
	
	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		mMeasuredWidth = -1;
	}
	
	public void resizeView() {
		mMeasuredWidth = getMeasuredWidth();
		if(mMeasuredWidth!=-1 && mMeasuredWidth>10) {
			LayoutParams lp = getLayoutParams();
			if (isLandscape()) {
				lp.height = (int) (mMeasuredWidth / mRatio);
			} else {
				lp.height = (int) (mMeasuredWidth * mRatio);
			}
			setLayoutParams(lp);
		}
	}
}
