package com.xbcx.util;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;

@SuppressLint("NewApi")
public class StorageUtils {
	
	public static boolean isExternalAvailable(){
		return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}
	
	public static long getExternalStorageAvailableSize(String folder) {
		if (isExternalAvailable()) {
			StatFs statfs = new StatFs(folder);
			long availableBlocks = 0;
			long blockSize = 0;
			if (Build.VERSION.SDK_INT >= 18) {
				availableBlocks = statfs.getAvailableBlocksLong();
				blockSize = statfs.getBlockSizeLong();
			} else {
				availableBlocks = (long) statfs.getAvailableBlocks();
				blockSize = (long) statfs.getBlockSize();
			}
			return availableBlocks * blockSize;
		}
		return -1;
	}
	
	public static long getExternalStorageAvailableSizeEx(String folder) {
		return getExternalStorageAvailableSize(folder,1048576*1024);
	}
	
	public static long getExternalStorageAvailableSize(String folder,long size) {
		if (isExternalAvailable()) {
			StatFs statfs = new StatFs(folder);
			long availableBlocks = 0;
			long blockSize = 0;
			if (Build.VERSION.SDK_INT >= 18) {
				availableBlocks = statfs.getAvailableBlocksLong();
				blockSize = statfs.getBlockSizeLong();
			} else {
				availableBlocks = (long) statfs.getAvailableBlocks();
				blockSize = (long) statfs.getBlockSize();
			}
			return Math.max(availableBlocks * blockSize - size, 0);
		}
		return -1;
	}
}
