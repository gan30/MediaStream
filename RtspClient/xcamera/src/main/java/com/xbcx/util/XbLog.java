package com.xbcx.util;

import com.xbcx.core.FileLogger;
import com.xbcx.utils.SystemUtils;

import android.util.Log;

public class XbLog {
	
	public static void i(String tag,String msg){
		final String log = tag+":"+msg;
		Log.i(tag, log);
		FileLogger.getInstance("camera_i").log(log);
	}
	
	public static void d(String tag,String msg){
		if(SystemUtils.isDebug()) {
			final String log = tag+":"+msg;
			Log.i(tag, log);
		}
	}
	
}
