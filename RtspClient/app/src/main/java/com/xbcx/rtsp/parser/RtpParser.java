package com.xbcx.rtsp.parser;

import com.xbcx.rtsp.Errors;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import gan.media.utils.XLog;

/**
 * Created by gan on 2018\3\28 0028.
 */
public class RtpParser {

    private final static String tag = "RtpParser";

    static {
        System.loadLibrary("RtspClient");
    }

    InputStream  mInputStream;
    boolean     mRuning;
    byte[]      mReadBuffer;

    ParserListener  mParserListener;

    public RtpParser(){
        mReadBuffer = new byte[1024];
    }

    public void setParserListener(ParserListener parserListener) {
        this.mParserListener = parserListener;
    }

    public void setInputStream(InputStream inputStream) {
        this.mInputStream = inputStream;
    }

    public void start(){
        new Thread(){
            @Override
            public void run() {
                super.run();
                mRuning = true;
                byte[] channel = new byte[4];
                try{
                    while (mRuning) {
                        int len = 0;
                        try {
                            len = mInputStream.read(channel);
                        } catch (IOException e) {
                            e.printStackTrace();
                            onError(Errors.Error_Socket, "read io exception");
                        }
                        if (len == 4) {
                            paser(RtpParser.this, channel, len);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    onError(2,"paser exception");
                }
            }
        }.start();
    }

    protected  void onError(int code,String message){
        if(mParserListener!=null){
            mParserListener.onError(code,message);
        }
    }

    public void stop(){
        mRuning = false;
    }

    public static native void paser(RtpParser rtpParser,byte[] channel,int len);

    public void onChannel(int channel,int packetLen)throws Exception{
        ByteBuffer rtpPacket = ByteBuffer.allocate(packetLen);
        int len = 0;
        int temp = 0;
        while (len<packetLen){
            int readLen = packetLen - len;
            if(readLen>=1024){
                int rLen = mInputStream.read(mReadBuffer);
                if(rLen>0){
                    len+=rLen;
                    rtpPacket.put(mReadBuffer,0, rLen);
                    temp += rLen;
                }
            }else{
                int len2 = 0;
                while(len2<readLen){
                    int rLen = mInputStream.read(mReadBuffer,0, readLen-len2);
                    if(rLen>0){
                        len2+=rLen;
                        rtpPacket.put(mReadBuffer,0, rLen);
                        temp += rLen;
                    }
                }
                break;
            }
        }
        XLog.i(tag,"temp："+temp);
        paserRtp(this,channel,rtpPacket.array(),packetLen);
    }

    public static native void paserRtp(RtpParser rtpParser,int channel,byte[] rtpPacket,int packetLen);

    public void onFrame(int channel,byte[] frame,int len,byte[] buf){
       if(mParserListener!=null){
           mParserListener.onFrame(channel,frame,len,buf);
       }
    }
}
