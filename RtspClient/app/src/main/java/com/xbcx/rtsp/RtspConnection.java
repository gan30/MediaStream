package com.xbcx.rtsp;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.URI;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;

public class RtspConnection{

	private final static String TAG = "RtspConnection";

	static {
		System.loadLibrary("RtspClient");
	}

	Socket	mSocket;
	String 	mHost;
	int 	mPort;
	String  mUrl;

	BufferedReader	mBufferedReader;
	OutputStream	mOutputStream;
	InputStream		mInputStream;

	private int 		mCSeq;
	private String 		mSessionID;
	private String 		mAuthorization;
	private long 		mTimestamp;
	private String  	mOrigin = "127.0.0.1";
	private String  	mDestination = mOrigin;
	String 				mUserName="",mPassword="";

	private String		mAgent;

	public RtspConnection(String url){
		mUrl = url;
		URI uri = URI.create(url);
		mHost = uri.getHost();
		mPort = uri.getPort();
		if(mPort == -1){
		    mPort = 80;
        }

		long uptime = System.currentTimeMillis();
		mTimestamp = uptime/1000;
	}

	public void setUser(String userName,String password) {
		this.mUserName = userName;
		mPassword = password;
	}

	public void setAgent(String agent) {
		this.mAgent = agent;
	}

	public void connect() throws UnknownHostException, IOException {
		mSocket = new Socket(mHost, mPort);
		mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream=mSocket.getInputStream()));
		mOutputStream = mSocket.getOutputStream();
	}
	
	public void close() throws IOException {
		try {
			sendRequestTeardown();
		} catch (Exception ignore) {
		}
		
		try {
			if(mSocket!=null) {
				mSocket.close();
			}
		} finally {
			try {
				if(mBufferedReader!=null) {
					mBufferedReader.close();
				}
			} finally {
				if(mOutputStream!=null) {
					mOutputStream.close();
				}
			}
		}
		destoryNative();
	}
	
	public void setCSeq(int cseq) {
		this.mCSeq = cseq;
	}

	public void addCsep(){
		mCSeq++;
	}

	private String addHeaders() {  
        return "CSeq: " + (++mCSeq) + "\r\n" +  
                (mSessionID != null ? "Session: " + mSessionID + "\r\n" :"") +
                "User-Agent: " + mAgent + "\r\n" +
                (mAuthorization != null ? "Authorization: " + mAuthorization +  "\r\n\r\n":"\r\n");  
    }
	
	/** 
     * Forges and sends the OPTIONS request  
     */  
    public void sendRequestOption() throws IOException {  
        String request = "OPTIONS " + mUrl + " RTSP/1.0\r\n" + addHeaders();
        Log.i(TAG,request.substring(0, request.indexOf("\r\n")));  
        mOutputStream.write(request.getBytes("UTF-8"));  
        Response.parseResponse(mBufferedReader);  
    }

	public void sendRequestDESCRIBE() throws IOException {
		String request = "DESCRIBE "+ mUrl +" RTSP/1.0\r\n"+
				"CSeq: " + (++mCSeq) + "\r\n" +
				"User-Agent: " + mAgent + "\r\n" +
				"Accept: "+"application/sdp"+"\r\n\r\n";
		mOutputStream.write(request.getBytes("UTF-8"));
		Response.parseResponse(mBufferedReader);
	}

    public void setDestination(String destination) {
		this.mDestination = destination;
	}
    
    /** 
	 * The origin address of the session.
	 * It appears in the sessionn description.
	 * @param origin The origin address
	 */
	public void setOrigin(String origin) {
		mOrigin = origin;
	}
	
    public String getSessionDescription() {
		StringBuilder sessionDescription = new StringBuilder();
		if (mDestination==null) {
			throw new IllegalStateException("setDestination() has not been called !");
		}
		sessionDescription.append("v=0\r\n");
		// TODO: Add IPV6 support
		sessionDescription.append("o=- "+mTimestamp+" "+mTimestamp+" IN IP4 "+mOrigin+"\r\n");
		sessionDescription.append("s=EasyDarwin\r\n");
		sessionDescription.append("i=EasyDarwin\r\n");
		sessionDescription.append("c=IN IP4 "+mOrigin+"\r\n");
		// t=0 0 means the session is permanent (we don't know when it will stop)
		sessionDescription.append("t=0 0\r\n");
		// Prevents two different sessions from using the same peripheral at the same time
		
		sessionDescription.append("a=x-qt-text-nam:EasyDarwin"+"\r\n");
		sessionDescription.append("a=x-qt-text-inf:EasyDarwin"+"\r\n");
		sessionDescription.append("a=x-qt-text-cmt:source application::EasyDarwin"+"\r\n");
		sessionDescription.append("a=x-qt-text-aut:"+"\r\n");
		sessionDescription.append("a=x-qt-text-cpy:"+"\r\n");
		
		//video
		String videoSessionDescription = "m=video "+ 0 +" RTP/AVP 96\r\n" +
		"a=rtpmap:96 H264/90000\r\n" +
		"a=fmtp:96 packetization-mode=1;sprop-parameter-sets="+" "+"\r\n";
		sessionDescription.append(videoSessionDescription);
		sessionDescription.append("a=control:trackID="+1+"\r\n");
		
		//audio
		String audioSessionDescription = "m=audio "+ 0 +" RTP/AVP 97\r\n" +
				"a=rtpmap:97 mpeg4-generic/8000/1"+"\r\n"+
				"a=fmtp:97 streamtype=5; profile-level-id=1; mode=AAC-hbr; SizeLength=13; IndexLength=3; IndexDeltaLength=3; config=1588\r\n";
		sessionDescription.append(audioSessionDescription);
		sessionDescription.append("a=control:trackID="+2+"\r\n");
		
		return sessionDescription.toString();
	}

    public void sendRequestAnnounce() throws IllegalStateException, SocketException, IOException {  
        String body = getSessionDescription();  
        String request = "ANNOUNCE "+ mUrl +" RTSP/1.0\r\n" +
        		"CSeq: " + (++mCSeq) + "\r\n" +  
        		"User-Agent: " + mAgent + "\r\n" +
                "Content-Type: application/sdp\r\n" +  
                "Content-Length: " + body.length() + "\r\n\r\n" +  
                body;   
        
        Log.i(TAG,request.substring(0, request.indexOf("\r\n")));  
  
        mOutputStream.write(request.getBytes("UTF-8"));  
        Response response = Response.parseResponse(mBufferedReader);  
  
        if (response.headers.containsKey("server")) {  
            Log.i(TAG,"RTSP server name:" + response.headers.get("server"));  
        } else {  
            Log.i(TAG,"RTSP server name unknown");  
        }  
  
        if (response.status == 401) {  
            String nonce, realm;  
            Matcher m;  
  
            if (mUserName == null || mPassword == null) throw new IllegalStateException("Authentication is enabled and setCredentials(String,String) was not called !");  
  
            try {  
                m = Response.rexegAuthenticate.matcher(response.headers.get("www-authenticate")); m.find();  
                nonce = m.group(2);  
                realm = m.group(1);  
            } catch (Exception e) {  
                throw new IOException("Invalid response from server");  
            }  
  
            String uri = mUrl;
            String hash1 = computeMd5Hash(mUserName+":"+m.group(1)+":"+mPassword);  
            String hash2 = computeMd5Hash("ANNOUNCE"+":"+uri);  
            String hash3 = computeMd5Hash(hash1+":"+m.group(2)+":"+hash2);  
  
            mAuthorization = "Digest username=\""+mUserName+"\",realm=\""+realm+"\",nonce=\""+nonce+"\",uri=\""+uri+"\",response=\""+hash3+"\"\r\n";  
  
            request = "ANNOUNCE "+ mUrl +" RTSP/1.0\r\n" +
                    "CSeq: " + (++mCSeq) + "\r\n" +  
                    "Content-Type: application/sdp"+ "\r\n" +  
                    "Content-Length: " + body.length() + "\r\n" +  
                    "Authorization: " + mAuthorization +  
                    "Session: " + mSessionID + "\r\n" +  
                    "User-Agent: " + mAgent + "\r\n\r\n" +
                    body+ "\r\n\r\n";  
  
            Log.i(TAG,request);  
  
            mOutputStream.write(request.getBytes("UTF-8"));  
            response = Response.parseResponse(mBufferedReader);  
  
            if (response.status == 401) throw new RuntimeException("Bad credentials !");  
  
        } else if (response.status == 403) {  
            throw new RuntimeException("Access forbidden !");  
        }  
    } 
    
    /**
	 * 推流setup
     * Forges and sends the SETUP request  
     */  
    public boolean sendRequestSetup() throws IllegalStateException, IOException {
    	int index = 0;
        for (int i= 1;i<3;i++) {  
            int trackId = i;  
            String interleaved = index+"-"+(++index);
            index++;
            String request = "SETUP "+ mUrl +"/trackID="+trackId+" RTSP/1.0\r\n"
            		+ "Transport: RTP/AVP/TCP;unicast;mode=record;interleaved="+interleaved+"\r\n"
            		+ addHeaders(); 

            mOutputStream.write(request.getBytes("UTF-8"));
            
            Response response = Response.parseResponse(mBufferedReader);//  
            if (i == 1){  
            	try {   
                    mSessionID = response.headers.get("session").trim();  
                    Log.i(TAG,"mSessionID: "+ mSessionID+ "response.status:"+response.status);  
                } catch (Exception e) {  
                    throw new IOException("Invalid response from server. Session id: "+mSessionID);  
                }  
            }  
            if (response.status != 200){  
                Log.i(TAG,"return for resp :" +response.status);  
                return false;  
            }  
        }  
        return true;  
    }

	/**
	 * 拉流
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public boolean sendRequestSetup2() throws IllegalStateException, IOException {
		int index = 0;
		for (int i= 1;i<3;i++) {
			int trackId = i;
			String interleaved = index+"-"+(++index);
			index++;
			String request = "SETUP "+ mUrl +"/trackID="+trackId+" RTSP/1.0\r\n"
					+ "Transport: RTP/AVP/TCP;unicast;interleaved="+interleaved+"\r\n"
					+ addHeaders();

			mOutputStream.write(request.getBytes("UTF-8"));

			Response response = Response.parseResponse(mBufferedReader);//
			if (i == 1){
				try {
					mSessionID = response.headers.get("session").trim();
					Log.i(TAG,"mSessionID: "+ mSessionID+ "response.status:"+response.status);
				} catch (Exception e) {
					throw new IOException("Invalid response from server. Session id: "+mSessionID);
				}
			}
			if (response.status != 200){
				Log.i(TAG,"return for resp :" +response.status);
				return false;
			}
		}
		return true;
	}
    
    /** 
     * Forges and sends the RECORD request  
     */  
    public boolean sendRequestPlay() throws IllegalStateException, SocketException, IOException {  
        String request = "PLAY "+ mUrl +" RTSP/1.0\r\n" +
                "Range: npt=0.000-\r\n" +
				"Scale: 0.000000\r\n" +
				addHeaders();
        Log.i(TAG,request.substring(0, request.indexOf("\r\n")));  
        mOutputStream.write(request.getBytes("UTF-8"));  
        Response resp = Response.parseResponse(mBufferedReader);  
        if (resp.status != 200){  
            return false;  
        }  
        return true;
    }  
    
    /**
	 * Forges and sends the TEARDOWN request 
	 */
	public void sendRequestTeardown() throws IOException {
		String request = "TEARDOWN "+ mUrl +" RTSP/1.0\r\n" + addHeaders();
		Log.i(TAG,request.substring(0, request.indexOf("\r\n")));
		mOutputStream.write(request.getBytes("UTF-8"));
	}

	public InputStream getInputStream() {
		return mInputStream;
	}

    final protected static char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

	private static String bytesToHex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for ( int j = 0; j < bytes.length; j++ ) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}

	/** Needed for the Digest Access Authentication. */
	private String computeMd5Hash(String buffer) {
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			return bytesToHex(md.digest(buffer.getBytes("UTF-8")));
		} catch (NoSuchAlgorithmException ignore) {
		} catch (UnsupportedEncodingException e) {}
		return "";
	}

    public void sendRTP(byte[] data,int len,long timesample,int playLoadType) throws IOException {
		sendRtpPacket(this,data,len,timesample,playLoadType);
    }

	public void sendPacket(byte[] packet) throws IOException {
		mOutputStream.write(packet);
	}

	public void reconnect(int timeout) throws IOException {
		SocketAddress address = new InetSocketAddress(mHost,mPort);
		mSocket.connect(address,timeout);
	}

	public static native void sendRtpPacket(RtspConnection connection,byte[] data,int len,long timesample,int playLoadType);

	public static native void destoryNative();
}