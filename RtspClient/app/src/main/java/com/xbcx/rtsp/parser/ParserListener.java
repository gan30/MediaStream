package com.xbcx.rtsp.parser;

/**
 * Created by Administrator on 2018\3\28 0028.
 */

public interface ParserListener {
    public void onFrame(int channel,byte[] frame,int len,byte[] buf);
    public void onError(int code,String message);
}
