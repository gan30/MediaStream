package gan.media;

import android.os.Process;
import android.view.Surface;

import com.xbcx.media.video.VideoDecoder;
import com.xbcx.rtsp.Errors;
import com.xbcx.rtsp.RtspConnection;
import com.xbcx.rtsp.parser.ParserListener;
import com.xbcx.rtsp.parser.RtpParser;

import org.easydarwin.audio.AudioPlayer;

import java.nio.ByteBuffer;
import java.util.Vector;

import gan.media.utils.XLog;

/**
 * Created by Administrator on 2018\3\29 0029.
 */

public class Player implements ParserListener{

    final static String TAG = "Player";

    RtspConnection      mConnection;
    RtpParser           mRtpParser;
    AudioPlayer         mAudioPlayer;
    Surface             mSurface;

    OnPlayerListener    mOnPlayerListener;

    public void setOnPlayerListener(OnPlayerListener onPlayerListener) {
        this.mOnPlayerListener = onPlayerListener;
    }

    public void setSurface(Surface surface) {
        this.mSurface = surface;
    }

    public void start(){
        new Thread(){
            @Override
            public void run() {
                super.run();
//                final String rtsp = "rtsp://184.72.239.149/vod/mp4://BigBuckBunny_175k.mov";
                final String rtsp = "rtsp://192.168.80.234:554/800.sdp";
                mConnection = new RtspConnection(rtsp);
                mConnection.setAgent("XPlayer");
                try {
                    mConnection.connect();
                    mConnection.sendRequestOption();
                    mConnection.sendRequestDESCRIBE();
                    if (mConnection.sendRequestSetup2()) {
                        mConnection.sendRequestPlay();
                    }

                    startAudio();
                    mRtpParser = new RtpParser();
                    mRtpParser.setParserListener(Player.this);
                    mRtpParser.setInputStream(mConnection.getInputStream());
                    mRtpParser.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void onPlayer(int code,String msg){
        if(mOnPlayerListener!=null){
            mOnPlayerListener.onPlayer(code,msg);
        }
    }

    public void stop(){
        if(mConnection!=null){
            try {
                stopAudio();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                stopVideo();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(mRtpParser!=null){
                mRtpParser.stop();
            }

            try{
                mConnection.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    ByteBuffer mCSD0;
    ByteBuffer mCSD1;

    @Override
    public void onFrame(int channel, byte[] frame, int len,byte[] buf) {
        if(channel == 2){
            if(mFrameCaches.size()>=10){
                mFrameCaches.remove(0);
            }
            mFrameCaches.add(new BufferFrame(frame,len));
        }else{
            if(mCSD0==null||mCSD1 == null){
                byte[] dataOut = new byte[128];
                int[] outLen = new int[]{128};
                int result = getXPS(frame, 0, len, dataOut, outLen, 7);
                if (result >= 0) {
                    ByteBuffer csd0 = ByteBuffer.allocate(outLen[0]);
                    csd0.put(dataOut, 0, outLen[0]);
                    csd0.clear();
                    mCSD0 = csd0;
                    XLog.i(TAG, String.format("CSD-0 searched"));
                }
                outLen[0] = 128;
                result = getXPS(frame, 0, len, dataOut, outLen, 8);
                if (result >= 0) {
                    ByteBuffer csd1 = ByteBuffer.allocate(outLen[0]);
                    csd1.put(dataOut, 0, outLen[0]);
                    csd1.clear();
                    mCSD1 = csd1;
                    XLog.i(TAG, String.format("CSD-1 searched"));
                }
                if(mCSD0!=null&&mCSD1!=null){
                    startVideo();
                }
            }

            VideoFrame videoFrame = new VideoFrame(frame,len);
            if(buf!=null&&buf.length>0){
                ByteBuffer byteBuffer = ByteBuffer.wrap(buf);
                byteBuffer.getInt();
                videoFrame.timeStamp = byteBuffer.getInt();
            }

            if(mVideoFrameCaches.size()>=10){
                mVideoFrameCaches.remove(0);
            }
            mVideoFrameCaches.add(videoFrame);
        }
    }

    @Override
    public void onError(int code, String message) {
        onPlayer(code,message);
        if(code == Errors.Error_Socket){
            onConnectionException(code,message);
        }
    }

    protected void onConnectionException(int code, String message){
        reconnect();
    }

    volatile  boolean mIsReconnecting;
    private void reconnect(){
        if(mIsReconnecting){
            return;
        }
        new Thread(){
            @Override
            public void run() {
                super.run();
                try{
                    mConnection.reconnect(10000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                mIsReconnecting = false;
            }
        }.start();
        mIsReconnecting = true;
    }

    private Vector<BufferFrame> mFrameCaches = new Vector<>();
    Thread mAudioThread;
    private void startAudio() {
        mAudioThread = new Thread(){
            @Override
            public void run() {
                super.run();
                try{
                    mAudioPlayer = new AudioPlayer();
                    mAudioPlayer.start();
                    while (mAudioThread!=null){
                        if(!mFrameCaches.isEmpty()){
                            BufferFrame frame = mFrameCaches.remove(0);
                            mAudioPlayer.onAudioFrame(frame.buffer,frame.length);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if(mAudioPlayer!=null){
                        mAudioPlayer.stop();
                    }
                }
            }
        };
        mAudioThread.start();
    }

    public void stopAudio(){
        try {
            Thread t = mAudioThread;
            mAudioThread = null;
            if (t != null) {
                t.interrupt();
                t.join();
            }
        } catch (InterruptedException e) {
            e.fillInStackTrace();
        }
    }

    public static class BufferFrame{
        byte[] buffer;
        int length;
        public BufferFrame(byte[] data,int len){
            buffer = data;
            length = len;
        }
    }

    public static class VideoFrame extends  BufferFrame{
        long timeStamp;
        public VideoFrame(byte[] data, int len) {
            super(data, len);
        }
    }

    private Vector<VideoFrame> mVideoFrameCaches = new Vector<>();
    Thread mVideoThread;
    public void startVideo(){
        if(mVideoThread!=null){
            return;
        }
        mVideoThread = new Thread(){
            @Override
            public void run() {
                super.run();
                Process.setThreadPriority(Process.THREAD_PRIORITY_DISPLAY);
                VideoDecoder decoder = null;
                try{
                    decoder = new VideoDecoder();
                    decoder.setCSD(mCSD0,mCSD1);
                    decoder.setSurface(mSurface);
                    decoder.start(640,480);
                    while (mVideoThread!=null){
                        if(!mVideoFrameCaches.isEmpty()){
                            VideoFrame frame = mVideoFrameCaches.remove(0);
                            decoder.input(frame.buffer,0,frame.length,frame.timeStamp);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if(decoder!=null){
                        decoder.release();
                    }
                }
            }
        };
        mVideoThread.start();
    }

    public void stopVideo(){
        try {
            Thread t = mVideoThread;
            mVideoThread = null;
            if (t != null) {
                t.interrupt();
                t.join();
            }
        } catch (InterruptedException e) {
            e.fillInStackTrace();
        }
    }

    private static int getXPS(byte[] data, int offset, int length, byte[] dataOut, int[] outLen, int type) {
        int i;
        int pos0;
        int pos1;
        pos0 = -1;
        for (i = offset; i < length - 4; i++) {
            if ((0 == data[i]) && (0 == data[i + 1]) && (1 == data[i + 2]) && (type == (0x0F & data[i + 3]))) {
                pos0 = i;
                break;
            }
        }
        if (-1 == pos0) {
            return -1;
        }
        if (pos0 > 0 && data[pos0-1] == 0){ // 0 0 0 1
            pos0 = pos0-1;
        }
        pos1 = -1;
        for (i = pos0 + 4; i < length - 4; i++) {
            if ((0 == data[i]) && (0 == data[i + 1]) && (1 == data[i + 2])) {
                pos1 = i;
                break;
            }
        }
        if (-1 == pos1 || pos1 == 0) {
            return -2;
        }
        if (data[pos1 - 1] == 0) {
            pos1 -= 1;
        }
        if (pos1 - pos0 > outLen[0]) {
            return -3; // 输入缓冲区太小
        }
        dataOut[0] = 0;
        System.arraycopy(data, pos0, dataOut, 0, pos1 - pos0);
        // memcpy(pXPS+1, pES+pos0, pos1-pos0);
        // *pMaxXPSLen = pos1-pos0+1;
        outLen[0] = pos1 - pos0 ;
        return pos1;
    }

    public static interface OnPlayerListener{
        public void onPlayer(int code,String msg);
    }
}
