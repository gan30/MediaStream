package gan.media;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import gan.media.rtsp.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.pusher).setOnClickListener(this);
        findViewById(R.id.player).setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        final int id = v.getId();
        if(id == R.id.pusher){
            startPusher();
        }else if(id == R.id.player){
            startPlayer();
        }
    }

    public void startPusher(){
        Intent intent = new Intent(this,PusherActivity.class);
//        Intent intent = new Intent(this,TestPreviewVideo.class);
        startActivity(intent);
    }

    public void startPlayer(){
        Intent intent = new Intent(this,PlayerActivity.class);
        startActivity(intent);
    }
}
