package gan.media;

import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;

import com.xbcx.camera.CameraService;
import com.xbcx.camera.CameraServiceActivity;
import com.xbcx.camera.preview.PreviewVideoEngine;
import com.xbcx.camera.video.VideoEngine;
import com.xbcx.camera.video.VideoRecoderListener;

public class PusherActivity extends CameraServiceActivity{

    final static String TAG = "PusherActivity";

    XPusher             mPusher;
    PreviewVideoEngine  mPreviewVideoEngine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        findViewById(R.id.picture).setVisibility(View.GONE);
        findViewById(R.id.video).setVisibility(View.VISIBLE);

        registerPlugin(mPusher = new XPusher());
        mPreviewVideoEngine= new PreviewVideoEngine(this);
        mPreviewVideoEngine.setAudioCodecFrameListener(mPusher);
        mPreviewVideoEngine.setVideoCodecFrameListener(mPusher);
        mPreviewVideoEngine.addVideoRecoderListener(new VideoRecoderListener() {
            @Override
            public void onRecordStart(VideoEngine recoder) {
                mPusher.start();
            }

            @Override
            public void onRecordEnd(VideoEngine recoder) {
                mPusher.stop();
            }

            @Override
            public void onRecordError(VideoEngine recoder, int error) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mPreviewVideoEngine!=null){
            mPreviewVideoEngine.release();
        }
    }

    @Override
    public void onUpdateParameters(Camera.Parameters parameters) {
        super.onUpdateParameters(parameters);
        parameters.setPreviewSize(640,480);
    }

    @Override
    protected void onPauseCamera() {
        stopPush();
        super.onPauseCamera();
    }

    @Override
    public void onCameraPerpared() {
        super.onCameraPerpared();
        startPush();
    }

    @Override
    protected void onAttachService(CameraService service) {
        super.onAttachService(service);
        getVideoCamera().setVideoEngine(mPreviewVideoEngine);
    }

    @Override
    protected void onDeathService(CameraService service) {
        super.onDeathService(service);
    }

    public void startPush(){
        getVideoCamera().startVideo();
    }

    public void stopPush(){
        getVideoCamera().stopVideo();
    }
}
