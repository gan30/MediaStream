package gan.media;

import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.xbcx.camera.XCamera;
import com.xbcx.camera.XCameraListener;
import com.xbcx.camera.ui.CameraView;
import com.xbcx.rtsp.RtspConnection;


/**
 * Created by Administrator on 2018\3\29 0029.
 */

public class Pusher implements SurfaceHolder.Callback,XCameraListener,XCamera.PreviewChangedListener,XCamera.UpdateCameraParameters,Camera.PreviewCallback{

    XCamera         mCamera = XCamera.get();

    RtspConnection  mConnection;
    SurfaceView     mSurfaceView;

    public Pusher(SurfaceView surfaceView){
        mSurfaceView = surfaceView;
        mSurfaceView.getHolder().addCallback(this);
    }

    protected  void initCamera(){
        mCamera.addCameraListener(this);
        mCamera.addUpdateCameraParameters(this);
        mCamera.addPreviewChangedListener(this);
    }

    protected  void destroyCamera(){
        mCamera.removeCameraListener(this);
        mCamera.removeUpdateCameraParameters(this);
        mCamera.removePreviewChangedListener(this);
        mCamera.onDestory();
    }

    public void destroy(){
        destroyCamera();
    }

    public void startPreviewCallBack(){
        mCamera.addPreviewCallBack(this);
        mCamera.startPreviewCallback();
    }

    public void stopPreviewCallBack(){
        mCamera.removePreviewCallBack(this);
        mCamera.stopPreviewCallBack();
    }

    public void start(){
        new Thread(){
            @Override
            public void run() {
                super.run();
                final String rtsp = "rtsp://192.168.80.106:554/50000482.sdp";
                mConnection = new RtspConnection(rtsp);
                try {
                    mConnection.setAgent("PusherClient");
                    mConnection.connect();
                    mConnection.sendRequestAnnounce();
                    if (mConnection.sendRequestSetup()) {
                        mConnection.sendRequestPlay();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void stop(){
        if(mConnection!=null){
            try{
                mConnection.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mCamera.setPreviewSurface(holder);
        mCamera.setDisplayOrientation(90);
        mCamera.onResume();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mCamera.onPause();
    }

    @Override
    public void onPreviewChanged(Camera.Size size) {
        if(mSurfaceView instanceof CameraView){
            ((CameraView)mSurfaceView).setRatio(size.width/size.height);
        }
    }

    @Override
    public void onUpdateParameters(Camera.Parameters parameters) {
        parameters.setPreviewSize(640,480);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
    }

    @Override
    public void onCameraOpend(Camera camera) {
        mCamera.startPreview();
    }

    @Override
    public void onCameraClose() {

    }

    @Override
    public void onCameraError(int error) {

    }
}
