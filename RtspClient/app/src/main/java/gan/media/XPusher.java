package gan.media;

import android.media.MediaCodec;
import android.media.MediaFormat;

import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.ToastManager;
import com.xbcx.media.audio.AudioCodec;
import com.xbcx.media.video.VideoCodec;
import com.xbcx.rtsp.RtspConnection;

import java.io.IOException;
import java.nio.ByteBuffer;

import gan.media.utils.XLog;

public class XPusher extends ActivityPlugin<PusherActivity> implements AudioCodec.AudioCodecFrameListener,VideoCodec.VideoCodecFrameListener{

    final static String TAG  = "XPusher";

    public static final int[] AUDIO_SAMPLING_RATES = {96000, // 0
            88200, // 1
            64000, // 2
            48000, // 3
            44100, // 4
            32000, // 5
            24000, // 6
            22050, // 7
            16000, // 8
            12000, // 9
            11025, // 10
            8000, // 11
            7350, // 12
            -1, // 13
            -1, // 14
            -1, // 15
    };

    RtspConnection       mConnection;
    private int         samplingRate = 8000;
    int                  mSamplingRateIndex = 0;

    public XPusher(){
        int i = 0;
        for (; i < AUDIO_SAMPLING_RATES.length; i++) {
            if (AUDIO_SAMPLING_RATES[i] == samplingRate) {
                mSamplingRateIndex = i;
                break;
            }
        }
    }

    byte[] mPpsSps;
    byte[] h264;
    public void start(){
        int width = 640,height = 480;
        mPpsSps = new byte[0];
        h264 = new byte[width * height];
        mActivity.showXProgressDialog("推流连接中...");
        new Thread(){
            @Override
            public void run() {
                super.run();
                final String rtsp = "rtsp://192.168.80.234:554/800.sdp";
                mConnection = new RtspConnection(rtsp);
                try {
                    mConnection.setAgent("PusherClient");
                    mConnection.connect();
                    mConnection.sendRequestAnnounce();
                    if (mConnection.sendRequestSetup()) {
                        mConnection.sendRequestPlay();
                    }
                    mActivity.post(new Runnable() {
                        @Override
                        public void run() {
                            onPushSucesses();
                        }
                    });
                } catch (final Exception e) {
                    e.printStackTrace();
                    mActivity.post(new Runnable() {
                        @Override
                        public void run() {
                            onPushError(1,e.getMessage());
                        }
                    });
                }
            }
        }.start();
    }

    protected void onPushSucesses(){
        mActivity.dismissXProgressDialog();
    }

    protected  void onPushError(int error,String msg){
        ToastManager.getInstance().show(msg);
    }

    public void stop(){
        if(mConnection!=null){
            try{
                mConnection.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    ByteBuffer mBuffer = ByteBuffer.allocate(10240);
    @Override
    public void onAudioCodecFrame(ByteBuffer outputBuffer, MediaCodec.BufferInfo info) {
        if(mConnection!=null){
            try {
                outputBuffer.get(mBuffer.array(), 7, info.size);
                outputBuffer.clear();
                mBuffer.position(7 + info.size);
                addADTStoPacket(mBuffer.array(), info.size + 7);
                mBuffer.flip();
                mConnection.sendRTP(mBuffer.array(),info.size+7, info.presentationTimeUs/1000,97);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void addADTStoPacket(byte[] packet, int packetLen) {
        packet[0] = (byte) 0xFF;
        packet[1] = (byte) 0xF1;
        packet[2] = (byte) (((2 - 1) << 6) + (mSamplingRateIndex << 2) + (1 >> 2));
        packet[3] = (byte) (((1 & 3) << 6) + (packetLen >> 11));
        packet[4] = (byte) ((packetLen & 0x7FF) >> 3);
        packet[5] = (byte) (((packetLen & 7) << 5) + 0x1F);
        packet[6] = (byte) 0xFC;
    }

    @Override
    public void onAudioCodecFormatChanged(MediaFormat format) {

    }

    @Override
    public void onVideoCodecFrame(ByteBuffer outputBuffer, MediaCodec.BufferInfo bufferInfo) {
        if(mConnection!=null){
            boolean sync = false;
            if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {// sps
                sync = (bufferInfo.flags & MediaCodec.BUFFER_FLAG_SYNC_FRAME) != 0;
                if (!sync) {
                    byte[] temp = new byte[bufferInfo.size];
                    outputBuffer.get(temp);
                    mPpsSps = temp;
                    return ;
                } else {
                    mPpsSps = new byte[0];
                }
            }

            try{
                sync |= (bufferInfo.flags & MediaCodec.BUFFER_FLAG_SYNC_FRAME) != 0;
                int len = mPpsSps.length + bufferInfo.size;
                if (len > h264.length) {
                    h264 = new byte[len];
                }
                if (sync) {
                    System.arraycopy(mPpsSps, 0, h264, 0, mPpsSps.length);
                    outputBuffer.get(h264, mPpsSps.length, bufferInfo.size);
                    mConnection.sendRTP(h264,mPpsSps.length + bufferInfo.size, bufferInfo.presentationTimeUs / 1000, 96);
                    if (BuildConfig.DEBUG)
                        XLog.i(TAG, String.format("push i video stamp:%d", bufferInfo.presentationTimeUs / 1000));
                } else {
                    outputBuffer.get(h264, 0, bufferInfo.size);
                    mConnection.sendRTP(h264, bufferInfo.size, bufferInfo.presentationTimeUs / 1000, 96);
                    if (BuildConfig.DEBUG)
                        XLog.i(TAG, String.format("push video stamp:%d", bufferInfo.presentationTimeUs / 1000));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onVideoCodecFormatChanged(MediaFormat format) {

    }
}
