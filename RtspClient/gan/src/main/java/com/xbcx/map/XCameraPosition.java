package com.xbcx.map;

public interface XCameraPosition {
	
	public float getZoom();
	
	public XLatLng getTarget();
}
