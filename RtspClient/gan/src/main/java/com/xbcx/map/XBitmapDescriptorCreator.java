package com.xbcx.map;

import android.graphics.Bitmap;
import android.view.View;

public interface XBitmapDescriptorCreator {
	public XBitmapDescriptor fromResource(int resId);
	
	public XBitmapDescriptor fromAsset(String assetName);
	
	public XBitmapDescriptor fromBitmap(Bitmap b);
	
	public XBitmapDescriptor fromFile(String fileName);
	
	public XBitmapDescriptor fromPath(String absolutePath);
	
	public XBitmapDescriptor fromView(View view);
	
	public XBitmapDescriptor defaultMarker();
	
	public XBitmapDescriptor defaultMarker(float f);
}
