package com.xbcx.map;

import java.util.List;

public interface XMarker extends XMapItem{
	public void 	setPosition(XLatLng ll);
	public void 	setAnchor(float fx, float fy);
	public void 	setIcon(XBitmapDescriptor xbd);
	public void 	setTitle(String sTitle);
	public void 	setSnippet(String sSnippet);
	public void 	setDraggable(boolean bDraggable);
	public void 	setPerspective(boolean bPerspective);
	public void 	setRotateAngle(int iRotateAngle);
	public void 	setFlat(boolean bIsFlat);
	public void		setObject(Object object);
	public void 	setToTop();
	public XLatLng	getPosition();
	public List<XBitmapDescriptor> getIcons();
	public Object 	getObject();
	public void 	destroy();
}
