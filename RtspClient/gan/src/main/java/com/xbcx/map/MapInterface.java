package com.xbcx.map;

public interface MapInterface {
	public XCircle addCircle(XCircleOptions option);
	
	public XMarker addMarker(XMarkerOptions option);
	
	public XPolyline addPolyLine(XPolylineOptions option);
	
	public XPolygon addPolygon(XPolygonOptions option);
	
	public XGroundOverlay addGroundOverlay(XGroundOverlayOptions option);
	
	public void setOnMarkerClickListener(OnMarkerClickListener l);
	
	public void setOnCameraChangeListener(OnCameraChangeListener l);
	
	public void setOnMapLoadedListener(OnMapLoadedListener l);
	
	public void setOnMapClickListener(OnMapClickListener l);
	
	public XUISettings getUiSettings();
	
	public void setLoadOfflineData(boolean b);
	
	public XCameraPosition getCameraPosition();
	
	public XProjection getProjection();
	
	public float getScalePerPixel();
	
	public void animateCamera(XCameraUpdate update);
	
	public void animateCamera(XCameraUpdate update, long millis, CancelableCallback cb);
	
	public void moveCamera(XCameraUpdate update);
	
	public void clear();
}
