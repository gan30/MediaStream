package com.xbcx.map;

import java.util.ArrayList;
import java.util.List;

public class XLatLngBounds {
	
	private XLatLngBoundsInterface impl;
	
	public XLatLngBounds(XLatLngBoundsInterface impl){
		this.impl = impl;
	}
	
	public XLatLngBounds(List<XLatLng> lls){
		impl = XMap.getMapCreator().createXLatLngBounds(lls);
	}
	
	public XLatLngBounds(XLatLng ll1,XLatLng ll2){
		impl = XMap.getMapCreator().createXLatLngBounds(ll1, ll2);
	}
	
	public boolean contains(XLatLng ll){
		return impl.contains(ll);
	}
	
	public XLatLng getNortheast(){
		return impl.getNortheast();
	}
	
	public XLatLng getSouthwest(){
		return impl.getSouthwest();
	}
	
	public XLatLngBounds including(XLatLng ll){
		return impl.including(ll);
	}
	
	public boolean intersects(XLatLngBounds bounds){
		return impl.intersects(bounds);
	}
	
	public XLatLngBoundsInterface getBoundsInterface(){
		return impl;
	}
	
	public static Builder builder(){
		return new Builder();
	}
	
	public static class Builder{
		List<XLatLng> latLngs = new ArrayList<XLatLng>();
		
		public void include(XLatLng ll){
			latLngs.add(ll);
		}
		
		public XLatLngBounds build(){
			return new XLatLngBounds(this.latLngs);
		}
		
		public boolean empty(){
			return latLngs.size() <= 0;
		}
	}
}
