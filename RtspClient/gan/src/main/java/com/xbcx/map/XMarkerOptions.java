package com.xbcx.map;


public class XMarkerOptions {
	
	private XLatLng				pos;
	private Float				anchorX;
	private Float				anchorY;
	private XBitmapDescriptor	icon;
	private String				title;
	private String				snippet;
	private Boolean				draggable;
	private Boolean				visible;
	private Boolean				perspective;
	private Integer				rotateAngle;
	private Boolean				flat;
	
	public XMarkerOptions position(XLatLng pos){
		this.pos = pos;
		return this;
	}
	
	public XMarkerOptions anchor(float anchorX,float anchorY){
		this.anchorX = anchorX;
		this.anchorY = anchorY;
		return this;
	}
	
	public XMarkerOptions icon(XBitmapDescriptor xbd){
		this.icon = xbd;
		return this;
	}
	
	public XMarkerOptions title(String s){
		this.title = s;
		return this;
	}
	
	public XMarkerOptions snippet(String s){
		this.snippet = s;
		return this;
	}
	
	public XMarkerOptions draggable(boolean b){
		this.draggable = b;
		return this;
	}
	
	public XMarkerOptions visible(boolean b){
		this.visible = b;
		return this;
	}
	
	public XMarkerOptions perspective(boolean b){
		this.perspective = b;
		return this;
	}
	
	public XMarkerOptions rotateAngle(int i){
		this.rotateAngle = i;
		return this;
	}
	
	public XMarkerOptions flat(boolean b){
		this.flat = b;
		return this;
	}

	public XLatLng getPos() {
		return pos;
	}

	public Float getAnchorX() {
		return anchorX;
	}

	public Float getAnchorY() {
		return anchorY;
	}

	public XBitmapDescriptor getIcon() {
		return icon;
	}

	public String getTitle() {
		return title;
	}

	public String getSnippet() {
		return snippet;
	}

	public Boolean getDraggable() {
		return draggable;
	}

	public Boolean getVisible() {
		return visible;
	}

	public Boolean getPerspective() {
		return perspective;
	}

	public Integer getRotateAngle() {
		return rotateAngle;
	}

	public Boolean getFlat() {
		return flat;
	}
}
