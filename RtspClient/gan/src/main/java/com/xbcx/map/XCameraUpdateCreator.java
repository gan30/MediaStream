package com.xbcx.map;

public interface XCameraUpdateCreator {
	public XCameraUpdate newLatLngBounds(XLatLngBounds b, int padding);
	
	public XCameraUpdate newLatLngZoom(XLatLng ll, float zoom);
	
	public XCameraUpdate scrollBy(float x, float y);
	
	public XCameraUpdate changeLatLng(XLatLng ll);
	
	public XCameraUpdate zoomIn();
	
	public XCameraUpdate zoomOut();
	
	public XCameraUpdate zoomTo(float zoom);
}
