package com.xbcx.map;

import android.graphics.Point;

public interface XProjection {
	public Point toScreenLocation(XLatLng ll);
	
	public XLatLng fromScreenLocation(Point p);
	
	public XVisibleRegion getVisibleRegion();
}
