package com.xbcx.map;

public abstract class XRegeocodeAddress {
	
	private double mRequestLat;
	private double mRequestLng;
	
	public XRegeocodeAddress(double requestLat,double requestLng) {
		mRequestLat = requestLat;
		mRequestLng = requestLng;
	}

	public abstract String getFormatAddress();
	
	public abstract String getCity();
	
	public abstract String getStreet();
	
	public abstract String getStreetNum();
	
	public abstract String getProvince();
	
	public abstract String getDistrict();
	
	public double getRequestLat(){
		return mRequestLat;
	}
	
	public double getRequestLng(){
		return mRequestLng;
	}
}
