package com.xbcx.map;

public interface XLatLngBoundsInterface {
	
	public boolean contains(XLatLng ll);
	
	public XLatLng getNortheast();
	
	public XLatLng getSouthwest();
	
	public XLatLngBounds including(XLatLng ll);
	
	public boolean intersects(XLatLngBounds bounds);
}
