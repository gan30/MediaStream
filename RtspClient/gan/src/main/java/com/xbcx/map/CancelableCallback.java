package com.xbcx.map;

public interface CancelableCallback {
	public void onFinish();
	
	public void onCancel();
}
