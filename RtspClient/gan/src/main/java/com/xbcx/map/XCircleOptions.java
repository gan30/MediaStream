package com.xbcx.map;

public class XCircleOptions {
	
	private XLatLng	center;
	private double	radius;
	private boolean	isSetFillColor;
	private int		fillColor;
	private boolean	isSetStrokeColor;
	private	int		strokeColor;
	private boolean	isSetStrokeWidth;
	private float	strokeWidth;
	
	public XCircleOptions center(XLatLng ll){
		this.center = ll;
		return this;
	}
	
	public XCircleOptions radius(double r){
		this.radius = r;
		return this;
	}
	
	public XCircleOptions fillColor(int color){
		isSetFillColor = true;
		this.fillColor = color;
		return this;
	}
	
	public XCircleOptions strokeColor(int color){
		isSetStrokeColor = true;
		this.strokeColor = color;
		return this;
	}
	
	public XCircleOptions strokeWidth(float width){
		isSetStrokeWidth = true;
		this.strokeWidth = width;
		return this;
	}

	public XLatLng getCenter() {
		return center;
	}

	public double getRadius() {
		return radius;
	}

	public boolean isSetFillColor() {
		return isSetFillColor;
	}

	public int getFillColor() {
		return fillColor;
	}

	public boolean isSetStrokeColor() {
		return isSetStrokeColor;
	}

	public int getStrokeColor() {
		return strokeColor;
	}

	public boolean isSetStrokeWidth() {
		return isSetStrokeWidth;
	}

	public float getStrokeWidth() {
		return strokeWidth;
	}
}
