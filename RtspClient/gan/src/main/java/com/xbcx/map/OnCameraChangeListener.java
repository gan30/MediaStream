package com.xbcx.map;

public interface OnCameraChangeListener {
	public void onCameraChange(XCameraPosition arg0);
	
	public void onCameraChangeFinish(XCameraPosition arg0);
}
