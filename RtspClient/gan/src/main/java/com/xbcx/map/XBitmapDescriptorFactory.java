package com.xbcx.map;

import android.graphics.Bitmap;
import android.view.View;

public class XBitmapDescriptorFactory {
	
	public static XBitmapDescriptor fromResource(int resId){
		return XMap.getMapCreator().createBitmapDescriptorCreator().fromResource(resId);
	}
	
	public static XBitmapDescriptor fromAsset(String assetName){
		return XMap.getMapCreator().createBitmapDescriptorCreator().fromAsset(assetName);
	}
	
	public static XBitmapDescriptor fromBitmap(Bitmap b){
		return XMap.getMapCreator().createBitmapDescriptorCreator().fromBitmap(b);
	}
	
	public static XBitmapDescriptor fromFile(String fileName){
		return XMap.getMapCreator().createBitmapDescriptorCreator().fromFile(fileName);
	}
	
	public static XBitmapDescriptor fromPath(String absolutePath){
		return XMap.getMapCreator().createBitmapDescriptorCreator().fromPath(absolutePath);
	}
	
	public static XBitmapDescriptor fromView(View view){
		return XMap.getMapCreator().createBitmapDescriptorCreator().fromView(view);
	}
	
	public static XBitmapDescriptor defaultMarker(){
		return XMap.getMapCreator().createBitmapDescriptorCreator().defaultMarker();
	}
	
	public static XBitmapDescriptor defaultMarker(float f){
		return XMap.getMapCreator().createBitmapDescriptorCreator().defaultMarker(f);
	}
}
