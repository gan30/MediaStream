package com.xbcx.map;

import android.graphics.Bitmap;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;

import com.xbcx.common.ObjectWrapper;
import com.xbcx.core.AndroidEventManager;
import com.xbcx.core.Creator;
import com.xbcx.core.Event;
import com.xbcx.core.EventCode;
import com.xbcx.core.EventManager.OnEventListener;
import com.xbcx.core.FileLogger;
import com.xbcx.core.XApplication;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import gan.core.R;

public abstract class XLocationManager {
	static{
		instance = XMap.getLocateCreator().createXLocationManager();
		
		OnEventListener listener = new OnEventListener() {
			@Override
			public void onEventRunEnd(Event event) {
				final int code = event.getEventCode();
				if(code == EventCode.AppBackground){
					for(XLocationListener listener : mLocateListeners){
						if(listener.mBackgroundPause){
							getInstance().removeUpdates(listener);
						}
					}
				}else if(code == EventCode.AppForceground){
					for(XLocationListener listener : mLocateListeners){
						if(listener.mBackgroundPause){
							getInstance().requestLocationData(5000, 0, listener);
						}
					}
				}
			}
		};
		AndroidEventManager.getInstance().addEventListener(EventCode.AppForceground, listener);
		AndroidEventManager.getInstance().addEventListener(EventCode.AppBackground, listener);
	}
	
	public static XLocationManager getInstance(){
		return instance;
	}
	
	private static XLocationManager instance;
	
	public static String URL_GetLocationImage = "http://maps.google.com/maps/api/staticmap?" +
			"center=%f,%f&" +
			"zoom=%d&" +
			"size=%dx%d&" +
			"maptype=roadmap&" +
			"markers=color:red%%7Clabel:%%7C%f,%f&" +
			"sensor=false&language=zh-Hans";
	
	public  static final String			Extra_MockLocation = "mock_location";
	
	public 	static boolean				OnlyGPS = false;
	
	public  static boolean				TestNoLocation = false;
	
	private static Creator<String, XLocation> 			locationDescFormater;
	private static Creator<String, XRegeocodeAddress> 	addressDescFormater;
	
	private static XLocation				mLastLocation;
	
	private static List<XLocationListener> 	mLocateListeners = new ArrayList<XLocationListener>();
	
	/**
	 * @param formater
	 * <pre>
	 * sample
	 * public String createObject(XLocation param) {
	 *    return param.getStreet() + param.getStreetNum();
	 * }
	 * <pre>
	 */
	public static void setLocationDescFormater(Creator<String, XLocation> formater){
		locationDescFormater = formater;
	}
	
	/**
	 * @param formater
	 * * <pre>
	 * sample
	 * public String createObject(XRegeocodeAddress param) {
	 *    return param.getStreet() + param.getStreetNum();
	 * }
	 * <pre>
	 */
	public static void setAddressDescFormater(Creator<String, XRegeocodeAddress> formater){
		addressDescFormater = formater;
	}
	
	public static void requestGetLocation(OnGetLocationListener listener){
		requestGetLocation(listener, 30000, true);
	}
	
	public static void requestGetLocation(OnGetLocationListener listener,boolean autoClear){
		requestGetLocation(listener, 30000, autoClear);
	}
	
	/**
	 * autoClear为false需要自己调用cancel
	 * @param listener
	 * @param timeOut 多少毫米后不进行回调，但仍会进行定位，传-1会一直回调
	 */
	public static void requestGetLocation(final OnGetLocationListener listener,final long timeOut){
		requestGetLocation(listener, timeOut, false);
	}
	
	public static void requestGetLocation(final OnGetLocationListener listener,final long timeOut,final boolean autoClear){
		requestGetLocation(listener, new LocateParamBuilder()
				.setAutoClear(autoClear)
				.setTimeOut(timeOut));
	}
	
	public static void requestGetLocation(final OnGetLocationListener listener,
			final LocateParamBuilder builder){
		XApplication.getMainThreadHandler().postDelayed(new Runnable() {
			@Override
			public void run() {
				XApplication.getLogger().fine("requestGetLocation");
				boolean bCreate = true;
				for(final XLocationListener curListener : mLocateListeners){
					if(curListener.hasListener(listener)){
						final XLocation location = curListener.mLocation;
						if(location != null && 
								(!curListener.mNeedDesc || !TextUtils.isEmpty(getLocationDesc(location)))){
							long timeDis = XApplication.getFixSystemTime() - location.getTime();
							if(timeDis > 0 && timeDis < 60000){
								XApplication.getMainThreadHandler().post(new Runnable() {
									@Override
									public void run() {
										if(curListener != null){
											curListener.notice(location);
										}
									}
								});
							}
						}
						curListener.startNotify(builder.timeOut);
						bCreate = false;
						XApplication.getLogger().fine("has current listener:" + curListener);
					}
				}
				if(bCreate){
					XLocationListener xlistener = getInstance().createXLocationListener(listener,builder);
					getInstance().requestLocationData(5000, 0, xlistener);
					
					mLocateListeners.add(xlistener);
					xlistener.startNotify(builder.timeOut);
					
					XApplication.getLogger().fine("create listener");
				}
			}
		}, 0);
	}
	
	public static void cancelLocationListener(final OnGetLocationListener listener){
		XApplication.getMainThreadHandler().post(new Runnable() {
			@Override
			public void run() {
				for(XLocationListener l : mLocateListeners){
					if(l.hasListener(listener)){
						getInstance().removeLocateListener(l);
						break;
					}
				}
			}
		});
	}
	
	public abstract void removeUpdates(XLocationListener l);
	
	public abstract void requestLocationData(long mills,float dis,XLocationListener l);
	
	public abstract XLocationListener createXLocationListener(OnGetLocationListener listener,LocateParamBuilder builder);
	
	private void removeLocateListener(XLocationListener l){
		if(l != null){
			XApplication.getMainThreadHandler().removeCallbacks(l);
			removeUpdates(l);
			mLocateListeners.remove(l);
		}
	}
	
	public Iterator<OnGetLocationListener> getLocationListeners(){
		return new OnGetLocationListenerIterator(mLocateListeners);
	}
	
	public static String getLocationDesc(XLocation l){
		return l.getAddress();
	}
	
	@Deprecated
	public static void addLocationDesc(XLocation l,String desc){
		l.setAddress(desc);
	}
	
	public static boolean isMockLocation(XLocation l){
		Bundle b = l.getExtras();
		if(b != null){
			return b.getBoolean(Extra_MockLocation, false);
		}
		return false;
	}
	
	private static HashMap<OnGetAddressListener, XLatLng> listenerToXLatLng;
	
	public final void requestGetAddressCancelPreListener(final double lat,final double lng,
			final OnGetAddressListener listener){
		if(listenerToXLatLng == null){
			listenerToXLatLng = new HashMap<OnGetAddressListener, XLatLng>();
		}
		listenerToXLatLng.put(listener, new XLatLng(lat, lng));
		requestGetAddress(lat, lng, new OnGetAddressListener() {
			@Override
			public void onGetAddressFinished(XRegeocodeAddress address, boolean bSuccess) {
				XLatLng request = listenerToXLatLng.get(listener);
				if(request != null){
					if(request.getLat() == lat && 
							request.getLng() == lng){
						listenerToXLatLng.remove(listener);
						listener.onGetAddressFinished(address, bSuccess);
					}
				}
			}
		});
	}
	
	public final void requestGetAddress(final double lat,final double lng,
			final OnGetAddressListener listener){
		if(addressDescFormater == null){
			onGetAddress(lat, lng, listener);
		}else{
			onGetAddress(lat, lng, new OnGetAddressListener() {
				@Override
				public void onGetAddressFinished(XRegeocodeAddress address, boolean bSuccess) {
					if(bSuccess){
						if(listener != null){
							final XRegeocodeAddress wrap = address;
							address = new XRegeocodeAddress(lat,lng) {
								@Override
								public String getStreetNum() {
									return wrap.getStreetNum();
								}
								
								@Override
								public String getStreet() {
									return wrap.getStreet();
								}
								
								@Override
								public String getProvince() {
									return wrap.getProvince();
								}
								
								@Override
								public String getFormatAddress() {
									return addressDescFormater.createObject(wrap);
								}
								
								@Override
								public String getDistrict() {
									return wrap.getDistrict();
								}
								
								@Override
								public String getCity() {
									return wrap.getCity();
								}
							};
							listener.onGetAddressFinished(address, bSuccess);
						}
					}else{
						if(listener != null){
							listener.onGetAddressFinished(address, bSuccess);
						}
					}
				}
			});
		}
	}
	
	protected abstract void onGetAddress(final double lat,final double lng,
			final OnGetAddressListener listener);
	
	public final XRegeocodeAddress requestGetAddressSync(final double lat,final double lng){
		final ObjectWrapper<XRegeocodeAddress> ow = new ObjectWrapper<XRegeocodeAddress>();
		requestGetAddress(lat, lng, new OnGetAddressListener() {
			@Override
			public void onGetAddressFinished(XRegeocodeAddress address, boolean bSuccess) {
				if(bSuccess){
					ow.mWrapper = address;
				}
				synchronized (ow) {
					ow.notify();
				}
			}
		});
		synchronized (ow) {
			try {
				ow.wait(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return ow.mWrapper;
	}
	
	public static XLocation getLastKnownLocation(){
		if(mLastLocation != null){
			return mLastLocation;
		}
		return getInstance().internalGetLastKnownLocation();
	}
	
	protected abstract XLocation internalGetLastKnownLocation();
	
	public static float calculateToSelfDistance(double lat,double lng){
		if(MapUtils.isLocationEffective(lat, lng)){
			XLocation l = getLastKnownLocation();
			if(l == null){
				return -1;
			}
			return MapUtils.calculateLocationDistance(l.getLatitude(), l.getLongitude(), 
					lat, lng);
		}
		return -1;
	}
	
	public static void requestGetImage(final Location l,final int width,final int height,
			final OnGetLocationImageListener listener){
		requestGetImage(l.getLatitude(), l.getLongitude(), width, height, listener);
	}
	
	public static void requestGetImage(final double lat,final double lng,final int width,final int height,
			final OnGetLocationImageListener listener){
		requestGetImage(lat, lng, width, height, 12, listener);
	}
	
	public static void requestGetImage(final double lat,final double lng,
			final int width,final int height,final int zoom,
			final OnGetLocationImageListener listener){
		
//		DisplayImageOptions options = new DisplayImageOptions.Builder()
//			.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
//			.cacheInMemory(true)
//			.cacheOnDisk(false)
//			.bitmapConfig(Bitmap.Config.RGB_565)
//			.build();
//		XApplication.getImageLoader().loadImage(String.format(Locale.getDefault(),
//						URL_GetLocationImage,
//						lat,lng,
//						zoom,
//						width,height,
//						lat,lng), options,
//						new ImageLoadingListener() {
//							@Override
//							public void onLoadingStarted(String imageUri, View view) {
//							}
//
//							@Override
//							public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
//								if(listener != null){
//									listener.onGetImageFinished(null, false);
//								}
//							}
//
//							@Override
//							public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
//								if(listener != null){
//									listener.onGetImageFinished(loadedImage, true);
//								}
//							}
//
//							@Override
//							public void onLoadingCancelled(String imageUri, View view) {
//							}
//						});

	}
	
	public static String getAddressDesc(Address a){
		StringBuffer sb = new StringBuffer();
		if(!TextUtils.isEmpty(a.getCountryName())){
			sb.append(a.getCountryName());
		}
		if(!TextUtils.isEmpty(a.getAdminArea())){
			sb.append(a.getAdminArea());
		}
		if(!TextUtils.isEmpty(a.getLocality())){
			sb.append(a.getLocality());
		}
		if(!TextUtils.isEmpty(a.getSubLocality())){
			sb.append(a.getSubLocality());
		}
		if(TextUtils.isEmpty(a.getFeatureName())){
			if(!TextUtils.isEmpty(a.getThoroughfare())){
				sb.append(a.getThoroughfare());
			}
			if(!TextUtils.isEmpty(a.getSubThoroughfare())){
				sb.append(a.getSubThoroughfare());
			}
		}else{
			sb.append(a.getFeatureName());
		}
		
		return sb.toString();
	}
	
	public static String modificationLocationDescNearby(String desc){
		if(!TextUtils.isEmpty(desc)){
			final String nearbys[] = XApplication.getApplication().getResources().getStringArray(R.array.location_nearbys);
			boolean bContain = false;
			for(String n : nearbys){
				if(desc.contains(n)){
					bContain = true;
					break;
				}
			}
			if(!bContain){
				return desc + XApplication.getApplication().getString(R.string.nearby);
			}
		}
		return desc;
	}
	
	/**
	 * 去掉省市区
	 * @param desc
	 * @return
	 */
	@Deprecated
	public static String removeProvinces(String desc){
		int pos = desc.indexOf(XApplication.getApplication().getString(R.string.location_province));
		if(pos >= 0){
			desc = desc.substring(pos + 1);
		}
		pos = desc.indexOf(XApplication.getApplication().getString(R.string.location_city));
		if(pos >= 0){
			desc = desc.substring(pos + 1);
		}
		pos = desc.indexOf(XApplication.getApplication().getString(R.string.location_district));
		if(pos >= 0){
			desc = desc.substring(pos + 1);
		}
		return desc;
	}
	
	protected static abstract class XLocationListener implements Runnable{
		protected WeakReference<OnGetLocationListener> mListener;
		protected boolean				mNotify;
		protected XLocation				mLocation;
		protected long					mTime;
		protected boolean				mIsAutoClear;
		protected boolean				mIsAutoClearFirstLocation = true;
		protected boolean				mNeedDesc = true;
		protected boolean				mBackgroundPause = true;
		
		private   boolean				mHasMockSoftware;
		
		public XLocationListener(OnGetLocationListener listener,LocateParamBuilder builder){
			mListener = new WeakReference<OnGetLocationListener>(listener);
			mIsAutoClear = builder.autoClear;
			mIsAutoClearFirstLocation = builder.autoClearFirstLocation;
			mNeedDesc = builder.needDesc;
			mBackgroundPause = builder.backgroundPause;
			
			mHasMockSoftware = MockLocationCheckManager.getInstance().hasMockSoftware();
		}
		
		public boolean hasListener(OnGetLocationListener listener){
			OnGetLocationListener ref = mListener.get();
			return listener == ref;
		}
		
		public boolean needDesc(){
			return mNeedDesc;
		}
		
		public void startNotify(long timeOut){
			mNotify = true;
			XApplication.getMainThreadHandler().removeCallbacks(this);
			if(timeOut > 0){
				XApplication.getMainThreadHandler().postDelayed(this, timeOut);
			}
		}
		
		public void handleLocation(XLocation location){
			boolean bMock = false;
			if(mHasMockSoftware || 
					MockLocationCheckManager.getInstance().isMock(location)){
				Bundle b = location.getExtras();
				if(b == null){
					b = new Bundle();
					b.putBoolean(Extra_MockLocation, true);
					location.setExtras(b);
				}else{
					b.putBoolean(Extra_MockLocation, true);
				}
				bMock = true;
			}
			if(mListener.get() == null){
				getInstance().removeLocateListener(this);
			}
			XLocation choose = null;
			if(location != null){
				XApplication.getLogger().fine("location:" + location);
				if(mLocation == null){
					choose = location;
					mTime = SystemClock.elapsedRealtime();
				}else{
					if(LocationManager.GPS_PROVIDER.equals(location.getProvider())){
						choose = location;
						mTime = SystemClock.elapsedRealtime();
						XApplication.getLogger().fine("GPS:" + location);
					}else{
						final long time = SystemClock.elapsedRealtime();
						if(time - mTime > 30000){
							choose = location;
							mTime = time;
							XApplication.getLogger().fine("time > 30s:" + location);
						}else{
							if(location.getAccuracy() < mLocation.getAccuracy()){
								choose = location;
								mTime = time;
								XApplication.getLogger().fine("accuracy:" + location.getAccuracy() + " " + location);
							}
						}
					}
				}
				if(choose != null){
					mLastLocation = location;
					XApplication.getLogger().fine("choose:" + location);
					if(!bMock){
						mLocation = choose;
					}
					final XLocation l = choose;
					final String text = getLocationDesc(choose);
					if(TextUtils.isEmpty(text) && mNeedDesc){
						getInstance().requestGetAddress(choose.getLatitude(), choose.getLongitude(), 
								new OnGetAddressListener() {
									@Override
									public void onGetAddressFinished(XRegeocodeAddress address, boolean bSuccess) {
										if(bSuccess){
											l.setAddress(address);
										}
										notice(l);
									}
								});
					}else{
						notice(location);
					}
				}
			}
		}
		
		protected void handleLocationFail(MapException exception){
			OnGetLocationListener l = mListener.get();
			if(l != null){
				if(l instanceof OnGetLocationListener2){
					((OnGetLocationListener2)l).onGetLocationFail(exception);
				}
			}
		}
		
		protected void notice(final XLocation location){
			if(mIsAutoClear && mIsAutoClearFirstLocation){
				getInstance().removeLocateListener(this);
			}
			
			if(locationDescFormater == null){
				String desc = getLocationDesc(location);
				if(!TextUtils.isEmpty(desc)){
					location.setAddress(modificationLocationDescNearby(desc));
				}
			}else{
				location.setAddress(locationDescFormater.createObject(location));
			}
			
			//for test
			//mListener = null;
			
			XApplication.getLogger().fine("notice:" + location + " callback:" + mListener);
			if(mNotify){
				if(!TestNoLocation && mListener != null){
					XApplication.getMainThreadHandler().post(new Runnable() {
						@Override
						public void run() {
							if(mListener != null){
								notifyGetLocationFinished(location, location != null);
							}
						}
					});
				}
			}
		}
		
		private void notifyGetLocationFinished(XLocation location,boolean bSuccess){
			OnGetLocationListener ref = mListener.get();
			if(ref != null){
				if(location != null){
					StringBuffer sb = new StringBuffer();
					sb.append(location.getClass().getSimpleName())
					.append(":")
					.append("#lat=")
					.append(location.getLatitude())
					.append("#lng=")
					.append(location.getLongitude())
					.append("#desc=")
					.append(getLocationDesc(location))
					.append("#city=")
					.append(location.getCity())
					.append("#acc=")
					.append(location.getAccuracy())
					.append("#provider=")
					.append(location.getProvider())
					.append("#time=")
					.append(location.getTime())
					.append("#extra=")
					.append(location.getExtras());
					FileLogger.getInstance("location")
						.setMaxFileNum(5)
						.log(sb.toString());
				}else{
					FileLogger.getInstance("location")
						.setMaxFileNum(5)
						.log("locate fail");
				}
				
				ref.onGetLocationFinished(location, location != null);
			}
		}
		
		@Override
		public void run() {
			if(mLocation == null){
				if(mLastLocation != null && 
						(!mNeedDesc || !TextUtils.isEmpty(getLocationDesc(mLastLocation)))){
					if(!TestNoLocation){
						long timeDis = XApplication.getFixSystemTime() - mLastLocation.getTime();
						if(timeDis > 0 && timeDis < 60000){
							notifyGetLocationFinished(mLastLocation, true);
						}else{
							notifyGetLocationFinished(null, false);
						}
					}
				}else{
					notifyGetLocationFinished(null, false);
				}
			}
			mNotify = false;
			if(mIsAutoClear){
				getInstance().removeLocateListener(this);
			}
		}
	}
	
	public static interface OnGetLocationListener{
		public void onGetLocationFinished(XLocation location, boolean bSuccess);
	}
	
	public static interface OnGetLocationListener2 extends OnGetLocationListener{
		public void onGetLocationFail(MapException mapException);
	}
	
	public static interface OnGetLocationImageListener{
		public void onGetImageFinished(Bitmap bmp, boolean bSuccess);
	}
	
	public static interface OnGetAddressListener{
		public void onGetAddressFinished(XRegeocodeAddress address, boolean bSuccess);
	}
	
	public static class LocateParamBuilder{
		boolean 	autoClear;
		boolean 	autoClearFirstLocation = true;
		boolean		needDesc = true;
		long	 	timeOut;
		boolean		backgroundPause = true;
		
		public LocateParamBuilder setAutoClear(boolean b){
			autoClear = b;
			return this;
		}
		
		public LocateParamBuilder setBackgroundPause(boolean b){
			backgroundPause = b;
			return this;
		}
		
		public LocateParamBuilder setAutoClearFirstLocation(boolean b){
			autoClearFirstLocation = b;
			return this;
		}
		
		public LocateParamBuilder setTimeOut(long timeOut){
			this.timeOut = timeOut;
			return this;
		}
		
		public LocateParamBuilder setNeedDesc(boolean b){
			needDesc = b;
			return this;
		}
	}
	
	private static class OnGetLocationListenerIterator implements Iterator<OnGetLocationListener>{

		private List<XLocationListener> mListeners;
		private int						mCurrentPos;
		private OnGetLocationListener	mRefListener;
		
		public OnGetLocationListenerIterator(List<XLocationListener> listeners) {
			mListeners = listeners;
		}
		
		@Override
		public boolean hasNext() {
			do{
				if(mCurrentPos >= mListeners.size()){
					break;
				}
				XLocationListener listener = mListeners.get(mCurrentPos++);
				OnGetLocationListener refListener = listener.mListener.get();
				if(refListener != null){
					mRefListener = refListener;
					return true;
				}
			}while(true);
			return false;
		}

		@Override
		public OnGetLocationListener next() {
			return mRefListener;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("can't remove");
		}
	}
}
