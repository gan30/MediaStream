package com.xbcx.map;

import java.util.List;

public interface XPolygon extends XMapItem{
	public void 		setFillColor(int fillColor);
	public int 			getFillColor();
	public void 		setStrokeColor(int strokeColor);
	public int 			getStrokeColor();
	public void 		setStrokeWidth(float strokeWidth);
	public float 		getStrokeWidth();
	public float   		getZIndex();
	public void 		setZIndex(float zIndex);
	public void	        setPoints(List<XLatLng> points);
	public List<XLatLng>getPoints();
}	
