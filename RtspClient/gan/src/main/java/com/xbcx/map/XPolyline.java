package com.xbcx.map;

import java.util.List;

public interface XPolyline extends XMapItem{
	public float  			getWidth();
	public void   			setWidth(float width);
	public int    			getColor();
	public void   			setColor(int color);
	public float   			getZIndex();
	public void 			setZIndex(float zIndex);
	public boolean      	isDottedLine();
	public List<XLatLng> 	getPoints();
}
