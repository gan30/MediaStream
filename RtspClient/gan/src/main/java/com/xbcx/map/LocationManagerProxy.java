package com.xbcx.map;

import com.xbcx.core.XApplication;

import android.content.Context;

public class LocationManagerProxy {
	
	public static final String GPS_PROVIDER = "gps";
	
	public static final String Network 		= "network";
	
	public static boolean	DefaultNeedDesc = true;
	
	public static LocationManagerProxy getInstance(Context context){
		return new LocationManagerProxy(XMap.getLocateCreator().createLocationManagerInterface());
	}
	
	private LocationManagerInterface	mInterface;
	
	public LocationManagerProxy(LocationManagerInterface i){
		mInterface = i;
	}
	
	public void setGpsEnable(boolean b){
		mInterface.setGpsEnable(b);
	}
	
	public void setNeedDesc(boolean b){
		mInterface.setNeedDesc(b);
	}
	
	public void requestLocationData(String provider,long time,float distance,LocationListener listener){
		XApplication.addCount("locus");
		mInterface.requestLocationData(provider, time, distance, listener);
	}
	
	public void removeUpdates(LocationListener l){
		XApplication.reduceCount("locus");
		mInterface.removeUpdates(l);
	}
	
	public static int getLocusListenerCount(){
		return XApplication.getCount("locus");
	}
}
