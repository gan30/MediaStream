package com.xbcx.map;

public interface OnMapItemRemoveListener {
	public void onMapItemRemoved(XMapItem item);
}
