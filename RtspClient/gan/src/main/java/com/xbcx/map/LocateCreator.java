package com.xbcx.map;

public interface LocateCreator {

	public XLocation createLocation(String provider);
	
	public XLocationManager createXLocationManager();
	
	public LocationManagerInterface createLocationManagerInterface();
}
