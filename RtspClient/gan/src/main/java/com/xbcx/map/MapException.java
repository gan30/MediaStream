package com.xbcx.map;

public abstract class MapException extends Exception{
	
	public static final int ErrorCode_Success 			= 0;
	public static final int ErrorCode_PermissionDeny 	= 31;

	private static final long serialVersionUID = 1L;

	public abstract int getErrorCode();
}
