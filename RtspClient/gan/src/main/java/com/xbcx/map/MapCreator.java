package com.xbcx.map;

import java.util.List;

import android.support.v4.app.Fragment;

public interface MapCreator {
	
	public MapInterface createMap(Fragment f);
	
	public String getFragmentClass();
	
	public int getFragmentLayoutId();
	
	public int getFragmentLayoutMapId();
	
	public XLatLngBoundsInterface createXLatLngBounds(List<XLatLng> lls);
	
	public XLatLngBoundsInterface createXLatLngBounds(XLatLng ll1, XLatLng ll2);
	
	public XBitmapDescriptorCreator createBitmapDescriptorCreator();
	
	public XCameraUpdateCreator createCameraUpdateCreator();
	
	public void setOfflineDataPath(String path);
}
