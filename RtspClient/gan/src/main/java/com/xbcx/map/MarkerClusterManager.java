package com.xbcx.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import com.xbcx.core.Creator;

import android.graphics.Point;

public class MarkerClusterManager implements OnCameraChangeListener{

	private XMap							mMap;
	private int								mGridSize = 100;
	private List<MarkerCluster> 			mMarkerClusters = new ArrayList<MarkerCluster>();
	private Creator<XMarkerOptions, Object> mMarkerOptionsCreator;
	private HashMap<Object, XMarkerDelegate> mMapParamToXMarker = new HashMap<Object, XMarkerDelegate>();
	
	private float							mZoom = -1;
	
	public MarkerClusterManager(XMap map,Creator<XMarkerOptions, Object> markerOptionsCreator) {
		mMap = map;
		mMarkerOptionsCreator = markerOptionsCreator;
		map.addOnCameraChangeListener(this);
	}
	
	public MarkerClusterManager setGridSize(int gridSize){
		mGridSize = gridSize;
		return this;
	}
	
	public XMarker addMarker(Object creatorParam,XLatLng pos){
		boolean bFind = false;
		MarkerCluster cluster = null;
		synchronized (mMarkerClusters) {
			for(MarkerCluster mc : mMarkerClusters){
				if(mc.contains(pos)){
					mc.addMarkerOptionCreatorParam(creatorParam);
					bFind = true;
					cluster = mc;
					break;
				}
			}
		}
		if(!bFind){
			MarkerCluster mc = new MarkerCluster(pos,creatorParam,this,null);
			synchronized (mMarkerClusters) {
				mMarkerClusters.add(mc);
			}
			cluster = mc;
		}
		XMarkerDelegate xmd = new XMarkerDelegate(cluster,creatorParam,pos);
		synchronized (mMapParamToXMarker) {
			mMapParamToXMarker.put(creatorParam, xmd);
		}
		if(!bFind){
			cluster.setCurrentMarkerDelegate(xmd);
		}
		
		return xmd;
	}
	
	public boolean isClusterMarker(XMarker m){
		synchronized (mMarkerClusters) {
			for(MarkerCluster mc : mMarkerClusters){
				if(mc.mMarker == m){
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void onCameraChange(XCameraPosition arg0) {
	}

	@Override
	public void onCameraChangeFinish(XCameraPosition arg0) {
		float zoom = arg0.getZoom();
		if(mZoom != zoom){
			mZoom = zoom;
			boolean bFind = false;
			synchronized (mMarkerClusters) {
				for(MarkerCluster mc : mMarkerClusters){
					mc.mMarkerOptionCreatorParam.clear();
					mc.updateLatLngBounds(mc.mMarker.getPosition(), MarkerClusterManager.this);
					mc.mUsed = false;
				}
			}
			
			synchronized (mMapParamToXMarker) {
				for(Entry<Object, XMarkerDelegate> e : mMapParamToXMarker.entrySet()){
					MarkerCluster cluster = null;
					XMarkerDelegate xm = e.getValue();
					if(!xm.isVisible()){
						continue;
					}
					XLatLng pos = xm.mPos;
					Object creatorParam = e.getKey();
					bFind = false;
					synchronized (mMarkerClusters) {
						for(MarkerCluster mc : mMarkerClusters){
							if(mc.contains(pos)){
								if(!mc.mCurrentMarkerDelegate.isVisible()){
									if(!creatorParam.equals(mc.mCurrentMarkerDelegate.mCreatorParam)){
										MapUtils.setMarkerOptions(mc.mMarker, createMarkerOptions(creatorParam));
										mc.setCurrentMarkerDelegate(xm);
									}
								}
								mc.mMarker.setVisible(true);
								mc.mUsed = true;
								mc.addMarkerOptionCreatorParam(e.getKey());
								bFind = true;
								cluster = mc;
								break;
							}
						}
					}
					if(!bFind){
						MarkerCluster mc = new MarkerCluster(pos,creatorParam,MarkerClusterManager.this,null);
						synchronized (mMarkerClusters) {
							mMarkerClusters.add(mc);
						}
						mc.setCurrentMarkerDelegate(xm);
						mc.mMarker.setObject(xm.getObject());
						mc.mMarker.setVisible(xm.isVisible());
						cluster = mc;
					}
					e.getValue().mCluster = cluster;
				}
			}
			synchronized (mMarkerClusters) {
				Iterator<MarkerCluster> it = mMarkerClusters.iterator();
				while(it.hasNext()){
					MarkerCluster mc = it.next();
					if(!mc.mUsed){
						//it.remove();
						mc.mMarker.setVisible(false);
					}
				}
			}
		}
	}
	
	private XMarkerOptions createMarkerOptions(Object param){
		return mMarkerOptionsCreator.createObject(param);
	}
	
	private class XMarkerDelegate implements XMarker{
		Object 			mCreatorParam;
		XLatLng			mPos;
		MarkerCluster 	mCluster;
		
		Object			mObject;
		boolean			mIsVisible = true;
		
		public XMarkerDelegate(MarkerCluster mc,Object creatorParam,XLatLng pos) {
			mCreatorParam = creatorParam;
			mPos = pos;
			mCluster = mc;
		}
		
		@Override
		public void remove() {
			synchronized (mMapParamToXMarker) {
				if(mCluster.mMarkerOptionCreatorParam.remove(mCreatorParam) != null){
					if(mCluster.mMarkerOptionCreatorParam.size() == 0){
						synchronized (mMarkerClusters) {
							mMarkerClusters.remove(mCluster);
						}
					}
				}
				mMapParamToXMarker.remove(mCreatorParam);
			}
		}

		@Override
		public void setVisible(boolean bVisible) {
			mIsVisible = bVisible;
			if(mCluster.mCurrentMarkerDelegate.equals(this)){
				 mCluster.mMarker.setVisible(bVisible);
			}
		}

		@Override
		public boolean isVisible() {
			return mIsVisible;
		}

		@Override
		public void setPosition(XLatLng ll) {
		}

		@Override
		public void setAnchor(float fx, float fy) {
		}

		@Override
		public void setIcon(XBitmapDescriptor xbd) {
		}

		@Override
		public void setTitle(String sTitle) {
		}

		@Override
		public void setSnippet(String sSnippet) {
		}

		@Override
		public void setDraggable(boolean bDraggable) {
		}

		@Override
		public void setPerspective(boolean bPerspective) {
		}

		@Override
		public void setRotateAngle(int iRotateAngle) {
		}

		@Override
		public void setFlat(boolean bIsFlat) {
		}

		@Override
		public void setObject(Object object) {
			if(mCluster.mCurrentMarkerDelegate.equals(this)){
				 mCluster.mMarker.setObject(object);
			}
			mObject = object;
		}

		@Override
		public void setToTop() {
		}

		@Override
		public XLatLng getPosition() {
			return mPos;
		}

		@Override
		public List<XBitmapDescriptor> getIcons() {
			return null;
		}

		@Override
		public Object getObject() {
			return mObject;
		}

		@Override
		public void destroy() {
			remove();
		}
	}

	private static class MarkerCluster{
		private XMarker					mMarker;
		private XMarkerDelegate			mCurrentMarkerDelegate;
		private XLatLngBounds			mBounds;
		private boolean					mUsed = true;
		
		private Map<Object, Object> 	mMarkerOptionCreatorParam = new ConcurrentHashMap<Object, Object>();
		
		public MarkerCluster(XLatLng pos,Object creatorParam,MarkerClusterManager m,XMarker marker) {
			XMarkerOptions mo = m.createMarkerOptions(creatorParam);
			if(marker == null){
				mMarker = m.mMap.addMarker(mo);
			}else{
				MapUtils.setMarkerOptions(marker, mo);
				mMarker = marker;
			}
			
			updateLatLngBounds(pos,m);
			addMarkerOptionCreatorParam(creatorParam);
		}
		
		public void setCurrentMarkerDelegate(XMarkerDelegate md){
			mCurrentMarkerDelegate = md;
		}
		
		private void updateLatLngBounds(XLatLng ll,MarkerClusterManager m){
			XProjection projection = m.mMap.getProjection();
			Point screenPoint = projection.toScreenLocation(ll);
			Point southwestPoint = new Point(screenPoint.x - m.mGridSize, screenPoint.y
					+ m.mGridSize);
			Point northeastPoint = new Point(screenPoint.x + m.mGridSize, screenPoint.y
					- m.mGridSize);
			mBounds = new XLatLngBounds(
					projection.fromScreenLocation(southwestPoint),
					projection.fromScreenLocation(northeastPoint));
		}
		
		public boolean contains(XLatLng ll){
			return mBounds.contains(ll);
		}
		
		public void addMarkerOptionCreatorParam(Object creatorParam){
			mMarkerOptionCreatorParam.put(creatorParam,creatorParam);
		}
	}
}
