package com.xbcx.map;

public interface XMapItem {

	public void remove();
	
	public void setVisible(boolean bVisible);
	
	public boolean	isVisible();
}
