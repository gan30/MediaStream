package com.xbcx.map;

import java.io.Serializable;

public class XLatLng implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public final double latitude;
	public final double longitude;
	
	public XLatLng(double lat,double lng){
		this.latitude = lat;
		this.longitude = lng;
	}
	
	public double getLat(){
		return latitude;
	}
	
	public double getLng(){
		return longitude;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this){
			return true;
		}
		if(o != null && o instanceof XLatLng){
			XLatLng other = (XLatLng)o;
			return other.latitude == this.latitude && other.longitude == this.longitude;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (int)(this.latitude * 19 + this.longitude);
	}
}
