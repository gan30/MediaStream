package com.xbcx.map;

public interface XCircle extends XMapItem{
	public void setCenter(XLatLng ll);
	
	public XLatLng getCenter();
	
	public void setRadius(float r);
	
	public double getRadius();
	
	public void destroy();
}
