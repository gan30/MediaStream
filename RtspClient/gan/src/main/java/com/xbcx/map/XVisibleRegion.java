package com.xbcx.map;

public interface XVisibleRegion {
	public XLatLngBounds getBounds();
}
