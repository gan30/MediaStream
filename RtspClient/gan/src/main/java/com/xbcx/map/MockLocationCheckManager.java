package com.xbcx.map;

import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import com.xbcx.core.BaseActivity;
import com.xbcx.core.Event;
import com.xbcx.core.Listener;
import com.xbcx.core.SharedPreferenceDefine;
import com.xbcx.core.XApplication;
import com.xbcx.core.module.HttpLoginListener;
import gan.core.R;
import com.xbcx.utils.JsonParseUtils;
import com.xbcx.utils.SystemUtils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;

public class MockLocationCheckManager implements HttpLoginListener{

	static{
		instance = new MockLocationCheckManager();
	}
	
	public static MockLocationCheckManager getInstance(){
		return instance;
	}
	
	private static MockLocationCheckManager instance;
	
	private enum Tip{
		Uninstall,
		Reboot
	}
	
	private String	mCheckMockHttpKey = "is_check_mock";
	
	private Boolean	mIsCheckMock;
	//有的项目没有在登录返回，需要本地判断
	private Boolean	mIsFromNet;
	
	private Boolean	mIsRoot;
	
	private HashMap<String, String> mMockSoftwares = new HashMap<String, String>();
	
	private MockLocationCheckManager(){
		addMockSoftWare("com.txy.anywhere");
		addMockSoftWare("net.anylocation");
	}
	
	private void addMockSoftWare(String pkgName){
		mMockSoftwares.put(pkgName, pkgName);
	}
	
	public void showCancelMockTip(final BaseActivity activity){
		if(!hasMockSoftware(new Listener<Tip>() {
			@Override
			public void onListenCallback(Tip param) {
				if(param == Tip.Uninstall){
					activity.showYesNoDialog(R.string.ok, R.string.cancel,
							R.string.location_mock_dialog_uninstall_msg,
							R.string.location_mock_dialog_title,
							new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
				}else if(param == Tip.Reboot){
					activity.showYesNoDialog(R.string.ok, R.string.cancel,
							R.string.location_mock_dialog_reboot_msg,
							R.string.location_mock_dialog_title,
							new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
				}
			}
		})){
			activity.showYesNoDialog(R.string.location_mock_go_setup, R.string.ok,
					R.string.location_mock_dialog_msg,
					R.string.location_mock_dialog_title,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if (which == DialogInterface.BUTTON_POSITIVE) {
								launchDevelopmentSetttings(activity);
							}
						}
					});
		}
	}
	
	public static void launchDevelopmentSetttings(Activity activity) {
		Intent intent = new Intent();
		intent.setAction("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS");
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try {
			activity.startActivity(intent);
		} catch (ActivityNotFoundException ex) {
			intent.setAction(Settings.ACTION_SETTINGS);
			try {
				activity.startActivity(intent);
			} catch (Exception e) {
			}
		} catch (Exception ex){
			intent.setAction(Settings.ACTION_SETTINGS);
			try {
				activity.startActivity(intent);
			} catch (Exception e) {
			}
		}
	}
	
	public boolean hasMockSoftware(){
		return hasMockSoftware(null);
	}
	
	private boolean hasMockSoftware(Listener<Tip> l){
		/**
		 * 不能根据包名判断运行的进程,天下游等软件虚拟定位对系统进程进行了注入，
		 * 强行停止了天下游，依然能够虚拟定位
		 */
		try{
			if(mIsRoot == null){
				mIsRoot = XApplication.getRootInterface().isRoot();
			}
			if(mIsRoot){
				for(String pkgName : mMockSoftwares.values()){
					String path = "/data/data/" + pkgName;
					final String spKey = "mock_soft_" + pkgName;
					if(XApplication.getRootInterface().exists(path,true)){
						XApplication.runOnBackground(new Runnable() {
							@Override
							public void run() {
								SharedPreferenceDefine.setLongValue(spKey, 
										XApplication.getFixSystemTime());
							}
						});
						if(l != null){
							l.onListenCallback(Tip.Uninstall);
						}
						return true;
					}else{
						if(checkUninstallAndOpenPhoneTime(spKey)){
							if(l != null){
								l.onListenCallback(Tip.Reboot);
							}
							return true;
						}
					}
				}
			}else{
				PackageManager pm = XApplication.getApplication().getPackageManager();
				/**
				 * 某些手机getInstalledPackages会崩溃(红米手机1s)
				 */
				List<PackageInfo> pis = pm.getInstalledPackages(0);
				if(pis != null){
					for(PackageInfo pi : pis){
						for(String pkgName : mMockSoftwares.values()){
							String spKey = "mock_soft_" + pkgName;
							if(pi.packageName.equals(pkgName)){
								SharedPreferenceDefine.setLongValue(spKey, XApplication.getFixSystemTime());
								if(l != null){
									l.onListenCallback(Tip.Uninstall);
								}
								return true;
							}else{
								if(checkUninstallAndOpenPhoneTime(spKey)){
									if(l != null){
										l.onListenCallback(Tip.Reboot);
									}
									return true;
								}
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	private boolean checkUninstallAndOpenPhoneTime(String spKey){
		long lastExistsTime = SharedPreferenceDefine.getLongValue(spKey, 0);
		long openPhoneTime = XApplication.getFixSystemTime() - SystemClock.elapsedRealtime();
		if(lastExistsTime > openPhoneTime){
			return true;
		}
		return false;
	}
	
	public boolean isMock(XLocation location){
		
		if(mIsCheckMock == null){
			mIsCheckMock = SharedPreferenceDefine.getBooleanValue("is_check_mock", true);
		}
		if(mIsCheckMock){
			if(mIsFromNet == null){
				mIsFromNet = SharedPreferenceDefine.getBooleanValue("is_from_net", false);
			}
			if(!mIsFromNet){
				if(Build.VERSION.SDK_INT >= 23){
					return false;
				}
			}
			if(SystemUtils.isMockGPSSettingOpen(XApplication.getApplication())){
				return true;
			}
		}
		return false;
	}

	@Override
	public void onHttpLogined(Event event, JSONObject joRet) {
		if(joRet.has(mCheckMockHttpKey)){
			mIsCheckMock = JsonParseUtils.safeGetBoolean(joRet, mCheckMockHttpKey);
			mIsFromNet = Boolean.valueOf(true);
			SharedPreferenceDefine.setBooleanValue("is_check_mock", mIsCheckMock);
			SharedPreferenceDefine.setBooleanValue("is_from_net", true);
		}
	}
}
