package com.xbcx.map;

public interface OnMarkerClickListener {
	public boolean onMarkerClick(XMarker marker);
}
