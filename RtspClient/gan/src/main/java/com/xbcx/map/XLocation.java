package com.xbcx.map;

import android.os.Bundle;

public interface XLocation {
	public Bundle getExtras();
	
	public void setExtras(Bundle b);
	
	public double getLatitude();
	
	public double getLongitude();
	
	public float getAccuracy();
	
	public String getProvider();
	
	public float getBearing();
	
	public long getTime();
	
	public float getSpeed();
	
	public double getAltitude();
	
	public void setLatitude(double lat);
	
	public void setLongitude(double lng);
	
	public void setAccuracy(float acc);
	
	public void setBearing(float bearing);
	
	public void setTime(long time);
	
	public void setSpeed(float f);
	
	public void setAltitude(float a);

	public void setAddress(XRegeocodeAddress ra);
	
	public void setAddress(String address);
	
	public String getAddress();
	
	public String getStreet();
	
	public String getStreetNum();
	
	public String getProvince();
	
	public String getCity();
	
	public String getDistrict();
	
	public MapException getException();
}
