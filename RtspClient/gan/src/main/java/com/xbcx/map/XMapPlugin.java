package com.xbcx.map;

import com.xbcx.core.BaseActivity;
import com.xbcx.core.module.AppBaseListener;

public interface XMapPlugin extends AppBaseListener{

	public void onXMapLoaded(XMap map, BaseActivity activity);
}
