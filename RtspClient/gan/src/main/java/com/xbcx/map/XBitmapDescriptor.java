package com.xbcx.map;

public interface XBitmapDescriptor {
	public int getHeight();
	
	public int getWidth();
}
