package com.xbcx.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XPolygonOptions {
	
	private List<XLatLng> 	latLngs = new ArrayList<XLatLng>();
	private boolean			isSetFillColor;
	private int				fillColor;
	private	boolean			isSetStrokeColor;
	private int				strokeColor;
	private boolean			isSetStrokeWidth;
	private int				strokeWidth;
	private boolean			isSetVisible;
	private boolean			visible;
	private boolean			isSetZIndex;
	private	int				zIndex;
	
	public XPolygonOptions add(XLatLng xll){
		latLngs.add(xll);
		return this;
	}
	
	public XPolygonOptions add(XLatLng ... params){
		for(XLatLng ll : params){
			latLngs.add(ll);
		}
		return this;
	}
	
	public XPolygonOptions addAll(Iterable<XLatLng> it){
		Iterator<XLatLng> ir = it.iterator();
		while(ir.hasNext()){
			latLngs.add(ir.next());
		}
		return this;
	}
	
	public XPolygonOptions fillColor(int fillColor){
		isSetFillColor = true;
		this.fillColor = fillColor;
		return this;
	}
	
	public XPolygonOptions strokeColor(int strokeColor){
		isSetStrokeColor = true;
		this.strokeColor = strokeColor;
		return this;
	}
	
	public XPolygonOptions strokeWidth(int width){
		isSetStrokeWidth = true;
		strokeWidth = width;
		return this;
	}
	
	public XPolygonOptions setVisible(boolean visible){
		isSetVisible = true;
		this.visible = visible;
		return this;
	}
	
	public XPolygonOptions setZIndex(int zIndex){
		isSetZIndex = true;
		this.zIndex = zIndex;
		return this;
	}

	public List<XLatLng> getPoints() {
		return latLngs;
	}

	public boolean isSetFillColor() {
		return isSetFillColor;
	}

	public int getFillColor() {
		return fillColor;
	}

	public boolean isSetStrokeColor() {
		return isSetStrokeColor;
	}

	public int getStrokeColor() {
		return strokeColor;
	}

	public boolean isSetStrokeWidth() {
		return isSetStrokeWidth;
	}

	public int getStrokeWidth() {
		return strokeWidth;
	}

	public boolean isSetVisible() {
		return isSetVisible;
	}

	public boolean isVisible() {
		return visible;
	}

	public boolean isSetZIndex() {
		return isSetZIndex;
	}

	public int getzIndex() {
		return zIndex;
	}
}


