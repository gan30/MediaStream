package com.xbcx.map;

import java.util.HashMap;
import java.util.Map.Entry;

public class XMapItemBoundsDrawManager implements OnCameraChangeListener,
													OnMapClearListener,
													OnMapItemRemoveListener{

	private XMap mMap;
	private HashMap<XMapItem, XLatLngBounds> mMapBoundsDrawMapItem = new HashMap<XMapItem, XLatLngBounds>();
	private HashMap<XMapItem, XMapItem> 	 mMapVisibleMapItems = new HashMap<XMapItem, XMapItem>();
	
	public XMapItemBoundsDrawManager(XMap map) {
		mMap = map;
		map.addOnCameraChangeListener(this);
		map.addOnMapClearListener(this);
		map.addOnMapItemRemoveListener(this);
	}
	
	public XMapItemBoundsDrawManager addToBoundsDraw(XMapItem item){
		if(item instanceof XMarker){
			XMarker m = (XMarker)item;
			mMapBoundsDrawMapItem.put(item, new XLatLngBounds(new SingleLatLngBoundsImpl(m.getPosition())));
		}else if(item instanceof XPolyline){
			XPolyline p = (XPolyline)item;
			mMapBoundsDrawMapItem.put(item, new XLatLngBounds(p.getPoints()));
		}else if(item instanceof XPolygon){
			XPolygon b = (XPolygon)item;
			mMapBoundsDrawMapItem.put(item, new XLatLngBounds(b.getPoints()));
		}
		mMapVisibleMapItems.put(item, item);
		return this;
	}

	@Override
	public void onMapItemRemoved(XMapItem item) {
		mMapBoundsDrawMapItem.remove(item);
		mMapVisibleMapItems.remove(item);
	}

	@Override
	public void onMapCleared() {
		mMapBoundsDrawMapItem.clear();
		mMapVisibleMapItems.clear();
	}
	
	@Override
	public void onCameraChange(XCameraPosition arg0) {
		
	}

	@Override
	public void onCameraChangeFinish(XCameraPosition arg0) {
		for(Entry<XMapItem, XLatLngBounds> e : mMapBoundsDrawMapItem.entrySet()){
			XMapItem item = e.getKey();
			if(mMap.getProjection().getVisibleRegion().getBounds().intersects(e.getValue())){
				if(mMapVisibleMapItems.put(item,item) == null){
					item.setVisible(true);
				}
			}else{
				XMapItem remove = mMapVisibleMapItems.remove(item);
				if(remove != null){
					item.setVisible(false);
				}
			}
		}
	}
	
	private static class SingleLatLngBoundsImpl implements XLatLngBoundsInterface{

		private XLatLng mLatLng;
		
		public SingleLatLngBoundsImpl(XLatLng ll) {
			mLatLng = ll;
		}
		
		@Override
		public boolean contains(XLatLng ll) {
			return mLatLng.equals(ll);
		}

		@Override
		public XLatLng getNortheast() {
			return mLatLng;
		}

		@Override
		public XLatLng getSouthwest() {
			return mLatLng;
		}

		@Override
		public XLatLngBounds including(XLatLng ll) {
			XLatLngBounds.Builder b = new XLatLngBounds.Builder();
			b.include(mLatLng);
			b.include(ll);
			return b.build();
		}

		@Override
		public boolean intersects(XLatLngBounds bounds) {
			return bounds.contains(mLatLng);
		}
	}
}
