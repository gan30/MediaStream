package com.xbcx.map;

public class XGroundOverlayOptions {

	private Float 				mAnchorX;
	private Float 				mAnchorY;
	private Float				mBearing;
	private XBitmapDescriptor	mImage;
	private Float				mTransparency;
	private XLatLng				mPosition;
	private Integer				mWidth;
	private Integer				mHeight;
	private XLatLngBounds		mBounds;
	
	public XGroundOverlayOptions anchor(float x,float y){
		mAnchorX = x;
		mAnchorY = y;
		return this;
	}
	
	public XGroundOverlayOptions bearing(float b){
		mBearing = b;
		return this;
	}
	
	public XGroundOverlayOptions image(XBitmapDescriptor bd){
		mImage = bd;
		return this;
	}
	
	public XGroundOverlayOptions transparency(float f){
		mTransparency = f;
		return this;
	}
	
	public XGroundOverlayOptions position(XLatLng ll,int width){
		mPosition = ll;
		mWidth = width;
		return this;
	}
	
	public XGroundOverlayOptions position(XLatLng ll,int width,int height){
		mPosition = ll;
		mWidth = width;
		mHeight = height;
		return this;
	}
	
	public XGroundOverlayOptions positionFromBounds(XLatLngBounds bounds){
		mBounds = bounds;
		return this;
	}
	
	public Float getAnchorX(){
		return mAnchorX;
	}
	
	public Float getAnchorY(){
		return mAnchorY;
	}

	public Float getBearing() {
		return mBearing;
	}

	public XBitmapDescriptor getImage() {
		return mImage;
	}

	public Float getTransparency() {
		return mTransparency;
	}

	public XLatLng getPosition() {
		return mPosition;
	}

	public Integer getPositionWidth() {
		return mWidth;
	}

	public Integer getPositionHeight() {
		return mHeight;
	}

	public XLatLngBounds getPositionBounds() {
		return mBounds;
	}
}
