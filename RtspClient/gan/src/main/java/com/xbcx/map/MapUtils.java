package com.xbcx.map;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.location.Location;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

public class MapUtils {
	
	private final static double pi = 3.1415926535897932384626;
	private final static double a = 6378245.0;
	private final static double ee = 0.00669342162296594323;
	
	public static float calculateLocationDistance(XLatLng ll1, XLatLng ll2) {
		return calculateLocationDistance(ll1.getLat(), ll1.getLng(),
				ll2.getLat(), ll2.getLng());
	}
	
	public static float calculateLocationDistance(double lat1, double lng1,
			double lat2, double lng2) {
		final float results[] = new float[1];
		Location.distanceBetween(lat1, lng1, lat2, lng2, results);
		return results[0];
	}
	
	public static XLatLng GPS84_To_Gcj02(double lat, double lon) {
		double mgLat;
		double mgLon;
		if (outOfChina(lat, lon)) {
			mgLat = lat;
			mgLon = lon;
			return new XLatLng(mgLat, mgLon);
		}
		double dLat = transformLat(lon - 105.0, lat - 35.0);
		double dLon = transformLon(lon - 105.0, lat - 35.0);
		double radLat = lat / 180.0 * pi;
		double magic = Math.sin(radLat);
		magic = 1 - ee * magic * magic;
		double sqrtMagic = Math.sqrt(magic);
		dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
		dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);
		mgLat = lat + dLat;
		mgLon = lon + dLon;
		return new XLatLng(mgLat, mgLon);
	}

	public static XLatLng gcj02_To_GPS84(double lat, double lon) {
		XLatLng GPS = transform(lat, lon);
		double lontitude = lon * 2 - GPS.getLng();
		double latitude = lat * 2 - GPS.getLat();
		return new XLatLng(latitude, lontitude);
	}

	public static boolean outOfChina(double lat, double lon) {
		if (lon < 72.004 || lon > 137.8347)
			return true;
		if (lat < 0.8293 || lat > 55.8271)
			return true;
		return false;
	}

	public static XLatLng transform(double lat, double lon) {
		if (outOfChina(lat, lon)) {
			return new XLatLng(lat, lon);
		}
		double dLat = transformLat(lon - 105.0, lat - 35.0);
		double dLon = transformLon(lon - 105.0, lat - 35.0);
		double radLat = lat / 180.0 * pi;
		double magic = Math.sin(radLat);
		magic = 1 - ee * magic * magic;
		double sqrtMagic = Math.sqrt(magic);
		dLat = (dLat * 180.0) / ((a * (1 - ee)) / (magic * sqrtMagic) * pi);
		dLon = (dLon * 180.0) / (a / sqrtMagic * Math.cos(radLat) * pi);
		double mgLat = lat + dLat;
		double mgLon = lon + dLon;
		return new XLatLng(mgLat, mgLon);
	}

	public static double transformLat(double x, double y) {
		double ret = -100.0 + 2.0 * x + 3.0 * y + 0.2 * y * y + 0.1 * x * y + 0.2 * Math.sqrt(Math.abs(x));
		ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;
		ret += (20.0 * Math.sin(y * pi) + 40.0 * Math.sin(y / 3.0 * pi)) * 2.0 / 3.0;
		ret += (160.0 * Math.sin(y / 12.0 * pi) + 320 * Math.sin(y * pi / 30.0)) * 2.0 / 3.0;
		return ret;
	}

	public static double transformLon(double x, double y) {
		double ret = 300.0 + x + 2.0 * y + 0.1 * x * x + 0.1 * x * y + 0.1 * Math.sqrt(Math.abs(x));
		ret += (20.0 * Math.sin(6.0 * x * pi) + 20.0 * Math.sin(2.0 * x * pi)) * 2.0 / 3.0;
		ret += (20.0 * Math.sin(x * pi) + 40.0 * Math.sin(x / 3.0 * pi)) * 2.0 / 3.0;
		ret += (150.0 * Math.sin(x / 12.0 * pi) + 300.0 * Math.sin(x / 30.0 * pi)) * 2.0 / 3.0;
		return ret;
	}
	
	public static XLatLng gcj02_To_Bd09(double gg_lat, double gg_lon) {
		double x = gg_lon, y = gg_lat;
		double z = Math.sqrt(x * x + y * y) + 0.00002 * Math.sin(y * pi);
		double theta = Math.atan2(y, x) + 0.000003 * Math.cos(x * pi);
		double bd_lon = z * Math.cos(theta) + 0.0065;
		double bd_lat = z * Math.sin(theta) + 0.006;
		return new XLatLng(bd_lat, bd_lon);
	}

	public static XLatLng bd09_To_Gcj02(double bd_lat, double bd_lon) {
		double x = bd_lon - 0.0065, y = bd_lat - 0.006;
		double z = Math.sqrt(x * x + y * y) - 0.00002 * Math.sin(y * pi);
		double theta = Math.atan2(y, x) - 0.000003 * Math.cos(x * pi);
		double gg_lon = z * Math.cos(theta);
		double gg_lat = z * Math.sin(theta);
		return new XLatLng(gg_lat, gg_lon);
	}

	public static XLatLng bd09_To_GPS84(double bd_lat, double bd_lon) {

		XLatLng gcj02 = bd09_To_Gcj02(bd_lat, bd_lon);
		XLatLng map84 = gcj02_To_GPS84(gcj02.getLat(), gcj02.getLng());
		return map84;
	}
	
	public static boolean isLocationEffective(double lat,double lng){
		return lat != 0 && lng != 0;
	}
	
	public static float getMapCurrentZoom(XMap map){
		if(map == null){
			return 0;
		}
		XCameraPosition cp = map.getCameraPosition();
		return cp == null ? 0 : cp.getZoom();
	}
	
	public static XLatLng toLatLng(XLocation l){
		return new XLatLng(l.getLatitude(), l.getLongitude());
	}
	
	public static void setMarkerOptions(XMarker xm,XMarkerOptions mo){
		xm.setIcon(mo.getIcon());
		xm.setPosition(mo.getPos());
		
		Float fx = mo.getAnchorX();
		Float fy = mo.getAnchorY();
		if(fx != null && fy != null){
			xm.setAnchor(fx, fy);
		}
		Boolean b = mo.getDraggable();
		if(b != null){
			xm.setDraggable(b);
		}
		b = mo.getFlat();
		if(b != null){
			xm.setFlat(b);
		}
		b = mo.getPerspective();
		if(b != null){
			xm.setPerspective(b);
		}
		Integer i = mo.getRotateAngle();
		if(i != null){
			xm.setRotateAngle(i);
		}
		String s = mo.getSnippet();
		if(s != null){
			xm.setSnippet(s);
		}
		s = mo.getTitle();
		if(s != null){
			xm.setTitle(s);
		}
		b = mo.getVisible();
		if(b != null){
			xm.setVisible(b);
		}
	}
	
	public static List<XLatLng> parseArrays(JSONArray ja){
		List<XLatLng> lls = new ArrayList<XLatLng>();
		int length = ja.length();
		for(int index = 0;index < length;++index){
			JSONObject jo = ja.optJSONObject(index);
			lls.add(new XLatLng(jo.optDouble("lat"), jo.optDouble("lng")));
		}
		return lls;
	}
	
	public static Bitmap getViewBitmapForMap(View v){
		FrameLayout fl = new FrameLayout(v.getContext());
		fl.addView(v);
		fl.setDrawingCacheEnabled(true);
		handleViewForMapBitmap(v);
		fl.destroyDrawingCache();
		fl.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), 
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		fl.layout(0, 0, fl.getMeasuredWidth(), fl.getMeasuredHeight());
		Bitmap bmp = fl.getDrawingCache().copy(Bitmap.Config.ARGB_8888, false);
		fl.destroyDrawingCache();
		return bmp;
	}
	
	/**
	 * TextView
	 * setSingleLine(true)会把mHorizontallyScrolling设为true
	 * 
	 * onMeasure中
	 * int want = width - getCompoundPaddingLeft() - getCompoundPaddingRight();
     * int unpaddedWidth = want;
     * if (mHorizontallyScrolling) want = VERY_WIDE(1024*1024);
     * 
     * want会传递给BoringLayout的width
     * 
	 * BoringLayout中Gravity为Center时
	 * drawText传递的值是1024*1024/2,普通Canvas画不出来,
	 * 系统窗口传递过来的是DisplayListCanvas能画出来
	 * @param v
	 */
	private static void handleViewForMapBitmap(View v) {
		if ((v instanceof ViewGroup)) {
			for (int i = 0; i < ((ViewGroup) v).getChildCount(); i++) {
				handleViewForMapBitmap(((ViewGroup) v).getChildAt(i));
			}
		}
		if ((v instanceof TextView)) {
			((TextView) v).setHorizontallyScrolling(false);
		}
	}
}
