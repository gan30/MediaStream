package com.xbcx.map;

import android.os.Bundle;

public interface LocationListener {
	
	public void onStatusChanged(String provider, int status, Bundle extras);

	public void onProviderEnabled(String provider);

	public void onProviderDisabled(String provider);

	public void onLocationChanged(XLocation location);
}
