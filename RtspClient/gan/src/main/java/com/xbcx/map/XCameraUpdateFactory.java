package com.xbcx.map;

public final class XCameraUpdateFactory {
	public static XCameraUpdate newLatLngBounds(XLatLngBounds b,int padding){
		return XMap.getMapCreator().createCameraUpdateCreator().newLatLngBounds(b, padding);
	}
	
	public static XCameraUpdate newLatLngZoom(XLatLng ll,float zoom){
		return XMap.getMapCreator().createCameraUpdateCreator().newLatLngZoom(ll, zoom);
	}
	
	public static XCameraUpdate scrollBy(float x,float y){
		return XMap.getMapCreator().createCameraUpdateCreator().scrollBy(x, y);
	}
	
	public static XCameraUpdate changeLatLng(XLatLng ll){
		return XMap.getMapCreator().createCameraUpdateCreator().changeLatLng(ll);
	}
	
	public static XCameraUpdate zoomIn(){
		return XMap.getMapCreator().createCameraUpdateCreator().zoomIn();
	}
	
	public static XCameraUpdate zoomOut(){
		return XMap.getMapCreator().createCameraUpdateCreator().zoomOut();
	}
	
	public static XCameraUpdate zoomTo(float zoom){
		return XMap.getMapCreator().createCameraUpdateCreator().zoomTo(zoom);
	}
}
