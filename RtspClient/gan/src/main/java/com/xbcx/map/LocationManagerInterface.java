package com.xbcx.map;

public interface LocationManagerInterface {
	public void setGpsEnable(boolean b);
	
	public void setNeedDesc(boolean b);
	
	public void requestLocationData(String provider, long time, float distance, LocationListener listener);
	
	public void removeUpdates(LocationListener l);
}
