package com.xbcx.map;

public interface XUISettings {
	public void setZoomControlsEnabled(boolean b);
	
	public void setRotateGesturesEnabled(boolean b);
	
	public void setTiltGesturesEnabled(boolean b);
	
	public void setCompassEnabled(boolean b);
}
