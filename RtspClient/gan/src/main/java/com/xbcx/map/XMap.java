package com.xbcx.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.XApplication;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;

public class XMap {
	
	private static MapCreator creator;
	private static LocateCreator locateCreator;
	
	public static void setMapCreator(MapCreator mc){
		creator = mc;
	}
	
	public static MapCreator getMapCreator(){
		return creator;
	}
	
	public static void setLocateCreator(LocateCreator creator){
		locateCreator = creator;
	}
	
	public static LocateCreator getLocateCreator(){
		return locateCreator;
	}
	
	public static enum CoorType{
		National,//国测局
		BD,//百度经纬度坐标系
	}
	
	public static CoorType locationCoorType = CoorType.National;
	
	private MapInterface 		impl;
	private View				mMapView;
	private OnMapLoadedListener				mMapLoadedListener;
	private List<OnCameraChangeListener> 	mOnCameraChangeListeners;
	private List<OnMapClearListener> 		mOnMapClearListeners;
	private List<OnMapItemRemoveListener> 	mOnMapItemRemoveListeners;
	private List<OnMarkerClickListener> 	mOnMarkerClickListeners;
	
	private Object 							mMapIgnoreClearItemsSync = new Object();
	private HashMap<XMapItem, XMapItem> 	mMapIgnoreClearItems;
	private HashMap<XMapItem, XMapItem> 	mMapAllItems = new HashMap<XMapItem, XMapItem>();
	
	public XMap(FragmentActivity activity,int resId) {
		Fragment f = activity.getSupportFragmentManager().findFragmentById(resId);
		if(f == null){
			mMapView = activity.findViewById(resId);
			activity.getLayoutInflater().inflate(creator.getFragmentLayoutId(), (ViewGroup)mMapView);
			f = activity.getSupportFragmentManager().findFragmentById(creator.getFragmentLayoutMapId());
			impl = creator.createMap(f);
		}else{
			mMapView = f.getView();
			impl = creator.createMap(f);
		}
		final FragmentActivity a = activity;
		impl.setOnMapLoadedListener(new OnMapLoadedListener() {
			@Override
			public void onMapLoaded() {
				if(mMapLoadedListener != null){
					mMapLoadedListener.onMapLoaded();
				}	
				if(a instanceof BaseActivity){
					for(XMapPlugin p : XApplication.getManagers(XMapPlugin.class)){
						p.onXMapLoaded(XMap.this,(BaseActivity)a);
					}
				}
			}
		});
		impl.setOnMarkerClickListener(new OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(XMarker marker) {
				if(mOnMarkerClickListeners != null){
					for(OnMarkerClickListener l : mOnMarkerClickListeners){
						if(l.onMarkerClick(marker)){
							return true;
						}
					}
				}
				return false;
			}
		});
		impl.setOnCameraChangeListener(new OnCameraChangeListener() {
			@Override
			public void onCameraChangeFinish(XCameraPosition arg0) {
				if(mOnCameraChangeListeners != null){
					for(OnCameraChangeListener l : mOnCameraChangeListeners){
						l.onCameraChangeFinish(arg0);
					}
				}
			}
			
			@Override
			public void onCameraChange(XCameraPosition arg0) {
				if(mOnCameraChangeListeners != null){
					for(OnCameraChangeListener l : mOnCameraChangeListeners){
						l.onCameraChange(arg0);
					}
				}
			}
		});
	}
	
	public static void setOfflineDataPath(String path){
		getMapCreator().setOfflineDataPath(path);
	}
	
	public View getView(){
		return mMapView;
	}
	
	public void handleZoomBug(BaseActivity activity){
		if(activity != null && activity instanceof BaseActivity){
			((BaseActivity)activity).registerPlugin(new FixBugPlugin());
		}
	}
	
	public void addToIgoneClearMapItem(XMapItem item){
		synchronized (mMapIgnoreClearItemsSync) {
			if(mMapIgnoreClearItems == null){
				mMapIgnoreClearItems = new HashMap<XMapItem, XMapItem>();
			}
			mMapIgnoreClearItems.put(item, item);
		}
	}
	
	public XCircle addCircle(XCircleOptions option){
		XCircle wrap = impl.addCircle(option);
		return proxyMapItem(new XCircleWrapper(wrap,option));
	}
	
	public XMarker addMarker(XMarkerOptions option){
		XMarker wrap = impl.addMarker(option);
		return proxyMapItem(new XMarkerWrapper(wrap,option));
	}
	
	public XPolyline addPolyline(XPolylineOptions option){
		XPolyline wrap = impl.addPolyLine(option);
		return proxyMapItem(new XPolylineWrapper(wrap,option));
	}
	
	public XPolygon addPolygon(XPolygonOptions option){
		XPolygon wrap = impl.addPolygon(option);
		return proxyMapItem(new XPolygonWrapper(wrap,option));
	}
	
	public XGroundOverlay addGroundOverlay(XGroundOverlayOptions option){
		XGroundOverlay wrap = impl.addGroundOverlay(option);
		return proxyMapItem(new XGroundOverlayWrapper(wrap, option));
	}
	
	@SuppressWarnings("unchecked")
	private <T extends XMapItem,Option> T proxyMapItem(XMapItemWrapper<T,Option> wrapper){
		synchronized (mMapAllItems) {
			mMapAllItems.put(wrapper, wrapper);
		}
		return (T)wrapper;
	}
	
	public void setOnMarkerClickListener(OnMarkerClickListener listener){
		addOnMarkerClickListener(listener);
	}
	
	void addOnMarkerClickListener(OnMarkerClickListener listener){
		if(mOnMarkerClickListeners == null){
			mOnMarkerClickListeners = new ArrayList<OnMarkerClickListener>();
		}
		mOnMarkerClickListeners.add(listener);
	}
	
	public void setOnCameraChangeListener(OnCameraChangeListener l){
		addOnCameraChangeListener(l);
	}
	
	void addOnCameraChangeListener(OnCameraChangeListener l){
		if(mOnCameraChangeListeners == null){
			mOnCameraChangeListeners = new ArrayList<OnCameraChangeListener>();
		}
		if(!mOnCameraChangeListeners.contains(l)){
			mOnCameraChangeListeners.add(l);
		}
	}
	
	public void addOnMapClearListener(OnMapClearListener l){
		if(mOnMapClearListeners == null){
			mOnMapClearListeners = new ArrayList<OnMapClearListener>();
		}
		if(!mOnMapClearListeners.contains(l)){
			mOnMapClearListeners.add(l);
		}
	}
	
	public void addOnMapItemRemoveListener(OnMapItemRemoveListener l){
		if(mOnMapItemRemoveListeners == null){
			mOnMapItemRemoveListeners = new ArrayList<OnMapItemRemoveListener>();
		}
		if(!mOnMapItemRemoveListeners.contains(l)){
			mOnMapItemRemoveListeners.add(l);
		}
	}
	
	public void setOnMapClickListener(OnMapClickListener l){
		impl.setOnMapClickListener(l);
	}
	
	public void setOnMapLoadedListener(OnMapLoadedListener l){
		mMapLoadedListener = l;
	}
	
	public XUISettings getUiSettings(){
		return impl.getUiSettings();
	}
	
	public void setLoadOfflineData(boolean b){
		impl.setLoadOfflineData(b);
	}
	
	public void animateCamera(XCameraUpdate update){
		impl.animateCamera(update);
	}
	
	public void animateCamera(XCameraUpdate update,long millis,CancelableCallback cb){
		impl.animateCamera(update,millis,cb);
	}
	
	public void moveCamera(XCameraUpdate update){
		impl.moveCamera(update);
	}
	
	public XCameraPosition getCameraPosition(){
		return impl.getCameraPosition();
	}
	
	public XProjection getProjection(){
		return impl.getProjection();
	}
	
	public float getScalePerPixel(){
		return impl.getScalePerPixel();
	}
	
	public void clear(){
		if(hasIgnoreClearMapItems()){
			List<XMapItem> deletes = new ArrayList<XMapItem>();
			synchronized (mMapAllItems) {
				for(Entry<XMapItem, XMapItem> e : mMapAllItems.entrySet()){
					synchronized (mMapIgnoreClearItemsSync) {
						if(!mMapIgnoreClearItems.containsKey(e.getKey())){
							deletes.add(e.getKey());
						}
					}
				}
			}
			
			for(XMapItem item : deletes){
				item.remove();
				mMapAllItems.remove(item);
			}
		}else{
			impl.clear();
			synchronized (mMapAllItems) {
				mMapAllItems.clear();
			}
			if(mOnMapClearListeners != null){
				for(OnMapClearListener l : mOnMapClearListeners){
					l.onMapCleared();
				}
			}
		}
	}
	
	private boolean hasIgnoreClearMapItems(){
		synchronized (mMapIgnoreClearItemsSync) {
			return mMapIgnoreClearItems != null && mMapIgnoreClearItems.size() > 0;
		}
	}
	
	abstract class XMapItemWrapper<T extends XMapItem,Option> implements XMapItem{
		
		protected T 		mWrapper;
		protected Option 	mOption;
		
		public XMapItemWrapper(T wrapper,Option option) {
			mWrapper = wrapper;
			mOption = option;
		}
		
		final void rebuildWrapper(){
			mWrapper = onRebuildWrapper();
		}
		
		protected abstract T onRebuildWrapper();
		
		@Override
		public void remove() {
			mWrapper.remove();
			onRemoved();
		}	
		
		protected void onRemoved(){
			synchronized (mMapIgnoreClearItemsSync) {
				if(mMapIgnoreClearItems != null){
					mMapIgnoreClearItems.remove(this);
				}	
			}	
			synchronized (mMapAllItems) {
				mMapAllItems.remove(this);
				if(mOnMapItemRemoveListeners != null){
					for(OnMapItemRemoveListener l : mOnMapItemRemoveListeners){
						l.onMapItemRemoved(this);
					}
				}
			}
		}
		
		@Override
		public void setVisible(boolean bVisible) {
			mWrapper.setVisible(bVisible);
		}
		
		@Override
		public boolean isVisible() {
			return mWrapper.isVisible();
		}
	}
	
	class XMarkerWrapper extends XMapItemWrapper<XMarker,XMarkerOptions> implements XMarker{
		
		public XMarkerWrapper(XMarker wrapper,XMarkerOptions mo) {
			super(wrapper,mo);
		}
		
		@Override
		protected XMarker onRebuildWrapper() {
			XMarker marker = impl.addMarker(mOption);
			marker.setObject(mWrapper.getObject());
			return marker;
		}

		@Override
		public void setPosition(XLatLng ll) {
			mWrapper.setPosition(ll);
		}

		@Override
		public void setAnchor(float fx, float fy) {
			mWrapper.setAnchor(fx, fy);
		}

		@Override
		public void setIcon(XBitmapDescriptor xbd) {
			mWrapper.setIcon(xbd);
		}

		@Override
		public void setTitle(String sTitle) {
			mWrapper.setTitle(sTitle);
		}

		@Override
		public void setSnippet(String sSnippet) {
			mWrapper.setSnippet(sSnippet);
		}

		@Override
		public void setDraggable(boolean bDraggable) {
			mWrapper.setDraggable(bDraggable);
		}

		@Override
		public void setPerspective(boolean bPerspective) {
			mWrapper.setPerspective(bPerspective);
		}

		@Override
		public void setRotateAngle(int iRotateAngle) {
			mWrapper.setRotateAngle(iRotateAngle);
		}

		@Override
		public void setFlat(boolean bIsFlat) {
			mWrapper.setFlat(bIsFlat);
		}

		@Override
		public void setObject(Object object) {
			mWrapper.setObject(object);
		}

		@Override
		public void setToTop() {
			mWrapper.setToTop();
		}

		@Override
		public XLatLng getPosition() {
			return mWrapper.getPosition();
		}

		@Override
		public List<XBitmapDescriptor> getIcons() {
			return mWrapper.getIcons();
		}

		@Override
		public Object getObject() {
			return mWrapper.getObject();
		}

		@Override
		public void destroy() {
			mWrapper.destroy();
			onRemoved();
		}
	}
	
	class XCircleWrapper extends XMapItemWrapper<XCircle,XCircleOptions> implements XCircle{

		@Override
		protected XCircle onRebuildWrapper() {
			return impl.addCircle(mOption);
		}
		
		public XCircleWrapper(XCircle wrapper,XCircleOptions o) {
			super(wrapper,o);
		}

		@Override
		public void setCenter(XLatLng ll) {
			mWrapper.setCenter(ll);
		}

		@Override
		public XLatLng getCenter() {
			return mWrapper.getCenter();
		}

		@Override
		public void setRadius(float r) {
			mWrapper.setRadius(r);
		}

		@Override
		public double getRadius() {
			return mWrapper.getRadius();
		}

		@Override
		public void destroy() {
			mWrapper.destroy();
			onRemoved();
		}
	}
	
	class XPolylineWrapper extends XMapItemWrapper<XPolyline,XPolylineOptions> implements XPolyline{

		public XPolylineWrapper(XPolyline wrapper,XPolylineOptions o) {
			super(wrapper,o);
		}
		
		@Override
		protected XPolyline onRebuildWrapper() {
			return impl.addPolyLine(mOption);
		}

		@Override
		public float getWidth() {
			return mWrapper.getWidth();
		}

		@Override
		public void setWidth(float width) {
			mWrapper.setWidth(width);
		}

		@Override
		public int getColor() {
			return mWrapper.getColor();
		}

		@Override
		public void setColor(int color) {
			mWrapper.setColor(color);
		}

		@Override
		public float getZIndex() {
			return mWrapper.getZIndex();
		}

		@Override
		public void setZIndex(float zIndex) {
			mWrapper.setZIndex(zIndex);
		}

		@Override
		public boolean isDottedLine() {
			return mWrapper.isDottedLine();
		}

		@Override
		public List<XLatLng> getPoints() {
			return mWrapper.getPoints();
		}
	}
	
	class XPolygonWrapper extends XMapItemWrapper<XPolygon,XPolygonOptions> implements XPolygon{

		public XPolygonWrapper(XPolygon wrapper,XPolygonOptions o) {
			super(wrapper,o);
		}
		
		@Override
		protected XPolygon onRebuildWrapper() {
			return impl.addPolygon(mOption);
		}

		@Override
		public void setFillColor(int fillColor) {
			mWrapper.setFillColor(fillColor);
		}

		@Override
		public int getFillColor() {
			return mWrapper.getFillColor();
		}

		@Override
		public void setStrokeColor(int strokeColor) {
			mWrapper.setStrokeColor(strokeColor);
		}

		@Override
		public int getStrokeColor() {
			return mWrapper.getStrokeColor();
		}

		@Override
		public void setStrokeWidth(float strokeWidth) {
			mWrapper.setStrokeWidth(strokeWidth);
		}

		@Override
		public float getStrokeWidth() {
			return mWrapper.getStrokeWidth();
		}

		@Override
		public float getZIndex() {
			return mWrapper.getZIndex();
		}

		@Override
		public void setZIndex(float zIndex) {
			mWrapper.setZIndex(zIndex);
		}

		@Override
		public void setPoints(List<XLatLng> points) {
			mWrapper.setPoints(points);
		}

		@Override
		public List<XLatLng> getPoints() {
			return mWrapper.getPoints();
		}
	}
	
	class XGroundOverlayWrapper extends XMapItemWrapper<XGroundOverlay, XGroundOverlayOptions> implements
													XGroundOverlay{

		public XGroundOverlayWrapper(XGroundOverlay wrapper, XGroundOverlayOptions option) {
			super(wrapper, option);
		}

		@Override
		protected XGroundOverlay onRebuildWrapper() {
			return impl.addGroundOverlay(mOption);
		}
	}
	
	/**
	 * @author Administrator
	 * 某些机器其它页面返回地图会发生地图变小,原因不明(高德)
	 */
	private class FixBugPlugin extends ActivityPlugin<BaseActivity> implements
												Runnable{
		private float	mMapSaveZoom;
		
		@Override
		protected void onResume() {
			super.onResume();
			if(mMapSaveZoom > 0){
				XApplication.getMainThreadHandler().post(this);
			}
		}
		
		@Override
		protected void onDestroy() {
			super.onDestroy();
			XApplication.getMainThreadHandler().removeCallbacks(this);
		}
		
		@Override
		protected void onPause() {
			super.onPause();
			mMapSaveZoom = getCameraPosition().getZoom();
		}

		@Override
		public void run() {
			XCameraPosition xcp = getCameraPosition();
			if(xcp != null){
				if(xcp.getZoom() > 0){
					if(xcp.getZoom() != mMapSaveZoom){
						moveCamera(XCameraUpdateFactory.newLatLngZoom(xcp.getTarget(), mMapSaveZoom));
					}
				}else{
					XApplication.getMainThreadHandler().post(this);
				}
			}
		}
	}
}
