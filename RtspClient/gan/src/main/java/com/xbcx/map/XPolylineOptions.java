package com.xbcx.map;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class XPolylineOptions {
	
	private List<XLatLng> 		latLngs = new ArrayList<XLatLng>();
	private	boolean				isSetColor;
	private int					color;
	private boolean				isSetWidth;
	private int					width;
	private boolean				isDottedLine;
	private boolean				isSetVisible;
	private boolean				visible;
	private boolean				isSetZIndex;
	private	int					zIndex;
	private Boolean				useTexture;
	private	XBitmapDescriptor	customTexture;
	
	private boolean				isSetGeodesic;
	private boolean				isGeodesic;
	
	public XPolylineOptions add(XLatLng xll){
		latLngs.add(xll);
		return this;
	}
	
	public XPolylineOptions add(XLatLng ... params){
		for(XLatLng ll : params){
			latLngs.add(ll);
		}
		return this;
	}
	
	public XPolylineOptions addAll(Iterable<XLatLng> it){
		Iterator<XLatLng> ir = it.iterator();
		while(ir.hasNext()){
			latLngs.add(ir.next());
		}
		return this;
	}
	
	public List<XLatLng> getPoints(){
		return latLngs;
	}
	
	public XPolylineOptions width(int width){
		isSetWidth = true;
		this.width = width;
		return this;
	}
	
	public XPolylineOptions color(int cor){
		isSetColor = true;
		this.color = cor;
		return this;
	}
	
	public XPolylineOptions zIndex(int zIndex){
		isSetZIndex = true;
		this.zIndex = zIndex;
		return this;
	}
	
	public XPolylineOptions visible(boolean visible){
		isSetVisible = true;
		this.visible = visible;
		return this;
	}
	
	public XPolylineOptions setDottedLine(boolean isDottedLine){
		this.isDottedLine = isDottedLine;
		return this;
	}
	
	public XPolylineOptions setUseTexture(boolean b){
		useTexture = b;
		return this;
	}
	
	public XPolylineOptions setCustomTexture(XBitmapDescriptor xbd){
		this.customTexture = xbd;
		return this;
	}
	
	public XPolylineOptions geodesic(boolean isGeodesic){
		isSetGeodesic = true;
		this.isGeodesic = isGeodesic;
		return this;
	}

	public boolean isSetColor() {
		return isSetColor;
	}

	public int getColor() {
		return color;
	}

	public boolean isSetWidth() {
		return isSetWidth;
	}

	public int getWidth() {
		return width;
	}

	public boolean isDottedLine() {
		return isDottedLine;
	}

	public boolean isSetVisible() {
		return isSetVisible;
	}

	public boolean isVisible() {
		return visible;
	}

	public boolean isSetZIndex() {
		return isSetZIndex;
	}

	public int getzIndex() {
		return zIndex;
	}
	
	public Boolean getUseTexture(){
		return useTexture;
	}

	public XBitmapDescriptor getCustomTexture() {
		return customTexture;
	}

	public boolean isSetGeodesic() {
		return isSetGeodesic;
	}

	public boolean isGeodesic() {
		return isGeodesic;
	}
}
