package com.xbcx.core.http.impl;

import java.util.HashMap;

import org.json.JSONObject;

import com.xbcx.core.Event;
import com.xbcx.core.http.RequestParams;
import com.xbcx.utils.JsonParseUtils;

import android.text.TextUtils;

public class SimpleGetDetailRunner extends SimpleItemBaseRunner {

	protected String	mIdHttpKey = "id";
	private   String	mSubJsonKey;
	
	public SimpleGetDetailRunner(String url, Class<?> itemClass) {
		super(url, itemClass);
	}
	
	public SimpleGetDetailRunner setIdHttpKey(String httpKey){
		mIdHttpKey = httpKey;
		return this;
	}
	
	public SimpleGetDetailRunner setSubJsonKey(String jsonKey){
		mSubJsonKey = jsonKey;
		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onEventRun(Event event) throws Exception {
		final Object param = event.getParamAtIndex(0);
		RequestParams params = null;
		JSONObject jo = null;
		if(param != null && param instanceof String){
			final String id = (String)param;
			params = new RequestParams(event.findParam(HashMap.class));
			params.add(mIdHttpKey, id);
			jo = doPost(event, mUrl, params);
		}else{
			params = new RequestParams(event.findParam(HashMap.class));
			jo = doPost(event, mUrl, params);
		}
		JSONObject itemJo = getItemJSONObject(jo, params);
		event.addReturnParam(JsonParseUtils.buildObject(mItemClass, itemJo));
		handleExtendItems(event, jo);
		event.addReturnParam(jo);
		event.setSuccess(true);
	}

	private JSONObject getItemJSONObject(JSONObject jo,RequestParams params) throws Exception{
		JSONObject itemJo;
		if(TextUtils.isEmpty(mSubJsonKey)){
			itemJo = jo;
		}else{
			itemJo = jo.optJSONObject(mSubJsonKey);
			if(itemJo == null){
				itemJo = jo;
			}
		}
		if(!itemJo.has(mIdHttpKey)){
			itemJo.put(mIdHttpKey, params.getUrlParams(mIdHttpKey));
		}
		return itemJo;
	}
}
