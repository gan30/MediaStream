package com.xbcx.core;

public interface CreatorEx<R,P> {
	public R createObject(P... param);
}
