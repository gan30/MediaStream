package com.xbcx.core.tinker;

import com.tencent.tinker.lib.listener.DefaultPatchListener;
import com.tencent.tinker.lib.listener.PatchListener;
import com.tencent.tinker.lib.patch.AbstractPatch;
import com.tencent.tinker.lib.patch.UpgradePatch;
import com.tencent.tinker.lib.reporter.DefaultLoadReporter;
import com.tencent.tinker.lib.reporter.DefaultPatchReporter;
import com.tencent.tinker.lib.reporter.LoadReporter;
import com.tencent.tinker.lib.reporter.PatchReporter;
import com.tencent.tinker.lib.service.AbstractResultService;
import com.tencent.tinker.lib.tinker.TinkerInstaller;
import com.tencent.tinker.loader.app.ApplicationLike;

import android.app.Application;
import android.content.Context;
import android.content.Intent;

/**
 * 
 * @author Administrator
 * 设置方法必须在attachBaseContext之前调用
 */
public class XApplicationLike extends ApplicationLike {
	
	static XApplicationLike instance;
	
	private static boolean			useTinker;
	
	private static LoadReporter 	loadReporter;
	private static PatchReporter 	patchReporter;
	private static PatchListener	patchListener;
	private static Class<? extends AbstractResultService> resultService;
	private static AbstractPatch	upgradePatchProcessor;
	
	public static void setUseTinker(boolean b){
		useTinker = b;
	}
	
	public static void setLoadReporter(LoadReporter lr){
		loadReporter = lr;
	}
	
	public static void setPatchReporter(PatchReporter pr){
		patchReporter = pr;
	}
	
	public static void setPatchListener(PatchListener pl){
		patchListener = pl;
	}
	
	public static void setResultService(Class<? extends AbstractResultService> cls){
		resultService = cls;
	}
	
	public static void setUpgradePatchProcessor(UpgradePatch up){
		upgradePatchProcessor = up;
	}

	public XApplicationLike(Application application, int tinkerFlags, boolean tinkerLoadVerifyFlag,
			long applicationStartElapsedTime, long applicationStartMillisTime, Intent tinkerResultIntent) {
		super(application, tinkerFlags, tinkerLoadVerifyFlag, applicationStartElapsedTime, applicationStartMillisTime,
				tinkerResultIntent);
		instance = this;
	}

	@Override
	public void onBaseContextAttached(Context base) {
		super.onBaseContextAttached(base);
		if(useTinker){
			Thread.setDefaultUncaughtExceptionHandler(new TinkerUncaughtExceptionHandler());
			if(patchReporter == null){
				patchReporter = new DefaultPatchReporter(getApplication());
			}
			if(patchListener == null){
				patchListener = new DefaultPatchListener(getApplication());
			}
			if(loadReporter == null){
				loadReporter = new DefaultLoadReporter(getApplication());
			}
			if(resultService == null){
				resultService = XPatchResultService.class;
			}
			if(upgradePatchProcessor == null){
				upgradePatchProcessor = new UpgradePatch();
			}
			TinkerInstaller.setLogIml(new XTinkerLogImpl());
			TinkerInstaller.install(this, loadReporter,
					patchReporter, 
					patchListener, 
					resultService, 
					upgradePatchProcessor);
		}
	}
}
