package com.xbcx.core;

import java.io.IOException;
import java.io.Serializable;

import org.json.JSONObject;

public class JSONObjectWrapper implements Serializable{
	private static final long serialVersionUID = 1L;
	private JSONObject		mValues;
	
	public JSONObjectWrapper(JSONObject jo){
		mValues = jo;
	}
	
	private void writeObject(java.io.ObjectOutputStream out) throws IOException {
		out.writeObject(mValues.toString());
	}

	private void readObject(java.io.ObjectInputStream in) throws IOException,
			ClassNotFoundException {
		Object o = in.readObject();
		try{
			mValues = new JSONObject(o.toString());
		}catch(Exception e){
			e.printStackTrace();
			mValues = new JSONObject();
		}
	}
	
	public JSONObject getJSONObject(){
		return mValues;
	}
}
