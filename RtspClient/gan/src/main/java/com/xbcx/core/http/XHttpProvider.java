package com.xbcx.core.http;

import com.xbcx.core.Event;

public interface XHttpProvider {
	public String get(XHttpProviderParams p);
	
	public String post(XHttpProviderParams p);
	
	public void download(Event event, String url, String path);

	public void cancel();
}
