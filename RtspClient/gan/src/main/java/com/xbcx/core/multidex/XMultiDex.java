package com.xbcx.core.multidex;

import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import com.xbcx.core.XApplication;
import gan.core.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;

public class XMultiDex {
	
	static Application sApplication;
	
	static XMultiDexListener sListener;
	
	public static void setXMultiDexListner(XMultiDexListener l){
		sListener = l;
	}

	public static void attachBaseContext(Application application,Context base){
		if("1".equals(application.getString(R.string.use_multi_dex))){
			sApplication = application;
			long time = System.currentTimeMillis();
			if (!quickStart()){// && Build.VERSION.SDK_INT < 21) {//>=5.0的系统默认对dex进行oat优化
	            if (needWait(base)){
	                waitForDexopt(base);
	            }
	            MultiDex.install (application);
	            System.out.println("attach Elapsed:" + (System.currentTimeMillis() - time));
	        } else {
	            return;
	        }
		}
	}
	
	private static boolean quickStart() {
		final String proName = XApplication.getCurProcessName(sApplication);
		if(!TextUtils.isEmpty(proName) && 
				(proName.contains(":loaddex")) ||
				proName.contains(":locate") ||
				proName.contains(":bdservice_v1")){
			System.out.println("loadDex:process-" + proName + " quick start");
			return true;
		}
        return false;
    }
	
	private static boolean needWait(Context context) {
		SharedPreferences sp = context.getSharedPreferences(getPackageInfo().versionName,
				getSharedPreferencesMode());
		ApplicationInfo ai = context.getApplicationInfo();
		String source = ai.sourceDir;
		try {
			JarFile jar = new JarFile(source);
			Manifest mf = jar.getManifest();
			Map<String, Attributes> map = mf.getEntries();
			int secondaryNumber = 2;
			Attributes a = map.get("assets/classes" + secondaryNumber + ".dex");
			while(a != null){
				String digest = a.getValue("SHA1-Digest");
				System.out.println("loadDex:" + "dex" + secondaryNumber + "-sha1:" + digest);
				if(!TextUtils.equals(digest, sp.getString(getDexSharedPreferenceKey(secondaryNumber), ""))){
					return true;
				}
				secondaryNumber++;
				a = map.get("assets/classes" + secondaryNumber + ".dex");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@SuppressLint("InlinedApi")
	private static int getSharedPreferencesMode(){
		if(Build.VERSION.SDK_INT >= 11){
			return Application.MODE_MULTI_PROCESS;
		}
		return 0x0004;
	}
	
	private static PackageInfo getPackageInfo(){
        PackageManager pm = sApplication.getPackageManager();
        try {
            return pm.getPackageInfo(sApplication.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
        }
        return  new PackageInfo();
    }
	
	public static void installFinish(Context context){
        SharedPreferences sp = context.getSharedPreferences(
                getPackageInfo().versionName, getSharedPreferencesMode());
        ApplicationInfo ai = context.getApplicationInfo();
		String source = ai.sourceDir;
		try {
			JarFile jar = new JarFile(source);
			Manifest mf = jar.getManifest();
			Map<String, Attributes> map = mf.getEntries();
			int secondaryNumber = 2;
			Attributes a = map.get("assets/classes" + secondaryNumber + ".dex");
			Editor editor = sp.edit();
			while(a != null){
				String digest = a.getValue("SHA1-Digest");
				editor.putString(getDexSharedPreferenceKey(secondaryNumber), digest);
				secondaryNumber++;
				a = map.get("assets/classes" + secondaryNumber + ".dex");
			}
			editor.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	private static String getDexSharedPreferenceKey(int secondaryNumber){
		return "dex" + secondaryNumber + "-SHA1-Digest";
	}
	
	private static void waitForDexopt(Context base) {
		if(sListener != null){
			sListener.onLoadDexActivityLaunched();
		}
        Intent intent = new Intent(sApplication, LoadDexActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        base.startActivity(intent);
        long startWait = System.currentTimeMillis ();
        long waitTime = 10 * 1000 ;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1 ) {
            waitTime = 20 * 1000 ;//实测发现某些场景下有些2.3版本有可能10s都不能完成optdex
        }
        while (needWait(base)) {
            try {
                long nowWait = System.currentTimeMillis() - startWait;
                System.out.println("loadDex:" + "wait ms:" + nowWait);
                if (nowWait >= waitTime) {
                    return;
                }
                Thread.sleep(200 );
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
	
	public static interface XMultiDexListener{
		public void onLoadDexActivityLaunched();
		
		public void onLoadDexActivityCreated(Activity activity);
	}
}
