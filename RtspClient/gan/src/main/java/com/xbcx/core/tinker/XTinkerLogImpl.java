package com.xbcx.core.tinker;

import com.tencent.tinker.lib.util.TinkerLog.TinkerLogImp;
import com.xbcx.core.FileLogger;

import android.util.Log;

public class XTinkerLogImpl implements TinkerLogImp{

	@Override
	public void d(String tag, String arg1, Object... arg2) {
		//FileLogger.getInstance("tinkerLogImpl").log("tag:" + tag + ":" + arg1,arg2);
	}

	@Override
	public void e(String tag, String arg1, Object... arg2) {
		FileLogger.getInstance("tinkerLogImpl").log("tag:" + tag + "--" + arg1,arg2);
	}

	@Override
	public void i(String tag, String arg1, Object... arg2) {
		FileLogger.getInstance("tinkerLogImpl").log("tag:" + tag + "--" + arg1,arg2);
	}

	@Override
	public void printErrStackTrace(String tag, Throwable throwable, String s1, Object... objects) {
		String log = objects == null ? s1 : String.format(s1, objects);
		if (log == null) {
			log = "";
		}
		log = log + "  " + Log.getStackTraceString(throwable);
		FileLogger.getInstance("tinkerLogImpl").log("tag:" + tag + "--" + log);
	}

	@Override
	public void v(String tag, String arg1, Object... arg2) {
		//FileLogger.getInstance("tinkerLogImpl").log("tag:" + tag + ":" + arg1,arg2);
	}

	@Override
	public void w(String tag, String arg1, Object... arg2) {
		FileLogger.getInstance("tinkerLogImpl").log("tag:" + tag + "--" + arg1,arg2);
	}

}
