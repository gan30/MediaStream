package com.xbcx.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;

import com.xbcx.core.AndroidEventManager;
import com.xbcx.core.XApplication;

public class UserDatabaseManager extends DatabaseManager{
	
	public static UserDatabaseManager getInstance(){
		return sInstance;
	}
	
	static{
		sInstance = new UserDatabaseManager();
	}
	
	private static UserDatabaseManager sInstance;

	private String mUid;
	
	private UserDatabaseManager(){
		AndroidEventManager eventManager = AndroidEventManager.getInstance();
	}
	
	public void initial(String uid){
		mUid = uid;
		release();
	}
	
	@Override
	protected SQLiteOpenHelper onInitDBHelper() {
		if(TextUtils.isEmpty(mUid)){
			return null;
		}
		return new DBUserHelper(XApplication.getApplication(), mUid);
	}
	
	private static class DBUserHelper extends SQLiteOpenHelper{

		private static final int DB_VERSION = 1;
		
		public DBUserHelper(Context context, String name) {
			super(context, name, null, DB_VERSION);
			XApplication.getLogger().info("DB Name:" + name);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}
}
