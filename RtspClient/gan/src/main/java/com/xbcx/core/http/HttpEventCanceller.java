package com.xbcx.core.http;

import com.xbcx.core.Event;
import com.xbcx.core.EventCanceller;

public class HttpEventCanceller implements EventCanceller {
	
	private XHttpProvider mRequest;
	
	public HttpEventCanceller(XHttpProvider request){
		mRequest = request;
	}

	@Override
	public void cancelEvent(Event e) {
		mRequest.cancel();
	}

}
