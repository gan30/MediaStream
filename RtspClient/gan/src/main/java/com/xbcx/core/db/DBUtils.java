package com.xbcx.core.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBUtils {
	
	public static boolean tabbleIsExist(String tableName,SQLiteDatabase db) {
		boolean result = false;
		Cursor cursor = null;
		try {
			String sql = "select count(*) as c from Sqlite_master  where type ='table' and name ='" + tableName.trim() + "' ";
			cursor = db.rawQuery(sql, null);
			if (cursor.moveToNext()) {
				int count = cursor.getInt(0);
				if (count > 0) {
					result = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			if(cursor != null){
				cursor.close();
			}
		}
		return result;
	}
	
	public static void safeInsert(SQLiteDatabase db,String strTableName,ContentValues cv,
			CreateTableSqlProvider p){
		long lRet = db.insert(strTableName, null, cv);
		if(lRet == -1){
			if(p != null){
				if(!DBUtils.tabbleIsExist(strTableName, db)){
					db.execSQL(p.getCreateTableSql());
					db.insert(strTableName, null, cv);
				}
			}
		}
	}
	
	public static void startTransaction(SQLiteDatabase db,TransactionExecutor te){
		db.beginTransaction();
		try{
			te.onExecuteTransaction(db);
			db.setTransactionSuccessful();
		}finally{
			db.endTransaction();
		}
	}
	
	public static interface CreateTableSqlProvider{
		public String getCreateTableSql();
	}
	
	public static interface TransactionExecutor{
		public void onExecuteTransaction(SQLiteDatabase db);
	}
}
