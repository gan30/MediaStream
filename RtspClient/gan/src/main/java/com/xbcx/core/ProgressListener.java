package com.xbcx.core;

public interface ProgressListener {
	public void onProgress(int progress, int max);
}
