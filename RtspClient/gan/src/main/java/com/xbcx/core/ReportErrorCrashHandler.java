package com.xbcx.core;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;

public class ReportErrorCrashHandler implements UncaughtExceptionHandler{

	private static ReportErrorCrashHandler INSTANCE = new ReportErrorCrashHandler();
	
	private UncaughtExceptionHandler mDefaultHandler;

	private ReportErrorCrashHandler() {
	}
	
	public static ReportErrorCrashHandler getInstance() {
		return INSTANCE;
	}
	
	public void init(Context context){
		mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
		Thread.setDefaultUncaughtExceptionHandler(this);
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		if(ex instanceof OutOfMemoryError){
			XApplication.reportThreadOutOfMemory((OutOfMemoryError)ex);
		}
		if(mDefaultHandler != null){
			mDefaultHandler.uncaughtException(thread, ex);
		}
	}
}
