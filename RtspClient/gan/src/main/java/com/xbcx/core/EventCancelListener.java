package com.xbcx.core;

public interface EventCancelListener {

	public void onEventCanceled(Event event);
}
