package com.xbcx.core.update;

import java.io.Serializable;

public class UpdateInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	boolean is_update;
	boolean is_must;
	String title;
	String content;
	String ver;
	String url;
	long time;
}
