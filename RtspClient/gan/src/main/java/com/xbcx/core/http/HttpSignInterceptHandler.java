package com.xbcx.core.http;

import com.xbcx.core.module.AppBaseListener;

public interface HttpSignInterceptHandler extends AppBaseListener{
	public void onInterceptHttpSign(RequestParams rp);
}
