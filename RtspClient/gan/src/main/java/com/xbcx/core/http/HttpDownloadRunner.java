package com.xbcx.core.http;

import com.xbcx.core.Event;
import com.xbcx.core.XApplication;
import com.xbcx.core.EventManager.OnEventRunner;

public class HttpDownloadRunner implements OnEventRunner {

	@Override
	public void onEventRun(Event event) throws Exception {
		final String url = (String)event.getParamAtIndex(0);
		final String filePath = (String)event.getParamAtIndex(1);
		XApplication.getLogger().info("download:url" + url + " path:" + filePath);
		XApplication.getHttpProvider().download(event,url, filePath);
		XApplication.getLogger().info("download success:" + (event.getFailMessage() == null) + " url" + url + " path:" + filePath);
		if(event.getFailException() != null){
			throw event.getFailException();
		}
	}
}
