package com.xbcx.core;

public interface RootInterface {
	public boolean isRoot();
	
	public boolean requestRoot();
	
	public boolean exists(String file, boolean isDir);
}
