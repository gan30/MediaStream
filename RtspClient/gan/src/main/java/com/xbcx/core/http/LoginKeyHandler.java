package com.xbcx.core.http;

import org.json.JSONObject;

import com.xbcx.core.Event;

public interface LoginKeyHandler {
	public void onHandleLoginKey(Event event, JSONObject jo);
}
