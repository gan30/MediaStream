package com.xbcx.core.http;

import com.xbcx.core.Event;

public interface XHttpKeyProvider {
	public String getHttpKey(Event event, String url, RequestParams params);
}
