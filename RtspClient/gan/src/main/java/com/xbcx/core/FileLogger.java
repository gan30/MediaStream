package com.xbcx.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.xbcx.utils.DateUtils;
import com.xbcx.utils.FileHelper;
import com.xbcx.utils.SystemUtils;

import android.annotation.SuppressLint;
import android.os.Environment;

public class FileLogger {

	public synchronized static FileLogger getInstance(String fileName){
		FileLogger fl = mapNameToLogger.get(fileName);
		if(fl == null){
			fl = new FileLogger(fileName);
			mapNameToLogger.put(fileName, fl);
		}
		return fl;
	}
	
	public static String getFilePath(String fileName){
		return getFilePath(fileName, XApplication.getFixSystemTime());
	}
	
	public static String getFilePath(String fileName,long time){
		String folder = fileName;
		String ext = ".log";
		int pos = fileName.lastIndexOf(".");
		if(pos > 0){
			folder = fileName.substring(0, pos);
			ext = fileName.substring(pos);
		}
		String formatTime = null;
		synchronized (dfFile) {
			formatTime = dfFile.format(new Date(time));
		}
		return SystemUtils.getExternalCachePath(XApplication.getApplication())
				 + File.separator + "logs" + File.separator + folder + File.separator + folder + "-" + formatTime + ext;
	}
	
	public static String getFilePath(String fileName,long time,String pkgName){
		String folder = fileName;
		String ext = ".log";
		int pos = fileName.lastIndexOf(".");
		if(pos > 0){
			folder = fileName.substring(0, pos);
			ext = fileName.substring(pos);
		}
		String formatTime = null;
		synchronized (dfFile) {
			formatTime = dfFile.format(new Date(time));
		}
		String pkgFilePath = Environment.getExternalStorageDirectory().getPath() + 
				"/Android/data/" + pkgName + File.separator + "files";
		return pkgFilePath + File.separator + 
				"logs" + File.separator + folder + File.separator + folder + "-" + formatTime + ext;
	}
	
	@SuppressLint("SimpleDateFormat")
	private static SimpleDateFormat				dfFile = new SimpleDateFormat("y-M-d");
		
	private static HashMap<String, FileLogger> 	mapNameToLogger = new HashMap<String, FileLogger>();
	
	private BufferedWriter 	mBw;
	
	private boolean			mNeedInit = true;
	private ExecutorService mSingleThreadPoolExecutor;
	private String			mFileName;
	private long			mDayTime;
	private int				mMaxFileNum = 30;
	private boolean			mLogcat;
	
	private FileLogger(String fileName){
		mFileName = fileName;
		mDayTime = XApplication.getFixSystemTime();
		initLogHandler();
	}
	
	public FileLogger setLogcat(boolean b){
		mLogcat = b;
		return this;
	}
	
	public FileLogger setMaxFileNum(int maxNum){
		mMaxFileNum = maxNum;
		return this;
	}
	
	private void initWriter(){
		if(mBw == null){
			if(mNeedInit){
				mNeedInit = false;
				String path = getFilePath(mFileName,mDayTime);
				if(FileHelper.checkOrCreateDirectory(path)){
					File f = new File(path);
					File parent = f.getParentFile();
					File childs[] = parent.listFiles();
					if(childs != null && childs.length > mMaxFileNum){
						long minTime = Long.MAX_VALUE;
						File delete = null;
						for(File child : childs){
							if(child.lastModified() < minTime){
								minTime = child.lastModified();
								delete = child;
							}
						}
						if(delete != null){
							delete.delete();
						}
					}
					try{
						mBw = new BufferedWriter(new FileWriter(path,true));
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private void initLogHandler(){
		mSingleThreadPoolExecutor = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, 
				new LinkedBlockingQueue<Runnable>());
	}
	
	public void log(String message,Object ...objects){
		if(objects != null){
			message = String.format(message, objects);
		}
		log(message);
	}
	
	public void log(String message){
		log(new Record(message));
	}
	
	public void log(final Record record){
		if(record.urgent){
			doLog(record);
		}else{
			mSingleThreadPoolExecutor.execute(new Runnable() {
				@Override
				public void run() {
					doLog(record);
				}
			});
		}
	}
	
	private void doLog(Record record){
		if(mLogcat){
			XApplication.getLogger().info(record.message);
			if(record.stackTrack != null){
				for(StackTraceElement e : record.stackTrack){
					System.out.println(e.toString());
				}
			}
		}
		try {
			long curTime = XApplication.getFixSystemTime();
			if(!DateUtils.isDateDayEqual(curTime, mDayTime)){
				mDayTime = curTime;
				mNeedInit = true;
				if(mBw != null){
					try{
						mBw.close();
					}catch(Exception e){
						e.printStackTrace();
					}finally{
						mBw = null;
					}
				}
			}
			initWriter();
			if(mBw != null){
				mBw.newLine();
				mBw.write(LoggerSystemOutHandler.df.format(new Date()) + ":");
				if(record.bytes == null){
					if(record.message != null){
						mBw.write(record.message);
					}
				}else{
					mBw.newLine();
					mBw.write(new String(record.bytes));
//					Charset cs = Charset.forName ("UTF-8");
//				    ByteBuffer bb = ByteBuffer.allocate(record.bytes.length);
//				    bb.put (record.bytes);
//				    bb.flip();
//				    CharBuffer cb = cs.decode(bb);
//				    mBw.write(cb.array());
				}
				if(record.stackTrack != null){
					for(StackTraceElement ste : record.stackTrack){
						if("setPrintCallStack".equals(ste.getMethodName())){
							continue;
						}
						mBw.newLine();
						mBw.append(ste.getClassName());
						mBw.append('.');
						mBw.append(ste.getMethodName());

				        if (ste.isNativeMethod()) {
				        	mBw.append("(Native Method)");
				        } else {
				            String fName = ste.getFileName();

				            if (fName == null) {
				            	mBw.append("(Unknown Source)");
				            } else {
				                int lineNum = ste.getLineNumber();

				                mBw.append('(');
				                mBw.append(fName);
				                if (lineNum >= 0) {
				                	mBw.append(':');
				                	mBw.append(String.valueOf(lineNum));
				                }
				                mBw.append(')');
				            }
				        }
					}
					mBw.newLine();
				}
				mBw.flush();
			}
		} catch (Exception e) {
			e.printStackTrace();
			if(!FileHelper.isFileExists(getFilePath(mFileName, mDayTime))){
				mBw = null;
			}
		}
	}
	
	public static class Record{
		String 	message;
		byte bytes[];
		StackTraceElement[] stackTrack;
		boolean urgent;
		
		public Record(String message) {
			this.message = message;
		}
		
		public Record(byte bytes[],int offset,int count) {
			this.bytes = new byte[count];
			System.arraycopy(bytes, offset, this.bytes, 0, count);
		}
		
		public Record(Throwable e) {
			this.message = e.getMessage();
			stackTrack = e.getStackTrace();
		}
		
		public Record(StackTraceElement[] stack){
			this.message = "";
			stackTrack = stack;
		}
		
		public Record setUrgent(){
			this.urgent = true;
			return this;
		}
		
		public Record setPrintCallStack(){
			stackTrack = new Throwable().getStackTrace();
			return this;
		}
	}
}
