package com.xbcx.core;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;

public class ActivityLaunchManager {
	static{
		sInstance = new ActivityLaunchManager();
	}
	public static ActivityLaunchManager getInstance(){
		return sInstance;
	}
	
	private static ActivityLaunchManager sInstance;
	
	private List<LaunchIntercepter>		mLaunchIntercepters = new ArrayList<LaunchIntercepter>();
	private List<Activity> 				mRunningActivitys = new ArrayList<Activity>();
	private List<LaunchIntercepter2> 	mLaunchIntercepters2 = null;
	
	private ComponentInterceptChecker	mComponentInterceptChecker;
	
	private ActivityLaunchManager(){
	}
	
	public void setComponentInterceptChecker(ComponentInterceptChecker checker){
		mComponentInterceptChecker = checker;
	}
	
	public void registerLaunchIntercepter(LaunchIntercepter li){
		mLaunchIntercepters.add(li);
	}
	
	public void registerLaunchIntercepter2(LaunchIntercepter2 li){
		if(mLaunchIntercepters2 == null){
			mLaunchIntercepters2 = new ArrayList<LaunchIntercepter2>();
		}
		mLaunchIntercepters2.add(li);
	}
	
	public void unregisterLaunchIntercepter2(LaunchIntercepter2 li){
		if(mLaunchIntercepters2 != null){
			mLaunchIntercepters2.remove(li);
		}
	}
	
	public Activity getCurrentActivity(){
		if(mRunningActivitys.size() > 0){
			Activity a = mRunningActivitys.get(mRunningActivitys.size() - 1);
			if(a.isFinishing()){
				mRunningActivitys.remove(mRunningActivitys.size() - 1);
				return getCurrentActivity();
			}
			return a;
		}
		return null;
	}
	
	public void onActivityCreate(Activity activity){
		mRunningActivitys.add(activity);
	}
	
	public void onActivityDestory(Activity activity){
		mRunningActivitys.remove(activity);
	}
	
	public boolean onStartActivity(Intent intent,Activity fromActivity){
		return onStartActivity(intent, fromActivity, -1);
	}
	
	public boolean onStartActivity(Intent intent,Activity fromActivity,int requestCode){
		if(intent.getComponent() == null){
			return false;
		}
		if(mComponentInterceptChecker != null){
			if(mComponentInterceptChecker.onCheckInterceptComponent(intent,fromActivity)){
				return false;
			}
		}
		for(LaunchIntercepter li : mLaunchIntercepters){
			li.onInterceptLaunchActivity(intent, fromActivity);
		}
		if(mLaunchIntercepters2 != null){
			for(LaunchIntercepter2 li : mLaunchIntercepters2){
				if(li.onInterceptLaunchActivity(intent, fromActivity,requestCode)){
					return true;
				}
			}
		}
		return false;
	}
	
	public static interface ComponentInterceptChecker{
		public boolean onCheckInterceptComponent(Intent intent, Activity fromActivity);
	}
	
	public static interface LaunchIntercepter{
		public void onInterceptLaunchActivity(Intent intent, Activity fromActivity);
	}
	
	public static interface LaunchIntercepter2{
		public boolean onInterceptLaunchActivity(Intent intent, Activity fromActivity, int requestCode);
	}
}
