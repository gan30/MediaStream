package com.xbcx.core.update;

import java.io.File;
import java.util.HashMap;

import com.xbcx.common.TransparentActivity;
import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.AndroidEventManager;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.Event;
import com.xbcx.core.EventCode;
import com.xbcx.core.SharedPreferenceDefine;
import com.xbcx.core.EventManager.OnEventListener;
import com.xbcx.core.ToastManager;
import com.xbcx.core.XApplication;
import com.xbcx.core.http.HttpMapValueBuilder;
import com.xbcx.core.http.impl.SimpleGetDetailRunner;
import com.xbcx.core.module.ActivityResumeListener;
import gan.core.R;
import com.xbcx.utils.DateUtils;
import com.xbcx.utils.FileHelper;
import com.xbcx.utils.SystemUtils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

/**
 * apk更新地址公司一般放到公有云上，不需要加签名，外勤判断规则是域名包含(public)就不会添加签名，
 * 公有云上的文件，添加签名反而下载失败
 */
public class UpdateManager implements ActivityResumeListener{

	public static UpdateManager getInstance(){
		return instance;
	}
	
	static{
		instance = new UpdateManager();
	}
	
	private static UpdateManager instance;
	
	private String	mUrl;
	
	private boolean	mCheckByUser;
	
	private boolean	mIsDownloadingBackground;
	
	private boolean	mResumeCheck = true;
	
	private UpdateManager(){
		AndroidEventManager.getInstance().addEventListener(EventCode.AppBackground, 
				new OnEventListener() {
					@Override
					public void onEventRunEnd(Event event) {
						mResumeCheck = true;
					}
				});
	}
	
	public UpdateManager setCheckUrl(String url){
		mUrl = url;
		AndroidEventManager.getInstance().registerEventRunner(url, 
				new SimpleGetDetailRunner(url,UpdateInfo.class));
		return this;
	}
	
	public Event requestCheckUpdate(){
		return checkUpdate(true);
	}
	
	@Override
	public void onActivityResume(BaseActivity activity) {
		if(mResumeCheck){
			mResumeCheck = false;
			checkUpdate(false);
		}
	}
	
	public String getApkDownloadPath(){
		return SystemUtils.getExternalCachePath(XApplication.getApplication()) + 
				File.separator +"update" + File.separator + "download.apk";
	}
	
	private Event checkUpdate(boolean bCheckByUser){
		mCheckByUser = bCheckByUser;
		if(bCheckByUser){
			return startCheck(bCheckByUser);
		}else{
			XApplication.runOnBackground(new Runnable() {
				@Override
				public void run() {
					long lastTime = SharedPreferenceDefine.getLongValue("last_cancel_update_time", 
							0);
					if(lastTime == 0 || 
							!DateUtils.isDateDayEqual(lastTime, XApplication.getFixSystemTime())){
						startCheck(false);
					}
				}
			});
			return null;
		}
	}
	
	private Event startCheck(boolean bCheckByUser){
		HashMap<String, String> params = new HttpMapValueBuilder()
				.put("is_auto", bCheckByUser)
				.build();
		Event e = AndroidEventManager.getInstance().getRuningEvent(mUrl, params);
		if(e == null){
			e = AndroidEventManager.getInstance().pushEventEx(mUrl,
					new OnEventListener() {
						
						@Override
						public void onEventRunEnd(Event event) {
							if(event.isSuccess()){
								UpdateInfo ui = event.findReturnParam(UpdateInfo.class);
								if(ui.is_update){
									doUpdate(ui);
								}else{
									if(mCheckByUser){
										ToastManager.getInstance(XApplication.getApplication())
										.show(R.string.update_not_need);
									}
								}
							}
						}
					},
					params);
		}
		return e;
	}
	
	Event doDownload(final UpdateInfo ui){
		Event e = AndroidEventManager.getInstance().getRuningEvent(EventCode.HTTP_Download, 
				ui.url,getApkDownloadPath());
		if(e == null || e.isCancel()){
			e = AndroidEventManager.getInstance().pushEventRepeatable(
					String.valueOf(EventCode.HTTP_Download), 
					new OnEventListener() {
						@Override
						public void onEventRunEnd(Event event) {
							if(event.isSuccess()){
								if(mIsDownloadingBackground){
									showReadyActivity(ui);
								}else{
									SystemUtils.install(XApplication.getApplication(), 
											new File(getApkDownloadPath()));
								}
							}else{
								if(mCheckByUser){
									if(!event.isCancel()){
										showRetryActivity(ui);
									}
								}
							}
						}
					},
					ui.url,getApkDownloadPath());
		}
		return e;
	}
	
	boolean isDownloading(UpdateInfo ui){
		return AndroidEventManager.getInstance().isEventRunning(EventCode.HTTP_Download, 
				ui.url,UpdateManager.getInstance().getApkDownloadPath());
	}
	
	void setIsDownloadBackground(boolean b){
		mIsDownloadingBackground = b;
	}
	
	Dialog showUpdateInfoDialog(Context context,String yesText,String noText,
			String message,String title,DialogInterface.OnClickListener listener){
		UpdateInfoDialog d = new UpdateInfoDialog(context);
		d.setMessage(message)
		.setPositiveButton(yesText, listener);
		if(noText != null){
			d.setNegativeButton(noText, listener);
		}
		if(title != null){
			d.setTitle(title);
		}
		d.show();
		return d;
	}
	
	private void doUpdate(UpdateInfo ui){
		String apkPath = getApkDownloadPath();
		if(mCheckByUser || ui.is_must){
			showUpdateActivity(ui);
		}else{
			if(isApkInvalid(ui, apkPath)){
				FileHelper.deleteFile(apkPath);
				if(SystemUtils.isWifi(XApplication.getApplication())){
					setIsDownloadBackground(true);
					doDownload(ui);
				}else{
					showUpdateActivity(ui);
				}
			}else{
				showReadyActivity(ui);
			}
		}
	}
	
	private void showReadyActivity(UpdateInfo ui){
		Intent i = SystemUtils.createSingleTaskIntent(
				XApplication.getApplication(), 
				TransparentActivity.class);
		i.putExtra("data", ui);
		SystemUtils.addPluginClassName(i, ApkReadyActivityPlugin.class);
		XApplication.getApplication().startActivity(i);
	}
	
	
	private void showRetryActivity(UpdateInfo ui){
		Intent i = SystemUtils.createSingleTaskIntent(XApplication.getApplication(), 
				UpdateActivity.class);
		i.putExtra("data", ui);
		i.putExtra("retry", true);
		XApplication.getApplication().startActivity(i);
	}
	
	private void showUpdateActivity(UpdateInfo ui){
		Intent i = SystemUtils.createSingleTaskIntent(XApplication.getApplication(), 
				UpdateActivity.class);
		i.putExtra("data", ui);
		XApplication.getApplication().startActivity(i);
	}
	
	private boolean isApkInvalid(UpdateInfo ui,String apkPath) {
		try {
			PackageManager pm = XApplication.getApplication().getPackageManager();
			PackageInfo pi = pm.getPackageArchiveInfo(apkPath,
					PackageManager.GET_ACTIVITIES);
			String serverApkVersionName = ui.ver;
			if (pi != null) {
				String localApkVersionName = pi.versionName;
				return !localApkVersionName.equals(serverApkVersionName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	private static class ApkReadyActivityPlugin extends ActivityPlugin<BaseActivity> implements
														OnDismissListener{ 
		@Override
		protected void onPostCreate(Bundle savedInstanceState) {
			super.onPostCreate(savedInstanceState);
			final UpdateInfo ui = (UpdateInfo)mActivity.getIntent().getSerializableExtra("data");
			getInstance().showUpdateInfoDialog(mActivity,
					mActivity.getString(R.string.update_now_install), 
					mActivity.getString(R.string.update_next_time), 
					mActivity.getString(R.string.update_last_version, ui.ver) + "\n" +
					mActivity.getString(R.string.update_content,"\n" + ui.content), 
					mActivity.getString(R.string.update_install_ready_title), 
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which == DialogInterface.BUTTON_POSITIVE){
								SystemUtils.install(mActivity, 
										new File(UpdateManager.getInstance().getApkDownloadPath()));
							}else{
								if(ui.is_must){
									mActivity.pushEvent(EventCode.LoginActivityLaunched);
								}
							}
						}
					}).setOnDismissListener(this);
		}

		@Override
		public void onDismiss(DialogInterface dialog) {
			mActivity.finish();
		}
	}
}
