package com.xbcx.core.tinker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.tencent.tinker.lib.service.DefaultTinkerResultService;
import com.tencent.tinker.lib.service.PatchResult;
import com.tencent.tinker.lib.util.TinkerLog;
import com.tencent.tinker.lib.util.TinkerServiceInternals;
import com.xbcx.core.FileLogger;
import com.xbcx.core.XApplication;

import java.io.File;

public class XPatchResultService extends DefaultTinkerResultService {
	
	private static final String TAG = "XPatchResultService";
	
	@Override
	public void onPatchResult(PatchResult result) {
		if (result == null) {
			FileLogger.getInstance("tinker").log("SampleResultService received null result!!!!");
	        return;
	    }
	    TinkerLog.i("XPatchResultService", "SampleResultService receive result: %s", result.toString());

	    //first, we want to kill the recover process
	    TinkerServiceInternals.killTinkerPatchServiceProcess(getApplicationContext());

	    // is success and newPatch, it is nice to delete the raw file, and restart at once
	    // for old patch, you can't delete the patch file
	    if (result.isSuccess) {
	        deleteRawPatchFile(new File(result.rawPatchFilePath));

	        boolean bNeedKill = checkIfNeedKill(result);
	        for(XPatchResultPlugin p : XApplication.getManagers(XPatchResultPlugin.class)){
	        	p.onPatchResultSuccess(bNeedKill);
	        }
	        //not like TinkerResultService, I want to restart just when I am at background!
	        //if you have not install tinker this moment, you can use TinkerApplicationHelper api
	        if (bNeedKill) {
	            if (XApplication.isInBackground()) {
	                TinkerLog.i(TAG, "it is in background, just restart process");
	                restartProcess();
	            } else {
	                //we can wait process at background, such as onAppBackground
	                //or we can restart when the screen off
	                TinkerLog.i(TAG, "tinker wait screen to restart process");
	                new ScreenState(getApplicationContext(), new ScreenState.IOnScreenOff() {
	                    @Override
	                    public void onScreenOff() {
	                        restartProcess();
	                    }
	                });
	            }
	        } else {
	            TinkerLog.i(TAG, "I have already install the newly patch version!");
	        }
	    }else{
	    	for(XPatchResultPlugin p : XApplication.getManagers(XPatchResultPlugin.class)){
	        	p.onPatchResultFail(result.e);
	        }	
	    }
	}
    
    /**
     * you can restart your process through service or broadcast
     */
    private void restartProcess() {
        TinkerLog.i(TAG, "app is background now, i can kill quietly");
        //you can send service or broadcast intent to restart your process
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    static class ScreenState {
        interface IOnScreenOff {
            void onScreenOff();
        }

        ScreenState(Context context, final IOnScreenOff onScreenOffInterface) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            context.registerReceiver(new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent in) {
                    String action = in == null ? "" : in.getAction();
                    TinkerLog.i(TAG, "ScreenReceiver action [%s] ", action);
                    if (Intent.ACTION_SCREEN_OFF.equals(action)) {

                        context.unregisterReceiver(this);

                        if (onScreenOffInterface != null) {
                            onScreenOffInterface.onScreenOff();
                        }
                    }
                }
            }, filter);
        }
    }
}
