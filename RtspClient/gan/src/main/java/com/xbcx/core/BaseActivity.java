package com.xbcx.core;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater.Factory;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xbcx.common.DatePickerDialogLauncher;
import com.xbcx.common.choose.ChooseCallbackInterface;
import com.xbcx.common.choose.ChooseFileProvider.ChooseFileCallback;
import com.xbcx.common.choose.ChooseProvider;
import com.xbcx.common.choose.ChooseProviderPlugin;
import com.xbcx.common.choose.ChooseVideoProvider.ChooseVideoCallback;
import com.xbcx.common.menu.Menu;
import com.xbcx.common.menu.MenuFactory;
import com.xbcx.common.menu.SimpleMenuDialogCreator;
import com.xbcx.core.ActivityScreen.onTitleListener;
import com.xbcx.core.EventManager.OnEventListener;
import com.xbcx.core.http.XHttpException;
import com.xbcx.utils.FileHelper;
import com.xbcx.utils.SystemUtils;

import net.simonvt.timepicker.TimePickerDialog;

import org.apache.commons.collections4.map.MultiValueMap;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import gan.core.R;

public abstract class BaseActivity extends FragmentActivity implements
												onTitleListener,
												OnEventListener,
												OnEventProgressListener{
	
	public static final String EXTRA_HasTitle		= "hastitle";
	public static final String EXTRA_AddBackButton 	= "addbackbutton";
	public static final String Extra_InputPluginClassNames	= "input_plugin_class_names";
	public static final String EXTRA_DestroyWhenLoginActivityLaunch = "destory_when_login_activity_launch";
	
	public static final int RequestCode_LaunchCamera 		= 15000;
	public static final int RequestCode_LaunchChoosePicture = 15001;
	public static final int RequestCode_LaunchChooseVideo	= 15002;
	public static final int RequestCode_LaunchChooseFile	= 15003;
	
	protected static 	ToastManager 			mToastManager = ToastManager.getInstance(XApplication.getApplication());
	
	protected static 	AndroidEventManager		mEventManager = AndroidEventManager.getInstance();

	private   static boolean 			sBackgrounded;
	private   static long				sBackgroundTime;
	
	protected BaseAttribute     		mBaseAttribute;
	protected ActivityScreen			mBaseScreen;
	
	protected RelativeLayout			mRelativeLayoutTitle;
	protected TextView					mTextViewTitle;
	protected TextView					mTextViewSubTitle;
	
	protected boolean mDestroyWhenLoginActivityLaunch 	= true;
	
	private HashMap<String,OnEventListener> 			mMapCodeToListener;
	private	HashMap<String, String>						mMapNotToastCode;
	private HashMap<Event, Event> 						mMapPushEvents;
	private HashMap<Event, Event>						mMapProgressEvent;
	private MultiValueMap<Event, OnEventListener> 		mMapManagerEventToListener;
	private MultiValueMap<String, ActivityEventHandler> mMapCodeToActivityEventHandler;
	private Event										mEventSuccessFinish;
	private int											mToastSuccessId;
	
	private boolean						mIsResume;
	private boolean						mIsForbidPushEvent;
	
	protected boolean					mIsChoosePhotoCrop;
	protected boolean					mIsCropPhotoSquare;
	protected boolean					mIsChoosePhotoCompression = true;
	protected int						mChoosePhotoReqWidth = 1024;
	protected int						mChoosePhotoReqHeight = 1024;
	
	private	  HashMap<String, Object> 	mTags;
	private   int						mRequestCodeInc = 300;
	
	private   boolean					mUseEditTextClickOutSideHideIMM = true;
	private   List<View> 				mEditTextesForClickOutSideHideIMM;
	protected boolean					mIsEditTextClickOutSideSwallowTouch = true;
	protected boolean					mIsEditTextClickOutSideHideWhenClick = false;
	private	  GestureDetector			mEditTextClickOutSideDetector;
	
	private PluginHelper<ActivityBasePlugin> mPluginHelper;
	
	private Handler						mHandler;
	
	private Factory						mLayoutInflateFactory;
	
	/**
	 * 实现了finalize的对象，创建和回收的过程都更耗时。
	 * 创建时，会新建一个额外的Finalizer对象指向新创建的对象。
	 * 而回收时，至少需要经过两次GC，第一次GC检测到对象只有被Finalizer引用，
	 * 将这个对象放入 Finalizer.ReferenceQueue 此时，因为Finalizer的引用，对象还无法被GC， 
	 * Finalizer$FinalizerThread会不停的清理Queue的对象，remove掉当前元素，
	 * 并执行对象的finalize方法，清理后对象没有任何引用，在下一次GC被回收，
	 * 所以说该对象存活时间更久，导致内存泄露
	 */
	public BaseActivity() {
		ActivityLaunchManager.getInstance().onActivityCreate(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		SystemUtils.registerInputPlugins(this);
		mLayoutInflateFactory = XUIProvider.getInstance().createLayoutInflateFactory(this);
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		onInitAttribute(mBaseAttribute = new BaseAttribute());
		
		XApplication.getApplication().activityCreate(this);
		
		mBaseScreen = onCreateScreen(mBaseAttribute);
		for(BaseScreenPreCreatePlugin p : getPlugins(BaseScreenPreCreatePlugin.class)){
			p.onBaseScreenPreCreate(mBaseScreen);
		}
		mBaseScreen.setOnTitleListener(this).onCreate();
		
		if(savedInstanceState != null){
			mIsChoosePhotoCrop = savedInstanceState.getBoolean("is_choose_photo_crop", false);
			mIsCropPhotoSquare = savedInstanceState.getBoolean("is_crop_photo_square", false);
		}
		
		onSetParam();
		
		addAndManageEventListener(EventCode.LoginActivityLaunched);
	}
	
	@Override
	public final void onTitleInited(){
		mRelativeLayoutTitle = mBaseScreen.getViewTitle();
		mTextViewTitle = mBaseScreen.getTextViewTitle();
	}
	
	@Override
	protected void onApplyThemeResource(Theme theme, int resid, boolean first) {
		super.onApplyThemeResource(theme, resid, first);
		if(mBaseScreen != null){
			mBaseScreen.onApplyThemeResource(theme, resid, first);
		}
	}
	
	@Override
	public View onCreateView(String name, Context context, AttributeSet attrs) {
		if(mLayoutInflateFactory != null){
			View v = mLayoutInflateFactory.onCreateView(name, context, attrs);
			if(v != null)return v;
		}
		return super.onCreateView(name, context, attrs);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mBaseScreen.onDestory();
		try{
			if(mPluginHelper != null){
				for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
					ap.onDestroy();
				}
				mPluginHelper.clear();
			}
		}finally{
			try{
				mTags = null;
				
				mIsForbidPushEvent = true;
				
				if(mMapCodeToListener != null){
					for(Entry<String, OnEventListener> e : mMapCodeToListener.entrySet()){
						mEventManager.removeEventListener(e.getKey(), e.getValue());
					}
					mMapCodeToListener.clear();
				}
				
				if(mMapManagerEventToListener != null){
					for(Event e : mMapManagerEventToListener.keySet()){
						Collection<OnEventListener> listeners = mMapManagerEventToListener.getCollection(e);
						if(listeners != null){
							for(OnEventListener l : listeners){
								e.removeEventListener(l);
							}
						}
					}
				}
				
				if(mMapPushEvents != null){
					for(Event e : mMapPushEvents.keySet()){
						mEventManager.removeEventListener(e.getStringCode(), this);
						mEventManager.clearEventListenerEx(e);
					}
					mMapPushEvents.clear();
				}
				
				if(mMapCodeToActivityEventHandler != null){
					mMapCodeToActivityEventHandler.clear();
				}
				
				if(mHandler != null){
					mHandler.removeCallbacksAndMessages(null);
				}
				
				XApplication.getApplication().activityDestroy(this);
			}finally{
				ActivityLaunchManager.getInstance().onActivityDestory(this);
			}
		}
	}

	protected void onSetParam(){
		if(getIntent().hasExtra(EXTRA_DestroyWhenLoginActivityLaunch)){
			mDestroyWhenLoginActivityLaunch = getIntent().getBooleanExtra(EXTRA_DestroyWhenLoginActivityLaunch, true);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		mIsResume = true;
		for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
			ap.onResume();
		}
		XApplication.getApplication().activityResume(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		mIsResume = false;
		for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
			ap.onPause();
		}
		XApplication.getApplication().activityPause(this);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		if(sBackgrounded){
			sBackgrounded = false;
			mEventManager.runEvent(EventCode.AppForceground,
					SystemClock.elapsedRealtime() - sBackgroundTime);
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		if(XApplication.getMainThreadHandler().post(new Runnable() {
			@Override
			public void run() {
				if(!sBackgrounded){
					if(SystemUtils.isInBackground(BaseActivity.this)){
						sBackgrounded = true;
						sBackgroundTime = SystemClock.elapsedRealtime();
						mEventManager.runEvent(EventCode.AppBackground);
					}
				}
			}
		}));
	}
	
	public BaseAttribute getBaseAttribute(){
		return mBaseAttribute;
	}
	
	public boolean isResume(){
		return mIsResume;
	}
	
	@Override
	public Resources getResources() {
		return XUIProvider.getInstance().createResources(super.getResources());
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
			ap.onPostCreate(savedInstanceState);
		}
		mBaseScreen.onPostCreate();
	}
	
	protected ActivityScreen	onCreateScreen(BaseAttribute ba){
		return XScreenFactory.wrap(this, ba);
	}
	
	public ActivityScreen getBaseScreen(){
		return mBaseScreen;
	}
	
	public RelativeLayout getViewTitle(){
		return mBaseScreen.getViewTitle();
	}
	
	public TextView getTextViewTitle(){
		return mBaseScreen.getTextViewTitle();
	}
	
	public View	getButtonBack(){
		return mBaseScreen.getButtonBack();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(mIsChoosePhotoCrop){
			outState.putBoolean("is_choose_photo_crop", true);
		}
		if(mIsCropPhotoSquare){
			outState.putBoolean("is_crop_photo_square", true);
		}
		for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
			ap.onSaveInstanceState(outState);
		}
	}
	
	public boolean isShowChatRoomBar(){
		return false;
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
			ap.onRestoreInstanceState(savedInstanceState);
		}
	}

	public void post(Runnable run){
		if(mHandler == null){
			mHandler = new Handler(Looper.getMainLooper());
		}
		mHandler.removeCallbacks(run);
		mHandler.post(run);
	}
	
	public void postDelayed(Runnable run,long delayMillis){
		if(mHandler == null){
			mHandler = new Handler();
		}
		mHandler.removeCallbacks(run);
		mHandler.postDelayed(run, delayMillis);
	}
	
	public void removeCallbacks(Runnable run){
		if(mHandler != null){
			mHandler.removeCallbacks(run);
		}
	}
	
	public final void setCompressPhoto(boolean b){
		mIsChoosePhotoCompression = b;
	}
	
	public final boolean isChoosePhotoCrop(){
		return mIsChoosePhotoCrop;
	}
	
	public final void registerChooseProvider(ChooseProviderPlugin<?> provider){
		registerPlugin(provider);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public final void registerPlugin(ActivityBasePlugin plugin){
		if(mPluginHelper == null){
			if(isSyncPlugin()){
				mPluginHelper = new SyncPluginHelper<ActivityBasePlugin>();
			}else{
				mPluginHelper = new PluginHelper<ActivityBasePlugin>();
			}
		}
		mPluginHelper.addManager(plugin);
		if(plugin instanceof ActivityPlugin){
			((ActivityPlugin)plugin).setActivity(this);
		}
	}
	
	public boolean isSyncPlugin(){
		return false;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public final void registerPluginAtHead(ActivityBasePlugin plugin){
		if(mPluginHelper == null){
			mPluginHelper = new PluginHelper();
		}
		mPluginHelper.addManagerAtHead(plugin);
		if(plugin instanceof ActivityPlugin){
			((ActivityPlugin)plugin).setActivity(this);
		}
	}
	
	public final void removePlugin(ActivityBasePlugin plugin){
		if(mPluginHelper != null){
			mPluginHelper.removeManager(plugin);
		}
	}
	
	public final <T extends ActivityBasePlugin> Collection<T> getPlugins(Class<T> cls){
		if(mPluginHelper == null){
			return Collections.emptySet();
		}
		return mPluginHelper.getManagers(cls);
	}
	
	public final PluginHelper<ActivityBasePlugin> getPluginHelper(){
		return mPluginHelper;
	}
	
	public void setIsXProgressFocusable(boolean bFocus){
		mBaseScreen.setIsXProgressFocusable(bFocus);
	}

	@Override
	public void setContentView(int layoutResID) {
		try{
			super.setContentView(layoutResID);
		}catch(OutOfMemoryError e){
			System.gc();
			ToastManager.getInstance(this).show(R.string.toast_out_of_memory);
			finish();
		}
	}
	
	public final void setUseEditTextClickOutSideHideIMM(boolean b){
		mUseEditTextClickOutSideHideIMM = b;
	}

	public final void registerEditTextForClickOutSideHideIMM(View v){
		if(mEditTextesForClickOutSideHideIMM == null){
			mEditTextesForClickOutSideHideIMM = new ArrayList<View>();
		}
		mEditTextesForClickOutSideHideIMM.add(v);
	}
	
	public final void unregisterEditTextForClickOutSideHideIMM(View v){
		if(mEditTextesForClickOutSideHideIMM != null){
			mEditTextesForClickOutSideHideIMM.remove(v);
		}
	}
	
	public final void setIsEditTextClickOutSideSwallowTouch(boolean bSwallow){
		mIsEditTextClickOutSideSwallowTouch = bSwallow;
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(mUseEditTextClickOutSideHideIMM && 
				mEditTextesForClickOutSideHideIMM != null &&
				mEditTextesForClickOutSideHideIMM.size() > 0){
			if(mIsEditTextClickOutSideHideWhenClick){
				if(mEditTextClickOutSideDetector == null){
					mEditTextClickOutSideDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener(){
						@Override
						public boolean onSingleTapUp(MotionEvent e) {
							if(handleEditTextOutSide(e)){
								return true;
							}
							return super.onSingleTapUp(e);
						}
					});
				}
				mEditTextClickOutSideDetector.onTouchEvent(ev);
			}else{
				if(ev.getAction() == MotionEvent.ACTION_DOWN){
					if(handleEditTextOutSide(ev)){
						return true;
					}
				}
			}
		}

		try{
			boolean bHandled = false;
			for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
				if(ap.dispatchTouchEvent(ev)){
					bHandled = true;
				}
			}
			if(!bHandled){
				return super.dispatchTouchEvent(ev);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean handleEditTextOutSide(MotionEvent ev){
		final int x = (int)ev.getRawX();
		final int y = (int)ev.getRawY();
		Rect r = new Rect();
		int location[] = new int[2];
		
		boolean bHide = true;
		
		for(View et : mEditTextesForClickOutSideHideIMM){
			et.getGlobalVisibleRect(r);
			et.getLocationOnScreen(location);
			r.offsetTo(location[0], location[1]);
			if(r.contains(x, y)){
				bHide = false;
				break;
			}
		}
		
		if(bHide){
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			if(imm.hideSoftInputFromWindow(mEditTextesForClickOutSideHideIMM.get(0).getWindowToken(), 0)){
				if(mIsEditTextClickOutSideSwallowTouch){
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public void onBackPressed() {
		try{
			boolean bHandled = false;
			for(BackKeyPriorityProvider p : getPlugins(BackKeyPriorityProvider.class)){
				if(p.priorHandleBackPressed()){
					bHandled = true;
					break;
				}
			}
			if(!bHandled){
				for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
					if(ap.onBackPressed()){
						bHandled = true;
					}
				}
			}
			if(!bHandled){
				onXBackPressed();
			}
		}catch(Exception e){
			finish();
		}
	}
	
	public void onXBackPressed(){
		super.onBackPressed();
	}
	
	public int	generateRequestCode(){
		return ++mRequestCodeInc;
	}
	
	public <T extends ActivityBasePlugin> T getInterface(Class<T> cls){
		if(mPluginHelper == null){
			return createInterface(cls);
		}
		Collection<T> plugins = mPluginHelper.getManagers(cls);
		if(plugins != null && plugins.size() > 0){
			return plugins.iterator().next();
		}else{
			return createInterface(cls);
		}
	}
	
	@SuppressWarnings("unchecked")
	private <T extends ActivityBasePlugin> T createInterface(Class<T> cls){
		Creator<ActivityBasePlugin, Class<?>> c = XUIProvider.getInstance().getActivityInterfaceCreator();
		if(c != null){
			ActivityBasePlugin plugin = c.createObject(cls);
			if(plugin != null){
				registerPlugin(plugin);
			}
			return (T)plugin;
		}else{
			return null;
		}
	}

	public void choosePhoto(){
		choosePhoto(true);
	}
	
	public void choosePhoto(boolean bCrop){
		choosePhoto(bCrop, null);
	}
	
	public void choosePhoto(boolean bCrop,String title){
		mIsChoosePhotoCrop = bCrop;
		final Context context = getDialogContext();
		List<Menu> menus = new ArrayList<Menu>();
		menus.add(new Menu(1, R.string.photograph));
		menus.add(new Menu(2, R.string.choose_from_albums));
		MenuFactory.getInstance().showMenu(context, menus, 
				new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(which == 0){
					launchCameraPhoto(mIsChoosePhotoCrop);
				}else if(which == 1){
					launchPictureChoose(mIsChoosePhotoCrop);
				}
			}
		}, new SimpleMenuDialogCreator(title));
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void chooseVideo(ChooseVideoCallback callback){
		ChooseProviderPlugin p = getChooseProvider(RequestCode_LaunchChooseVideo);
		if(p != null){
			p.choose(this, callback);
		}
	}

	public void launchCamera(boolean bVideo){
		launchCameraPhoto(true);
	}
	
	public void launchCameraPhoto(boolean bCrop){
		mIsChoosePhotoCrop = bCrop;
		try{
			final String path = getCameraSaveFilePath();
			if(FileHelper.checkOrCreateDirectory(path)){
				XUIProvider.getInstance().launchCameraPhoto(this, path, RequestCode_LaunchCamera);
			}else{
				if(path.startsWith(SystemUtils.getExternalPath(this))){
					if(SystemUtils.isExternalStorageMounted()){
						ToastManager.getInstance(this).show(R.string.toast_cannot_create_file_on_sdcard_reboot);
					}else{
						ToastManager.getInstance(this).show(R.string.toast_cannot_create_file_on_sdcard);
					}
				}else{
					ToastManager.getInstance(this).show(R.string.toast_cannot_create_file_on_sdcard);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void launchPictureChoose(){
		launchPictureChoose(true);
	}
	
	public void launchPictureChoose(boolean bCrop){
		mIsChoosePhotoCrop = bCrop;
		try{
			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
			intent.setType("image/*");
			FileHelper.checkOrCreateDirectory(FilePaths.getPictureChooseFilePath());
			FileHelper.deleteFile(FilePaths.getPictureChooseFilePath());
			if(bCrop){
				onSetCropExtra(intent);
			}
			startActivityForResult(intent,RequestCode_LaunchChoosePicture);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void launchFileChoose(ChooseFileCallback cb){
		ChooseProviderPlugin provider = getChooseProvider(RequestCode_LaunchChooseFile);
		if(provider != null){
			provider.choose(this, cb);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends ChooseCallbackInterface> void chooseProvider(int requestCode,T callback){
		ChooseProvider<T> provider = (ChooseProvider<T>)getChooseProvider(requestCode);
		if(provider != null){
			provider.choose(this, callback);
		}
	}
	
	public ChooseProviderPlugin<?> getChooseProvider(int requestCode){
		for(ChooseProviderPlugin<?> p : getPlugins(ChooseProviderPlugin.class)){
			if(p.acceptRequestCode(requestCode)){
				return p;
			}
		}
		return null;
	}
	
	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		if(ActivityLaunchManager.getInstance().onStartActivity(intent, this,requestCode)){
			return;
		}
		if(requestCode >= 0){
			Activity parent = getParent();
			if(parent!=null && parent.getParent()!=null){
				parent.startActivityForResult(intent, requestCode);
			}else{
				super.startActivityForResult(intent, requestCode);
			}
		}else{
			super.startActivityForResult(intent, requestCode);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == RequestCode_LaunchCamera){
			for(CameraResultIntercepter i : getPlugins(CameraResultIntercepter.class)){
				if(i.onInterceptCameraResult(requestCode,resultCode,data)){
					return;
				}
			}
			if(resultCode == RESULT_OK){
				onCameraResult(data);
			}
		}else{
			if(requestCode == RequestCode_LaunchChoosePicture){
				if(resultCode == RESULT_OK){
					onPictureChooseResult(data);
				}
			}
		}
		
		ChooseProviderPlugin<?> provider = getChooseProvider(requestCode);
		if(provider != null){
			provider.onActivityResult(this, requestCode, resultCode, data);
		}
		
		for(ActivityPlugin<?> ap : getPlugins(ActivityPlugin.class)){
			try{
				ap.onActivityResult(requestCode, resultCode, data);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	protected void onCameraResult(Intent data){
		handleCameraPhotoResult(mIsChoosePhotoCrop);
	}
	
	public void handleCameraPhotoResult(boolean bCrop){
		final String path = getCameraSaveFilePath();
		if(FileHelper.isBitmapFile(path)){
			SystemUtils.handlePictureExif(path, path,XApplication.getScreenWidth(),
					XApplication.getScreenHeight());
			if(FileHelper.isBitmapFile(path)){
				if(bCrop){
					try {
						Intent intent = new Intent("com.android.camera.action.CROP");
						intent.setDataAndType(Uri.fromFile(new File(path)), 
								"image/*");
						FileHelper.deleteFile(FilePaths.getPictureChooseFilePath());
						onSetCropExtra(intent);
						startActivityForResult(intent, RequestCode_LaunchChoosePicture);
					} catch (Exception e) {
						e.printStackTrace();
						handleCameraPhotoResult(false);
					}
				}else{
					if(mIsChoosePhotoCompression){
						SystemUtils.compressBitmapFile(path, path, 
								mChoosePhotoReqWidth,
								mChoosePhotoReqHeight);
						if(!FileHelper.isBitmapFile(path)){
							mToastManager.show(R.string.toast_camera_photo_error);
							FileLogger.getInstance("error").log("compressBitmapFileError:" + path);
							return;
						}
					}
					onPictureChoosed(path, null);
				}
			}else{
				mToastManager.show(R.string.toast_camera_photo_error);
				FileLogger.getInstance("error").log("handlePictureExifError:" + path);
			}
		}else{
			mToastManager.show(R.string.toast_camera_photo_error);
			FileLogger.getInstance("error").log("camera_photo_error:" + path);
		}
	}
	
	protected void onPictureChooseResult(Intent data){
		if(data != null){
			if(mIsChoosePhotoCrop){
				File file = new File(FilePaths.getPictureChooseFilePath());
				if(file.exists()){
					if(mIsChoosePhotoCompression){
						final String path = FilePaths.getPictureChooseFilePath();
						SystemUtils.compressBitmapFile(path, path, 
								mChoosePhotoReqWidth, mChoosePhotoReqHeight);
					}
					onPictureChoosed(FilePaths.getPictureChooseFilePath(), null);
				}else{
					if(!handlePictureChooseResultUri(data)){
						Object obj = data.getParcelableExtra("data");
						if(obj != null && obj instanceof Bitmap){
							final Bitmap bmp = (Bitmap)obj;
							FileHelper.saveBitmapToFile(
									FilePaths.getPictureChooseFilePath(), bmp);
							bmp.recycle();
							onPictureChoosed(FilePaths.getPictureChooseFilePath(), null);
						}
					}
				}
			}else{
				if(!handlePictureChooseResultUri(data)){
					File file = new File(FilePaths.getPictureChooseFilePath());
					if(file.exists()){
						if(mIsChoosePhotoCompression){
							SystemUtils.compressBitmapFile(FilePaths.getPictureChooseFilePath(), 
									FilePaths.getPictureChooseFilePath(), 
									mChoosePhotoReqWidth, mChoosePhotoReqHeight);
						}
						onPictureChoosed(FilePaths.getPictureChooseFilePath(), null);
					}else{
						Object obj = data.getParcelableExtra("data");
						if(obj != null && obj instanceof Bitmap){
							final Bitmap bmp = (Bitmap)obj;
							FileHelper.saveBitmapToFile(
									FilePaths.getPictureChooseFilePath(), bmp);
							onPictureChoosed(FilePaths.getPictureChooseFilePath(), null);
						}
					}
				}
			}
		}
	}
	
	protected boolean handlePictureChooseResultUri(Intent data){
		Uri uri = data.getData();
		if(uri != null){
			if(uri.getScheme().startsWith("file")){
				onPictureChoosed(uri.getPath(),new File(uri.getPath()).getName());
				return true;
			}else{
				@SuppressWarnings("deprecation")
				Cursor cursor = managedQuery(uri, 
						new String[]{MediaStore.Images.ImageColumns.DISPLAY_NAME,
									MediaStore.Images.ImageColumns.DATA}, 
						null, null, null);
				if(cursor != null && cursor.moveToFirst()){
					String displayName = cursor.getString(
							cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
					final String srcPath = cursor.getString(
							cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
					if(TextUtils.isEmpty(srcPath)){
						try{
							Bitmap bmp = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
							if(bmp != null){
								if(mIsChoosePhotoCompression){
									SystemUtils.compressBitmap(
											FilePaths.getPictureChooseFilePath(), 
											bmp, 
											mChoosePhotoReqWidth, mChoosePhotoReqHeight);
								}else{
									FileHelper.saveBitmapToFile(FilePaths.getPictureChooseFilePath(), bmp);
								}
								bmp.recycle();
								onPictureChoosed(FilePaths.getPictureChooseFilePath(), 
										displayName);
								return true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}catch(OutOfMemoryError e){
							e.printStackTrace();
							ToastManager.getInstance(this).show(R.string.toast_out_of_memory);
						}
					}else{
						if(mIsChoosePhotoCompression){
							String dstPath = FilePaths.getPictureChooseFilePath();
							if(!SystemUtils.compressBitmapFile(
									dstPath,
									srcPath, 
									mChoosePhotoReqWidth, mChoosePhotoReqHeight)){
								FileHelper.copyFile(dstPath, srcPath);
							}
							onPictureChoosed(dstPath, 
									displayName);
						}else{
							onPictureChoosed(srcPath, 
									displayName);
						}
						return true;
					}
				}
			}
		}
		return false;
	}
	
	protected void onPictureChoosed(String filePath,String displayName){
		for(ChoosePictureEndPlugin p : getPlugins(ChoosePictureEndPlugin.class)){
			p.onPictureChoosed(filePath, displayName);
		}
	}
	
	protected void onSetCropExtra(Intent intent){
		intent.putExtra("crop", "true"); 
        intent.putExtra("return-data", false);
        intent.putExtra("noFaceDetection", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, 
				Uri.fromFile(new File(FilePaths.getPictureChooseFilePath())));
		intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
		if(mIsCropPhotoSquare){
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
		}
		for(SetCropExtraPlugin p : getPlugins(SetCropExtraPlugin.class)){
			p.onSetCropExtra(intent);
		}
	}
	
	public String 	getCameraSaveFilePath(){
		return FilePaths.getCameraSaveFilePath();
	}
	
	public final Object setTag(Object object){
		return setIdTag("default", object);
	}
	
	public final Object setIdTag(String id,Object tag){
		if(mTags == null){
			mTags = new HashMap<String, Object>();
		}
		return mTags.put(id, tag);
	}
	
	public final Object getTag(){
		return getIdTag("default");
	}
	
	public final Object	getIdTag(String id){
		if(mTags == null){
			return null;
		}
		return mTags.get(id);
	}
	
	public void showProgressDialog(){
		mBaseScreen.showProgressDialog();
	}
	
	public void showProgressDialog(String strTitle,int nStringId){
		mBaseScreen.showProgressDialog(strTitle, nStringId);
	}
	
	public void showProgressDialog(String strTitle,String strMessage){
		mBaseScreen.showProgressDialog(strTitle, strMessage);
	}
	
	public void dismissProgressDialog(){
		mBaseScreen.dismissProgressDialog();
	}
	
	public void showXProgressDialog(){
		mBaseScreen.showXProgressDialog();
	}
	
	public void showXProgressDialog(String text){
		mBaseScreen.showXProgressDialog(text);
	}
	
	public boolean	isXProgressDialogShowing(){
		return mBaseScreen.isXProgressDialogShowing();
	}
	
	public FrameLayout addCoverView(){
	    return mBaseScreen.addCoverView();
	}
	
	public void dismissXProgressDialog(){
		mBaseScreen.dismissXProgressDialog();
	}
	
	public void setXProgressText(String text){
		mBaseScreen.setXProgressText(text);
	}
	
	public Dialog showYesNoDialog(int msgTextId,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(msgTextId, listener);
	}
	
	public Dialog showYesNoDialog(String msgText,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(getString(R.string.ok), getString(R.string.cancel), msgText, 0, null, listener);
	}
	
	public Dialog showYesNoDialog(int yesTextId,int msgTextId,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(yesTextId, msgTextId, listener);
	}
	
	public Dialog showYesNoDialog(int yesTextId,int noTextId,int msgTextId,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(yesTextId, noTextId, msgTextId, listener);
	}
	
	public Dialog showYesNoDialog(int yesTextId,int noTextId,String message,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(yesTextId, noTextId, message, listener);
	}
	
	public Dialog showYesNoDialog(int yesTextId,int noTextId,int msgTextId,int titleTextId,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(yesTextId, noTextId, msgTextId, titleTextId, listener);
	}
	
	public Dialog showYesNoDialog(String yesText, String noText, String message,
			int titleIcon, String title,DialogInterface.OnClickListener listener){
		return mBaseScreen.showYesNoDialog(yesText, noText, message, titleIcon, title, listener);
	}
	
	public Context getDialogContext(){
		return mBaseScreen.getDialogContext();
	}
	
	public void showDatePicker(){
		showDatePicker(XApplication.getFixSystemTime(), 0, 0);
	}
	
	public void showDatePicker(long maxDate,long minDate){
		showDatePicker(XApplication.getFixSystemTime(), maxDate, minDate);
	}
	
	public void showDatePicker(long time,long maxDate,long minDate){
		new DatePickerDialogLauncher()
			.setTime(time)
			.setMaxTime(maxDate)
			.setMinTime(minDate)
			.setOnDateChooseListener(new DatePickerDialogLauncher.OnDateChooseListener() {
				@Override
				public void onDateChoosed(Calendar cal) {
					onDateChooseResult(cal);
				}
			})
			.onLaunch(this);
	}
	
	protected void onDateChooseResult(Calendar cal){
		
	}
	
	public TimePickerDialog showTimePicker(){
		Calendar c = Calendar.getInstance();
		return showTimePicker(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true);
	}
	
	public TimePickerDialog showTimePicker(int hourOfDay, int minute, boolean is24HourView){
		TimePickerDialog.OnTimeSetListener listener = new TimePickerDialog.OnTimeSetListener(){
			@Override
			public void onTimeSet(net.simonvt.timepicker.TimePicker view, int hourOfDay, int minute) {
				onTimeChooseResult(hourOfDay, minute);
			}
		};
		
		TimePickerDialog d = new TimePickerDialog(this, R.style.DatePickerDialog,
				listener, hourOfDay,minute,is24HourView);
		d.show();
		return d;
	}
	
	protected void onTimeChooseResult(int hourOfDay,int minute){
	}
	
	public TextView addSubTitle(){
		return mTextViewSubTitle = mBaseScreen.addSubTitle();
	}
	
	public View addImageButtonInTitleRight(int resId){
		View v = mBaseScreen.addImageButtonInTitleRight(resId);
		v.setOnClickListener(mOnClickListener);
		return v;
	}
	
	public View addTextButtonInTitleRight(int textId){
		View v = mBaseScreen.addTextButtonInTitleRight(textId);
		v.setOnClickListener(mOnClickListener);
		return v;
	}

	protected void 		onInitAttribute(BaseAttribute ba){
		if(getIntent().hasExtra(EXTRA_HasTitle)){
			ba.mHasTitle = getIntent().getBooleanExtra(EXTRA_HasTitle, true);
		}
		if(getIntent().hasExtra(EXTRA_AddBackButton)){
			ba.mAddBackButton = getIntent().getBooleanExtra(EXTRA_AddBackButton, false);
		}
		for(InitAttributePlugin p : getPlugins(InitAttributePlugin.class)){
			p.onInitAttribute(ba);
		}
	}
	
	protected void onTitleRightButtonClicked(View v){
		for(TitleRightButtonClickActivityPlugin ap : getPlugins(TitleRightButtonClickActivityPlugin.class)){
			ap.onHandleTitleRightButtonClick(v);
		}
	}
	
	public void setIsForbidPushEvent(boolean bForbid){
		mIsForbidPushEvent = bForbid;
	}
	
	protected void setAvatar(ImageView iv,String userId){
	}
	
	protected void setName(TextView tv,String userId,String defaultName){
	}
	
	public Event pushEvent(int eventCode,Object...params){
		return pushEventEx(eventCode, true, false,null,params);
	}
	
	public Event pushEvent(String eventCode,Object...params){
		return pushEventEx(eventCode, true, false,null,params);
	}
	
	public Event pushEventSuccessFinish(int eventCode,int toastSuccessId,Object...params){
		return pushEventSuccessFinish(String.valueOf(eventCode), toastSuccessId, params);
	}
	
	public Event pushEventSuccessFinish(String code,int toastSuccessId,Object...params){
		mEventSuccessFinish = pushEvent(code, params);
		mToastSuccessId = toastSuccessId;
		return mEventSuccessFinish;
	}
	
	public Event pushEventBlock(int eventCode,Object...params){
		return pushEventEx(eventCode, true, true, null, params);
	}
	
	public Event pushEventNoProgress(int eventCode,Object...params){
		return pushEventEx(eventCode, false, false, null, params);
	}
	
	public Event pushEventNoProgress(String code,Object...params){
		return pushEventEx(code, false, false, null, params);
	}
	
	public Event pushEventShowProgress(int eventCode,Object...params){
		Event e = pushEvent(eventCode, params);
		mEventManager.addEventProgressListener(e, this);
		setXProgressText(generateProgressText(0));
		return e;
	}
	
	@SuppressLint("UseSparseArrays")
	protected Event pushEventEx(int eventCode,
			boolean bShowProgress,boolean bBlock,String progressMsg,
			Object... params){
		return pushEventEx(String.valueOf(eventCode), 
				bShowProgress, bBlock, progressMsg, params);
	}
	
	@SuppressLint("UseSparseArrays")
	protected Event pushEventEx(String code,
			boolean bShowProgress,boolean bBlock,String progressMsg,
			Object... params){
		return pushEventEx(code, new EventBuilder()
					.setShowProgress(bShowProgress), params);
	}
	
	public Event pushEventEx(String code,EventBuilder eb,Object... params){
		if(mIsForbidPushEvent){
			return new Event(-1, null);
		}
		
		for(OnEventParamInterceptActivityPlugin ap : getPlugins(OnEventParamInterceptActivityPlugin.class)){
			params = ap.onInterceptEventParam(params);
		}
		
		Event e = null;
		if(mMapCodeToListener != null && mMapCodeToListener.get(code) != null){
			if(eb.repeatable){
				e = mEventManager.pushEventRepeatable(code, null, params);
			}else{
				e = mEventManager.pushEvent(code, params);
			}
		}else{
			if(eb.repeatable){
				e = mEventManager.pushEventRepeatable(code, this, params);
			}else{
				e = mEventManager.pushEventEx(code,this,params);
			}
			if(mMapPushEvents == null){
				mMapPushEvents = new HashMap<Event, Event>();
			}
			mMapPushEvents.put(e, e);
		}
		HashMap<String, String> values = null;
		for(OnEventHttpParamInterceptActivityPlugin ap : getPlugins(OnEventHttpParamInterceptActivityPlugin.class)){
			HashMap<String, String> temp = ap.onBuildAdditionalEventHttpParam();
			if(temp != null){
				if(values == null){
					values = temp;
				}else{
					values.putAll(temp);
				}
			}
		}

		if(mMapProgressEvent == null){
			mMapProgressEvent = new HashMap<Event, Event>();
		}
		
		if(!mMapProgressEvent.containsKey(e)){
			if(eb.bShowProgress){
				showXProgressDialog();
				mMapProgressEvent.put(e, e);
			}
		}
		
		return e;
	}

	public void addAndManageEventListener(int eventCode){
		addAndManageEventListener(eventCode, true);
	}
	
	public void addAndManageEventListener(String code){
		addAndManageEventListener(code, true);
	}
	
	public void addAndManageEventListener(int eventCode,boolean bToast){
		addAndManageEventListener(String.valueOf(eventCode), bToast);
	}
	
	public void addAndManageEventListener(String code,boolean bToast){
		if(mMapCodeToListener == null){
			mMapCodeToListener = new HashMap<String, EventManager.OnEventListener>();
		}
		if(mMapCodeToListener.get(code) == null){
			mMapCodeToListener.put(code, this);
			
			mEventManager.addEventListener(code, this);
		}
		
		if(!bToast){
			if(mMapNotToastCode == null){
				mMapNotToastCode = new HashMap<String, String>();
			}
			mMapNotToastCode.put(code, code);
		}
	}
	
	public void manageEventListener(Event event,OnEventListener listener){
		if(mMapManagerEventToListener == null){
			mMapManagerEventToListener = new MultiValueMap<Event, EventManager.OnEventListener>();
		}
		if(!mMapManagerEventToListener.containsKey(event)){
			OnEventListener l = new OnEventListener() {
				@Override
				public void onEventRunEnd(Event event) {
					mMapManagerEventToListener.remove(event);
				}
			};
			event.addEventListener(l);
			mMapManagerEventToListener.put(event, l);
		}
		mMapManagerEventToListener.put(event, listener);
	}
	
	public void removeEventListener(int eventCode){
		if(mMapCodeToListener == null){
			return;
		}
		mMapCodeToListener.remove(eventCode);
		
		mEventManager.removeEventListener(eventCode, this);
	}
	
	public void registerActivityEventHandler(int eventCode,ActivityEventHandler handler){
		registerActivityEventHandler(String.valueOf(eventCode), handler);
	}
	
	public void registerActivityEventHandler(String code,ActivityEventHandler handler){
		if(mMapCodeToActivityEventHandler == null){
			mMapCodeToActivityEventHandler = new MultiValueMap<String, ActivityEventHandler>();
		}
		mMapCodeToActivityEventHandler.put(code, handler);
	}
	
	public void unregisterActivityEventHandler(int eventCode,ActivityEventHandler handler) {
		unregisterActivityEventHandler(String.valueOf(eventCode), handler);
	}
	
	public void unregisterActivityEventHandler(String code,ActivityEventHandler handler) {
		if(mMapCodeToActivityEventHandler == null){
			return;
		}
		mMapCodeToActivityEventHandler.removeMapping(code, handler);
	}
	
	public void registerActivityEventHandlerEx(int eventCode,ActivityEventHandler handler){
		registerActivityEventHandler(eventCode, handler);
		addAndManageEventListener(eventCode);
	}
	
	public void registerActivityEventHandlerEx(String code,ActivityEventHandler handler){
		registerActivityEventHandler(code, handler);
		addAndManageEventListener(code);
	}
	
	@Override
	public void onEventProgress(Event e, int progress) {
		setXProgressText(generateProgressText(progress));
	}
	
	protected String	generateProgressText(int progress){
		return progress + "%";
	}

	@Override
	public void onEventRunEnd(Event event) {
		final String code = event.getStringCode();
		
		if(!event.isSuccess()){
			final Exception e = event.getFailException();
			if(e != null){
				onHandleEventException(event,e);
			}
		}
		
		if(mMapPushEvents != null){
			mMapPushEvents.remove(event);
			event.removeEventListener(this);
		}
		
		if(mMapProgressEvent != null){
			Event pe = mMapProgressEvent.remove(event);
			if(pe != null){
				dismissXProgressDialog();
			}
		}
		
		if(event.getEventCode() == EventCode.LoginActivityLaunched){
			if(mDestroyWhenLoginActivityLaunch){
				finish();
			}
		}else{
			if(event.equals(mEventSuccessFinish)){
				onSuccessFinishEventEnd(event);
			}
		}
		
		if(mMapCodeToActivityEventHandler != null){
			Collection<ActivityEventHandler> list = mMapCodeToActivityEventHandler.getCollection(code);
			if(list != null){
				for(ActivityEventHandler handler : list){
					handler.onHandleEventEnd(event, this);
				}
			}
		}
		
		for(OnActivityEventEndPlugin p : getPlugins(OnActivityEventEndPlugin.class)){
			p.onActivityEventEnd(event);
		}
	}
	
	protected void onHandleEventException(Event event,Exception e){
		if(mMapNotToastCode == null || 
				!mMapNotToastCode.containsKey(event.getStringCode())){
			if(e instanceof XException){
				onHandleXException(event, (XException)e);
			}
		}
	}
	
	protected void onHandleXException(Event event,XException exception){
		boolean bRet = false;
		for(OnHandleXExceptionPlugin p : getPlugins(OnHandleXExceptionPlugin.class)){
			if(p.onHandleXException(event, exception)){
				bRet = true;
			}
		}
		if(!bRet){
			if (exception instanceof StringIdException){
				onHandleStringIdException(event,(StringIdException)exception);
			} else if(exception instanceof XHttpException){
				onHandleXHttpException(event,(XHttpException)exception);
			}
		}
	}
	
	public void onHandleStringIdException(Event event,StringIdException exception){
		if(mIsResume){
			final int id = exception.getStringId();
			if(id == R.string.toast_disconnect){
				onHandleDisconnectStringId(event);
			}else{
				onHandleOtherStringId(event, exception);
			}
		}
	}
	
	public boolean isDisconnectException(XException	exception){
		if (exception instanceof StringIdException){
			return ((StringIdException)exception).getStringId() == R.string.toast_disconnect;
		}
		return false;
	}
	
	protected void onHandleDisconnectStringId(Event event){
		if(SystemUtils.isNetworkAvailable(this)){
			mToastManager.show(R.string.toast_disconnect);
		}else{
			getBaseScreen().showNetworkErrorTip();
		}
	}
	
	protected void onHandleOtherStringId(Event event,StringIdException exception){
		mToastManager.show(exception.getStringId());
	}
	
	protected void onHandleXHttpException(Event event,XHttpException exception){
		if(mIsResume){
			if(!TextUtils.isEmpty(exception.getMessage())){
				mToastManager.show(exception.getMessage());
			}
		}
	}
	
	protected void onSuccessFinishEventEnd(Event event){
		if(event.isSuccess()){
			if(mToastSuccessId != 0){
				mToastManager.show(mToastSuccessId);
			}
			finish();
		}else{
			mEventSuccessFinish = null;
		}
	}
	
	public static interface OnHandleXExceptionPlugin extends ActivityBasePlugin{
		public boolean onHandleXException(Event event, XException exception);
	}
	
	public static interface OnActivityEventEndPlugin extends ActivityBasePlugin{
		public void onActivityEventEnd(Event event);
	}
	
	public static interface OnEventParamInterceptActivityPlugin extends ActivityBasePlugin{
		public Object[] onInterceptEventParam(Object params[]);
	}
	
	public static interface OnEventHttpParamInterceptActivityPlugin extends ActivityBasePlugin{
		public HashMap<String, String> onBuildAdditionalEventHttpParam();
	}
	
	public static interface ActivityEventHandler{
		public void onHandleEventEnd(Event event, BaseActivity activity);
	}
	
	private View.OnClickListener mOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(v == getButtonBack()){
				onBackPressed();
			}else{
				onTitleRightButtonClicked(v);
			}
		}
	};
	
	/**
	 * 之前为了方便，最好别用，以后会删
	 * @author Administrator
	 */
	public static interface TitleRightButtonClickActivityPlugin extends ActivityBasePlugin{
		public void onHandleTitleRightButtonClick(View v);
	}
	
	public static interface ChoosePictureEndPlugin extends ActivityBasePlugin{
		public void onPictureChoosed(String filePath, String displayName);
	}
	
	public static interface InitAttributePlugin extends ActivityBasePlugin{
		public void onInitAttribute(BaseAttribute ba);
	}
	
	public static interface BaseScreenPreCreatePlugin extends ActivityBasePlugin{
		public void onBaseScreenPreCreate(BaseScreen bs);
	}
	
	/**
	 * 以这种方式也是无语，怕拍了照回来进程重启，在函数中传递难以恢复现场
	 * @author Administrator
	 */
	public static interface SetCropExtraPlugin extends ActivityBasePlugin{
		public void onSetCropExtra(Intent intent);
	}
	
	/**
	 * 有前台View需要处理BackPressed时使用
	 * @author Administrator
	 */
	public static interface BackKeyPriorityProvider extends ActivityBasePlugin{
		public boolean priorHandleBackPressed();
	}
	
	public static interface CameraResultIntercepter extends ActivityBasePlugin{
		public boolean onInterceptCameraResult(int requestCode, int resultCode, Intent data);
	}
	
	public static class EventBuilder{
		public boolean 	bShowProgress;
		public boolean	repeatable;
		
		public EventBuilder setShowProgress(boolean bShowProgress) {
			this.bShowProgress = bShowProgress;
			return this;
		}

		public EventBuilder setRepeatable(boolean repeatable) {
			this.repeatable = repeatable;
			return this;
		}
	}

	public static class BaseAttribute{
		public boolean  mSetContentView = true;
		public int		mActivityLayoutId;
		
		public boolean  mHasTitle = true;
		
		public boolean 	mAddTitleText = true;
		public int		mTitleTextStringId;
		public String	mTitleText;
		
		public boolean 	mAddBackButton = false;
	}
}
