package com.xbcx.core;

import java.util.Collection;

public class SyncPluginHelper<BaseInterface> extends PluginHelper<BaseInterface> {

	@Override
	public boolean addManager(BaseInterface manager) {
		synchronized (this) {
			return super.addManager(manager);
		}
	}
	
	@Override
	public boolean addManagerAtHead(BaseInterface manager) {
		synchronized (this) {
			return super.addManagerAtHead(manager);
		}
	}
	
	@Override
	public <T extends BaseInterface> Collection<T> getManagers(Class<T> cls) {
		synchronized (this) {
			return super.getManagers(cls);
		}
	}
	
	@Override
	public boolean removeManager(Object manager) {
		synchronized (this) {
			return super.removeManager(manager);
		}
	}
	
	@Override
	public <T extends BaseInterface> void interceptPlugin(Class<T> plugin, Class<?> wrapClass, BaseInterface manager) {
		synchronized (this) {
			super.interceptPlugin(plugin, wrapClass, manager);
		}
	}
}
