package com.xbcx.core;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.StatFs;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ImageView;

import com.tencent.tinker.loader.TinkerLoader;
import com.tencent.tinker.loader.app.TinkerApplication;
import com.xbcx.core.EventManager.OnEventListener;
import com.xbcx.core.http.HttpDownloadRunner;
import com.xbcx.core.http.XHttpProvider;
import com.xbcx.core.http.XHttpRunner;
import com.xbcx.core.http.okhttp.XHttpProviderByOkHttp;
import com.xbcx.core.module.ActivityCreateListener;
import com.xbcx.core.module.ActivityDestoryListener;
import com.xbcx.core.module.ActivityPauseListener;
import com.xbcx.core.module.ActivityResumeListener;
import com.xbcx.core.module.AppBaseListener;
import com.xbcx.core.module.HttpLoginListener;
import com.xbcx.core.module.ListValueLoaderlListener;
import com.xbcx.core.module.OnLowMemoryListener;
import com.xbcx.core.multidex.XMultiDex;
import com.xbcx.map.LocationManagerProxy;
import com.xbcx.map.XLocationManager;
import com.xbcx.map.XLocationManager.OnGetLocationListener;
import com.xbcx.utils.Encrypter;
import com.xbcx.utils.FileHelper;
import com.xbcx.utils.LeakHelper;
import com.xbcx.utils.SystemUtils;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

import gan.core.R;

public class XApplication extends TinkerApplication implements OnEventListener {

	public static XApplication getApplication() {
		return sInstance;
	}

	private static XApplication sInstance;

	private static Logger sLogger;

	private static Object sHttpProviderSync = new Object();
	private static XHttpProvider sHttpProvider;

	private static Handler sMainThreadHandler;

	private static int sScreenWidth;
	private static int sScreenHeight;
	private static int sScreenDpi;

	private static String HTTP_KEY = "";

	private static AtomicLong timeDifference = new AtomicLong(0);

	private static boolean isStorageChecked;

	private static PluginHelper<AppBaseListener> pluginHelper = new SyncPluginHelper<AppBaseListener>();

	private static volatile boolean isValueLoaderResume = true;


	private static ThreadPoolExecutor executorService;
	private static HashSet<Runnable> executingRunnables;

	private static HashMap<String, Integer> countHelper;

	private static RootInterface rootInterface;

	public XApplication() {
		super(7, "com.xbcx.core.tinker.XApplicationLike", TinkerLoader.class.getName(), false);
		sInstance = this;
	}

	@Override
	public void onCreate() {
		super.onCreate();

		TimeZone.setDefault(TimeZone.getTimeZone("GMT+08"));

		CrashHandler.getInstance().init(this);
		ReportErrorCrashHandler.getInstance().init(this);

		sMainThreadHandler = new Handler();

		WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics dm = new DisplayMetrics();
		wm.getDefaultDisplay().getMetrics(dm);
		sScreenWidth = dm.widthPixels;
		sScreenHeight = dm.heightPixels;
		sScreenDpi = dm.densityDpi;
		if (sScreenWidth > sScreenHeight) {
			int temp = sScreenWidth;
			sScreenWidth = sScreenHeight;
			sScreenHeight = temp;
		}

		executorService = new ThreadPoolExecutor(0, Integer.MAX_VALUE,
				60L, TimeUnit.SECONDS,
				new SynchronousQueue<Runnable>(),
				new XThreadFactory("XApp-Thread")) {
			@Override
			protected void beforeExecute(Thread t, Runnable r) {
				super.beforeExecute(t, r);
				synchronized (executorService) {
					if (executingRunnables == null) {
						executingRunnables = new HashSet<Runnable>();
					}
					executingRunnables.add(r);
				}
			}

			@Override
			protected void afterExecute(Runnable r, Throwable t) {
				super.afterExecute(r, t);
				synchronized (executorService) {
					if (executingRunnables != null) {
						if (executingRunnables.remove(r)) {
							if (executingRunnables.size() == 0) {
								executingRunnables = null;
							}
						}
					}
				}
			}
		};

		AndroidEventManager.getInstance().registerEventRunner(EventCode.HTTP_Download, new HttpDownloadRunner());
	}

	public static void setRootInterface(RootInterface ri) {
		rootInterface = ri;
	}

	public static RootInterface getRootInterface() {
		if (rootInterface == null) {
			rootInterface = new RootInterface() {
				@Override
				public boolean isRoot() {
					return false;
				}

				@Override
				public boolean requestRoot() {
					return false;
				}

				@Override
				public boolean exists(String file, boolean isDir) {
					return false;
				}
			};
		}
		return rootInterface;
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		XApplication.getLogger().info("onLowMemory");
		for (OnLowMemoryListener listener : getManagers(OnLowMemoryListener.class)) {
			listener.onLowMemory();
		}
	}

	public static void loadStringArrayPlugin() {
		try {
			Class<?> cls = Class.forName(XApplication.getApplication().getPackageName() + ".R$array");
			for (Field f : cls.getFields()) {
				if (f.getName().startsWith("plugin_")) {
					try {
						int id = f.getInt(null);
						String pluginArrays[] = XApplication.getApplication().getResources().getStringArray(id);
						for (String className : pluginArrays) {
							try {
								Class.forName(className);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String getCurProcessName(Context context) {
		int pid = android.os.Process.myPid();
		ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo appProcess : mActivityManager.getRunningAppProcesses()) {
			if (appProcess.pid == pid) {
				return appProcess.processName;
			}
		}
		return null;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
		XMultiDex.attachBaseContext(this, base);
	}

	public static void addCount(String id) {
		if (countHelper == null) {
			countHelper = new HashMap<String, Integer>();
		}
		Integer i = countHelper.get(id);
		int count = i == null ? 1 : (i.intValue() + 1);
		countHelper.put(id, count);
	}

	public static boolean reduceCount(String id) {
		if (countHelper == null) {
			return true;
		}
		Integer i = countHelper.get(id);
		if (i == null) {
			return true;
		}
		int count = i - 1;
		if (count == 0) {
			countHelper.remove(id);
			return true;
		}
		countHelper.put(id, count);
		return false;
	}

	public static int getCount(String id) {
		if (countHelper == null) {
			return 0;
		}
		Integer i = countHelper.get(id);
		return i == null ? 0 : i.intValue();
	}

	public static boolean addManager(AppBaseListener manager) {
		return pluginHelper.addManager(manager);
	}

	public static <T extends AppBaseListener> Collection<T> getManagers(
			Class<T> cls) {
		return pluginHelper.getManagers(cls);
	}

	public static boolean removeManager(Object manager) {
		return pluginHelper.removeManager(manager);
	}

	public static PluginHelper<AppBaseListener> getPluginHelper() {
		return pluginHelper;
	}

	public void notifyHttpLogin(Event event, JSONObject joRet) {
		for (HttpLoginListener l : getManagers(HttpLoginListener.class)) {
			try {
				l.onHttpLogined(event, joRet);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void logout() {
		NotificationManager nm = (NotificationManager) XApplication.getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancelAll();

		CookieSyncManager.createInstance(getApplication());
		CookieManager.getInstance().removeAllCookie();
	}

	public static String getHttpKey() {
		return HTTP_KEY;
	}

	public static void setHttpKey(String key) {
		HTTP_KEY = key;
	}

	public static long getFixSystemTime() {
		return System.currentTimeMillis() + timeDifference.get();
	}

	public static long getTimeDifference() {
		return timeDifference.get();
	}

	public static String getDeviceUUID() {
		return getDeviceUUID(XApplication.getApplication());
	}

	public static String getDeviceUUID(Context context) {
		String uuid = SharedPreferenceDefine.getStringValue(SharedPreferenceDefine.KEY_DeviceUUID, null);
		if (TextUtils.isEmpty(uuid)) {
			final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
			String strIMEI = "1";
			if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
				strIMEI = tm.getDeviceId();
			}
			if(TextUtils.isEmpty(strIMEI)){
				strIMEI = "1";
			}
			
			String strMacAddress = SystemUtils.getMacAddress(context);
			if(TextUtils.isEmpty(strMacAddress)){
				strMacAddress = "1";
			}

			uuid = strIMEI + strMacAddress;
			uuid = Encrypter.encryptBySHA1(uuid);
			SharedPreferenceDefine.setStringValue(SharedPreferenceDefine.KEY_DeviceUUID, uuid);
		}
		return uuid;
	}
	
	/**
	 * 校正服务器与本地时间差
	 * @param serverTime
     */
	public static void		updateServerTimeDifference(long serverTime){
		final long curTime = System.currentTimeMillis();
		timeDifference.set(serverTime - curTime);
	}
	
	final void activityCreate(BaseActivity activity){
		for(ActivityCreateListener l : getManagers(ActivityCreateListener.class)){
			l.onActivityCreate(activity);
		}
		onActivityCreate(activity);
	}
	
	protected void onActivityCreate(BaseActivity activity){
	}
	
	final void activityDestroy(BaseActivity activity){
		LeakHelper.clearTextLineCache();
		for(ActivityDestoryListener l : getManagers(ActivityDestoryListener.class)){
			l.onActivityDestory(activity);
		}
		onActivityDestory(activity);
	}
	
	protected void onActivityDestory(BaseActivity activity){
	}
	
	final void activityResume(BaseActivity activity){
		for(ActivityResumeListener l : getManagers(ActivityResumeListener.class)){
			l.onActivityResume(activity);
		}
		onActivityResume(activity);
	}
	
	protected void onActivityResume(BaseActivity activity){
		if(!isStorageChecked){
			isStorageChecked = true;
			runOnBackground(new Runnable() {
				@Override
				public void run() {
					checkExternalStorageAvailable();
				}
			});
			if(Build.VERSION.SDK_INT == 21){
				try{
					Class<?> cls = Class.forName("android.media.session.MediaSessionLegacyHelper");
					Method m = cls.getMethod("getHelper", Context.class);
					m.invoke(null, this);
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
	
	final void activityPause(BaseActivity activity){
		for(ActivityPauseListener l : getManagers(ActivityPauseListener.class)){
			l.onActivityPause(activity);
		}
		onActivityPause(activity);
	}
	
	protected void onActivityPause(BaseActivity activity){
		
	}
	
	public static Logger getLogger(){
		if(sLogger == null){
			sLogger = Logger.getLogger(sInstance.getPackageName());
			sLogger.setLevel(Level.ALL);
			LoggerSystemOutHandler handler = new LoggerSystemOutHandler();
			handler.setLevel(Level.ALL);
			sLogger.addHandler(handler);
		}
		return sLogger;
	}
	
	public static Handler getMainThreadHandler(){
		return sMainThreadHandler;
	}

	public static int	getScreenWidth(){
		return sScreenWidth;
	}
	
	public static int	getScreenHeight(){
		return sScreenHeight;
	}
	
	public static int 	getScreenDpi(){
		return sScreenDpi;
	}
	
	public static void runOnBackground(Runnable run){
		executorService.execute(run);
	}
	
	public static void reportThreadOutOfMemory(OutOfMemoryError e){
		System.gc();
		try{
			FileLogger.getInstance("error")
			.log(new FileLogger.Record("pkg:" + getApplication().getPackageName())
					.setUrgent());
			FileLogger.getInstance("error")
			.log(new FileLogger.Record(e)
					.setUrgent());
			
			Thread threads[] = SystemUtils.findAllThreads();
			if(threads != null){
				FileLogger.getInstance("error")
				.log(new FileLogger.Record("OOM threadCount:" + threads.length)
						.setUrgent());
				for(Thread t : threads){
					StringBuilder sb = new StringBuilder("runBg OOM thread:");
					sb.append(t.getName());
					FileLogger.getInstance("error")
					.log(new FileLogger.Record(sb.toString())
							.setUrgent());
				}
			}
			Iterator<OnGetLocationListener> it = XLocationManager.getInstance().getLocationListeners();
			while(it.hasNext()){
				FileLogger.getInstance("error")
				.log(new FileLogger.Record(new StringBuilder("normal locate:")
							.append(it.next().toString())
							.toString())
						.setUrgent());
			}
			FileLogger.getInstance("error")
				.log(new FileLogger.Record(new StringBuilder("locus locate:")
							.append(LocationManagerProxy.getLocusListenerCount())
							.toString())
						.setUrgent());
			
			FileLogger.getInstance("error")
				.log(new FileLogger.Record("RunningUrls:")
							.setUrgent());
			for(String url : XHttpRunner.getRunningUrls()){
				FileLogger.getInstance("error")
					.log(new FileLogger.Record(url)
							.setUrgent());
			}
			
			final HashSet<Runnable> runnables = executingRunnables;
			if(runnables != null){
				for(Runnable r : runnables){
					FileLogger.getInstance("error")
					.log(new FileLogger.Record("OOM runnable:" + r.getClass().getName())
							.setUrgent());
				}
			}
		}catch(Exception e1){
			e1.printStackTrace();
			FileLogger.getInstance("error")
			.log(new FileLogger.Record(e1)
					.setUrgent());
		}
		for(ErrorReportPlugin p : XApplication.getManagers(ErrorReportPlugin.class)){
			p.onErrorReport("error");
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	public static boolean checkExternalStorageAvailable(){
		boolean bAvailable = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
		if(bAvailable){
			String path = SystemUtils.getExternalCachePath(getApplication());
			if(!FileHelper.checkOrCreateDirectory(path)){
				ToastManager.getInstance(sInstance).show(R.string.toast_cannot_create_file_on_sdcard);
				return false;
			}
			try{
				StatFs statfs = new StatFs(Environment.getExternalStorageDirectory().getPath());
				long availableBlocks = 0;
				long blockSize = 0;
				if(Build.VERSION.SDK_INT >= 18){
					availableBlocks = statfs.getAvailableBlocksLong();
					blockSize = statfs.getBlockSizeLong();
				}else{
					availableBlocks = (long)statfs.getAvailableBlocks();
					blockSize = (long)statfs.getBlockSize();
				}
				if(availableBlocks * blockSize < 1048576){
					ToastManager.getInstance(sInstance).show(R.string.prompt_sdcard_full);
					bAvailable = false;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			ToastManager.getInstance(sInstance).show(R.string.prompt_sdcard_unavailable);
		}
		return bAvailable;
	}
	
	public static void pauseImageLoader(){
		isValueLoaderResume = false;
		for(ListValueLoaderlListener l : getManagers(ListValueLoaderlListener.class)){
			l.onPauseLoader();
		}
	}
	
	public static void resumeImageLoader(){
		isValueLoaderResume = true;
		for(ListValueLoaderlListener l : getManagers(ListValueLoaderlListener.class)){
			l.onResumeLoader();
		}
	}
	
	public static boolean	isImageLoaderResume(){
		return isValueLoaderResume;
	}
	
	public static boolean setBitmapFromFile(final ImageView iv,String filePath){
		return true;
	}
	
	/**
     * can use anywhere
     */
	public static void	setBitmap(final ImageView view,String url,int defaultResId){
	}
	
	public static void setBitmap(final ImageView view,String url,int maxWidth,int maxHeight,int defaultResId){
	}
	
	public static void setHttpProvider(XHttpProvider p){
		synchronized (sHttpProviderSync) {
			sHttpProvider = p;
		}
	}
	
	public static XHttpProvider getHttpProvider(){
		synchronized (sHttpProviderSync) {
			if(sHttpProvider == null){
				sHttpProvider = new XHttpProviderByOkHttp();
			}
			return sHttpProvider;
		}
	}

	@Override
	public void onEventRunEnd(Event event) {
	}

	public String getUser(){
		return null;
	}

	public static String getLocalUser(){
		return getApplication().getUser();
	}

	public static boolean isInBackground(){
		return SystemUtils.isInBackground(XApplication.getApplication());
	}
}
