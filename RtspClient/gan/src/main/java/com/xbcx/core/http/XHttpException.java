package com.xbcx.core.http;

import org.json.JSONObject;

import com.xbcx.core.JSONObjectWrapper;
import com.xbcx.core.XException;
import com.xbcx.utils.JsonParseUtils;

public class XHttpException extends XException {
	private static final long serialVersionUID = 1L;
	
	private int 	errorid;
	private String	error;
	private JSONObjectWrapper joWrapper;
	
	public XHttpException(JSONObject jo){
		JsonParseUtils.parse(jo, this,XHttpException.class);
		joWrapper = new JSONObjectWrapper(jo);
	}
	
	public XHttpException(int errorId,String error){
		super(error);
		this.errorid = errorId;
		this.error = error;
		joWrapper = new JSONObjectWrapper(new JSONObject());
	}
	
	public int getErrorId(){
		return errorid;
	}
	
	@Override
	public String getMessage() {
		return error;
	}
	
	public JSONObjectWrapper getJSONObjectWrapper(){
		return joWrapper;
	}
}
