package com.xbcx.core.http;

import android.text.TextUtils;

import com.xbcx.core.Event;
import com.xbcx.core.EventManager.OnEventRunner;
import com.xbcx.core.FileLogger;
import com.xbcx.core.StringIdException;
import com.xbcx.core.XApplication;
import com.xbcx.utils.Encrypter;
import com.xbcx.utils.SystemUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import gan.core.R;

public abstract class XHttpRunner implements OnEventRunner {
	
	public static final String HttpInputParamKey = "http_input_param";
	public static final String HttpReturnParamKey = "http_return_param";
	public static final String HttpReturnMapParamKey = "http_return_map_param";
	public static final String HttpReturnJsonKey = "http_return_json";
	public static final String HttpReturnStringKey = "http_return_string";
	
	private static List<String> runningUrls = Collections.synchronizedList(new LinkedList<String>());

	private HashMap<String, String> mParams;
	
	public static String	XHttpKeyEncrypt(String value,String key){
		return value + "-" + Encrypter.encryptBySHA1(value + key);
	}
	
	public XHttpRunner addParam(String key,String value){
		if(mParams == null){
			mParams = new HashMap<String, String>();
		}
		mParams.put(key, value);
		return this;
	}
	
	protected boolean checkRequestSuccess(JSONObject jsonObject){
		try{
			return "true".equals(jsonObject.getString("ok"));
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	protected boolean checkRequestSuccess(String strJson){
		try{
			JSONObject jsonObject = new JSONObject(strJson);
			return "true".equals(jsonObject.getString("ok"));
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	protected void addOffset(RequestParams params,Event event){
		final XHttpPagination p = event.findParam(XHttpPagination.class);
		if(p == null){
			if(TextUtils.isEmpty(params.getUrlParams("offset"))){
				params.add("offset", "0");
			}
		}else{
			params.add("offset", p.getOffset());
		}
	}
	
	public final JSONObject doGet(final Event event,String url,RequestParams rp) throws Exception{
		if(rp == null){
			rp = new RequestParams();
		}

		for(HttpInterceptHandler handler : XApplication.getManagers(HttpInterceptHandler.class)){
			final JSONObject jo = handler.onInterceptHandleHttp(this,event, url, rp);
			if(jo != null){
				return handleReturn(event, jo);
			}
		}
		
		try{
			runningUrls.add(url);
			
			final String fixUrl = addUrlCommonParams(event,url,rp);
			XApplication.getLogger().info(getClass().getName() + " execute url = " + fixUrl);
			
			return onHandleHttpRet(event, url, rp, XApplication.getHttpProvider().get(
					new XHttpProviderParams(event, url, fixUrl, rp)));
		}finally{
			runningUrls.remove(url);
		}
	}
	
	public final JSONObject doPost(final Event event,String url,
			RequestParams params) throws Exception{
		if(params == null){
			params = new RequestParams();
		}

		for(HttpInterceptHandler handler : XApplication.getManagers(HttpInterceptHandler.class)){
			final JSONObject jo = handler.onInterceptHandleHttp(this,event, url, params);
			if(jo != null){
				return handleReturn(event, jo);
			}
		}
		
		try{
			runningUrls.add(url);
			return internalPost(event, url, params);
		}finally{
			runningUrls.remove(url);
		}
	}
	
	public static Collection<String> getRunningUrls(){
		return new ArrayList<String>(runningUrls);
	}

	public JSONObject internalPost(final Event event,String url,
			RequestParams params) throws Exception{
		final String fixUrl = addUrlCommonParams(event,url,params);
		XApplication.getLogger().info(getClass().getName() + " execute url = " + fixUrl + " post prams:" + params.toString());
		String ret = null;
		try{
			ret = XApplication.getHttpProvider().post(new XHttpProviderParams(event, url, fixUrl, params));
			return onHandleHttpRet(event, url, params, ret);
		}finally{
			FileLogger.getInstance("http")
			.setMaxFileNum(5)
			.log("url:" + url + " post params:" + params.toString() + "\n" + "ret:" +
					ret);
		}
	}
	
	protected JSONObject onHandleHttpRet(Event event,String url,RequestParams params,String ret) throws Exception{
		try{
			XApplication.getLogger().info(getClass().getName() + " ret:" + ret);
			event.addMapReturnParam(HttpReturnStringKey, ret);
			if(TextUtils.isEmpty(ret)){
				logException(event,url);
				throw new StringIdException(R.string.toast_disconnect);
			}else{
				JSONObject jo = null;
				try{
					jo = new JSONObject(ret);
				}catch(Exception e){
					logException(event,url);
					throw new StringIdException(R.string.toast_server_error);
				}
				if(jo != null){
					XApplication.updateServerTimeDifference(parseServerTime(jo));
					if(!checkRequestSuccess(jo)){
						event.addReturnParam(jo);
						onHandleXError(jo);
					}
				}
				for(HttpResultHandler handler : XApplication.getManagers(HttpResultHandler.class)){
					handler.onHandleHttpResult(event,url, params, ret, jo);
				}
				return handleReturn(event, jo);
			}
		}catch(Exception e){
			for(HttpResultErrorHandler handler : XApplication.getManagers(HttpResultErrorHandler.class)){
				handler.onHandleHttpResultError(event,url,params,ret,e);
			}
			throw e;
		}
	}
	
	private void logException(Event event,String url){
		Exception exception = event.getFailException();
		if(exception != null){
			if(SystemUtils.isNetworkAvailable(XApplication.getApplication())){
				FileLogger.getInstance("httpFail").log(
						new StringBuilder("url:")
						.append(url).toString());
				FileLogger.getInstance("httpFail").log(
						new FileLogger.Record(exception));
			}
		}
	}
	
	private JSONObject handleReturn(Event event,JSONObject jo) throws InterruptedException{
		event.addMapReturnParam(HttpReturnJsonKey, jo);
		handleCountDownLatch(event);
		return jo;
	}
	
	private void handleCountDownLatch(Event event) throws InterruptedException{
		final CountDownLatch cdl = event.findParam(CountDownLatch.class);
		if(cdl != null){
			cdl.await(60, TimeUnit.SECONDS);
		}
	}
	
	protected void onHandleXError(JSONObject jo) throws Exception{
		throw new XHttpException(jo);
	}
	
	protected long	parseServerTime(JSONObject jo){
		try{
			return jo.getLong("servertime") * 1000;
		}catch(Exception e){
		}
		return System.currentTimeMillis();
	}

	protected String addUrlCommonParams(Event event,String url,RequestParams params) throws Exception{
		if(mParams != null){
			for(Entry<String, String> e : mParams.entrySet()){
				params.add(e.getKey(), e.getValue());
			}
		}
		
		HashMap<String, String> inputValues =  event.getTag(HttpInputParamKey);
		if(inputValues != null){
			for(Entry<String, String> e : inputValues.entrySet()){
				params.add(e.getKey(), e.getValue());
			}
		}
		
		for(HttpCommonParamsIntercepter i : XApplication.getManagers(HttpCommonParamsIntercepter.class)){
			url = i.onInterceptAddCommonParams(event, url, params);
		}
		
		
		final long time = XApplication.getFixSystemTime();
		final String strTime = String.valueOf(time / 1000);
		if(TextUtils.isEmpty(params.getUrlParams("device"))){
			params.add("device", "android");
		}
		params.add("ver", SystemUtils.getVersionName(XApplication.getApplication()));
		if(TextUtils.isEmpty(params.getUrlParams("deviceuuid"))){
			final String strDeviceUUID = XApplication.getDeviceUUID(XApplication.getApplication());
			params.add("deviceuuid", strDeviceUUID);
		}
		
		params.add("timesign",strTime);
		params.add("width", String.valueOf(XApplication.getScreenWidth()));
		params.add("height", String.valueOf(XApplication.getScreenHeight()));
		params.add("dpi", String.valueOf(XApplication.getScreenDpi()));
		
		params.remove("sign");
		for(HttpSignInterceptHandler h : XApplication.getManagers(HttpSignInterceptHandler.class)){
			h.onInterceptHttpSign(params);
		}
		return url;
	}
}
