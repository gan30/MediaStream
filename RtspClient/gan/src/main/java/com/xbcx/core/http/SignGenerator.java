package com.xbcx.core.http;

import com.xbcx.core.Event;

public interface SignGenerator {
	public String onGenerateSign(Event event, String url, RequestParams params);
}
