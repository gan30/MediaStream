package com.xbcx.core.update;

import gan.core.R;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UpdateInfoDialog extends Dialog {

	private TextView	mTextViewTitle;
	private TextView	mTextViewMessage;
	private TextView	mBtnPositive;
	private TextView	mBtnNegative;

	public UpdateInfoDialog(Context context) {
		super(context,R.style.update_dialog);
		setContentView(R.layout.xlibrary_update_dialog);
		mTextViewTitle = (TextView)findViewById(R.id.tvTitle);
		mTextViewMessage = (TextView)findViewById(R.id.tvMessage);
		mBtnPositive = (TextView)findViewById(R.id.btnOK);
		mBtnNegative = (TextView)findViewById(R.id.btnCancel);
		setCanceledOnTouchOutside(false);
	}
	
	public UpdateInfoDialog setMessage(CharSequence message){
		mTextViewMessage.setText(message);
		return this;
	}
	
	public UpdateInfoDialog setMessage(int messageId){
		mTextViewMessage.setText(messageId);
		return this;
	}
	
	public UpdateInfoDialog setPositiveButton(String text, final OnClickListener listener){
		mBtnPositive.setText(text);
		mBtnPositive.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(listener != null){
					listener.onClick(UpdateInfoDialog.this, DialogInterface.BUTTON_POSITIVE);
				}
			}
		});
		return this;
	}
	
	public UpdateInfoDialog setNegativeButton(String text, final OnClickListener listener) {
		mBtnNegative.setText(text);
		mBtnNegative.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
				if(listener != null){
					listener.onClick(UpdateInfoDialog.this, DialogInterface.BUTTON_NEGATIVE);
				}
			}
		});
		return this;
	}
	
	@Override
	public void setTitle(CharSequence title) {
		mTextViewTitle.setText(title);
	}

	@Override
	public void show() {
		if(TextUtils.isEmpty(mBtnNegative.getText())){
			mBtnNegative.setVisibility(View.GONE);
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)mBtnPositive.getLayoutParams();
			lp.weight = 0;
			lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
			mBtnPositive.setLayoutParams(lp);
		}else if(TextUtils.isEmpty(mBtnPositive.getText())){
			mBtnPositive.setVisibility(View.GONE);
			LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams)mBtnNegative.getLayoutParams();
			lp.weight = 0;
			lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
			mBtnNegative.setLayoutParams(lp);
		}
		if(TextUtils.isEmpty(mTextViewTitle.getText())){
			mTextViewTitle.setVisibility(View.GONE);
		}else{
			mTextViewMessage.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		}
		super.show();
	}

}
