package com.xbcx.core.http.impl;

import org.json.JSONObject;

import com.xbcx.core.http.XHttpPagination;
import com.xbcx.utils.JsonParseUtils;

public class HttpPageParam implements XHttpPagination {
	
	private boolean	hasmore;
	private String	offset;
	
	public HttpPageParam(JSONObject jo){
		JsonParseUtils.parse(jo, this);
	}

	@Override
	public boolean hasMore() {
		return hasmore;
	}
	
	@Override
	public String getOffset(){
		return offset;
	}

	public void setHasMore(boolean bHasMore){
		hasmore = bHasMore;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o == this){
			return true;
		}
		if(o != null && o instanceof HttpPageParam){
			HttpPageParam other = (HttpPageParam)o;
			return other.hasmore == this.hasmore && other.offset == this.offset;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return (offset == null ? "0".hashCode() : offset.hashCode()) * 29 + 
				Boolean.valueOf(hasmore).hashCode();
	}
}
