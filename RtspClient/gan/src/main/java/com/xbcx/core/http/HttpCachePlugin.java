package com.xbcx.core.http;

import com.xbcx.core.Event;
import com.xbcx.core.module.AppBaseListener;

import org.json.JSONObject;

public interface HttpCachePlugin extends AppBaseListener{
	public JSONObject onReadHttpCache(XHttpRunner runner, Event event, String url, RequestParams rp);
}
