package com.xbcx.core;

public interface Creator<R,P>{
	public R createObject(P param);
}
