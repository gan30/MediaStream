package com.xbcx.core;

public interface NameProtocol{
    public void setName(String name);
    public String getName();
}
