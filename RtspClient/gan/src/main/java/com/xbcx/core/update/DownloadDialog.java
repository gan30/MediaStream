package com.xbcx.core.update;

import com.xbcx.core.AndroidEventManager;
import com.xbcx.core.Event;
import com.xbcx.core.OnEventProgressListener;
import com.xbcx.core.XApplication;
import gan.core.R;
import com.xbcx.core.EventManager.OnEventListener;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class DownloadDialog extends Dialog{
	
	private DownloadProvider	mDownloadProvider;
	
	private ProgressBar			mProgressBar;
	private TextView			mTextViewPb;
	private Event				mDownloadEvent;
	
	private EventHandle			mEventHandle;
	
	private boolean				mIsCanceled;
	
	private boolean				mShowBackgroundRunBtn = true;
	
	public DownloadDialog(Context context,DownloadProvider dp) {
		super(context);
		init(dp);
	}

	public DownloadDialog(Context context,DownloadProvider dp,int theme){
		super(context,theme);
		init(dp);
	}
	
	private void init(DownloadProvider dp){
		mDownloadProvider = dp;
		setContentView(R.layout.xlibrary_update_dialog_download);
	}
	
	public DownloadDialog setShowBackgroundRunBtn(boolean b){
		mShowBackgroundRunBtn = b;
		return this;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mEventHandle = new EventHandle();
		mProgressBar = (ProgressBar)findViewById(R.id.pb);
		mTextViewPb = (TextView)findViewById(R.id.tvPb);
		TextView tvTitle = (TextView)findViewById(R.id.tvTitle);
		tvTitle.setText(mDownloadProvider.getTitle());
		mTextViewPb.setText(XApplication.getApplication().getString(R.string.update_progress, "0%"));
		findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AndroidEventManager.getInstance().cancelEvent(mDownloadEvent);
				mIsCanceled = true;
				cancel();
			}
		});
		if(mShowBackgroundRunBtn){
			findViewById(R.id.btnBackground).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					cancel();
				}
			});
		}else{
			findViewById(R.id.btnBackground).setVisibility(View.GONE);
		}
		
		setCancelable(false);
		mDownloadEvent = mDownloadProvider.download();
		mDownloadEvent.addEventListener(mEventHandle);
		AndroidEventManager.getInstance().addEventProgressListener(mDownloadEvent, 
				mEventHandle);
	}
	
	public boolean isCanceled(){
		return mIsCanceled;
	}
	
	@Override
	public void dismiss() {
		super.dismiss();
		if(mDownloadEvent != null){
			mDownloadEvent.removeEventListener(mEventHandle);
			AndroidEventManager.getInstance().removeEventProgressListener(mDownloadEvent, mEventHandle);
		}
	}
	
	private class EventHandle implements OnEventListener,
									OnEventProgressListener{
		@Override
		public void onEventRunEnd(Event event) {
			cancel();
			mDownloadEvent = null;
		}

		@Override
		public void onEventProgress(Event e, int progress) {
			mProgressBar.setProgress(progress);
			mTextViewPb.setText(XApplication.getApplication().getString(R.string.update_progress, progress + "%"));
		}
	}
	
	public static interface DownloadProvider{
		public String getTitle();
		
		public Event download();
	}
}
