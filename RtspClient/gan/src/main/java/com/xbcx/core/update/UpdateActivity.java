package com.xbcx.core.update;

import com.xbcx.core.BaseActivity;
import com.xbcx.core.Event;
import com.xbcx.core.EventCode;
import com.xbcx.core.SharedPreferenceDefine;
import com.xbcx.core.XApplication;
import com.xbcx.core.update.DownloadDialog.DownloadProvider;
import com.xbcx.core.update.DownloadUIHandler.DownloadDialogUIListener;
import gan.core.R;

import android.content.DialogInterface;
import android.os.Bundle;

public class UpdateActivity extends BaseActivity implements
										DownloadDialogUIListener{

	private UpdateInfo	mUpdateInfo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mUpdateInfo = (UpdateInfo)getIntent().getSerializableExtra("data");
		new DownloadUIHandler(this, getIntent().getBooleanExtra("retry", false), this);
	}
	
	@Override
	public void finish() {
		super.finish();
		UpdateManager.getInstance().setIsDownloadBackground(true);
	}

	@Override
	public void onRetryCancel() {
		finish();
	}

	@Override
	public void onShowDownloadDialog(boolean bFromRetry) {
		if(bFromRetry){
			showDowloadingDialog();
		}else{
			if(UpdateManager.getInstance().isDownloading(mUpdateInfo)){
				showDowloadingDialog();
			}else{
				UpdateManager.getInstance().showUpdateInfoDialog(this,getString(R.string.yes), getString(R.string.no), 
						getString(R.string.update_last_version, mUpdateInfo.ver) + "\n" +
						getString(R.string.update_content,"\n" + mUpdateInfo.content),
						mUpdateInfo.title, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if(which == DialogInterface.BUTTON_POSITIVE){
									showDowloadingDialog();
								}else{
									if(mUpdateInfo.is_must){
										pushEvent(EventCode.LoginActivityLaunched);
									}else{
										SharedPreferenceDefine.setLongValue("last_cancel_update_time", XApplication.getFixSystemTime());
										finish();
									}
								}
							}
						}).setCancelable(false);
			}
		}
	}
	
	private void showDowloadingDialog(){
		UpdateManager.getInstance().setIsDownloadBackground(false);
		DownloadDialog dd = new DownloadDialog(this, new DownloadProvider() {
			
			@Override
			public String getTitle() {
				return getString(R.string.update_download,getString(R.string.app_name) + "v" + mUpdateInfo.ver);
			}

			@Override
			public Event download() {
				return UpdateManager.getInstance().doDownload(mUpdateInfo);
			}
			
		}, R.style.update_dialog);
		dd.setShowBackgroundRunBtn(!mUpdateInfo.is_must);
		dd.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				finish();
			}
		});
		dd.show();
	}
}
