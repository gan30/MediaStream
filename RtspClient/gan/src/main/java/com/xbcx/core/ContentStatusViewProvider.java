package com.xbcx.core;

import gan.core.R;
import com.xbcx.utils.SystemUtils;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;

public abstract class ContentStatusViewProvider {

	private CharSequence			mFailText;
	private CharSequence			mNoResultText;
	
	private	TopMarginProvider		mTopMarginProvider;
	
	private	NoResultViewProvider	mNoResultViewProvider;
	
	public abstract void showFailView();
	
	public abstract void hideFailView();
	
	public abstract boolean isFailViewVisible();
	
	public void setFailText(CharSequence text){
		mFailText = text;
	}
	
	public CharSequence getFailText(){
		return mFailText;
	}
	
	public abstract void showNoResultView();
	
	public abstract void hideNoResultView();
	
	public abstract boolean isNoResultViewVisible();
	
	public void setNoResultText(CharSequence text){
		mNoResultText = text;
	}
	
	public boolean hasSetNoResultText(){
		return !TextUtils.isEmpty(mNoResultText);
	}
	
	public CharSequence getNoResultText(){
		return mNoResultText;
	}
	
	public abstract void addContentView(View v,FrameLayout.LayoutParams lp);
	
	public void setNoResultViewProvider(NoResultViewProvider provider){
		mNoResultViewProvider = provider;
	}
	
	public void	setTopMarginProvider(TopMarginProvider provider){
		mTopMarginProvider = provider;
	}
	
	public NoResultViewProvider getNoResultViewProvider(){
		return mNoResultViewProvider;
	}
	
	public TopMarginProvider getTopMarginProvider(){
		return mTopMarginProvider;
	}
	
	public static interface TopMarginProvider{
		public int getTopMargin(View statusView);
	}
	
	public static interface NoResultViewProvider{
		public View createNoResultView(Context context);
	}
	
	
	public static class SimpleNoResultViewProvider implements NoResultViewProvider{
		@Override
		public View createNoResultView(Context context) {
			return SystemUtils.inflate(context,R.layout.xlibrary_view_no_search_result);
		}
	}
}
