package com.xbcx.core.http;

import com.xbcx.core.Event;
import com.xbcx.core.module.AppBaseListener;

import org.json.JSONObject;

public interface HttpInterceptHandler extends AppBaseListener{

	JSONObject onInterceptHandleHttp(XHttpRunner runner, Event event, String url, RequestParams rp) throws Exception;
}
