package com.xbcx.core.update;

import com.xbcx.core.BaseActivity;
import gan.core.R;

import android.app.Dialog;
import android.content.DialogInterface;

public class DownloadUIHandler{

	private DownloadDialogUIListener	mUIListener;
	
	public DownloadUIHandler(BaseActivity activity,
			boolean bRetry,
			DownloadDialogUIListener listener) {
		mUIListener = listener;
		if(bRetry){
			Dialog d = activity.showYesNoDialog(R.string.retry, R.string.no, R.string.update_retry_content, 
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which == DialogInterface.BUTTON_POSITIVE){
								showDowloadingDialog(true);
							}else{
								if(mUIListener != null){
									mUIListener.onRetryCancel();
								}
							}
						}
					});
			d.setCancelable(false);
		}else{
			showDowloadingDialog(false);
		}
	}
	
	private void showDowloadingDialog(boolean bFromRetry){
		if(mUIListener != null){
			 mUIListener.onShowDownloadDialog(bFromRetry);
		}
	}
	
	public static interface DownloadDialogUIListener{
		public void onRetryCancel();
		
		public void onShowDownloadDialog(boolean bFromRetry);
	}
}
