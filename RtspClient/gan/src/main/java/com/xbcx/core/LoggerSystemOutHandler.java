package com.xbcx.core;

import java.text.SimpleDateFormat;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import android.annotation.SuppressLint;

public class LoggerSystemOutHandler extends Handler {
	
	@SuppressLint("SimpleDateFormat")
	public static final SimpleDateFormat df = new SimpleDateFormat("y/M/d HH:mm:ss.sss");
	
	public static boolean DEBUG 		= true;
	
	public static boolean WARNING_FILE 	= false;
	
	@Override
	public void close() {
	}

	@Override
	public void flush() {
	}

	@Override
	public void publish(LogRecord record) {
		if(!DEBUG){
			return;
		}
		
		if(WARNING_FILE){
			if(record.getLevel() == Level.WARNING){
				FileLogger.getInstance("warning")
				.setMaxFileNum(5)
				.log(record.getMessage());
			}else if(record.getLevel() == Level.FINE){
				FileLogger.getInstance("fine")
				.setMaxFileNum(5)
				.log(record.getMessage());
			}
		}
		System.out.println(record.getMessage());
	}

}
