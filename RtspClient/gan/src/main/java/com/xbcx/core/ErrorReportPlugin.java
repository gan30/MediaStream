package com.xbcx.core;

import com.xbcx.core.module.AppBaseListener;

public interface ErrorReportPlugin extends AppBaseListener{
	public void onErrorReport(String fileLoggerName);
}
