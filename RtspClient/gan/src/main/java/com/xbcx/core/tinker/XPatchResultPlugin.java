package com.xbcx.core.tinker;

import com.xbcx.core.module.AppBaseListener;

public interface XPatchResultPlugin extends AppBaseListener{
	public void onPatchResultSuccess(boolean bNeedKill);
	
	public void onPatchResultFail(Throwable e);
}
