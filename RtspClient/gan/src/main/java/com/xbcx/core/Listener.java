package com.xbcx.core;

public interface Listener<Param> {
	public void onListenCallback(Param param);
}
