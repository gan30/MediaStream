package com.xbcx.core.multidex;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

public class LoadDexActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if(XMultiDex.sListener != null){
			XMultiDex.sListener.onLoadDexActivityCreated(this);
		}else{
			TextView tv = new TextView(this);
			tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
			tv.setTextColor(0xffff0000);
			tv.setGravity(Gravity.CENTER);
			StringBuffer sb = new StringBuffer();
			sb.append("Please Set LoadDexActivity UI");
			sb.append("\n")
			.append("com.xbcx.core.multidex.XMultiDex:")
			.append("\n")
			.append("setXMultiDexListner");
			tv.setText(sb);
			setContentView(tv);
		}
		
		new LoadDexTask().execute();
	}
	
	private class LoadDexTask extends AsyncTask<Void,Void,Void> {
       
		@Override
		protected Void doInBackground(Void... params) {
			try {
                MultiDex.install(getApplication());
				System.out.println("loadDex:install finish");
				XMultiDex.installFinish(getApplication());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
		}
		
        @Override
        protected void onPostExecute(Void o) {
            finish();
            System.exit(0);
        }
    }
    @Override
    public void onBackPressed() {
        //cannot backpress
    }
}
