package com.xbcx.core;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;

import gan.core.R;
import com.xbcx.utils.SystemUtils;

public abstract class SimpleContentStatusViewProvider extends ContentStatusViewProvider {

	protected	Context					mContext;
	
	protected	View					mViewFail;
	protected 	TextView				mTextViewFail;
	
	protected	View					mViewNoResult;
	protected 	TextView				mTextViewNoResult;
	
	public SimpleContentStatusViewProvider(Context context){
		mContext = context;
	}
	
	@Override
	public void setFailText(CharSequence text){
		super.setFailText(text);
		if(mTextViewFail != null){
			mTextViewFail.setText(getFailText());
		}
	}
	
	@Override
	public void showFailView(){
		hideNoResultView();
		if(mViewFail == null){
			mViewFail = SystemUtils.inflate(mContext,R.layout.xlibrary_view_fail);
			mTextViewFail = (TextView)mViewFail.findViewById(R.id.tvFail);
			setFailText(getFailText());
			FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT, 
					ViewGroup.LayoutParams.MATCH_PARENT);
			lp.gravity = Gravity.TOP;
			addContentView(mViewFail, lp);
			calculateTopMargin(mViewFail);
		}else{
			mViewFail.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void hideFailView(){
		if(mViewFail != null){
			mViewFail.setVisibility(View.GONE);
		}
	}
	
	@Override
	public boolean isFailViewVisible(){
		if(mViewFail != null){
			return mViewFail.getVisibility() == View.VISIBLE;
		}
		return false;
	}
	
	@Override
	public void setNoResultText(CharSequence text){
		super.setNoResultText(text);
		if(mTextViewNoResult != null){
			mTextViewNoResult.setText(getNoResultText());
		}
	}
	
	public void showNoResultView(){
		hideFailView();
		if(mViewNoResult == null){
			NoResultViewProvider p = getNoResultViewProvider();
			if(p == null){
				p = new SimpleNoResultViewProvider();
			}
			mViewNoResult = p.createNoResultView(mContext);
			mTextViewNoResult = (TextView)mViewNoResult.findViewById(R.id.tv);
			if(mTextViewNoResult != null){
				mTextViewNoResult.setText(getNoResultText());
			}
			if(mViewNoResult.getParent() == null){
				FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT, 
						ViewGroup.LayoutParams.MATCH_PARENT);
				lp.gravity = Gravity.BOTTOM;
				addContentView(mViewNoResult, lp);
				calculateTopMargin(mViewNoResult);
			}else{
				mViewNoResult.setVisibility(View.VISIBLE);
			}
		}else{
			mViewNoResult.setVisibility(View.VISIBLE);
		}
	}
	
	public void calculateTopMargin(View view){
		MarginLayoutParams mlp = (MarginLayoutParams)view.getLayoutParams();
		if(mlp != null){
			mlp.topMargin = getTopMargin(view);
			view.setLayoutParams(mlp);
		}
	}
	
	public int getTopMargin(View statusView){
		final TopMarginProvider p = getTopMarginProvider();
		if(p == null){
			return 0;
		}
		return p.getTopMargin(statusView);
	}
	
	public void hideNoResultView(){
		if(mViewNoResult != null){
			mViewNoResult.setVisibility(View.GONE);
		}
	}
	
	public boolean isNoResultViewVisible(){
		return mViewNoResult != null && mViewNoResult.getVisibility() == View.VISIBLE;
	}
	
	/**
	 * 兼容老代码
	 * @author Administrator
	 */
	public static class SimpleNoResultViewProvider implements NoResultViewProvider{
		@Override
		public View createNoResultView(Context context) {
			return SystemUtils.inflate(context,R.layout.xlibrary_view_no_search_result);
		}
	}
}
