package com.xbcx.core.http;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HttpEventInterceptHandler {
	
	private boolean						mSuccess = true;
	
	private Exception					mException;
	
	private List<HttpEventIntercepter> 	mIntercepters = new ArrayList<HttpEventIntercepter>();
	
	private Object						mSync = new Object();
	
	private long						mWaitTime;
	
	public HttpEventInterceptHandler() {
	}
	
	public void setWaitTime(long waitTime){
		mWaitTime = waitTime;
	}
	
	public void addIntercepter(HttpEventIntercepter hei){
		mIntercepters.add(hei);
	}
	
	public void continueExecute(HttpEventIntercepter hei){
		mSuccess = true;
		synchronized (mSync) {
			mSync.notify();
		}
	}
	
	public void interruptExecute(HttpEventIntercepter hei){
		mSuccess = false;
		synchronized (mSync) {
			mSync.notify();
		}
	}
	
	public boolean startIntercept(){
		for(HttpEventIntercepter hei : mIntercepters){
			mSuccess = false;
			if(hei.onIntercept()){
				synchronized (mSync) {
					try{
						if(mWaitTime > 0){
							mSync.wait(mWaitTime);
						}else{
							mSync.wait(600000);
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				if(!mSuccess){
					return true;
				}
			}else{
				mSuccess = true;
			}
		}
		return !mSuccess;
	}
	
	public Exception getException(){
		return mException;
	}
	
	public List<HttpEventIntercepter> getIntecepters(){
		return Collections.unmodifiableList(mIntercepters);
	}
	
	public interface HttpEventIntercepter{
		public boolean onIntercept();
	}
}
