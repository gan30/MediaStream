package com.xbcx.core;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.map.MultiKeyMap;

public class InterfaceHelper<BaseInterface> {

	private Map<Class<? extends BaseInterface>, BaseInterface> registeredInterfaces;
	private MultiKeyMap<Object, BaseInterface> registeredInterfacesEx;
	
	@SuppressWarnings("unchecked")
	public <T extends BaseInterface> T setInterface(Class<T> cls,T inter){
		if(registeredInterfaces == null){
			registeredInterfaces = new HashMap<Class<? extends BaseInterface>, BaseInterface>();
		}
		return (T)registeredInterfaces.put(cls, inter);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends BaseInterface> T setInterface(Class<T> cls,String name,T inter){
		if(registeredInterfacesEx == null){
			registeredInterfacesEx = new MultiKeyMap<Object, BaseInterface>();
		}
		return (T)registeredInterfacesEx.put(cls, name, inter);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends BaseInterface> T getInterface(Class<T> cls){
		if(registeredInterfaces == null){
			return null;
		}
		return (T)registeredInterfaces.get(cls);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends BaseInterface> T getInterface(Class<T> cls,String name){
		if(registeredInterfacesEx == null){
			return null;
		}
		return (T)registeredInterfacesEx.get(cls, name);
	}
}
