package com.xbcx.core.http;

import com.xbcx.core.Event;

public class XHttpProviderParams {
	
	public static boolean ForceHttp;

	public Event 			event;
	public String			url;
	public String			fixUrl;
	public RequestParams	requestParams;
	
	public XHttpProviderParams(Event event,String url,String fixUrl,RequestParams rp) {
		this.event = event;
		this.url = url;
		if(ForceHttp){
			this.fixUrl = fixUrl.replaceFirst("https", "http");
		}else{
			this.fixUrl = fixUrl;
		}
		this.requestParams = rp;
	}
}
