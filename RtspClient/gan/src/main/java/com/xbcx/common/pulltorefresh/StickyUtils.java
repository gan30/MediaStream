package com.xbcx.common.pulltorefresh;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;

public class StickyUtils {
	
	private static Rect	rectTemp = new Rect();

	public static void handleStickyViewWrapMargin(View stickyViewWrap,View refreshView){
		if(stickyViewWrap.getParent() != refreshView.getParent()){
			refreshView.getGlobalVisibleRect(rectTemp);
			int refreshViewTop = rectTemp.top;
			ViewGroup parent = (ViewGroup)stickyViewWrap.getParent();
			if(parent.getGlobalVisibleRect(rectTemp)){
				int top = refreshViewTop - rectTemp.top - parent.getPaddingTop();
				MarginLayoutParams mlp = (MarginLayoutParams)stickyViewWrap.getLayoutParams();
				if(mlp.topMargin != top){
					mlp.topMargin = top;
					stickyViewWrap.setLayoutParams(mlp);
				}
			}
		}else{
			LayoutParams rvlp = refreshView.getLayoutParams();
			if(rvlp != null && rvlp instanceof MarginLayoutParams){
				MarginLayoutParams mlp = (MarginLayoutParams)stickyViewWrap.getLayoutParams();
				final int topMargin = ((MarginLayoutParams)rvlp).topMargin;
				if(mlp.topMargin != topMargin){
					mlp.topMargin = topMargin;
					stickyViewWrap.setLayoutParams(mlp);
				}
			}
		}
	}
}
