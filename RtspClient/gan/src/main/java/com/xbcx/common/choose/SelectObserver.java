package com.xbcx.common.choose;

public interface SelectObserver<E> {

	public void onSelectChanged(Selectable<E> p);
}
