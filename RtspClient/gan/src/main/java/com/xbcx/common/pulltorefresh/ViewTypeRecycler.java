package com.xbcx.common.pulltorefresh;

import java.util.LinkedList;
import java.util.List;

import com.nineoldandroids.view.ViewHelper;

import android.util.SparseArray;
import android.view.View;

public class ViewTypeRecycler {
	SparseArray<List<View>> mMapViewTypeToConvertViews = new SparseArray<List<View>>();
	
	public View obtainView(int viewType){
		List<View> views = mMapViewTypeToConvertViews.get(viewType);
		if(views == null){
			return null;
		}else{
			if(views.size() > 0){
				View v = views.remove(0);
				ViewHelper.setTranslationY(v, 0);
				return v;
			}
			return null;
		}
	}
	
	public void recycleView(int viewType,View v){
		List<View> views = mMapViewTypeToConvertViews.get(viewType);
		if(views == null){
			views = new LinkedList<View>();
			mMapViewTypeToConvertViews.put(viewType, views);
		}
		views.add(v);
	}
}
