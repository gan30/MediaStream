package com.xbcx.common.pulltorefresh;

import com.xbcx.core.XEndlessAdapter;

import java.util.List;

import com.xbcx.adapter.SetBaseAdapter;
import com.xbcx.common.pulltorefresh.PullToRefreshPlugin.PullDownToRefreshPlugin;
import com.xbcx.common.pulltorefresh.PullToRefreshPlugin.PullToRefreshListener;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.Creator;
import com.xbcx.core.Event;
import com.xbcx.core.EventManager.OnEventListener;
import com.xbcx.core.XEndlessAdapter.OnLoadMoreListener;
import com.xbcx.core.http.XHttpPagination;

public class ListLoader implements OnLoadMoreListener{

	private String							mEventCode;
	private BaseActivity					mActivity;
	private XHttpPagination 				mHttpPagination;
	private XEndlessAdapter					mEndlessAdapter;
	private Event							mRefreshEvent;
	private Event							mLoadMoreEvent;
	
	private SetBaseAdapter<?> 				mSetAdapter;
	
	private Creator<Object, Void> 			mParamsCreator;
	
	public ListLoader(String eventCode,BaseActivity activity,
			XEndlessAdapter adapter,
			SetBaseAdapter<?> setAdapter) {
		mEventCode = eventCode;
		mActivity = activity;
		mEndlessAdapter = adapter;
		mSetAdapter = setAdapter;
		adapter.setOnLoadMoreListener(this);
	}
	
	public ListLoader setParamsCreator(Creator<Object, Void> c){
		mParamsCreator = c;
		return this;
	}
	
	public ListLoader setWrapPullToRefresh(final PullToRefreshPlugin<?> plugin,boolean bClear){
		mActivity.registerPlugin(new WrapPullDownToRefreshPlugin(plugin,bClear));
		return this;
	}
	
	public void startRefresh(){
		Event e = mActivity.pushEventNoProgress(mEventCode, mParamsCreator == null ? 
				null : mParamsCreator.createObject(null));
		mRefreshEvent = e;
		e.addEventListener(mRefreshEventListener);
		cancelLoadMore();
	}
	
	public boolean isRefresh(){
		return mRefreshEvent != null;
	}
	
	public void cancelLoadMore(){
		if(mLoadMoreEvent != null){
			mEndlessAdapter.endLoad();
			mLoadMoreEvent = null;
		}
	}
	
	@Override
	public void onStartLoadMore(XEndlessAdapter adapter) {
		XHttpPagination p = mHttpPagination;
		if(p == null){
			Event e = mActivity.pushEventNoProgress(mEventCode, mParamsCreator == null ? 
					null : mParamsCreator.createObject(null));
			mLoadMoreEvent = e;
			e.addEventListener(mOnLoadMoreEventListener);
			return;
		}
		Object[] params = mParamsCreator == null ? null : 
			new Object[]{mParamsCreator.createObject(null)};
		if(params == null){
			mLoadMoreEvent = mActivity.pushEventNoProgress(mEventCode, p.getOffset(),p);
		}else{
			final int length = params.length + 2;
			Object newParams[] = new Object[length];
			for(int index = 0;index < length - 2;++index){
				newParams[index] = params[index];
			}
			newParams[length - 2] = p.getOffset();
			newParams[length - 1] = p;
			mLoadMoreEvent = mActivity.pushEventNoProgress(mEventCode, newParams);
		}
		mLoadMoreEvent.addEventListener(mOnLoadMoreEventListener);
	}
	
	private void setRefreshFail(){
		mEndlessAdapter.setHasMore(true);
		mEndlessAdapter.setLoadFail();
	}
	
	private class WrapPullDownToRefreshPlugin implements PullDownToRefreshPlugin,
														PullToRefreshListener,
														OnEventListener{
		PullToRefreshPlugin<?> 	mWrap;
		PullToRefreshListener	mWrapListener;
		boolean					mClear;
		
		public WrapPullDownToRefreshPlugin(PullToRefreshPlugin<?> wrap,boolean bClear) {
			mWrap = wrap;
			mClear = bClear;
		}
		
		@Override
		public PullToRefreshListener onWrapPullDownToRefresh(PullToRefreshPlugin<?> pp,
				PullToRefreshListener listener) {
			if(mWrap == pp){
				if(mClear){
					mSetAdapter.clear();
					mHttpPagination = null;
					mEndlessAdapter.setHasMore(true);
				}else{
					mWrapListener = listener;
					return this;
				}
			}
			return listener;
		}

		@Override
		public void onPullDownToRefresh() {
			startRefresh();
			mRefreshEvent.addEventListener(this);
		}

		@Override
		public void onPullUpToRefresh() {
		}

		@Override
		public void onEventRunEnd(Event event) {
			if(event.isSuccess()){
				if(mWrapListener != null){
					mWrapListener.onPullDownToRefresh();
					mWrapListener = null;
				}
			}else{
				setRefreshFail();
			}
		}
	}

	private OnEventListener mRefreshEventListener = new OnEventListener() {
		@SuppressWarnings("unchecked")
		@Override
		public void onEventRunEnd(Event event) {
			mRefreshEvent = null;
			if(event.isSuccess()){
				XHttpPagination p = event.findReturnParam(XHttpPagination.class);
				if(p != null){
					mHttpPagination = p;
					mEndlessAdapter.setHasMore(p.hasMore());
				}else{
					mEndlessAdapter.hideBottomView();
				}
				mSetAdapter.replaceAll(event.findReturnParam(List.class));
			}else{
				setRefreshFail();
			}
		}
	};
	
	private OnEventListener mOnLoadMoreEventListener = new OnEventListener() {
		
		@SuppressWarnings("unchecked")
		@Override
		public void onEventRunEnd(Event event) {
			mEndlessAdapter.endLoad();
			mLoadMoreEvent = null;
			if(event.isSuccess()){
				XHttpPagination p = event.findReturnParam(XHttpPagination.class);
				if(p != null){
					mHttpPagination = p;
					mEndlessAdapter.setHasMore(p.hasMore());
				}else{
					mEndlessAdapter.hideBottomView();
				}
				mSetAdapter.addAll(event.findReturnParam(List.class));
			}else{
				mEndlessAdapter.setLoadFail();
			}
		}
	};
}
