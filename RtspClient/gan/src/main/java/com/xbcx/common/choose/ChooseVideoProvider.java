package com.xbcx.common.choose;

import java.util.ArrayList;
import java.util.List;

import com.xbcx.common.menu.Menu;
import com.xbcx.common.menu.MenuFactory;
import com.xbcx.common.menu.SimpleMenuDialogCreator;
import com.xbcx.core.FileLogger;
import gan.core.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

public class ChooseVideoProvider extends ChooseProvider<ChooseVideoProvider.ChooseVideoCallback> {

	public static final int ChooseType_All 		= 0;
	public static final int ChooseType_Capture 	= 1;
	public static final int ChooseType_Albums 	= 2;
	
	protected final int MENUID_PHOTO_CAMERA	= 1;
	protected final int MENUID_PHOTO_FILE	= 2;
	
	protected int				mRequestCodeCamera;
	
	protected int				mChooseType;
	protected CharSequence		mTitle;
	
	public ChooseVideoProvider(int requestCode) {
		super(requestCode);
		mRequestCodeCamera = requestCode * 2;
	}
	
	public ChooseVideoProvider(int requestCode,int requestCodeCamera) {
		super(requestCode);
		mRequestCodeCamera = requestCodeCamera;
	}
	
	public ChooseVideoProvider setChooseType(int type){
		mChooseType = type;
		return this;
	}
	
	public ChooseVideoProvider setTitle(CharSequence title){
		mTitle = title;
		return this;
	}
	
	@Override
	public boolean acceptRequestCode(int requestCode) {
		return super.acceptRequestCode(requestCode) || requestCode == mRequestCodeCamera;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onChooseResult(Activity activity, int requestCode,Intent data) {
		if(requestCode == mRequestCodeCamera){
			if(data.getData() == null){
				Cursor cursor = activity.managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
						new String[]{MediaStore.Video.Media.DATA,
						MediaStore.Video.Media.DURATION}, 
						null, null, MediaStore.Video.VideoColumns.DATE_ADDED + " DESC");
				if(cursor != null && cursor.moveToFirst()){
					final String videoPath = cursor.getString(cursor.getColumnIndex(MediaStore.Video.Media.DATA));
					final long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
					onVideoChoose(videoPath,duration);
				}
			}else{
				onVideoChooseResult(data);
			}
		}else{
			onVideoChooseResult(data);
		}
	}
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	protected void onVideoChooseResult(Intent data){  
		Uri uri = data.getData();
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(mActivity, uri)) {     
            if (isExternalStorageDocument(uri)) {    
                final String docId = DocumentsContract.getDocumentId(uri);    
                final String[] split = docId.split(":");    
                final String type = split[0];    
        
                if ("primary".equalsIgnoreCase(type)) {
                	String path = Environment.getExternalStorageDirectory() + "/" + split[1];
                	long duration = 0;
        			Cursor cursor = mActivity.managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
        					new String[]{MediaStore.Video.Media.DURATION},
        					MediaStore.Video.Media.DATA + "='" + path + "'", 
        					null, null);
        			if(cursor != null && cursor.moveToFirst()){
        				try{
        					duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
        					onVideoChoose(path, duration);
        				}catch(Exception e){
        					e.printStackTrace();
        				}
        			}else{
        				onVideoChoose(path, 1);
        			}
                }else{
                	FileLogger.getInstance("error").log("Choose Video Fail:" + data.getDataString());
                }
            }
            else if (isDownloadsDocument(uri)) {    
        
                final String id = DocumentsContract.getDocumentId(uri);    
                final Uri contentUri = ContentUris.withAppendedId(    
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));    
                handleContentUri(contentUri,null,null);  
            }     
            else if (isMediaDocument(uri)) {    
                final String docId = DocumentsContract.getDocumentId(uri);    
                final String[] split = docId.split(":");    
                final String type = split[0];    
        
                Uri contentUri = null;    
                if ("image".equals(type)) {    
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;    
                } else if ("video".equals(type)) {    
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;    
                } else if ("audio".equals(type)) {    
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;    
                }    
        
                final String selection = "_id=?";    
                final String[] selectionArgs = new String[] {    
                        split[1]    
                }; 
                handleContentUri(contentUri,selection,selectionArgs);
            }else{
            	FileLogger.getInstance("error").log("Choose Video Fail:" + data.getDataString());
            }
        }else{
        	final String url = data.getDataString();
        	if(url.startsWith("file:")){
    			MediaPlayer mp = new MediaPlayer();
    			try{
    				mp.setDataSource(url);
    				mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
						@Override
						public void onPrepared(MediaPlayer mp) {
							String path = url.substring("file:".length());
							while(path.startsWith("/")){
								path = path.substring(1);
							}
							onVideoChoose(path, mp.getDuration());
							mp.release();
						}
					});
    				mp.prepareAsync();
    			}catch(Exception e){
    				e.printStackTrace();
    				FileLogger.getInstance("error").log("Choose Video MediaPlayer Fail:" + url);
    				FileLogger.getInstance("error").log(new FileLogger.Record(e));
    				mp.release();
    			}
        	}
        	else if(url.contains("content")){
    			handleContentUri(uri,null,null);
    		}else{
    			long duration = 0;
    			Cursor cursor = mActivity.managedQuery(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
    					new String[]{MediaStore.Video.Media.DURATION},
    					MediaStore.Video.Media.DATA + "='" + url + "'", 
    					null, null);
    			if(cursor != null && cursor.moveToFirst()){
    				try{
    					duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
    					onVideoChoose(url, duration);
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    				
    			}
    		}
        }
	}
	
	@SuppressWarnings("deprecation")
	private void handleContentUri(Uri uri,String selection,String[] selectionArgs){
		try{
			Cursor cursor = mActivity.managedQuery(uri, null, selection, selectionArgs, null);
			if(cursor != null && cursor.moveToFirst()){
				int columnIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATA);
				if(columnIndex >= 0){
					final String videoPath = cursor.getString(columnIndex);
					final long duration = cursor.getLong(cursor.getColumnIndex(MediaStore.Video.Media.DURATION));
					onVideoChoose(videoPath,duration);
				}else{
					StringBuilder sb = new StringBuilder("Choose Video Fail:");
					sb.append(uri.toString());
					int colCount = cursor.getColumnCount();
					for(int index = 0;index < colCount;++index){
						sb.append("  col:")
						.append(cursor.getColumnName(index));
					}
					FileLogger.getInstance("error").log(sb.toString());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	   
    private boolean isExternalStorageDocument(Uri uri) {    
        return "com.android.externalstorage.documents".equals(uri.getAuthority());    
    }  
       
    private boolean isDownloadsDocument(Uri uri) {    
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());    
    }   
    

    private boolean isMediaDocument(Uri uri) {    
        return "com.android.providers.media.documents".equals(uri.getAuthority());    
    } 

	@Override
	protected void onChoose(Activity activity) {
		if(mChooseType == ChooseType_All){
			final Context context = mActivity.getDialogContext();
			List<Menu> menus = new ArrayList<Menu>();
			menus.add(new Menu(MENUID_PHOTO_CAMERA, R.string.shoot_video));
			menus.add(new Menu(MENUID_PHOTO_FILE, R.string.choose_from_albums));
			
			MenuFactory.getInstance().showMenu(context, 
					menus, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which == 0){
								launchVideoCapture(mActivity);
							}else if(which == 1){
								launchVideoChoose(mActivity);
							}
						}
					}, 
					new SimpleMenuDialogCreator(mTitle));
		}
	}
	
	protected void onVideoChoose(String path,long duration){
		if(mCallBack != null){
			mCallBack.onVideoChoosed(mActivity,path, duration);
		}
	}
	
	public void launchVideoCapture(Activity activity){
		try{
			Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
			activity.startActivityForResult(intent, mRequestCodeCamera);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void launchVideoChoose(Activity activity){
		try{
			Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
			intent.setType("video/*");
			activity.startActivityForResult(intent,mRequestCode);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static interface ChooseVideoCallback extends ChooseCallbackInterface{
		public void onVideoChoosed(Activity activity, String videoPath, long duration);
	}
}
