package com.xbcx.common;

import java.util.HashMap;
import java.util.Map.Entry;

import com.xbcx.core.AndroidEventManager;
import com.xbcx.core.EventManager.OnEventRunner;

public class EventRunnerHelper {
	
	private HashMap<String,OnEventRunner> 	mMapCodeToRunners = new HashMap<String,OnEventRunner>();
	
	public void managerRegisterRunner(int eventCode,OnEventRunner runner){
		managerRegisterRunner(String.valueOf(eventCode), runner);
	}
	
	public void managerRegisterRunner(String code,OnEventRunner runner){
		AndroidEventManager.getInstance().registerEventRunner(code, runner);
		mMapCodeToRunners.put(code, runner);
	}
	
	public void destory(){
		AndroidEventManager eventManager = AndroidEventManager.getInstance();
		for(Entry<String, OnEventRunner> e : mMapCodeToRunners.entrySet()){
			eventManager.removeEventRunner(e.getKey(), e.getValue());
		}
	}
}
