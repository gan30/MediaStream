package com.xbcx.common;

import java.util.HashMap;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.xbcx.adapter.SetBaseAdapter;
import com.xbcx.common.pulltorefresh.PullToRefreshPlugin;
import com.xbcx.core.ActivityBasePlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.utils.SystemUtils;

public class SearchHandler implements TextWatcher,Runnable{

	private BaseActivity						mActivity;
	
	private PullToRefreshPlugin<?> 				mPullToRefreshPlugin;
	private SearchInterface						mSearchInterface;
	private SearchHttpValueProvider				mSearchHttpValueProvider;
	private EditText							mSearchEditText;
	
	private boolean								mAutoSearch = true;
	private boolean								mClearWhenEmptyKey = true;
	
	private SetBaseAdapter<?> 					mSearchAdapter;
	
	private OnSearchListener					mOnSearchListener;
	
	public SearchHandler(PullToRefreshPlugin<?> pp,SearchInterface si,EditText et){
		mPullToRefreshPlugin = pp;
		mSearchInterface = si;
		mSearchEditText = et;
		SystemUtils.filterEnterKey(et);
		et.setOnEditorActionListener(new EditText.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_SEARCH){
					InputMethodManager imm = (InputMethodManager)mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);
					startSearch(true);
					return true;
				}
				return false;
			}
		});
		et.addTextChangedListener(this);
	}
	
	public void setActivity(BaseActivity activity){
		mActivity = activity;
	}
	
	public SearchHandler setSearchHttpValueProvider(SearchHttpValueProvider provider){
		mSearchHttpValueProvider = provider;
		return this;
	}
	
	public SearchHandler setOnSearchListener(OnSearchListener listener){
		mOnSearchListener = listener;
		return this;
	}
	
	public SearchHandler setAutoSearch(boolean bAuto){
		mAutoSearch = bAuto;
		if(!bAuto){
			mSearchEditText.removeCallbacks(this);
		}
		return this;
	}
	
	public SearchHandler setClearWhenEmptyKey(boolean b){
		mClearWhenEmptyKey = b;
		return this;
	}
	
	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if(mAutoSearch){
			mSearchEditText.removeCallbacks(this);
			mSearchEditText.postDelayed(this, 200);
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
	}
	
	@Override
	public void run() {
		startSearch();
	}
	
	public void startSearch(){
		startSearch(false);
	}
	
	public void startSearch(boolean bFromBtn){
		if(mSearchEditText != null){
			mPullToRefreshPlugin.clearRefreshEvents();
			mPullToRefreshPlugin.cancelLoadMore();
			if(mPullToRefreshPlugin.getEndlessAdapter() != null){
				mPullToRefreshPlugin.getEndlessAdapter().hideBottomView();
			}
			final String s = SystemUtils.getTrimText(mSearchEditText);
			
			if(mClearWhenEmptyKey && TextUtils.isEmpty(s)){
				getSearchAdapter().clear();
			}else{
				for(SearchInterceptPlugin p : mActivity.getPlugins(SearchInterceptPlugin.class)){
					if(p.onInterceptSearch(mSearchEditText, s,bFromBtn)){
						return;
					}
				}
				mPullToRefreshPlugin.addRefreshEvent(
						mActivity.pushEvent(
								mSearchInterface.getSearchEventCode(), 
								buildSearchHttpValue(s)));
			}
			if(mOnSearchListener != null){
				mOnSearchListener.onStartSearch(s);
			}
		}
	}
	
	public SetBaseAdapter<?> getSearchAdapter(){
		if(mSearchAdapter == null){
			mSearchAdapter = mSearchInterface.createSearchAdapter();
		}
		return mSearchAdapter;
	}
	
	public String getSearchEventCode(){
		return mSearchInterface.getSearchEventCode();
	}
	
	public Object buildSearchHttpValue(){
		final String s = SystemUtils.getTrimText(mSearchEditText);
		return buildSearchHttpValue(s);
	}
	
	public Object buildSearchHttpValue(String s){
		return mSearchHttpValueProvider == null ?
				s : mSearchHttpValueProvider.buildSearchHttpValues(s);
	}
	
	public static interface OnSearchListener{
		public void onStartSearch(String searchKey);
	}
	
	public static interface SearchInterceptPlugin extends ActivityBasePlugin{
		public boolean onInterceptSearch(EditText et, CharSequence search, boolean bFromBtn);
	}
	
	public static interface SearchInterface{
		public String 				getSearchEventCode();
		
		public SetBaseAdapter<?> 	createSearchAdapter();
	}
	
	public static interface SearchHttpValueProvider{
		public Object 	buildSearchHttpValues(String searchKey);
	}
	
	public static class SimpleSearchHttpValueProvider implements SearchHttpValueProvider{
		
		protected String	mSearchHttpKey;
		
		public SimpleSearchHttpValueProvider setSearchHttpKey(String key){
			mSearchHttpKey = key;
			return this;
		}
		
		@Override
		public Object buildSearchHttpValues(String searchKey) {
			if(TextUtils.isEmpty(mSearchHttpKey)){
				return searchKey;
			}else{
				HashMap<String, String> values = new HashMap<String, String>();
				values.put(mSearchHttpKey, searchKey);
				return values;
			}
		}
	}
}
