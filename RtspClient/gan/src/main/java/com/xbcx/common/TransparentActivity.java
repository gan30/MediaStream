package com.xbcx.common;

import com.xbcx.core.BaseActivity;

import android.os.Bundle;

public class TransparentActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onInitAttribute(BaseAttribute ba) {
		super.onInitAttribute(ba);
		ba.mHasTitle = false;
		ba.mSetContentView = false;
	}
}
