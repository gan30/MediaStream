package com.xbcx.common.pulltorefresh;

import com.xbcx.adapter.SetBaseAdapter;
import com.xbcx.core.Event;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.BaseActivity.ActivityEventHandler;

public class AddOrModifyActivityEventHandler<T> implements ActivityEventHandler{

	private Class<T> 			mItemClass;
	private SetBaseAdapter<T> 	mAdapter;
	private boolean				mIsAddToLast;
	
	public AddOrModifyActivityEventHandler(SetBaseAdapter<T> adapter,Class<T> itemClass){
		mAdapter = adapter;
		mItemClass = itemClass;
	}
	
	public AddOrModifyActivityEventHandler<T> setIsAddToLast(boolean b){
		mIsAddToLast = b;
		return this;
	}
	
	@Override
	public void onHandleEventEnd(Event event, BaseActivity activity) {
		if(event.isSuccess()){
			T item = event.findReturnParam(mItemClass);
			if(item != null){
				if(mIsAddToLast){
					mAdapter.updateOrInsertItem(item);
				}else{
					mAdapter.updateOrInsertItem(0, item);
				}
			}
		}
	}

}
