package com.xbcx.common.menu;

import com.xbcx.core.XApplication;
import com.xbcx.utils.SystemUtils;

import android.graphics.drawable.Drawable;

public class Menu{
	private String 			mId;
	private int				mIcon;
	private Drawable		mIconDrawable;
	private CharSequence 	mText;
	private int 			mTextAppearance;
	private Integer			mTextColor;
	
	public Menu(int nId,int nStringId){
		this(nId, nStringId,0);
	}
	
	public Menu(int nId,String text){
		this(String.valueOf(nId),text);
	}
	
	public Menu(int nId,int nStringId,int textAppearance){
		this(String.valueOf(nId),XApplication.getApplication().getString(nStringId));
		mTextAppearance = textAppearance;
	}
	
	public Menu(String id,String text){
		this.mId = id;
		this.mText = text;
	}
	
	public Menu setTextAppearance(int textAppearance){
		this.mTextAppearance = textAppearance;
		return this;
	}
	
	public Menu setText(CharSequence text){
		mText = text;
		return this;
	}
	
	public Menu setIcon(int icon){
		mIcon = icon;
		return this;
	}
	
	public Menu setIconDrawable(Drawable icon){
		mIconDrawable = icon;
		return this;
	}
	
	public Menu setTextColor(int color){
		mTextColor = color;
		return this;
	}
	
	public int getId(){
		return SystemUtils.safeParseInt(mId);
	}
	
	public String getStringId(){
		return mId;
	}
	
	public int getIcon(){
		return mIcon;
	}
	
	public Drawable getIconDrawable(){
		return mIconDrawable;
	}
	
	public CharSequence getText(){
		return mText;
	}
	
	public int getTextAppearance(){
		return mTextAppearance;
	}
	
	public Integer getTextColor(){
		return mTextColor;
	}
}
