package com.xbcx.common;

import java.util.HashMap;
import java.util.List;

import com.xbcx.core.Creator;
import com.xbcx.core.Listener;
import com.xbcx.core.XApplication;

public class ExecutorManager<Param> {

	private HashMap<String, ExecuteInfo<Param>> mMapIdToUploadInfo = new HashMap<String, ExecuteInfo<Param>>();
	
	private Creator<String, Param> 	mIdCreator;
	
	private Listener<Param> 		mExecutor;
	
	private ExecuteListener<Param> mExecuteListener;
	
	public ExecutorManager(Creator<String, Param> idCreator,Listener<Param> executor) {
		mIdCreator = idCreator;
		mExecutor = executor;
	}
	
	public ExecutorManager<Param> setExecuteListener(ExecuteListener<Param> l){
		mExecuteListener = l;
		return this;
	}
	
	public boolean requestExecute(Param param,List<ExecutorIntercepter<Param>> uis){
		String id = mIdCreator.createObject(param);
		if(!isExcuting(id)){
			ExecuteInfo<Param> ei = new ExecuteInfo<Param>();
			ei.param = param;
			ei.executorIntercepters = uis;
			mMapIdToUploadInfo.put(id , ei);
			continueUpload(ei, null);
			return true;
		}
		return false;
	}
	
	private void continueUpload(ExecuteInfo<Param> ui,ExecutorIntercepter<Param> intercept){
		if(ui.executorIntercepters != null){
			boolean bFind = intercept == null;
			for(ExecutorIntercepter<Param> inter : ui.executorIntercepters){
				if(intercept == inter){
					bFind = true;
				}else if(bFind){
					if(inter.onInterceptExecute(ui.param, new InterceptContinuerImpl(ui, inter))){
						return;
					}
				}
			}
		}
		
		mExecutor.onListenCallback(ui.param);
	}
	
	private void interruptExecute(ExecuteInfo<Param> ui){
		setUploadFinish(ui.param,false);
	}
	
	public boolean isExcuting(String id){
		return mMapIdToUploadInfo.containsKey(id);
	}
	
	public void setUploadFinish(Param param){
		setUploadFinish(param, true);
	}
	
	public void setUploadFinish(Param param,boolean bSuccess){
		if(mExecuteListener != null){
			mExecuteListener.onExecuteFinish(param, bSuccess);
		}
		mMapIdToUploadInfo.remove(mIdCreator.createObject(param));
	}
	
	private static class ExecuteInfo<Param>{
		Param param;
		List<ExecutorIntercepter<Param>> executorIntercepters;
	}
	
	private class InterceptContinuerImpl implements InterceptContinuer{
		ExecuteInfo<Param> 			mExecuteInfo;
		ExecutorIntercepter<Param> 	mIntercepter;
		
		public InterceptContinuerImpl(ExecuteInfo<Param> ui,ExecutorIntercepter<Param> intercept) {
			mExecuteInfo = ui;
			mIntercepter = intercept;
		}
		
		@Override
		public void continueExecute() {
			ExecutorManager.this.continueUpload(mExecuteInfo, mIntercepter);
		}
		
		@Override
		public void interruptExecute() {
			ExecutorManager.this.interruptExecute(mExecuteInfo);
		}
	}
	
	public static interface ExecutorIntercepter<Param>{
		public boolean onInterceptExecute(Param p, InterceptContinuer ic);
	}
	
	public static interface InterceptContinuer{
		public void continueExecute();
		
		public void interruptExecute();
	}
	
	public static interface ExecuteListener<Param>{
		public void onExecuteFinish(Param param, boolean bSuccess);
	}
	
	public static class PostIntercepter<Param> implements ExecutorIntercepter<Param>, Runnable {
		private InterceptContinuer mIc;

		@Override
		public boolean onInterceptExecute(Param p, InterceptContinuer ic) {
			mIc = ic;
			XApplication.getMainThreadHandler().post(this);
			return true;
		}

		@Override
		public void run() {
			mIc.continueExecute();
		}
	}
}
