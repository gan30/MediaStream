package com.xbcx.common.choose;

import android.widget.BaseAdapter;

public class NotifyAdapterSelectObserver<E> implements SelectObserver<E>{

	private BaseAdapter	mAdapter;
	
	public NotifyAdapterSelectObserver(BaseAdapter adapter) {
		mAdapter = adapter;
	}
	
	@Override
	public void onSelectChanged(Selectable<E> p) {
		mAdapter.notifyDataSetChanged();
	}

}
