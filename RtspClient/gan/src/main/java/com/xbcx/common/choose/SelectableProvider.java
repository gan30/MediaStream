package com.xbcx.common.choose;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class SelectableProvider<E> extends Selectable<E>{

	private HashMap<E, E> 			mMapSeleteItem = new HashMap<E, E>();
	private boolean					mMultiSelect = false;
	private List<E> 				mSortItem;
	
	@Override
	public void addSelectItem(E item) {
		if(!mMultiSelect){
			mMapSeleteItem.clear();
		}
		mMapSeleteItem.put(item, item);
		addSort(item);
		notifySelectObserver();
	}	
	
	@Override
	public boolean setSort(boolean b) {
		if(b){
			mSortItem = new ArrayList<E>();
			mSortItem.addAll(mMapSeleteItem.keySet());
		}else{
			mSortItem = null;
		}
		return true;
	}
	
	@Override
	public <T extends E> void addAllSelectItem(Collection<T> items) {
		if(items == null){
			return;
		}
		if(isMultiSelect()){
			for(E item : items){
				mMapSeleteItem.put(item, item);
				addSort(item);
			}
		}else{
			if(items.size() > 0){
				mMapSeleteItem.clear();
				final E item = items.iterator().next();
				mMapSeleteItem.put(item, item);
				addSort(item);
			}
		}
		notifySelectObserver();
	}

	@Override
	public boolean removeSelectItem(E item) {
		if(mMapSeleteItem.remove(item) != null){
			removeSort(item);
			notifySelectObserver();
			return true;
		}
		return false;
	}

	@Override
	public boolean toggleSelectItem(E item) {
		boolean bRet = false;
		if(mMapSeleteItem.containsKey(item)){
			mMapSeleteItem.remove(item);
			removeSort(item);
		}else{
			if(!mMultiSelect){
				mMapSeleteItem.clear();
			}
			mMapSeleteItem.put(item, item);
			addSort(item);
			bRet = true;
		}
		notifySelectObserver();
		return bRet;
	}

	@Override
	public void clearSelectItem() {
		mMapSeleteItem.clear();
		if(mSortItem != null){
			mSortItem.clear();
		}
		notifySelectObserver();
	}

	@Override
	public boolean isSelected(E item) {
		return mMapSeleteItem.containsKey(item);
	}

	@Override
	public boolean containSelect() {
		return mMapSeleteItem.size() > 0;
	}

	@Override
	public Collection<E> getAllSelectItem() {
		if(mSortItem != null){
			return mSortItem;
		}
		return mMapSeleteItem.values();
	}
	
	@Override
	public E getSelectItem(E item) {
		return mMapSeleteItem.get(item);
	}
	
	@Override
	public int getSelectCount() {
		return mMapSeleteItem.size();
	}

	@Override
	public void setMultiSelect(boolean b) {
		mMultiSelect = b;
	}
	
	@Override
	public boolean isMultiSelect() {
		return mMultiSelect;
	}

	private void addSort(E item){
		if(mSortItem != null){
			mSortItem.add(item);
		}
	}
	
	private void removeSort(E item){
		if(mSortItem != null){
			mSortItem.remove(item);
		}
	}
}
