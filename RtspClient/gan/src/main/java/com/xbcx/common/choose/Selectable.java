package com.xbcx.common.choose;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class Selectable<E> {
	
	private List<SelectObserver<E>> mSelectObservers;
	
	public abstract void			addSelectItem(E item);
	
	public abstract <T extends E> void	addAllSelectItem(Collection<T> items);
	
	public abstract boolean 		removeSelectItem(E item);
	
	public abstract boolean			toggleSelectItem(E item);

	public abstract void 			clearSelectItem();
	
	public abstract boolean 		isSelected(E item);
	
	public abstract void			setMultiSelect(boolean b);
	
	public abstract boolean			isMultiSelect();
		
	public abstract boolean 		containSelect();
	
	public abstract Collection<E> 	getAllSelectItem();
	
	public abstract E				getSelectItem(E item);
	
	public abstract int				getSelectCount();
	
	public boolean setSort(boolean b){
		return false;
	}
	
	public void addSelectObserver(SelectObserver<E> so){
		if(mSelectObservers == null){
			mSelectObservers = new ArrayList<SelectObserver<E>>();
		}
		mSelectObservers.add(so);
	}
	
	public void removeSelectObserver(SelectObserver<E> so){
		if(mSelectObservers == null){
			return;
		}
		mSelectObservers.remove(so);
	}
	
	public void notifySelectObserver(){
		if(mSelectObservers != null){
			for(SelectObserver<E> so : mSelectObservers){
				so.onSelectChanged(this);
			}
		}
	}
}
