package com.xbcx.common;

import com.xbcx.core.ActivityPlugin;
import com.xbcx.core.BaseActivity;
import com.xbcx.core.Event;
import com.xbcx.core.EventManager.OnEventListener;

public class SimpleEventExecuteUI extends ActivityPlugin<BaseActivity> implements
													OnEventListener{
	private BaseActivity	mActivity;
	private Event			mCheckEvent;
	
	public SimpleEventExecuteUI(BaseActivity activity,Event event) {
		mActivity = activity;
		mCheckEvent = event;
		mCheckEvent.addEventListener(this);
		mActivity.showXProgressDialog();
		activity.registerPlugin(this);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mCheckEvent.removeEventListener(this);
	}

	@Override
	public void onEventRunEnd(Event event) {
		mActivity.dismissXProgressDialog();
	}
}
