package com.xbcx.camera.preview;

import android.content.Context;
import android.hardware.Camera;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.os.SystemClock;

import com.xbcx.camera.video.VideoEngine;
import com.xbcx.camera.video.VideoRecoderListener;
import com.xbcx.core.ToastManager;
import com.xbcx.core.XApplication;
import com.xbcx.media.MediaError;
import com.xbcx.media.Recorder;
import com.xbcx.media.audio.AudioCodec;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.media.video.VideoCodec;
import com.xbcx.media.video.VideoFrame;
import com.xbcx.media.video.VideoMuxer;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class PreviewVideoEngine extends PreviewVideo implements VideoEngine{

	final static String Tag = "PreviewVideoEngine";
	
	VideoMuxer					mVideoMuxer;
	long						mStartTime;
	List<VideoRecoderListener>  mVideoRecoderListeners;

	AudioCodec.AudioCodecFrameListener mAudioCodecFrameListener;
	VideoCodec.VideoCodecFrameListener mVideoCodecFrameListener;

	public PreviewVideoEngine(Context context) {
		super(context);
	}

	public PreviewVideoEngine setAudioCodecFrameListener(AudioCodec.AudioCodecFrameListener listener){
		mAudioCodecFrameListener = listener;
		return this;
	}

	public PreviewVideoEngine setVideoCodecFrameListener(VideoCodec.VideoCodecFrameListener listener){
		mVideoCodecFrameListener = listener;
		return  this;
	}

	@Override
	public void release() {
		super.release();
		if(mVideoMuxer!=null) {
			mVideoMuxer.release();
		}
	}
	
	@Override
	public void onAudioCodecFrame(ByteBuffer buffer, BufferInfo info) {
		MediaLog.d(Tag, "onAudioCodecFrame");
		if(mVideoMuxer!=null) {
			mVideoMuxer.pumpStream(buffer, info, false);
		}

		if(mAudioCodecFrameListener!=null){
			mAudioCodecFrameListener.onAudioCodecFrame(buffer,info);
		}
	}

	@Override
	public void onAudioCodecFormatChanged(MediaFormat format) {
		MediaLog.d(Tag, "onAudioCodecFormatChanged");
		if(mVideoMuxer!=null) {
			mVideoMuxer.addTrack(format, false);
		}

		if(mAudioCodecFrameListener!=null){
			mAudioCodecFrameListener.onAudioCodecFormatChanged(format);
		}
	}

	@Override
	public void onVideoCodecFrame(ByteBuffer buffer, BufferInfo info) {
		MediaLog.d(Tag, "onVideoCodecFrame");
		if(mVideoMuxer!=null) {
			mVideoMuxer.pumpStream(buffer, info, true);
		}

		if(mVideoCodecFrameListener!=null){
			mVideoCodecFrameListener.onVideoCodecFrame(buffer,info);
		}
	}

	@Override
	public void onVideoCodecFormatChanged(MediaFormat format) {
		MediaLog.d(Tag, "onVideoCodecFormatChanged");
		if(mVideoMuxer!=null) {
			mVideoMuxer.addTrack(format, true);
		}

		if(mVideoCodecFrameListener!=null){
			mVideoCodecFrameListener.onVideoCodecFormatChanged(format);
		}
	}

	@Override
	public void onVideoFrame(VideoFrame frame) {
		
	}

	@Override
	public boolean startVideo(String filePath) {
		try {
			MediaLog.i(Tag, "startVideo");
			mVideoMuxer = new VideoMuxer(filePath);
			mVideoMuxer.setOrientationHint(getRotation());
			start();
			onVideoStart();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			MediaLog.i(Tag, "start io exception:"+e.getMessage());
			release();
			onVideoError(MediaError.Error_IO, "start io exception");
			return false;
		}
	}

	@Override
	public boolean stopVideo() {
		if(stop()) {
			MediaLog.i(Tag, "stopVideo");
			mVideoMuxer.release();
			onVideoEnd();
			return true;
		}
		return false;
	}

	public boolean canStop() {
		return SystemClock.uptimeMillis() - mStartTime > 1000;
	}
	
	public boolean clipVideo() {
		if(mVideoMuxer!=null) {
			MediaLog.i(Tag, "clipVideo");
			return mVideoMuxer.stopAndRestart();
		}
		return  false;
	}

	@Override
	public long getVideoStartTime() {
		return mStartTime;
	}

	@Override
	public String getVideoFile() {
		if(mVideoMuxer!=null) {
			return mVideoMuxer.getRecordFile();
		}
		return null;
	}

	@Override
	public void addVideoRecoderListener(VideoRecoderListener listener) {
		if(mVideoRecoderListeners == null) {
			mVideoRecoderListeners = new ArrayList<>();
		}
		mVideoRecoderListeners.add(listener);
	}

	@Override
	public void removeVideoRecoderListener(VideoRecoderListener listener) {
		if(mVideoRecoderListeners!=null) {
			mVideoRecoderListeners.remove(listener);
		}
	}
	
	protected void onVideoStart() {
		mStartTime = SystemClock.uptimeMillis();
		if(mVideoRecoderListeners!=null) {
			for(VideoRecoderListener listener:mVideoRecoderListeners) {
				listener.onRecordStart(this);
			}
		}
	}
	
	protected void onVideoEnd() {
		if(mVideoRecoderListeners!=null) {
			for(VideoRecoderListener listener:mVideoRecoderListeners) {
				listener.onRecordEnd(this);
			}
		}
	}
	
	protected void onVideoError(int error, String msg) {
		if(mVideoRecoderListeners!=null) {
			for(VideoRecoderListener listener:mVideoRecoderListeners) {
				listener.onRecordError(this, error);
			}
		}
	}
	
	@Override
	public void onRecoderError(Recorder recoder, int error, String message) {
		super.onRecoderError(recoder, error, message);
		XApplication.getMainThreadHandler().post(new Runnable() {
			@Override
			public void run() {
				ToastManager.getInstance().show("recoder error");
				stopVideo();
			}
		});
	}

	@Override
	public void setCamera(Camera camera) {
		
	}
}
