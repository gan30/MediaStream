package com.xbcx.camera.preview;


import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;

import com.xbcx.media.MediaError;
import com.xbcx.media.RecordListener;
import com.xbcx.media.Recorder;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.media.video.VideoCodec;
import com.xbcx.media.video.VideoCodec.VideoCodecFrameListener;
import com.xbcx.media.video.VideoFrame;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.os.Process;
import gan.xmedia.YUV;

public class PreviewVideoRecorder implements Recorder{
	
	final static String Tag = "PreviewVideoRecorder";
	
	private  boolean 						mRuning;
	Context									mContext;
	int 									mRotation;
	private int								mFrameCachesSize = 30;
	private Vector<VideoFrame>				mFrameCaches = new Vector<>();
	private ArrayBlockingQueue<byte[]> 		mByteCaches = new ArrayBlockingQueue<byte[]>(5);
	protected int							mWidth,mHeight;
	
	VideoCodecFrameListener					mVideoCodecFrameListener;
	OnVideoFrameListener					mOnVideoFrameListener;
	List<RecordListener> 					mRecordListeners;
	List<VideoProcessor>					mVideoProcessors;
	
	int			mBitRate = 5;
	
	public PreviewVideoRecorder(Context context) {
		mContext = context;
	}
	
	public void setRotation(int rotation) {
		this.mRotation = rotation;
	}
	
	public int getRotation() {
		return mRotation;
	}
	
	public void setVideoSize(int width,int height) {
		mWidth = width;
		mHeight = height;
	}
	
	public void setBitRate(int bitRate) {
		this.mBitRate = bitRate;
	}
	
	public int getWidth() {
		return mWidth;
	}
	
	public int getHeight() {
		return mHeight;
	}
	
	public void clearCache() {
		mFrameCaches.clear();
		mByteCaches.clear();
	}
	
	public void setFrameCacheSize(int size) {
		mFrameCachesSize = size;
	}
	
	/**
	 * camera previewCallBack
	 * @param camera
	 * @param data
	 * @param format
	 */
	public void onPreviewFrame(Camera camera,byte[] data,int format){
		byte[] buffer = mByteCaches.poll();
        if (buffer == null || buffer.length != data.length) {
            buffer = new byte[data.length];
        }
		System.arraycopy(data, 0, buffer, 0, data.length);
		onVideo(buffer,format);
	}
	
	protected void onVideo(byte[] data,int format) {
		if(mFrameCaches.size()>=mFrameCachesSize) {
			VideoFrame frame = mFrameCaches.remove(0);
			mByteCaches.offer(frame.data);
		}
		
		VideoFrame frame = new VideoFrame();
		frame.data = data;
		frame.format = format;
		mFrameCaches.add(frame);
	}

	Thread mVideoThread = null;
	private void startVideoThread() throws Exception{
		if(mVideoThread!=null) {
			throw new Exception("recoder busy now");
		}
		mVideoThread = new Thread() {
			@Override
			public void run() {
				super.run();
				Process.setThreadPriority(Process.THREAD_PRIORITY_DISPLAY);
				mRuning = true;
				VideoCodec videoCodec = null;
				try {
					videoCodec = new VideoCodec(mContext);
					videoCodec.setBitRate(mBitRate);
					videoCodec.setVideoCodecFrameListener(mVideoCodecFrameListener);
					videoCodec.start(mWidth, mHeight);
					
					if(mVideoProcessors!=null) {
						for(VideoProcessor processor:mVideoProcessors) {
							processor.init(PreviewVideoRecorder.this);
						}
					}
					
					do {
						if(!mFrameCaches.isEmpty()) {
							VideoFrame frame = mFrameCaches.remove(0);
							frame.colorFormat = videoCodec.getColorFormat();
							MediaLog.d(Tag, "frame.format:"+frame.format);
							MediaLog.d(Tag, "frame.colorFormat:"+frame.colorFormat);
							if(frame.format == ImageFormat.YV12) {
								if(VideoCodec.is420sp(frame.colorFormat)) {
//									YUVUtil.YV12toYUV420SP(frame.data, mWidth, mHeight);
									YUV.YV12toYUV420SP(frame.data, mWidth, mHeight);
								}else {
//									YUVUtil.YV12toYUV420P(frame.data, mWidth, mHeight);
									YUV.YV12toYUV420P(frame.data, mWidth, mHeight);
								}
							}else if(frame.format == ImageFormat.NV21){
								if(VideoCodec.is420sp(frame.colorFormat)) {
//									YUVUtil.NV21toYUV420SP(frame.data, mWidth, mHeight);
									YUV.NV21toYUV420SP(frame.data, mWidth, mHeight);
								}else {
//									YUVUtil.NV21toYUV420P(frame.data, mWidth, mHeight);
									YUV.NV21toYUV420P(frame.data, mWidth, mHeight);
								}
							}
							
							if(mVideoProcessors!=null) {
								for(VideoProcessor processor:mVideoProcessors) {
									processor.processData(frame);
								}
							}
							
							videoCodec.encoder(frame.data);
							onVideoFrame(frame);
							mByteCaches.offer(frame.data);
						}
					} while (mRuning);
				} catch (Exception e) {
					e.printStackTrace();
					onVideoError(MediaError.Error_VideoEncode, "video encode exception");
					MediaLog.i(Tag, "run e:"+e.getMessage());
				} finally {
					if (videoCodec != null) {
						videoCodec.release();
					}
					
					if(mVideoProcessors!=null) {
						for(VideoProcessor processor:mVideoProcessors) {
							processor.release();
						}
					}
					
					mVideoThread = null;
				}
			}
		};
		mVideoThread.start();
		onVideoStart();
	}
	
	private void stopVideo() {
		if(mVideoThread!=null) {
			mRuning = false;
			Thread t = mVideoThread;
			try {
				t.interrupt();
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			clearCache();
			onVideoEnd();
		}
	}
	
	protected void onVideoStart() {
		if(mRecordListeners!=null) {
			for(RecordListener listener:mRecordListeners) {
				listener.onRecoderStart(this);
			}
		}
	}
	
	protected void onVideoEnd() {
		if(mRecordListeners!=null) {
			for(RecordListener listener:mRecordListeners) {
				listener.onRecoderEnd(this);
			}
		}
	}
	
	protected void onVideoError(int error,String msg) {
		if(mRecordListeners!=null) {
			for(RecordListener listener:mRecordListeners) {
				listener.onRecoderError(this, error, msg);
			}
		}
	}
	
	public void setVideoCodecFrameListener(VideoCodecFrameListener videoCodecFrameListener) {
		this.mVideoCodecFrameListener = videoCodecFrameListener;
	}
	
	public void setOnVideoFrameListener(OnVideoFrameListener onVideoFrameListener) {
		this.mOnVideoFrameListener = onVideoFrameListener;
	}
	
	protected void onVideoFrame(VideoFrame frame) {
		if(mOnVideoFrameListener!=null) {
			mOnVideoFrameListener.onVideoFrame(frame);
		}
	}
	
	public static interface OnVideoFrameListener{
		public void onVideoFrame(VideoFrame frame);
	}

	@Override
	public void startRecord() throws Exception {
		startVideoThread();
	}

	@Override
	public void stopRecord() {
		stopVideo();
	}

	@Override
	public void release() {
		if(mRuning) {
			stopVideo();
		}
		clearCache();
		if(mRecordListeners!=null) {
			mRecordListeners.clear();
		}
		if(mVideoProcessors!=null) {
			mVideoProcessors.clear();
		}
	}

	@Override
	public void addRecoderListener(RecordListener listener) {
		if(mRecordListeners==null) {
			mRecordListeners = new ArrayList<>();
		}
		mRecordListeners.add(listener);
	}

	@Override
	public void removeRecoderListener(RecordListener listener) {
		if(mRecordListeners!=null) {
			mRecordListeners.remove(listener);
		}
	}
	
	public void addVideoProcessor(VideoProcessor processor) {
		if(mVideoProcessors==null) {
			mVideoProcessors = new ArrayList<>();
		}
		mVideoProcessors.add(processor);
	}
	
	public void removeVideoProcessor(VideoProcessor processor) {
		if(mVideoProcessors != null) {
			mVideoProcessors.remove(processor);
		}
	}
	
	public static interface VideoProcessor{
		public void init(PreviewVideoRecorder recoder);
		public void processData(VideoFrame frame)throws Exception;
		public void release();
	}
}
