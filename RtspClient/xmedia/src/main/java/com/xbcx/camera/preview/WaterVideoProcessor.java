package com.xbcx.camera.preview;

import com.xbcx.camera.preview.PreviewVideoRecorder.VideoProcessor;
import com.xbcx.media.video.VideoCodec;
import com.xbcx.media.video.VideoFrame;
import com.xbcx.media.video.YUVWater;
import com.xbcx.media.video.YUVWater.SimpleWaterViewAdapter;
import com.xbcx.media.video.YUVWater.WaterViewAdapter;

public class WaterVideoProcessor implements VideoProcessor{

	YUVWater			mVideoWater;
	WaterViewAdapter 	mWaterViewAdapter;
	
	@Override
	public void init(PreviewVideoRecorder recoder) {
		mVideoWater = new YUVWater(recoder.getWidth(), 
				recoder.getHeight(), recoder.getRotation());
		if(mWaterViewAdapter == null) {
			mWaterViewAdapter = new SimpleWaterViewAdapter();
		}
		mVideoWater.setWaterViewAdapter(mWaterViewAdapter);
	}
	
	/**
	 * call before init
	 * @return
	 */
	public WaterVideoProcessor setWaterViewAdapter(WaterViewAdapter waterViewAdapter) {
		this.mWaterViewAdapter = waterViewAdapter;
		return this;
	}

	public WaterViewAdapter getWaterViewAdapter() {
		return mWaterViewAdapter;
	}
	
	@Override
	public void processData(VideoFrame frame) throws Exception {
		if(mVideoWater!=null) {
			mVideoWater.setColorFormat(VideoCodec.is420sp(frame.colorFormat)? 
					YUVWater.ColorFormat_420SP:
						YUVWater.ColorFormat_420P);
			mVideoWater.addYUVWater(frame.data);
		}
	}

	@Override
	public void release() {
		if(mVideoWater!=null) {
			mVideoWater.stopWater();
		}
	}
	
}
