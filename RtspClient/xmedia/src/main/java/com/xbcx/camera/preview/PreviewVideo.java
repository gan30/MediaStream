package com.xbcx.camera.preview;

import java.io.IOException;

import com.xbcx.camera.XCamera;
import com.xbcx.camera.preview.PreviewVideoRecorder.OnVideoFrameListener;
import com.xbcx.media.RecordListener;
import com.xbcx.media.Recorder;
import com.xbcx.media.audio.AudioCodec.AudioCodecFrameListener;
import com.xbcx.media.audio.AudioCodecProcessor;
import com.xbcx.media.audio.AudioConfig;
import com.xbcx.media.audio.AudioRecorder;
import com.xbcx.media.audio.AudioRecorder.AudioFrameListener;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.media.audio.WebrtcNoiseProcessor;
import com.xbcx.media.video.VideoCodec.VideoCodecFrameListener;

import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;

public abstract class PreviewVideo implements AudioFrameListener, AudioCodecFrameListener, RecordListener, VideoCodecFrameListener, OnVideoFrameListener, PreviewCallback{
	
	final static String Tag = "PreviewVideo";
	
	protected Context			mContext;
	private   int				mRotation;
	private   int				mPreviewFormat;
	protected final XCamera		mCamera = XCamera.get();
	
	protected AudioRecorder			mAudioRecoder;
	protected PreviewVideoRecorder	mPreviewVideo;
	private   boolean				mIsRuning;
	
	public PreviewVideo(Context context) {
		mContext = context;
	}
	
	public void release() {
		if(isVideoRecording()) {
			stop();
		}
		if(mAudioRecoder!=null) {
			mAudioRecoder.removeRecoderListener(this);
			mAudioRecoder.release();
			mAudioRecoder = null;
		}
		if(mPreviewVideo!=null) {
			mPreviewVideo.release();
			mPreviewVideo = null;
		}
	}
	
	public boolean start() throws Exception {
		try {
			MediaLog.i(Tag, "start");
			startAudioRecoder();
			startVideoRecorder();
			mIsRuning = true;
			return true;
		} catch (IOException e) {
			MediaLog.i(Tag, "start io exception:"+e.getMessage());
			release();
			throw e;
		}
	}
	
	public void startAudioRecoder() throws Exception {
		MediaLog.i(Tag, "startAudioRecoder");
		if(mAudioRecoder == null) {
			mAudioRecoder = onCreateAudioRecorder();
			mAudioRecoder.addRecoderListener(this);
			mAudioRecoder.setAudioFrameListener(this);
		}
		onPerpareAudio();
		mAudioRecoder.startRecord();
	}
	
	protected AudioRecorder onCreateAudioRecorder() {
		AudioRecorder audioRecoder = new AudioRecorder();
		audioRecoder.addAudioProcessor(new WebrtcNoiseProcessor());
		audioRecoder.addAudioProcessor(new AudioCodecProcessor(this));
		return audioRecoder;
	}
	
	protected void onPerpareAudio() {
		mAudioRecoder.setAudioConfig(new AudioConfig());
	}
	
	public void startVideoRecorder() throws Exception {
		MediaLog.i(Tag, "startVideoCodec");
		if(mPreviewVideo == null) {
			mPreviewVideo = onCreatePreviewVideoRecorder();
			mPreviewVideo.addRecoderListener(this);
			mPreviewVideo.setVideoCodecFrameListener(this);
			mPreviewVideo.setOnVideoFrameListener(this);
		}
		onPerpareVideo();
		mPreviewVideo.startRecord();
	}
	
	protected PreviewVideoRecorder onCreatePreviewVideoRecorder() {
		PreviewVideoRecorder previewVideo = new PreviewVideoRecorder(mContext);
		previewVideo.addVideoProcessor(new WaterVideoProcessor());
		return previewVideo;
	}
	
	protected void onPerpareVideo() {
		mPreviewVideo.setRotation(mRotation);
		mCamera.setPreviewFormat(ImageFormat.YV12);
		Size size = mCamera.getParameters().getPreviewSize();
		mPreviewFormat = mCamera.getParameters().getPreviewFormat();
		mPreviewVideo.setVideoSize(size.width, size.height);
	}
	
	public int getRotation() {
		return mRotation;
	}
	
	public AudioRecorder getAudioRecoder() {
		return mAudioRecoder;
	}
	
	public PreviewVideoRecorder getPreviewVideoRecorder() {
		return mPreviewVideo;
	}

	@Override
	public void onAudioFrame(byte[] data) {
	}
	
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		if(mPreviewVideo!=null) {
			mPreviewVideo.onPreviewFrame(camera, data, mPreviewFormat);
		}
	}

	public void startPreviewCallback() {
		mCamera.addPreviewCallBack(this);
		mCamera.startPreviewCallback();
	}
	
	public void stopPreviewCallback() {
		mCamera.removePreviewCallBack(this);
		mCamera.stopPreviewCallBack();
	}
	
	@Override
	public void onRecoderStart(Recorder recoder) {
		if(recoder==mPreviewVideo) {
			startPreviewCallback();
		}
	}

	@Override
	public void onRecoderEnd(Recorder recoder) {
		if(recoder==mPreviewVideo) {
			stopPreviewCallback();
		}
	}

	@Override
	public void onRecoderError(Recorder recoder, int error, String message) {
	}

	public boolean stop() {
		MediaLog.i(Tag, "stop");
		mAudioRecoder.stopRecord();
		mPreviewVideo.stopRecord();
		mIsRuning = false;
		return true;
	}
	
	public boolean isVideoRecording() {
		return mIsRuning;
	}

	public void setRotation(int orientation) {
		mRotation = orientation;
	}
}
