package com.xbcx.media.image;

import java.io.FileOutputStream;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;

import com.xbcx.camera.CameraOrientationManager;
import com.xbcx.camera.CameraUtil;
import com.xbcx.camera.video.FileNumber;
import com.xbcx.media.video.YUVUtil;
import com.xbcx.utils.FileHelper;

import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import gan.xmedia.YUV;

public class PreviewTakePicture implements PreviewCallback{

	Camera 				mCamera;
	int 				mRotation;
	int					mCount;
	OnPerpareListener	mOnPerpareListener;
	boolean 			mAutoRotation;
	FileNumber			mFileNumber;
	TakePictureCallBack	mListener;
	
	private ArrayBlockingQueue<byte[]> 	mByteCaches = new ArrayBlockingQueue<byte[]>(5);
	private Vector<TakeThread>    		mTakeThreads;
	
	public PreviewTakePicture setOnPerpareListener(OnPerpareListener onPerpareListener) {
		this.mOnPerpareListener = onPerpareListener;
		return this;
	}
	
	public PreviewTakePicture setAutoRotation(boolean autoRotation) {
		this.mAutoRotation = autoRotation;
		return this;
	}
	
	public void setCamera(Camera camera) {
		this.mCamera = camera;
	}
	
	public void perpare(){
		mRotation = CameraOrientationManager.get().getRotation();
		try {
			Parameters parameters = mCamera.getParameters();
			Size previewsize = parameters.getPreviewSize();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(mOnPerpareListener!=null){
			mOnPerpareListener.onPerpare(this);
		}
	}
	
	@Override
	public void onPreviewFrame(byte[] data, Camera camera) {
		synchronized (this) {
			if(mCount>0) {
				if(mTakeThreads == null) {
					mTakeThreads = new Vector<>();
				}else if(!mTakeThreads.isEmpty()) {
					return;
				}
				
				Size size = camera.getParameters().getPreviewSize();
				int format = camera.getParameters().getPreviewFormat();
				if (mAutoRotation) {
					mRotation = CameraOrientationManager.get().getRotation();
				}
				TakeThread takeThread = new TakeThread(data, size.width, size.height, format, mRotation);
				mTakeThreads.add(takeThread);
				takeThread.start();
			}
		}
	}
	
	public boolean canTake() {
		return !isTakeing();
	}
	
	public boolean isTakeing() {
		return mCount>0;
	}
	
	public synchronized void takePicture(String filePath,TakePictureCallBack callBack) {
		takePicture(filePath, callBack, 1);
	}
	
	public synchronized void takePicture(String filePath,TakePictureCallBack callBack,int count) {
		if(mCamera == null) {
			return;
		}
		if(mCount>0) {
			throw new IllegalStateException("take busy now");
		}
		mFileNumber = new FileNumber(filePath);
		mListener = callBack;
		mCount = count;
		perpare();
	}
	
	public void cancelTake() {
		mCount = 0;
		mByteCaches.clear();
	}
	
	public static void takePicture(final String filePath,byte[] nv21,int width,int height) throws Exception {
		takePicture(filePath, nv21, width, height, 0);
	}
	
	public static void takePicture(final String filePath,byte[] nv21,int width,int height,int rotation) throws Exception {
		FileOutputStream os = FileHelper.createFileOutputStream(filePath);
		try {
			boolean rotate = rotation % 180 != 0;
			if (rotate) {
				YUVUtil.rotateNV21(nv21, width, height, rotation);
				int temp = width;
				width = height;
				height = temp;
			}
			YuvImage image = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
			Rect rect = new Rect(0, 0, width, height);
			image.compressToJpeg(rect, 100, os);
			os.flush();
		} finally {
			if(os!=null) {
				try {
					os.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public static interface TakePictureCallBack{
		public void onTakeComplete(final String filePath);
	}
	
	public static interface TakePictureProcessor extends TakePictureCallBack{
		public void onTakeProcessor(Frame frame);
	}
	
	public static class Frame{
		public byte[] data;
		public int width;
		public int height;
		public int format;
		public int rotation;
		public Frame(byte[] data,int width,int height,int format,int rotation) {
			this.data = data;
			this.width = width;
			this.height = height;
			this.format = format;
			this.rotation = rotation;
		}
	}
	
	public class TakeThread extends Thread{
		
		Frame								mFrame;

		public TakeThread(byte[] data ,int width, int height,int format,int rotate) {
			byte[] buffer = mByteCaches.poll();
	        if (buffer == null || buffer.length != data.length) {
	            buffer = new byte[data.length];
	        }
	        System.arraycopy(data, 0, buffer, 0, data.length);
	        mFrame = new Frame(buffer, width, height,format, rotate);
		}
		
		@Override
		public void run() {
			super.run();
			final String filePath = mFileNumber.nextFilePath();
			Frame frame = mFrame;
			try {
				if (frame.format == ImageFormat.YV12) {
					YUV.YV12toNV21(frame.data, frame.width, frame.height);
				}
				if (mListener instanceof TakePictureProcessor) {
					((TakePictureProcessor) mListener).onTakeProcessor(frame);
				}
				takePicture(filePath, frame.data, frame.width, frame.height, frame.rotation);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			mByteCaches.offer(frame.data);
			CameraUtil.checkEmptyFile(filePath);
			CameraUtil.insertImageToMediaStore(filePath, mRotation);
			if (mListener != null) {
				mListener.onTakeComplete(filePath);
			}
			finish();	
		}
		
		public void finish() {
			mFrame = null;
			mCount--;
			mTakeThreads.remove(this);
		}
	}
	
	public static interface OnPerpareListener{
		public void onPerpare(PreviewTakePicture picture);
	}
}
