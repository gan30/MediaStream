package com.xbcx.media.audio;

import java.io.FileOutputStream;
import java.io.IOException;

import com.xbcx.camera.CameraUtil;
import com.xbcx.media.audio.AudioRecorder.AudioProcessor;
import com.xbcx.utils.FileHelper;

public class AudioFileProcessor implements AudioProcessor{

	String mFilePath;
	FileOutputStream mOutputStream;
	
	public AudioFileProcessor(String filePath) {
		mFilePath = filePath;
	}
	
	@Override
	public void init(AudioRecorder recoder) {
		if(CameraUtil.isAvailableFile(mFilePath)) {
			try {
				mOutputStream = FileHelper.createFileOutputStream(mFilePath);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void processData(byte[] data) throws IOException {
		if(mOutputStream!=null) {
			mOutputStream.write(data);
		}
	}

	@Override
	public void release() {
		if(mOutputStream!=null) {
			try {
				mOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
