package com.xbcx.media.video;

import java.nio.ByteBuffer;

import gan.xmedia.YUV;

/**
 * java 提供思路，实际效率非常低，{@link YUV}
 * @author gan
 */
public class YUVUtil {

	private final static String Tag = "YUVUtil";
	
	public static void YV12toYUV420P(byte[] yv12,int width,int height) {
		byte[] temp = yv12.clone();
		int total = width*height;
		int size = total/4;
		
		for (int i = total; i < total+size; i++) {  
			temp[i] = yv12[i + size];  
	    }
	    for (int i = total + size; i < total+size+size; i++) {
	    	temp[i] = yv12[i - size];  
	    }
	    
	    System.arraycopy(temp, total, yv12, total, size+size);
	    temp = null;
	}
	
	public static void YV12toYUV420SP(byte[] yv12,int width,int height) {
		byte[] temp = yv12.clone();
		int total = width*height;
		int size = total/4;
		
		byte[] bufferV = new byte[size];
		byte[] bufferU = new byte[size];
		System.arraycopy(yv12, total, bufferV, 0, size);
		System.arraycopy(yv12, total+size, bufferU, 0, size);
	    int index = 0;
	    for (int i= total; i<yv12.length; i+=2) {
	    	temp[i] = bufferU[index];
	    	temp[i+1] = bufferV[index];
	    	index++;
	    }
	    bufferV = null;
	    bufferU = null;
		
	    System.arraycopy(temp, total, yv12, total, size+size);
	    temp = null;
	}
	
	public static void NV21toYUV420P(byte[] nv21,int width,int height) {
		int total = width * height;
		int size = total/4;
	    byte[] temp = nv21.clone();  
	    
	    ByteBuffer bufferU = ByteBuffer.wrap(temp, total, size);  
	    ByteBuffer bufferV = ByteBuffer.wrap(temp, total + size, size);  
	    for (int i=total; i<nv21.length; i+=2) {  
	        bufferV.put(nv21[i]);  
	        bufferU.put(nv21[i+1]);  
	    }
	    
	    System.arraycopy(temp, total, nv21, total, size+size);
	    temp = null;
	}
	
	public static void NV21toYUV420SP(byte[] nv21,int width,int height) {
		int total = width * height;
		int size = total/4;
	    byte[] temp = nv21.clone();  
	    
	    ByteBuffer bufferU = ByteBuffer.wrap(temp, total, size);  
	    ByteBuffer bufferV = ByteBuffer.wrap(temp, total + size, size);  
	    for (int i=total; i<nv21.length; i+=2) {  
	        bufferV.put(nv21[i]);  
	        bufferU.put(nv21[i+1]);  
	    }
	    
	    byte[] tempU = new byte[size];
	    byte[] tempV = new byte[size];
		System.arraycopy(temp, total, tempU, 0, size);
		System.arraycopy(temp, total+size, tempV, 0, size);
	    int index = 0;
	    for (int i= total; i<temp.length; i+=2) {
	    	temp[i] = tempU[index];
	    	temp[i+1] = tempV[index];
	    	index++;
	    }
	    tempV = null;
	    tempU = null;
	    
	    System.arraycopy(temp, total, nv21, total, size+size);
	    temp = null;
	}
	
	public static void YV12toNV21(final byte[] Yv12, final int width, final int height) {
    	byte[] nv21 = Yv12.clone();  
    	final int total = width * height;
        final int size = total / 4;
        final int tempSize = total * 5 / 4;

        for (int i = 0; i < size; i++) {
        	nv21[total + i * 2] = Yv12[total + i]; // Cb (U)
        	nv21[total + i * 2 + 1] = Yv12[tempSize + i]; // Cr (V)
        }
        
        System.arraycopy(nv21, total, Yv12, total, size+size);
        nv21 = null;
    }
	
	public static void YUV420SProtate90(byte[] yuv420sp, int width, int height)  {  
	    int nWidth = 0, nHeight = 0;  
	    int wh = 0;  
	    int uvHeight = 0;  
	    if(width != nWidth || height != nHeight){  
	        nWidth = width;  
	        nHeight = height;  
	        wh = width * height;  
	        uvHeight = height >> 1;//uvHeight = height / 2  
	    }  
	  
	    byte[] temp = new byte[yuv420sp.length];
	    //旋转Y  
	    int k = 0;  
	    for(int i = 0; i < width; i++) {  
	        int nPos = 0;  
	        for(int j = 0; j < height; j++) {  
	        	temp[k] = yuv420sp[nPos + i];  
	            k++;  
	            nPos += width;  
	        }  
	    }  
	    for(int i = 0; i < width; i+=2){  
	        int nPos = wh;  
	        for(int j = 0; j < uvHeight; j++) {  
	        	temp[k] = yuv420sp[nPos + i];  
	        	temp[k + 1] = yuv420sp[nPos + i + 1];  
	            k += 2;  
	            nPos += width;  
	        }  
	    }
	    
	    System.arraycopy(temp, 0, yuv420sp, 0, temp.length);
	    temp = null;
	} 
	
	public static void rgb2NV21(byte[] nv21,int[] argb, int width, int height) {  
		int uvIndex = width * height;
		int R, G, B, Y, U, V;
		int index = 0;
		int yIndex = 0;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				R = (argb[index] & 0xff0000) >> 16;
				G = (argb[index] & 0xff00) >> 8;
				B = (argb[index] & 0xff) >> 0;

				Y = (int) (0.299*R + 0.587*G + 0.114*B);
				nv21[yIndex++] = (byte) ((Y < 0) ? 0 : ((Y > 255) ? 255 : Y));
				if (j % 2 == 0 && index % 2 == 0) {
					U = (int) (-0.1687*R-0.3313*G + 0.5*B + 128);
					V = (int) (0.5*R - 0.4187*G - 0.0813*B + 128);
					nv21[uvIndex++] = (byte) ((V < 0) ? 0 : ((V > 255) ? 255 : V));
					nv21[uvIndex++] = (byte) ((U < 0) ? 0 : ((U > 255) ? 255 : U));
				}
				index++;
			}
		} 
	}
	
	public static void rgb2YUV420(byte[] yuv420,int[] argb, int width, int height) {  
		int yIndex = 0;
		int uIndex = width * height;
		int vIndex = uIndex+uIndex/4;
		
		int R, G, B, Y, U, V;
		int index = 0;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				// a = (argb[index] & 0xff000000) >> 24; // a is not used
				
				R = (argb[index] & 0xff0000) >> 16;
				G = (argb[index] & 0xff00) >> 8;
				B = (argb[index] & 0xff) >> 0;

				Y = (int) (0.299*R + 0.587*G + 0.114*B);
				yuv420[yIndex++] = (byte) ((Y < 0) ? 0 : ((Y > 255) ? 255 : Y));
				if (j % 2 == 0 && index % 2 == 0) {
					U = (int) (-0.1687*R - 0.3313*G + 0.5*B + 128);
					V = (int) (0.5*R - 0.4187*G - 0.0813*B + 128);
					yuv420[uIndex++] = (byte) ((U < 0) ? 0 : ((U > 255) ? 255 : U));
					yuv420[vIndex++] = (byte) ((V < 0) ? 0 : ((V > 255) ? 255 : V));
				}
				index++;
			}
		} 
	} 
	
	public static void rgb2YUV420SP(byte[] yuv420sp, int[] argb, int width, int height) {
		int uvIndex = width * height;
		int R, G, B, Y, U, V;
		int index = 0;
		int yIndex = 0;
		for (int j = 0; j < height; j++) {
			for (int i = 0; i < width; i++) {
				R = (argb[index] & 0xff0000) >> 16;
				G = (argb[index] & 0xff00) >> 8;
				B = (argb[index] & 0xff) >> 0;

				Y = (int) (0.299*R + 0.587*G + 0.114*B);
				yuv420sp[yIndex++] = (byte) ((Y < 0) ? 0 : ((Y > 255) ? 255 : Y));
				if (j % 2 == 0 && index % 2 == 0) {
					U = (int) (-0.1687*R - 0.3313*G + 0.5*B + 128);
					V = (int) (0.5*R - 0.4187*G - 0.0813*B + 128);
					yuv420sp[uvIndex++] = (byte) ((U < 0) ? 0 : ((U > 255) ? 255 : U));
					yuv420sp[uvIndex++] = (byte) ((V < 0) ? 0 : ((V > 255) ? 255 : V));
				}
				index++;
			}
		}
	}
	
	public static void rotateNV21(byte[] nv21,int width,int height,int degree) {
		// 1、Y，旋转
		int offset = 0;
		YUV.rotateByteMatrix(nv21, 0, width, height, degree);
		// 2、UV交错，旋转
		offset += width * height;
		YUV.rotateShortMatrix(nv21, offset, width / 2, height / 2, degree);
	}
}
