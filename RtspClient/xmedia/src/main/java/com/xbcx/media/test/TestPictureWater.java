package com.xbcx.media.test;

import android.graphics.ImageFormat;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.xbcx.camera.CameraOrientationManager;
import com.xbcx.camera.CameraServiceActivity;
import com.xbcx.camera.PictureCamera;
import com.xbcx.camera.PictureCamera.CameraPictureTakenIntercepter;
import com.xbcx.camera.PictureCamera.OnPerpareTakePictureListener;
import com.xbcx.media.image.PreviewTakePicture;
import com.xbcx.media.video.YUVWater;

import gan.media.R;

public class TestPictureWater extends CameraServiceActivity {
	
	YUVWater	mYuvWater;
	int 		mWidth,mHeight;
	int			mRotation;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mYuvWater = new YUVWater();
		mYuvWater.setSimpleWaterViewAdapter();
		getPictureCamera().setOnPerpareTakePictureListener(new OnPerpareTakePictureListener() {
			@Override
			public void onPerpareTakePicture(PictureCamera camera) {
				Parameters p = mCamera.getParameters();
				p.setPictureFormat(ImageFormat.NV21);
				p.setPictureSize(1280, 720);
				mCamera.setParameters(p);
				mCamera.setRotation(0);
				Size size = p.getPictureSize();
				mYuvWater.setSize(mWidth = size.width, mHeight = size.height);
				mYuvWater.setRotation(mRotation= CameraOrientationManager.get().getRotation());
				mYuvWater.setColorFormat(YUVWater.ColorFormat_NV21);
			}
		}).setCameraPictureTakenIntercepter(new CameraPictureTakenIntercepter() {
			@Override
			public Object onIntercepterTakeData(byte[] nv21, String filePath) {
				try {
					mYuvWater.addYUVWater(nv21);
					PreviewTakePicture.takePicture(filePath, nv21, mWidth, mHeight, mRotation);
					return null;
				} catch (Exception e) {
					e.printStackTrace();
					return nv21;
				}
			}
		});
		
		findViewById(R.id.picture).setVisibility(View.VISIBLE);
		findViewById(R.id.cameraid).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCamera.switchCamera();
			}
		});
	}
	
	@Override
	protected void onInitAttribute(BaseAttribute ba) {
		super.onInitAttribute(ba);
	}
}
