package com.xbcx.media.utils;

import com.xbcx.core.FileLogger;

import android.util.Log;

public class MediaLog {

	public static void i(String tag,String msg){
		final String log = tag+":"+msg;
		Log.i(tag, log);
		FileLogger.getInstance("media_i").log(log);
	}
	
	public static void d(String tag,String msg){
		final String log = tag+":"+msg;
		Log.i(tag, log);
	}
	
}
