package com.xbcx.media.audio;

import java.io.IOException;

import com.xbcx.media.audio.AudioCodec.AudioCodecFrameListener;
import com.xbcx.media.audio.AudioRecorder.AudioProcessor;

public class AudioCodecProcessor implements AudioProcessor{

	AudioCodec	mAudioCodec;
	AudioCodecFrameListener mAudioCodecFrameListener;
	
	public AudioCodecProcessor(AudioCodecFrameListener listener) {
		mAudioCodecFrameListener = listener;
	}
	
	@Override
	public void init(AudioRecorder recoder){
		try {
			if(mAudioCodec==null) {
				mAudioCodec = new AudioCodec();
			}
			mAudioCodec.start(recoder.mAudioConfig);
			mAudioCodec.setAudioCodecFrameListener(mAudioCodecFrameListener);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void processData(byte[] data) {
		if(mAudioCodec!=null) {
			mAudioCodec.encoder(data, 0, data.length);
		}
	}

	@Override
	public void release() {
		if(mAudioCodec!=null) {
			mAudioCodec.release();
			mAudioCodec = null;
		}
	}
}
