package com.xbcx.media.audio;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.xbcx.media.Encoder;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.media.utils.SDKINT;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;

/**
 * 使用Android MediaCodec 编码PCM
 * @author gan
 */
@SuppressLint("NewApi")
public class AudioCodec implements Encoder{

	final static String Tag = "AudioEncoder";
	
	MediaCodec					mMediaCodec;
	int							mBufferSize = 1920;
	ByteBuffer[] 				mInputBuffers,mOutputBuffers;
	BufferInfo 		mBufferInfo = new BufferInfo();
	
	AudioCodecFrameListener		mAudioCodecFrameListener;
	
	public void start(AudioConfig config) throws IOException {
		start(config.bitRate, config.sampleRate);
	}
	
	public void start(int bitRate,int sampleRate) throws IOException {
		startMediaCodec(bitRate, sampleRate);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void encoder(byte[] data, int offset, int length) {
		int inputIndex = mMediaCodec.dequeueInputBuffer(1000);
		if(inputIndex>=0) {
			ByteBuffer inputBuffer = null;
			if(SDKINT.isMin(android.os.Build.VERSION_CODES.LOLLIPOP)){
				inputBuffer = mMediaCodec.getInputBuffer(inputIndex);
			}else {
				inputBuffer = mInputBuffers[inputIndex];
			}
			inputBuffer.clear();
			inputBuffer.put(data, offset, length);
			
			long presentationTimeUs = System.nanoTime() / 1000;
			mMediaCodec.queueInputBuffer(inputIndex, offset, length, presentationTimeUs, 0);
		}
		
		int index = mMediaCodec.dequeueOutputBuffer(mBufferInfo, 10000);
		if (index >= 0) {
			if (mBufferInfo.flags == MediaCodec.BUFFER_FLAG_CODEC_CONFIG) {
				return ;
			}
			ByteBuffer outputBuffer = null;
			if (SDKINT.isMin(android.os.Build.VERSION_CODES.LOLLIPOP)) {
				outputBuffer = mMediaCodec.getOutputBuffer(index);
			} else {
				outputBuffer = mOutputBuffers[index];
			}
			if(mAudioCodecFrameListener!=null) {
				mAudioCodecFrameListener.onAudioCodecFrame(outputBuffer, mBufferInfo);
			}
			mMediaCodec.releaseOutputBuffer(index, false);
		} else if (index == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
			mOutputBuffers = mMediaCodec.getOutputBuffers();
		} else if (index == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
			MediaLog.i(Tag, "output format changed...");
			MediaFormat newFormat = mMediaCodec.getOutputFormat();
			if(mAudioCodecFrameListener!=null) {
				mAudioCodecFrameListener.onAudioCodecFormatChanged(newFormat);
			}
		} else if (index == MediaCodec.INFO_TRY_AGAIN_LATER) {
			// Log.v(TAG, "No buffer available...");
		} else {
			MediaLog.i(Tag, "index:" + index);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void startMediaCodec(int bitRate,int sampleRate) throws IOException {
		mMediaCodec = MediaCodec.createEncoderByType("audio/mp4a-latm");
		MediaFormat format = new MediaFormat();
		format.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
		format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
		format.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 1);
		format.setInteger(MediaFormat.KEY_SAMPLE_RATE, sampleRate);
		format.setInteger(MediaFormat.KEY_AAC_PROFILE,
				MediaCodecInfo.CodecProfileLevel.AACObjectLC);
		format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, mBufferSize);
		mMediaCodec.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
    	mMediaCodec.start();
		if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
			mInputBuffers = mMediaCodec.getInputBuffers();
			mOutputBuffers = mMediaCodec.getOutputBuffers();	
		}
    }
	
	/**
     * 停止编码并释放编码资源占用
     */
    public void stopMediaCodec() {
        mMediaCodec.stop();
        mMediaCodec.release();
    }
    
    public void release() {
    	if(mMediaCodec!=null) {
    		try {
				mMediaCodec.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
    		mMediaCodec.release();
    	}
    }
    
	public AudioCodec setAudioCodecFrameListener(AudioCodecFrameListener audioCodecFrameListener) {
		this.mAudioCodecFrameListener = audioCodecFrameListener;
		return this;
	}
	
	public static interface AudioCodecFrameListener{
		public void onAudioCodecFrame(ByteBuffer buffer, BufferInfo info);
		public void onAudioCodecFormatChanged(MediaFormat format);
	}
}
