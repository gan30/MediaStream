package com.xbcx.media.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.xbcx.camera.CameraUtil;
import com.xbcx.media.MediaFile;
import com.xbcx.media.audio.AudioConfig;
import com.xbcx.media.audio.AudioFileProcessor;
import com.xbcx.media.audio.AudioRecorder;
import com.xbcx.media.audio.WebrtcNoiseProcessor;
import gan.media.R;

import android.app.Activity;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class TestAudio extends Activity implements OnClickListener{

	TextView mTvRecoder,mTvPlay;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.test_activity);
		
		mTvRecoder = (TextView) findViewById(R.id.recoder);
		mTvRecoder.setOnClickListener(this);
		mTvPlay = (TextView) findViewById(R.id.play);
		mTvPlay.setOnClickListener(this);
	}
	
	AudioRecorder	mAudioRecoder;
	AudioConfig		mAudioConfig;
	public void start(String filePath) throws Exception {
		mAudioRecoder = new AudioRecorder();
		mAudioRecoder.setAudioConfig(mAudioConfig = new AudioConfig());
		mAudioRecoder.addAudioProcessor(new WebrtcNoiseProcessor());
		mAudioRecoder.addAudioProcessor(new AudioFileProcessor(filePath));
		mAudioRecoder.startRecord();
		mTvRecoder.setText("停止");
		mTvRecoder.setTag(true);
		mTvPlay.setTag(filePath);
	}
	
	public void stop() {
		mAudioRecoder.release();
		mTvRecoder.setText("录制");
		mTvRecoder.setTag(false);
	}

	public void play(String filePath) {
		 // Get the file we want to playback.
	      File file = new File(filePath);
	      // Get the length of the audio stored in the file (16 bit so 2 bytes per short)
	      // and create a short array to store the recorded audio.
	      int musicLength = (int)(file.length()/2);
//	      short[] music = new short[musicLength];
	 
	      try {
	        // Create a DataInputStream to read the audio data back from the saved file.
	    	  
//	        InputStream is = new FileInputStream(file);
//	        BufferedInputStream bis = new BufferedInputStream(is);
//	        DataInputStream dis = new DataInputStream(bis);
//	        int i = 0;
//	        while (dis.available() > 0) {
//	          music[i] = dis.readShort();
//	          i++;
//	        }
//	        dis.close();
	    	
	    	  
	    	ByteArrayOutputStream os = new ByteArrayOutputStream();
	    	FileInputStream is = new FileInputStream(file);
	    	byte[] buffer = new byte[1024];
	    	int len = 0;
	    	while ((len=is.read(buffer))!=-1) {
	    		os.write(buffer, 0, len);
	    	}
	    	is.close();
	    	byte[] music = os.toByteArray();
	    	os.close();
	 
	        // Create a new AudioTrack object using the same parameters as the AudioRecord
	        // object used to create the file.
			int sampleRateInHz = (int) (mAudioConfig.sampleRate * 1.05);
			int channelConfig =  AudioFormat.CHANNEL_OUT_MONO;
			int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
			int bfSize = AudioTrack.getMinBufferSize(sampleRateInHz, channelConfig, audioFormat) * 4;
			AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRateInHz, channelConfig, audioFormat, bfSize,
					AudioTrack.MODE_STREAM);
			
	        // Start playback
	        audioTrack.play();
	     
	        // Write the music buffer to the AudioTrack object
//	        audioTrack.write(music, 0, musicLength);
	        audioTrack.write(music, 0, music.length);
	        
	        audioTrack.stop() ;
	      } catch (Exception e) {
	    	  e.printStackTrace();
	      }
	}
	
	@Override
	public void onClick(View v) {
		final int id = v.getId();
		if(id == R.id.recoder) {
			Boolean tag = (Boolean) mTvRecoder.getTag();
			if(tag!=null&&tag) {
				stop();
			}else {
				try {
					start(MediaFile.generateAudioFilePath(".pcm"));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}else if(id == R.id.play) {
			final String filePath = (String) v.getTag();
			if(filePath!=null) {
				play(filePath);
			}
		}
	}
	
	boolean mIsRecording;
	RecordTask	mRecorder;
	
	public void startRecord(String filePath) {
		mAudioConfig = new AudioConfig();
		CameraUtil.isAvailableFile(filePath);
		mRecorder = new RecordTask();  
        mRecorder.execute(filePath);
		mIsRecording = true;
		
		mTvRecoder.setText("停止");
		mTvRecoder.setTag(true);
		mTvPlay.setTag(filePath);
	}
	
	public void stopRecord() {
		mIsRecording = false;
		
		mTvRecoder.setText("录制");
		mTvRecoder.setTag(false);
	}
	
    class RecordTask extends AsyncTask<String,Integer,Void> {  
        @Override  
        protected Void doInBackground(String... arg) {
        	final String mAudioFile = arg[0];
            mIsRecording = true;  
            try {  
                // 开通输出流到指定的文件  
//                DataOutputStream dos = new DataOutputStream(  
//                        new BufferedOutputStream(  
//                                new FileOutputStream(mAudioFile))); 
            	FileOutputStream dos = new FileOutputStream(mAudioFile);
            	
                // 根据定义好的几个配置，来获取合适的缓冲大小  
                int bufferSize = AudioRecord.getMinBufferSize(mAudioConfig.sampleRate,  
                		mAudioConfig.channelConfig, mAudioConfig.audioFormat);  
                // 实例化AudioRecord  
                AudioRecord record = new AudioRecord(  
                        MediaRecorder.AudioSource.MIC, mAudioConfig.sampleRate,  
                        mAudioConfig.channelConfig, mAudioConfig.audioFormat, bufferSize);  
                // 定义缓冲  
//                short[] buffer = new short[bufferSize];  
  
                byte[] buffer = new byte[1092];
                
                // 开始录制  
                record.startRecording();  
                
                // 定义循环，根据isRecording的值来判断是否继续录制  
                while (mIsRecording) {  
                    // 从bufferSize中读取字节，返回读取的short个数  
                    int bufferReadResult = record  
                            .read(buffer, 0, buffer.length);  
                    // 循环将buffer中的音频数据写入到OutputStream中  
//                    for (int i = 0; i < bufferReadResult; i++) {  
//                        dos.writeShort(buffer[i]);  
//                    }
                    dos.write(buffer, 0, bufferReadResult);
                }  
                
                // 录制结束  
                record.stop();
                dos.close();  
            } catch (Exception e) {  
            	e.printStackTrace();
            }  
            return null;  
        }  
  
        protected void onProgressUpdate(Integer... progress) {  
        }  
  
  
        protected void onPostExecute(Void result) {  
  
        }  
    } 
}
