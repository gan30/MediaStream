package com.xbcx.media.audio;

import android.media.AudioFormat;
import android.media.MediaRecorder;

public class AudioConfig {
	
	public int sampleRate = 8000;
	public int bitRate = 16000;
	public int audioSource = MediaRecorder.AudioSource.MIC;
	public int channelConfig = AudioFormat.CHANNEL_IN_MONO;
	public int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
	
	public AudioConfig setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
		return this;
	}
	
	public AudioConfig setBitRate(int bitRate) {
		this.bitRate = bitRate;
		return this;
	}
	
	public AudioConfig setAudioSource(int audioSource) {
		this.audioSource = audioSource;
		return this;
	}
	
	public AudioConfig setChannelConfig(int channelConfig) {
		this.channelConfig = channelConfig;
		return this;
	}
	
	public AudioConfig setAudioFormat(int audioFormat) {
		this.audioFormat = audioFormat;
		return this;
	}
}
