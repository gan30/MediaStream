package com.xbcx.media.test;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.xbcx.camera.CameraFile;
import com.xbcx.camera.CameraServiceActivity;
import com.xbcx.camera.ui.FastFocusRectangle;
import com.xbcx.media.image.PreviewTakePicture;
import com.xbcx.media.image.PreviewTakePicture.Frame;
import com.xbcx.media.image.PreviewTakePicture.TakePictureProcessor;
import com.xbcx.media.video.YUVWater;

import gan.media.R;

public class TestPreviewPicture extends CameraServiceActivity {
	
	PreviewTakePicture	mPreviewTakePicture;
	FastFocusRectangle 	mFastFocusRectangle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mFastFocusRectangle = (FastFocusRectangle) findViewById(R.id.fastfocus_rectangle);
		findViewById(R.id.picture).setVisibility(View.VISIBLE);
		findViewById(R.id.cameraid).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCamera.switchCamera();
			}
		});
		findViewById(R.id.picture).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(canTakePreview()) {
					previewPicture();
				}
			}
		});
	}
	
	@Override
	protected void onInitAttribute(BaseAttribute ba) {
		super.onInitAttribute(ba);
	}
	
	public boolean canTakePreview() {
		return mPreviewTakePicture==null||mPreviewTakePicture.canTake();
	}
	
	public void previewPicture() {
		if(mPreviewTakePicture == null) {
			mPreviewTakePicture = new PreviewTakePicture();
			mPreviewTakePicture.setCamera(mCamera.getCamera());
		}
		mPreviewTakePicture.takePicture(CameraFile.generatePictureFilePath(), new TakePictureProcessor() {
			YUVWater	mYuvWater;
			
			@Override
			public void onTakeComplete(String filePath) {
				mCamera.removePreviewCallBack(mPreviewTakePicture);
				mCamera.stopPreviewCallBack();
				mFastFocusRectangle.post(new Runnable() {
					@Override
					public void run() {
						mFastFocusRectangle.showEnd(true);
					}
				});
			}

			@Override
			public void onTakeProcessor(Frame frame) {
				if(mYuvWater == null) {
					mYuvWater = new YUVWater();
					mYuvWater.setSimpleWaterViewAdapter();
				}
				
				mYuvWater.setSize(frame.width, frame.height);
				mYuvWater.setRotation(frame.rotation);
				mYuvWater.setColorFormat(YUVWater.ColorFormat_NV21);
				mYuvWater.addYUVWater(frame.data);
				mYuvWater.stopWater();
				
			}
		},10);
		mCamera.addPreviewCallBack(mPreviewTakePicture);
		mCamera.startPreviewCallback();
		mFastFocusRectangle.showStart();
	}
	
}
