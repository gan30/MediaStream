package com.xbcx.media.audio;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;

import com.xbcx.media.utils.SDKINT;

import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 使用Android MediaCodec 编码PCM
 * @author gan
 */
@SuppressLint("NewApi")
public class AudioDecoder{

    final static String Tag = "AudioEncoder";

    MediaCodec					mMediaCodec;
    int							mBufferSize = 1920;
    ByteBuffer[] 				mInputBuffers,mOutputBuffers;
    BufferInfo 					mBufferInfo = new BufferInfo();

    AudioCodecFrameListener		mAudioCodecFrameListener;

    public void dequeueInput(byte[] data, int offset, int length) {
        int inputIndex = mMediaCodec.dequeueInputBuffer(1000);
        if(inputIndex>=0) {
            ByteBuffer inputBuffer = null;
            if(SDKINT.isMin(android.os.Build.VERSION_CODES.LOLLIPOP)){
                inputBuffer = mMediaCodec.getInputBuffer(inputIndex);
            }else {
                inputBuffer = mInputBuffers[inputIndex];
            }
            inputBuffer.clear();
            inputBuffer.put(data, offset, length);

            long presentationTimeUs = System.nanoTime() / 1000;
            mMediaCodec.queueInputBuffer(inputIndex, offset, length, presentationTimeUs, 0);
        }

        int index = mMediaCodec.dequeueOutputBuffer(mBufferInfo, 0);
        if (index >= 0) {
            if (mBufferInfo.flags == MediaCodec.BUFFER_FLAG_CODEC_CONFIG) {
                return ;
            }
            ByteBuffer outputBuffer = null;
            if (SDKINT.isMin(android.os.Build.VERSION_CODES.LOLLIPOP)) {
                outputBuffer = mMediaCodec.getOutputBuffer(index);
            } else {
                outputBuffer = mOutputBuffers[index];
            }
            if(mAudioCodecFrameListener!=null) {
                mAudioCodecFrameListener.onAudioCodecFrame(outputBuffer, mBufferInfo);
            }
            mMediaCodec.releaseOutputBuffer(index, false);
        } else if (index == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
            mOutputBuffers = mMediaCodec.getOutputBuffers();
        } else if (index == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
            MediaFormat newFormat = mMediaCodec.getOutputFormat();
            if(mAudioCodecFrameListener!=null) {
                mAudioCodecFrameListener.onAudioCodecFormatChanged(newFormat);
            }
        } else if (index == MediaCodec.INFO_TRY_AGAIN_LATER) {
            // Log.v(TAG, "No buffer available...");
        } else {
        }
    }

    @SuppressWarnings("deprecation")
    public void startMediaCodec(int bitRate,int sampleRate) throws IOException {
        mMediaCodec = MediaCodec.createDecoderByType("audio/mp4a-latm");
        MediaFormat format = MediaFormat.createAudioFormat("audio/mp4a-latm",sampleRate,1);
        format.setInteger(MediaFormat.KEY_BIT_RATE, bitRate);
        format.setInteger(MediaFormat.KEY_IS_ADTS, 1);
        //用来标记aac的类型
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        //ByteBuffer key（暂时不了解该参数的含义，但必须设置）
        byte[] data = new byte[]{(byte) 0x11, (byte) 0x90};
        ByteBuffer csd_0 = ByteBuffer.wrap(data);
        format.setByteBuffer("csd-0", csd_0);
        mMediaCodec.configure(format, null, null, 0);
        mMediaCodec.start();
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
            mInputBuffers = mMediaCodec.getInputBuffers();
            mOutputBuffers = mMediaCodec.getOutputBuffers();
        }
    }

    public void release() {
        if(mMediaCodec!=null) {
            try {
                mMediaCodec.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
            mMediaCodec.release();
            mMediaCodec = null;
        }
    }

    public AudioDecoder setAudioCodecFrameListener(AudioCodecFrameListener audioCodecFrameListener) {
        this.mAudioCodecFrameListener = audioCodecFrameListener;
        return this;
    }

    public static interface AudioCodecFrameListener{
        public void onAudioCodecFrame(ByteBuffer buffer, BufferInfo info);
        public void onAudioCodecFormatChanged(MediaFormat format);
    }
}
