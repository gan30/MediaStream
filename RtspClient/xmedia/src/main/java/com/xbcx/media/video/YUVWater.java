package com.xbcx.media.video;

import com.xbcx.core.XApplication;
import com.xbcx.media.utils.DateFormatUtils;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.utils.SystemUtils;
import gan.media.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.TextView;

public class YUVWater {
	
	final static String Tag = "VideoWater";
	
	public static final int ColorFormat_420P = 1; 
	public static final int ColorFormat_420SP = 2;
	public static final int ColorFormat_NV21 = 3;
	
	Handler				mMainHandler = XApplication.getMainThreadHandler();
	int 				mRotation;
	int 				mWidth;
	int 				mHeight;
	int 				mWaterWidth;
	int					mWaterHeight;
	byte[]				mWaterYUV;
	int					mColorFormat;
	View				mWaterView;
	WaterViewAdapter	mWaterViewAdapter;
	
	public YUVWater() {
	}
	
	public YUVWater(int width,int height,int rotation) {
		mWidth = width;
		mHeight = height;
		mRotation = rotation;
	}
	
	public void setSize(int width,int height) {
		mWidth = width;
		mHeight = height;
	}
	
	public void setRotation(int rotation) {
		this.mRotation = rotation;
	}
	
	public void setColorFormat(int colorFormat) {
		this.mColorFormat = colorFormat;
	}
	
	public YUVWater setWaterViewAdapter(WaterViewAdapter waterViewAdapter) {
		this.mWaterViewAdapter = waterViewAdapter;
		return this;
	}
	
	public YUVWater setSimpleWaterViewAdapter() {
		return setWaterViewAdapter(new SimpleWaterViewAdapter());
	}
	
	long time;
	public void addYUVWater(byte[] Yuv) {
//		if(mWaterYUV == null) {
//			runWater();
//		}
		
		//同步更新时间水印
		long currenttime = System.currentTimeMillis();
		long timex = currenttime - time;
		if (timex >= 999) {
			waterYUV();
			time = currenttime;
		}
		
		waterYUV(Yuv, mWidth, mHeight, mWaterYUV, mWaterWidth, mWaterHeight,mRotation);
	}
	
	public static void waterYUV(byte[] y1,int width,int height,byte[] y2,int y2Width,int y2Height,int rotation) {
//		YUV.waterYUV(y1, width, height, y2, y2Width, y2Height, rotation);
		if(rotation == 0) {
			int start = height - y2Height;
			int i = 0;
			for (int y = start; y<height; y++) {
				for (int x= 0; x<width; x++) {
					if(y2[i++]==(byte)0xff) {
						int p = y*width+x;
						y1[p] = (byte)0xff;
					}
				}
			}
		}else if(rotation == 90) {
			int start = width - y2Height;
			int i = 0;
			for (int y = start; y<width; y++) {
				for (int x = height; x>0; x--) {
					if(y2[i++]==(byte)0xff) {
						int p = x*width -(width-y);
						y1[p] = (byte)0xff;
					}
				}
			}
		}else if(rotation == 180) {
			int start = height - y2Height;
			int i=0;
			for (int y = start; y<height; y++) {
				for (int x= width; x>0 ; x--) {
					if(y2[i++]==(byte)0xff) {
						int p = width*(height-start)-i;
						y1[p] = (byte)0xff;
					}
				}
			}
		}else if(rotation == 270) {
			int start = width - y2Height;
			int i = 0;
			for (int y = start; y<width; y++) {
				for (int x = 0; x<height; x++){
					if(y2[i++]==(byte)0xff) {
						int p = x*width + width-y;
						y1[p] = (byte)0xff;
					}
				}
			}
		}
	}
	
	/**
	 * 异步添加，水印时间可能跳帧，对视频帧处理时间影响更小
	 */
	public void runWater() {
		waterYUV();
		mMainHandler.removeCallbacks(mTimeRunnable);
		mMainHandler.postDelayed(mTimeRunnable, 999);
	}
	
	public void stopWater() {
		time = 0;
		mMainHandler.removeCallbacks(mTimeRunnable);
		mWaterYUV = null;
	}
	
	Runnable mTimeRunnable = new Runnable() {
		@Override
		public void run() {
			synchronized (mWaterYUV) {
				waterYUV();
			}
			mMainHandler.postDelayed(this, 999);
		}
	};
	
	public void waterYUV() {
		Bitmap waterBitmap = waterBitmap();
		mWaterWidth = waterBitmap.getWidth();
		mWaterHeight = waterBitmap.getHeight();
		try {
			int length = (int) (mWaterWidth * mWaterHeight * 1.5);
			if (mWaterYUV == null || length != mWaterYUV.length) {
				mWaterYUV = new byte[length];
			}
			int size = mWaterWidth * mWaterHeight;
			int[] intArray = new int[size];
			waterBitmap.getPixels(intArray, 0, mWaterWidth, 0, 0, mWaterWidth, mWaterHeight);
			waterBitmap.recycle();
			rgb2YUV(intArray, mWaterYUV, mWaterWidth, mWaterHeight, mColorFormat);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void rgb2YUV(int[] rgb,byte[] yuv,int width,int height,int colorFormat) {
		if(colorFormat == ColorFormat_420SP) {
			YUVUtil.rgb2YUV420SP(yuv, rgb, width, height);
		}else if(colorFormat == ColorFormat_420P){
			YUVUtil.rgb2YUV420(yuv, rgb, width, height);
		}else if(colorFormat == ColorFormat_NV21) {
			YUVUtil.rgb2NV21(yuv, rgb, width, height);
		}
	}
	
	public Bitmap waterBitmap() {
		float width = mRotation%180==0?mWidth:mHeight;
		float ratio = width/480;
		View view = getWater(mWaterView, ratio);
		view.measure(MeasureSpec.makeMeasureSpec(
				mRotation%180==0?mWidth:mHeight, MeasureSpec.EXACTLY), 
				MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
		int waterWidth = view.getMeasuredWidth();
		int waterHeight = view.getMeasuredHeight();
		if(waterHeight%2!=0) {
			waterHeight = waterHeight+1;//yuv 要求是偶数
		}
		Bitmap waterBmp = Bitmap.createBitmap(waterWidth, waterHeight, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(waterBmp);
		c.drawColor(0x00000000);
		view.draw(c);
		return waterBmp;
	}
	
	public static Bitmap rotateBitmap(Bitmap bitmap, int rotate) {
		try {
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			Matrix m = new Matrix();
			m.setRotate(rotate, width / 2, height / 2);
			return Bitmap.createBitmap(bitmap, 0, 0, width, height, m, false);
		} catch (Exception e) {
			e.printStackTrace();
			MediaLog.i(Tag,"rotateBitmap exception:"+e.getMessage());
		}
		return null;
	}
	
	protected View getWater(View waterView,float ratio) {
		return mWaterViewAdapter.getView(waterView, ratio);
	}
	
	public static interface WaterViewAdapter{
		public View getView(View waterView, float ratio);
	}
	
	public static class SimpleWaterViewAdapter implements WaterViewAdapter{

		@Override
		public View getView(View waterView, float ratio) {
			final Context context = XApplication.getApplication();
			if(waterView == null) {
				waterView= SystemUtils.inflate(context, R.layout.common_watermark);
			}
			
			View view = waterView;
			TextView tvCode = (TextView) view.findViewById(R.id.tvCode);
			TextView tvInfo = (TextView) view.findViewById(R.id.tvInfo);
			TextView tvLocation = (TextView) view.findViewById(R.id.tvLocation);
			float textSize = 20 * ratio;
			tvCode.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			tvInfo.setTextSize(TypedValue.COMPLEX_UNIT_PX,textSize);
			tvLocation.setTextSize(TypedValue.COMPLEX_UNIT_PX,textSize);

			tvInfo.setText(DateFormatUtils.getBarsYMdHms().format(XApplication.getFixSystemTime()));
			
			int padding = 10;
			waterView.setPadding(padding, 0, padding, padding);
			return waterView;
		}
	}
}
