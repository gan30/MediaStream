package com.xbcx.media.audio;

import com.test.jni.WebrtcProcessor;
import com.xbcx.media.audio.AudioRecorder.AudioProcessor;

public class WebrtcNoiseProcessor implements AudioProcessor{

	@Override
	public void init(AudioRecorder recoder) {
		WebrtcProcessor.init(recoder.mAudioConfig.sampleRate);
	}

	@Override
	public void processData(byte[] data) {
		try {
			WebrtcProcessor.processNoise(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void release() {
		WebrtcProcessor.release();
	}
}
