package com.xbcx.media;

import com.xbcx.camera.CameraFile;

public class MediaFile extends CameraFile{

	public static String generateAudioFilePath() {
		return generateAudioFilePath(".mp3");
	}
	
	public static String generateAudioFilePath(String suffix) {
		return DCIM_FilePath+"/audio"+ "/AIO_"+formatFile(suffix);
	}
	
}
