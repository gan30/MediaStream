package com.xbcx.media.audio;

import java.util.ArrayList;
import java.util.List;

import com.xbcx.media.MediaError;
import com.xbcx.media.RecordListener;
import com.xbcx.media.Recorder;
import com.xbcx.media.utils.MediaLog;

import android.media.AudioRecord;
import android.os.Process;

/**
 * 使用AudioRecord 采集音频数据
 * @author gan
 */
public class AudioRecorder implements Recorder{

	private static final String Tag = "AudioRecoder";
	
	AudioConfig				mAudioConfig;
	List<RecordListener> 	mRecoderListeners;
	AudioFrameListener		mAudioFrameListener;
	List<AudioProcessor>	mAudioProcessors;
	boolean  				mRuning;
	
	public AudioRecorder setAudioConfig(AudioConfig audioConfig) {
		this.mAudioConfig = audioConfig;
		return this;
	}
	
	public AudioRecorder setAudioFrameListener(AudioFrameListener audioFrameListener) {
		this.mAudioFrameListener = audioFrameListener;
		return this;
	}
	
	public AudioConfig getAudioConfig() {
		return mAudioConfig;
	}
	
	@Override
	public void startRecord() throws Exception {
		startAudioThread();
		onRecordStart();
	}

	@Override
	public void stopRecord() {
		stopAudioThread();
		onRecordEnd();
	}

	@Override
	public void release() {
		if(isAudioRecording()) {
			stopRecord();
		}
		if(mRecoderListeners!=null) {
			mRecoderListeners.clear();
		}
		if(mAudioProcessors!=null) {
			mAudioProcessors.clear();
		}
	}
	
	public boolean isAudioRecording() {
		return mReadThread!=null;
	}
	
	private Thread 	mReadThread;
	private int 	mReadBufferSize = 1920;
	public AudioRecorder setReadBufferSize(int readBufferSize) {
		this.mReadBufferSize = readBufferSize;
		return this;
	}
	
	private void startAudioThread() throws Exception {
		if(mReadThread != null) {
			throw new Exception("recoder busy now");
		}
    	mReadThread = new Thread(new Runnable() {
			@Override
            public void run() {
				Process.setThreadPriority(Process.THREAD_PRIORITY_AUDIO);
				mRuning = true;
				
				int bufferSize = AudioRecord.getMinBufferSize(mAudioConfig.sampleRate, mAudioConfig.channelConfig, mAudioConfig.audioFormat);
				AudioRecord audioRecord = new AudioRecord(mAudioConfig.audioSource, mAudioConfig.sampleRate, mAudioConfig.channelConfig, mAudioConfig.audioFormat, bufferSize);
				audioRecord.startRecording();
				
            	if(mAudioProcessors!=null) {
            		for(AudioProcessor processor:mAudioProcessors) {
            			processor.init(AudioRecorder.this);
            		}
            	}
            	
            	int len = 0;
            	byte[] buffer = new byte[mReadBufferSize];
                try {
                    while (mRuning) {
                    	len = audioRecord.read(buffer, 0, mReadBufferSize);
        				if(len>=0) {
        					byte[] data = new byte[len];
        					System.arraycopy(buffer, 0, data, 0, len);
        					
        					if(mAudioProcessors!=null) {
        	            		for(AudioProcessor processor:mAudioProcessors) {
        	            			processor.processData(data);
        	            		}
        	            	}
        					
        					if(mAudioFrameListener!=null) {
        						mAudioFrameListener.onAudioFrame(data);
        					}
        				}
                    }
                } catch (Exception e) {
                	e.printStackTrace();
                	MediaLog.i(Tag, "read e:"+e.getMessage());
                    onRecordError(MediaError.Error_Read, "read thread exception");
                }finally {
                	if(audioRecord!=null) {
            			try {
            				audioRecord.stop();
						} catch (Exception e2) {
							e2.printStackTrace();
						}
            			audioRecord.release();
            			audioRecord = null;
            		}
                	try {
                		buffer = null;
                		if(mAudioProcessors!=null) {
    	            		for(AudioProcessor processor:mAudioProcessors) {
    	            			processor.release();
    	            		}
    	            	}
					} catch (Exception e2) {
						e2.printStackTrace();
						MediaLog.i(Tag, "read e2:"+e2.getMessage());
					}
                	mReadThread = null;
                }
            }
        }, "audio_read_thread");
    	mReadThread.start();
    }
	
	private void stopAudioThread() {
		if(mReadThread!=null) {
			try {
				mRuning = false;
				Thread t = mReadThread;
				if (t != null) {
					t.interrupt();
					t.join();
				}
			} catch (InterruptedException e) {
				e.fillInStackTrace();
			}
		}
	}
	
	protected void onRecordStart() {
		if(mRecoderListeners!=null) {
			for(RecordListener listener:mRecoderListeners) {
				listener.onRecoderStart(this);
			}
		}
	}
	
	protected void onRecordEnd() {
		if(mRecoderListeners!=null) {
			for(RecordListener listener:mRecoderListeners) {
				listener.onRecoderEnd(this);
			}
		}
	}
	
	protected void onRecordError(int error,String message) {
		if(mRecoderListeners!=null) {
			for(RecordListener listener:mRecoderListeners) {
				listener.onRecoderError(this, error, message);
			}
		}
	}

	@Override
	public void addRecoderListener(RecordListener listener) {
		if(mRecoderListeners == null) {
			mRecoderListeners = new ArrayList<>();
		}
		mRecoderListeners.add(listener);
	}

	@Override
	public void removeRecoderListener(RecordListener listener) {
		if(mRecoderListeners!=null) {
			mRecoderListeners.remove(listener);
		}
	}
	
	public void addAudioProcessor(AudioProcessor processor) {
		if(mAudioProcessors==null) {
			mAudioProcessors = new ArrayList<>();
		}
		mAudioProcessors.add(processor);
	}
	
	public void removeAudioProcessor(AudioProcessor processor) {
		if(mAudioProcessors!=null) {
			mAudioProcessors.remove(processor);
		}
	}
	
	public static interface AudioFrameListener{
		public void onAudioFrame(byte[] data);
	}
	
	public static interface AudioProcessor{
		public void init(AudioRecorder recoder);
		public void processData(byte[] data)throws Exception;
		public void release();
	}
}
