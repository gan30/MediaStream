package com.xbcx.media.video;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

import com.xbcx.camera.CameraUtil;
import com.xbcx.camera.video.FileNumber;
import com.xbcx.core.XApplication;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.media.utils.SDKINT;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

/**
 * Created by John on 2017/1/10.
 */

@SuppressLint("NewApi")
public class VideoMuxer {

    private static final String TAG = "VideoMuxer";
    
    private MediaMuxer mMuxer;
    private int mVideoTrackIndex = -1;
    private int mAudioTrackIndex = -1;
    private MediaFormat mVideoFormat;
    private MediaFormat mAudioFormat;
    
    private String  	mRecordPath;
    private FileNumber	mVideoFileNumber;
    private boolean 	mIsRecording;
    
    private int mDegrees;

    public VideoMuxer(String path) throws Exception {
        mVideoFileNumber = new FileNumber(path); 
        if(SDKINT.isMin(18)) {
        	mMuxer = new MediaMuxer(mRecordPath = generateRecordPath(), 
        			MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
        	mMuxer.setOrientationHint(mDegrees);
        }else {
        	throw new Exception("sdk int must >= 18");
        }
    }

    public void setOrientationHint(int degrees) {
    	if(mMuxer!=null) {
    		mMuxer.setOrientationHint(degrees);
    	}
    	mDegrees = degrees;
    }
    
    public String generateRecordPath(){
    	final String file = mVideoFileNumber.nextFilePath();
    	if(isAvailableFile(file)){
    		return file;
    	}
    	return null;
    }
    
    public static boolean isAvailableFile(String filePath){
		return isAvailableFile(new File(filePath));
	}
	
	public static boolean isAvailableFile(File file){
		if(file.exists()){
			return true;
		}else{
			try {
				String parent = file.getParent();
				File folder = new File(parent);
				if(!folder.exists()){
					folder.mkdirs();
				}
				file.createNewFile();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
    public synchronized void addTrack(MediaFormat format, boolean isVideo) {
        if (mAudioTrackIndex != -1 && mVideoTrackIndex != -1)
            throw new RuntimeException("already add all tracks");

        int track = mMuxer.addTrack(format);
        if (isVideo) {
            mVideoFormat = format;
            mVideoTrackIndex = track;
            if (mAudioTrackIndex != -1) {
                start();
            }
        } else {
            mAudioFormat = format;
            mAudioTrackIndex = track;
            if (mVideoTrackIndex != -1) {
               start();
            }
        }
    }
    
    public void start(){
    	if(mMuxer!=null) {
    		mMuxer.start();
    		mIsRecording = true;
    	}
    }
    
    public boolean isRecording() {
		return mIsRecording;
	}

    public synchronized void pumpStream(ByteBuffer outputBuffer, MediaCodec.BufferInfo bufferInfo, boolean isVideo) {
    	if(!mIsRecording){
    		return;
    	}
    	if(mMuxer == null){
    		return;
    	}
    	
    	if (mAudioTrackIndex == -1 || mVideoTrackIndex == -1) {
            Log.i(TAG, String.format("pumpStream [%s] but muxer is not start.ignore..", isVideo ? "video" : "audio"));
            return;
        }
        if ((bufferInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
        } else if (bufferInfo.size != 0) {
            if (isVideo && mVideoTrackIndex == -1) {
                throw new RuntimeException("muxer hasn't started");
            }
            outputBuffer.position(bufferInfo.offset);
            outputBuffer.limit(bufferInfo.offset + bufferInfo.size);
            
            MediaLog.d(TAG, "writeSampleData isVideo:"+isVideo);
            mMuxer.writeSampleData(isVideo ? mVideoTrackIndex : mAudioTrackIndex, outputBuffer, bufferInfo);
        }
    }
    
    public boolean stopAndRestart(){
    	mIsRecording = false;
		stop();
		mVideoTrackIndex = mAudioTrackIndex = -1;
		try {
			mMuxer = new MediaMuxer(mRecordPath = generateRecordPath(),
					MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
			mMuxer.setOrientationHint(mDegrees);
			if(mVideoFormat!=null){
				addTrack(mVideoFormat, true);
			}
			if(mAudioFormat!=null){
				addTrack(mAudioFormat, false);
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
    }

    public final void stop(){
    	 try {
    		 if(mMuxer!=null) {
    			 mMuxer.stop();
    			 mMuxer.release();
    			 mMuxer = null;
    			 mIsRecording = false;
    			 mVideoTrackIndex = mAudioTrackIndex = -1;
    			 cleanupEmptyFile();
    			 File file = new File(mRecordPath);
    			 if(file.exists()){
    				 CameraUtil.insertVideoToMediaStore(XApplication.getApplication(), file);
    			 }
    		 }
         } catch (IllegalStateException ex) {
             ex.printStackTrace();
         }
    }
    
    public synchronized void release() {
    	if (mMuxer != null) {
            if (mAudioTrackIndex != -1 && mVideoTrackIndex != -1) {
                stop();
            }
        }
    }
    
    public String getRecordFile(){
    	return mRecordPath;
    }
    
    private void cleanupEmptyFile() {
		File f = new File(mRecordPath);
		if (f.exists() && f.length() == 0) {
			deleteVideoFile(f.getAbsolutePath());
		}
    }
    
    private void deleteVideoFile(String fileName) {
        File f = new File(fileName);
        if (!f.delete()) {
        	CameraUtil.deleteVideoToMiastore(XApplication.getApplication(), fileName);
        }
    }
	
	public static void sendMeidaScan(Context context, File path) {
		// 最后通知图库更新
		context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path.getAbsolutePath())));
	}
	
	public static void insertVideoToMediaStore(Context context, File path) {
		ContentResolver localContentResolver = context.getContentResolver();
		ContentValues localContentValues = getVideoContentValues(context, path, System.currentTimeMillis());
		localContentResolver.insert(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, localContentValues);
	}
	
	public static void deleteVideoToMiastore(Context context,String path){
		ContentResolver localContentResolver = context.getContentResolver();
		localContentResolver.delete(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, MediaStore.Video.Media.DATA+" like ?", new String[]{path});
	}

	private static ContentValues getVideoContentValues(Context paramContext, File paramFile, long paramLong) {
		ContentValues localContentValues = new ContentValues();
		localContentValues.put("title", paramFile.getName());
		localContentValues.put("_display_name", paramFile.getName());
		localContentValues.put("mime_type", "video/avc");
		localContentValues.put("datetaken", Long.valueOf(paramLong));
		localContentValues.put("date_modified", Long.valueOf(paramLong));
		localContentValues.put("date_added", Long.valueOf(paramLong));
		localContentValues.put("_data", paramFile.getAbsolutePath());
		localContentValues.put("_size", Long.valueOf(paramFile.length()));
		return localContentValues;
	}
}
