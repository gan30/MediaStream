package com.xbcx.media;

public interface RecordListener {
	public void onRecoderStart(Recorder recoder);
	public void onRecoderEnd(Recorder recoder);
	public void onRecoderError(Recorder recoder, int error, String message);
}
