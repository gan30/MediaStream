package com.xbcx.media;

public interface Recorder {
	public void startRecord()throws Exception;
	public void stopRecord();
	public void release();
	public void addRecoderListener(RecordListener listener);
	public void removeRecoderListener(RecordListener listener);
}
