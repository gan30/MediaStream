package com.xbcx.media.video;

public class VideoFrame {
	public byte[] data;
	public int 	  format;
	public int 	  colorFormat;
	public long   time;
	
	public Object other;
	
	public VideoFrame() {
		time = System.currentTimeMillis();
	}
	
	public void setOther(Object other) {
		this.other = other;
	}
	
	public Object getOther() {
		return other;
	}
}
