package com.xbcx.media.test;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.xbcx.camera.CameraService;
import com.xbcx.camera.CameraServiceActivity;
import com.xbcx.camera.VideoCamera2;
import com.xbcx.camera.VideoCamera2.OnPrepareListener;
import com.xbcx.camera.preview.PreviewVideoEngine;

import gan.media.R;

public class TestPreviewVideo extends CameraServiceActivity {

	PreviewVideoEngine	mPreviewVideoEngine;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		findViewById(R.id.picture).setVisibility(View.GONE);
		findViewById(R.id.video).setVisibility(View.VISIBLE);
		findViewById(R.id.cameraid).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mCamera.switchCamera();
			}
		});
	}
	
	@Override
	protected void onInitAttribute(BaseAttribute ba) {
		super.onInitAttribute(ba);
	}
	
	@Override
	protected void onAttachService(CameraService service) {
		super.onAttachService(service);
		mPreviewVideoEngine = new PreviewVideoEngine(this);
		getVideoCamera().setVideoEngine(mPreviewVideoEngine);
		getVideoCamera().setOnPrepareListener(new OnPrepareListener() {
			@Override
			public void onPrepare(VideoCamera2 camera) {
				mCamera.setPreviewSize(1280, 720);
			}
		});
	}
}
