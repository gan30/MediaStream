package com.xbcx.media;

public class MediaError {

	/**
	 * io exception;
	 */
	public final static int Error_IO  = 1;
	
	public final static int Error_Read = 2;
	
	public final static int Error_Write = 3;
	
	public final static int Error_VideoEncode = 4;
	
}
