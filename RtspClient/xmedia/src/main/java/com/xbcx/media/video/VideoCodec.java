package com.xbcx.media.video;

import java.io.IOException;
import java.nio.ByteBuffer;

import com.xbcx.media.Encoder;
import com.xbcx.media.utils.MediaLog;
import com.xbcx.media.utils.SDKINT;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;

public class VideoCodec implements Encoder{

	final static String Tag 	= "VideoCodec";
	
	private static final String MIME_TYPE = "video/avc";
	private static final int 	FRAME_RATE = 30;
	
	Context						mContext;
	MediaCodec					mMediaCodec;
	int							mBufferSize = 1920;
	ByteBuffer[] 				mInputBuffers,mOutputBuffers;
	BufferInfo 		mBufferInfo = new BufferInfo();
	VideoCodecFrameListener		mVideoCodecFrameListener;
	
	int							mBitRate = 5;
	int							mColorFormat;
	Surface 					mSurface;
	protected static int[] 		recognizedFormats;
	static {
		recognizedFormats = new int[] {
				 MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar,
				 MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar};
	}
	
	public static boolean is420sp(int colorFormat) {
		return colorFormat == MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar;
	}
	
	public VideoCodec(Context context) {
		mContext = context;
	}
	
	public void setBitRate(int bitRate) {
		this.mBitRate = bitRate;
	}
	
	public void setSurface(Surface surface) {
		this.mSurface = surface;
	}
	
	public Surface getSurface() {
		return mSurface;
	}
	
	int mWidth,mHeight;
	public void start(int width,int height) throws IOException {
		mWidth = width;
		mHeight = height;
		startMediaCodec();
	}
	
	/**
     * 初始化编码器
     */
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private void startMediaCodec() throws IOException {
		final int bitrate = (int) (mBitRate * mWidth * mHeight);
		MediaLog.i(Tag, String.format("bitrate=%5.2f[Mbps]", bitrate / 1024f / 1024f));
		
		MediaCodecInfo videoCodecInfo = selectVideoCodec(MIME_TYPE);
		final MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, mWidth, mHeight);
		
//		if(SDKINT.isMin(21)) {
//			int colorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Flexible;
//			if(isSupportColorFormat(MIME_TYPE, colorFormat)) {
//				mColorFormat = colorFormat;
//			}else {
//				mColorFormat = selectColorFormat(videoCodecInfo, MIME_TYPE);
//			}
//		}
		
//		mColorFormat = selectColorFormat(videoCodecInfo, MIME_TYPE);
		mColorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar;
//		mColorFormat = MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar;
		
		MediaLog.i(Tag, "colorFormat:"+mColorFormat);
		format.setInteger(MediaFormat.KEY_COLOR_FORMAT, mColorFormat);
		format.setInteger(MediaFormat.KEY_BIT_RATE, bitrate);
		format.setInteger(MediaFormat.KEY_FRAME_RATE, FRAME_RATE);
		format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);
		
		final String codecName = videoCodecInfo.getName();
		MediaLog.i(Tag, "codecName:"+codecName);
		mMediaCodec = MediaCodec.createByCodecName(codecName);
//		mMediaCodec = MediaCodec.createEncoderByType(MIME_TYPE);
		mMediaCodec.configure(format, mSurface, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
		// get Surface for encoder input
		// this method only can call between #configure and #start
		mMediaCodec.start();

		Bundle params = new Bundle();
        params.putInt(MediaCodec.PARAMETER_KEY_REQUEST_SYNC_FRAME, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mMediaCodec.setParameters(params);
        }
        
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mInputBuffers = mOutputBuffers = null;
		} else {
			mInputBuffers = mMediaCodec.getInputBuffers();
			mOutputBuffers = mMediaCodec.getOutputBuffers();
		}
    }
    
    /**
     * 停止编码并释放编码资源占用
     */
    public void stopMediaCodec() {
        mMediaCodec.stop();
        mMediaCodec.release();
    }
    
    public void release() {
    	if (mSurface != null) {
    		mSurface.release();
    		mSurface = null;
    	}
    	if(mMediaCodec!=null) {
    		try {
    			 mMediaCodec.stop();
			} catch (Exception e) {
				e.printStackTrace();
			}
    		mMediaCodec.release();
    		mMediaCodec = null;
    	}
    }
    
    public int getColorFormat() {
		return mColorFormat;
	}
    
    public void encoder(byte[] data) {
    	encoder(data, 0, data.length);
    }
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	public void encoder(byte[] data, int offset, int length) {
		int inputIndex = mMediaCodec.dequeueInputBuffer(0);
		if(inputIndex>=0) {
			ByteBuffer inputBuffer = null;
			if(SDKINT.isMin(21)) {
				inputBuffer = mMediaCodec.getInputBuffer(inputIndex);
			}else {
				inputBuffer = mInputBuffers[inputIndex];
			}
			inputBuffer.clear();
			inputBuffer.put(data, offset, length);
			inputBuffer.clear();
			long presentationTimeUs = System.nanoTime() / 1000;
			mMediaCodec.queueInputBuffer(inputIndex, offset, length, presentationTimeUs, 
					MediaCodec.BUFFER_FLAG_KEY_FRAME);
		}
		
		int index = mMediaCodec.dequeueOutputBuffer(mBufferInfo, 30000);
		if (index >= 0) {
			ByteBuffer outputBuffer = null;
			if (SDKINT.isMin(21)) {
				outputBuffer = mMediaCodec.getOutputBuffer(index);
			} else {
				outputBuffer = mOutputBuffers[index];
			}
			outputBuffer.position(mBufferInfo.offset);
			outputBuffer.limit(mBufferInfo.offset + mBufferInfo.size);
			
			if(mVideoCodecFrameListener!=null) {
				mVideoCodecFrameListener.onVideoCodecFrame(outputBuffer, mBufferInfo);
			}
			outputBuffer.clear();
			mMediaCodec.releaseOutputBuffer(index, false);
		} else if (index == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
			mOutputBuffers = mMediaCodec.getOutputBuffers();
		} else if (index == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
			MediaLog.i(Tag, "output format changed...");
			MediaFormat newFormat = mMediaCodec.getOutputFormat();
			if(mVideoCodecFrameListener!=null) {
				mVideoCodecFrameListener.onVideoCodecFormatChanged(newFormat);
			}
		} else if (index == MediaCodec.INFO_TRY_AGAIN_LATER) {
			// Log.v(TAG, "No buffer available...");
		} else {
			MediaLog.i(Tag, "index:" + index);
		}
	}
	
	 /**
     * select the first codec that match a specific MIME type
     * @param mimeType
     * @return null if no codec matched
     */
    protected static final MediaCodecInfo selectVideoCodec(final String mimeType) {
    	MediaLog.i(Tag, "selectVideoCodec:");
    	// get the list of available codecs
        final int numCodecs = MediaCodecList.getCodecCount();
        for (int i = 0; i < numCodecs; i++) {
        	final MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);

            if (!codecInfo.isEncoder()) {	// skipp decoder
                continue;
            }
            // select first codec that match a specific MIME type and color format
            final String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                	MediaLog.d(Tag, "codec:" + codecInfo.getName() + ",MIME=" + types[j]);
            		final int format = selectColorFormat(codecInfo, mimeType);
                	if (format > 0) {
                		return codecInfo;
                	}
                }
            }
        }
        return null;
    }
    
    /**
     * select color format available on specific codec and we can use.
     * @return 0 if no colorFormat is matched
     */
    protected static final int selectColorFormat(final MediaCodecInfo codecInfo, final String mimeType) {
		MediaLog.i(Tag, "selectColorFormat: ");
    	int result = 0;
    	final MediaCodecInfo.CodecCapabilities caps;
    	try {
    		Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
    		caps = codecInfo.getCapabilitiesForType(mimeType);
    	} finally {
    		Thread.currentThread().setPriority(Thread.NORM_PRIORITY);
    	}
        int colorFormat;
        for (int i = 0; i < caps.colorFormats.length; i++) {
        	colorFormat = caps.colorFormats[i];
            if (isRecognizedViewoFormat(colorFormat)) {
            	if (result == 0) {
            		result = colorFormat;
            		break;
            	}
            }
        }
        if (result == 0) {
        	MediaLog.i(Tag, "couldn't find a good color format for " + codecInfo.getName() + " / " + mimeType);
        }
        return result;
    }
    
    public static final boolean isSupportColorFormat(String mimeType, int colorFormat) {
    	final MediaCodecInfo codecInfo = selectVideoCodec(mimeType);
    	final MediaCodecInfo.CodecCapabilities caps;
    	try {
    		Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
    		caps = codecInfo.getCapabilitiesForType(mimeType);
    	} finally {
    		Thread.currentThread().setPriority(Thread.NORM_PRIORITY);
    	}
        for (int i = 0; i < caps.colorFormats.length; i++) {
        	if(caps.colorFormats[i] == colorFormat) {
        		return true;
        	}
        }
        return false;
    }
	
    private static final boolean isRecognizedViewoFormat(final int colorFormat) {
		MediaLog.i(Tag, "isRecognizedViewoFormat:colorFormat=" + colorFormat);
    	final int n = recognizedFormats != null ? recognizedFormats.length : 0;
    	for (int i = 0; i < n; i++) {
    		if (recognizedFormats[i] == colorFormat) {
    			return true;
    		}
    	}
    	return false;
    }
    
	public VideoCodec setVideoCodecFrameListener(VideoCodecFrameListener VideoCodecFrameListener) {
		this.mVideoCodecFrameListener = VideoCodecFrameListener;
		return this;
	}
	
	public static interface VideoCodecFrameListener{
		public void onVideoCodecFrame(ByteBuffer buffer, BufferInfo info);
		public void onVideoCodecFormatChanged(MediaFormat format);
	}
}
