package com.xbcx.media.utils;

import com.xbcx.core.BaseActivity;
import com.xbcx.core.XApplication;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

public class XUtils {
	
	public static float getProximityValue(Activity activity){
		return activity.getIntent().getFloatExtra("proximity", 1.0f);
	}
	 
	public static  boolean isPowerClose(Activity activity){
		return getProximityValue(activity) == 0.0f;
	}
	
	public static void vibrate(long time){
		Vibrator vib = (Vibrator) XApplication.getApplication().getSystemService(Service.VIBRATOR_SERVICE);
		vib.vibrate(time);
	}
	
	public static boolean isNearOpened(BaseActivity activity){
		return activity.getIntent().getBooleanExtra("near_opened", false);
	}
	
	public static boolean isRedOpened(BaseActivity activity){
		return activity.getIntent().getBooleanExtra("red_opened", false);
	}
	
	public static int[] parseSize(String value) {
		int index = value.indexOf("x");
		int width = Integer.parseInt(value.substring(0, index));
		int height = Integer.parseInt(value.substring(index + 1, value.length()));
		return new int[] { width, height };
	}
	
	public static void setTextEmptyGone(TextView tv,CharSequence text,View goneView){
		if(TextUtils.isEmpty(text)){
			goneView.setVisibility(View.GONE);
		}else{
			goneView.setVisibility(View.VISIBLE);
			tv.setText(text);
		}
	}
	
	public static String safeText(String text){
		return TextUtils.isEmpty(text)? "":text;
	}
	
	public static void resumeActivity(Activity activity) {
		ActivityManager am = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
		am.moveTaskToFront(activity.getTaskId(), ActivityManager.MOVE_TASK_NO_USER_ACTION);
	}
}
