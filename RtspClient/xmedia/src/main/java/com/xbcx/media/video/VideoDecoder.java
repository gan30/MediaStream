package com.xbcx.media.video;

import android.annotation.SuppressLint;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;
import android.view.Surface;

import com.xbcx.media.utils.SDKINT;
import com.xbcx.util.XbLog;

import java.io.IOException;
import java.nio.ByteBuffer;

public class VideoDecoder{

    final static String Tag 	= "VideoCodec";

    private static final String MIME_TYPE = "video/avc";
    private static final int 	FRAME_RATE = 30;

    MediaCodec					mMediaCodec;
    int							mBufferSize = 1920;
    ByteBuffer[] 				mInputBuffers,mOutputBuffers;
    BufferInfo 					mBufferInfo = new BufferInfo();
    VideoCodecFrameListener		mVideoCodecFrameListener;

    int							mBitRate = 5;
    int							mColorFormat;
    Surface 					mSurface;
    ByteBuffer 					mCSD0;
    ByteBuffer 					mCSD1;

    protected static int[] 		recognizedFormats;
    static {
        recognizedFormats = new int[] {
                MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar,
                MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar};
    }

    public static boolean is420sp(int colorFormat) {
        return colorFormat == MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar;
    }

    public void setBitRate(int bitRate) {
        this.mBitRate = bitRate;
    }

    public void setSurface(Surface surface) {
        this.mSurface = surface;
    }

    public Surface getSurface() {
        return mSurface;
    }

    public void setCSD(ByteBuffer CSD0,ByteBuffer CSD1) {
        this.mCSD0 = CSD0;
        mCSD1 = CSD1;
    }

    int mWidth,mHeight;
    public void start(int width,int height) throws IOException {
        mWidth = width;
        mHeight = height;
        startMediaCodec();
    }

    /**
     * 初始化编码器
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private void startMediaCodec() throws IOException {
        boolean pushBlankBuffersOnStop = true;
        MediaFormat format = MediaFormat.createVideoFormat("video/avc", mWidth, mHeight);
        format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, 0);
        format.setInteger(MediaFormat.KEY_PUSH_BLANK_BUFFERS_ON_STOP, pushBlankBuffersOnStop ? 1 : 0);
        if (mCSD0 != null) {
            format.setByteBuffer("csd-0", mCSD0);
        }
        if (mCSD1 != null) {
            format.setByteBuffer("csd-1", mCSD1);
        }
        mMediaCodec = MediaCodec.createDecoderByType("video/avc");
        XbLog.i(Tag, String.format("config codec:%s", format));
        mMediaCodec.configure(format, mSurface, null, 0);
        mMediaCodec.setVideoScalingMode(MediaCodec.VIDEO_SCALING_MODE_SCALE_TO_FIT);
        mMediaCodec.start();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mInputBuffers = mOutputBuffers = null;
        } else {
            mInputBuffers = mMediaCodec.getInputBuffers();
            mOutputBuffers = mMediaCodec.getOutputBuffers();
        }
    }

    /**
     * 停止编码并释放编码资源占用
     */
    public void stopMediaCodec() {
        mMediaCodec.stop();
        mMediaCodec.release();
    }

    public void release() {
        if(mMediaCodec!=null) {
            try {
                mMediaCodec.stop();
            } catch (Exception e) {
            }
            mMediaCodec.release();

            if(mCSD0!=null){
                mCSD0.clear();
            }
            if(mCSD1!=null){
                mCSD1.clear();
            }
            if (mSurface != null) {
                mSurface.release();
                mSurface = null;
            }
        }
    }

    public int getColorFormat() {
        return mColorFormat;
    }

    public void input(byte[] data) throws Exception{
        long presentationTimeUs = System.nanoTime() / 1000;
        input(data, 0, data.length, presentationTimeUs);
    }

    long previewStampUs = 0l;
    long differ=0;

    @SuppressLint("NewApi")
    public void input(byte[] data, int offset, int length,long timeStamp) throws Exception{
        int inputIndex = mMediaCodec.dequeueInputBuffer(0);
        if(inputIndex>=0) {
            ByteBuffer inputBuffer = null;
            if(SDKINT.isMin(21)) {
                inputBuffer = mMediaCodec.getInputBuffer(inputIndex);
            }else {
                inputBuffer = mInputBuffers[inputIndex];
            }
            inputBuffer.clear();
            if (length > inputBuffer.remaining()) {
                mMediaCodec.queueInputBuffer(inputIndex, 0, 0, timeStamp, 0);
            } else {
                inputBuffer.put(data, offset, length);
                mMediaCodec.queueInputBuffer(inputIndex, 0, inputBuffer.position(), timeStamp, 0);
            }
        }

        int index = mMediaCodec.dequeueOutputBuffer(mBufferInfo, 10);
        switch (index) {
            case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                Log.i(Tag, "INFO_OUTPUT_BUFFERS_CHANGED");
                break;
            case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                MediaFormat mf = mMediaCodec.getOutputFormat();
                Log.i(Tag, "INFO_OUTPUT_FORMAT_CHANGED ：" +mf);
                break;
            case MediaCodec.INFO_TRY_AGAIN_LATER:
                // 输出为空
                break;
            default:
                // 输出队列不为空
                // -1表示为第一帧数据
                long newSleepUs = -1;
                long previewStampUs = 0;
                previewStampUs = mBufferInfo.presentationTimeUs;
                if (Build.VERSION.SDK_INT >= 21) {
                    Log.d(Tag, String.format("releaseoutputbuffer:%d,stampUs:%d", index, previewStampUs));
                    mMediaCodec.releaseOutputBuffer(index, previewStampUs);
                } else {
                    if (newSleepUs < 0) {
                        newSleepUs = 0;
                    }
                    Thread.sleep(newSleepUs / 1000);
                    mMediaCodec.releaseOutputBuffer(index, true);
                }
        }
    }

    public static interface VideoCodecFrameListener{
        public void onVideoCodecFrame(ByteBuffer buffer, BufferInfo info);
        public void onVideoCodecFormatChanged(MediaFormat format);
    }
}
