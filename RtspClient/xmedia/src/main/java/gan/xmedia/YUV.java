package gan.xmedia;

/**
 * Created by gan on 2018\1\10 0010.
 */

public class YUV {

    static{
        System.loadLibrary("YUV");
    }

    public static native void YV12toYUV420P(byte[] yv12,int width,int height);

    public static native void YV12toYUV420SP(byte[] yv12,int width,int height);

    public static native void NV21toYUV420P(byte[] nv21,int width,int height);
    
    public static native void NV21toYUV420SP(byte[] nv21,int width,int height);
    
    public static native void YV12toNV21(byte[] Yv12, final int width, final int height);
    
    public static native void rgb2YUV420(byte[] yuv,int[] pixels, int width, int height); 
    
    public static native void waterYUV(byte[] y1,int width,int height,byte[] y2,int y2Width,int y2Height,int rotation);

    public static native void rotateByteMatrix(byte[] data,int offset,int width,int height,int degree);

    public static native void rotateShortMatrix(byte[] data,int offset,int width,int height,int degree);
}
