package org.easydarwin.audio;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;

import com.xbcx.media.audio.AudioConfig;
import com.xbcx.media.audio.AudioDecoder.AudioCodecFrameListener;

import java.io.IOException;
import java.nio.ByteBuffer;

public class AudioPlayer implements AudioCodecFrameListener {

    AudioConfig mAudioConfig;
    AudioTrack   mAudioTrack;

    int 	mHanlde;

    byte[] mBufferReuse = new byte[16000];
    int[] outLen = new int[1];

    public void start() throws IOException {
        mAudioConfig = new AudioConfig();
        mAudioConfig.setChannelConfig(AudioFormat.CHANNEL_OUT_MONO);

        int playBufSize = AudioTrack.getMinBufferSize(mAudioConfig.sampleRate, mAudioConfig.channelConfig, mAudioConfig.audioFormat);
        mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, mAudioConfig.sampleRate, mAudioConfig.channelConfig, mAudioConfig.audioFormat, playBufSize, AudioTrack.MODE_STREAM);
        mAudioTrack.play();

        int code = 86018;
        mHanlde = AudioCodec.create(code, mAudioConfig.sampleRate,1, mAudioConfig.bitRate);
    }

    public void stop() {
        if(mAudioTrack!=null) {
            mAudioTrack.stop();
        }
        AudioCodec.close(mHanlde);
    }

    public void onAudioFrame(byte[] data,int length) {
//		mAudioDecoder.dequeueInput(data, 0, length);
        outLen[0] = mBufferReuse.length;
        int nRet = AudioCodec.decode(mHanlde,data,0,length,mBufferReuse,outLen);
        if (nRet == 0) {
            mAudioTrack.write(mBufferReuse, 0, outLen[0]);
        }
    }

    @Override
    public void onAudioCodecFrame(ByteBuffer buffer, BufferInfo info) {
        byte[] dst = new byte[info.size];
        buffer.get(dst , 0, info.size);
        mAudioTrack.write(dst, 0, info.size);
    }

    @Override
    public void onAudioCodecFormatChanged(MediaFormat format) {
    }
}
